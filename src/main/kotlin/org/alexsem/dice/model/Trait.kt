package org.alexsem.dice.model

/**
 * Special rules for various [Threat] cards
 */
enum class Trait {

    DAMAGE_ONE, //Deals 1 damage
    DAMAGE_TWO, //Deals 2 damage
    DAMAGE_THREE, //Deals 3 damage
    DAMAGE_MINUS_ONE, //Dealt damage is reduced by 1
    DAMAGE_MINUS_TWO, //Dealt damage is reduced by 2
    DAMAGE_PLUS_ONE, //Dealt damage is increased by 1
    DAMAGE_PLUS_TWO, //Dealt damage is increased by 2
    DAMAGE_ZERO, //Does not deal any damage
    DAMAGE_MAGICAL, //Dealt damage is magical
    DAMAGE_WOUND_ONE, //Deals 1 wound instead of actual damage
    DAMAGE_WOUND_TWO, //Deals 2 wounds instead of actual damage
    DAMAGE_ENTIRE_LOCATION, //Deals damage to all heroes in location
    DAMAGE_EVERYONE, //Deals damage to all heroes
    DAMAGE_HIDE_HAND, //Hide all dice from hand
    DAMAGE_DISCARD_HAND, //Discard all dice from hand
    DAMAGE_HIDE_DIE, //Hide one non-wound die from hand
    DAMAGE_DISCARD_DIE, //Discard one non-wound die from hand
    DAMAGE_DETER_DIE, //Deter one non-wound die from hand
    DAMAGE_PREVENT_EXPLORATION, //Do not explore again
    DAMAGE_FINISH_TURN, //Finish turn
    DAMAGE_FINISH_TURN_IMMEDIATELY, //Finish turn (without refill)
    DAMAGE_TIMER_MINUS_ONE, //Reduce timer by 1
    DAMAGE_TIMER_MINUS_TWO, //Reduce timer by 2
    DAMAGE_TRAVEL, //Travel to another location
    DAMAGE_DISCARD_PHYSICAL, //Discard all physical dice
    DAMAGE_DISCARD_SOMATIC, //Discard all somatic dice
    DAMAGE_DISCARD_MENTAL, //Discard all mental dice
    DAMAGE_DISCARD_VERBAL, //Discard all verbal dice
    DAMAGE_DISCARD_DIVINE, //Discard all divine dice
    DAMAGE_DISCARD_ALLY, //Discard all ally dice
    DAMAGE_DISCARD_RANDOM_STAT, //Discard all dice of random stat type
    MODIFIER_MINUS_ONE, //Add -1 modifier
    MODIFIER_MINUS_TWO, //Add -2 modifier
    MODIFIER_PLUS_ONE, //Add +1 modifier
    MODIFIER_PLUS_TWO, //Add +2 modifier
    MODIFIER_PLUS_THREE, //Add +3 modifier
    DEFEAT_THRESHOLD_ONE, //Roll 1 or more to defeat
    DEFEAT_THRESHOLD_TWO, //Roll 2 or more to defeat
    DEFEAT_THRESHOLD_THREE, //Roll 3 or more to defeat
    DEFEAT_THRESHOLD_EVEN, //Roll even to defeat
    DEFEAT_THRESHOLD_ODD, //Roll odd to defeat
    DEFEAT_THRESHOLD_INFINITE, //Can not be defeated
    IMMUNE_PHYSICAL, //Immune to physical damage
    IMMUNE_MAGICAL, //Immune to magical damage
    IMMUNE_AVOID_ENCOUNTER, //Immune to being avoided
    IMMUNE_MODIFIERS, //Immune to modifier application
    IMMUNE_REDUCTION, //Immune to damage reduction
    ENCOUNTER_HIDE_DIE, //Hide one non-wound die to bag
    ENCOUNTER_DISCARD_DIE, //Discard one non-wound die from hand
    ENCOUNTER_DAMAGE_ONE, //Receive one damage
    ENCOUNTER_DAMAGE_TWO, //Receive two damage
    ENCOUNTER_TAKE_WOUND, //Add wound to hand
    ENCOUNTER_TIMER_MINUS_ONE, //Reduce timer
    ENCOUNTER_ENTIRE_LOCATION, //Encounter rules apply to all heroes at location
    WIN_DRAW_DIE, //Draw die form bag (to hand)
    WIN_TAKE_STAT_DIE, //Get random stat die (to hand)
    WIN_HIDE_STAT_DIE, //Get random stat die (to bag)
    WIN_TAKE_DIVINE_DIE, //Get random divine die (to hand)
    WIN_HIDE_DIVINE_DIE, //Get random divine die (to bag)
    WIN_TAKE_ALLY_DIE, //Get random ally die (to hand)
    WIN_DISCARD_DIE, //Discard die from hand
    WIN_DETER_DIE, //Deter die from hand
    WIN_TIMER_PLUS_ONE, //Add 1 to scenario value
    WIN_TIMER_PLUS_TWO, //Add 2 to scenario value
    WIN_EXPLORE_AGAIN, //Explore again
    WIN_CLOSE_ATTEMPT, //Attempt to close location
    WIN_CLOSE_AUTO, //Automatically close location
    WIN_DISCARD_ALL, //All heroes at location discard entire hand
}
