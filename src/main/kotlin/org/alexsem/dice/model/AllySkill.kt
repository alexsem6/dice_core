package org.alexsem.dice.model

/**
 * Ally skills heroes can use
 */
class AllySkill(val type: Type) {

    enum class Type {
        COMBAT_ADVISOR,
        DARK_ENCHANTER,
        DEFENDER,
        DIPLOMAT,
        FALCON,
        FIELD_MEDIC,
        HERMIT,
        INTERN,
        ILLUSIONIST,
        LOCAL_DWELLER,
        MASTERFUL_DISTRACTION,
        PATHFINDER,
        PHYSICIAN,
        PYROMANIAC,
        RETIRED_SOLDIER,
        SABOTEUR,
        SCRYER,
        TOUR_GUIDE,
        TRICKSTER,
    }

    //------------------------------------------------------------------------------------------------------------------

    var modifier1: Int = 0
    var modifier2: Int = 0

}
