package org.alexsem.dice.model

/**
 * Pile of dice. All dice are visible compared to regular [Bag]
 */
class Pile : Bag() {

    fun removeDie(die: Die) = dice.remove(die)

}
