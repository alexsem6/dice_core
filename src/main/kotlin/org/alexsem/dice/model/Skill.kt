package org.alexsem.dice.model

/**
 * Various skills which can be used by heroes during the game
 */
class Skill(val type: Type) {

    enum class Type {
        //Brawler
        HIT,
        DODGE,
        INTIMIDATE,
        PROTECT,
        SUBMIT,
        POWER_PUNCH,
        STONE_SKIN,
        WARCRY,
        BERSERK,
        INTERVENE,
        //Hunter
        SHOOT,
        EVASION,
        STEALTH,
        READ_TRACES,
        SURVIVALIST,
        VOLLEY,
        COVER_FIRE,
        LAY_TRAP,
        SELF_BANDAGE,
        ORIENTEERING,
        //Mystic
        MAGIC_BOLT,
        ELEMENTAL_SHIELD,
        TELEPORT,
        MIND_CONTROL,
        CONCENTRATION,
        WARP,
        INVIGORATE,
        INGENUITY,
        CAUTERIZE,
        DEVASTATION,
        //Prophet
        HOLY_LIGHT,
        PRAYER,
        SERMON,
        DIVINE_AID,
        HEAL,
        FORESIGHT,
        BLESSING,
        DISCIPLINE,
        LORDS_GRACE,
        CELESTIAL_OMEN,
        //Mentor
        NEGOTIATION,
        RECRUITMENT,
        SUMMON,
        INSPIRER,
        PAST_EXPERIENCE,
        RESUSCITATION,
        GOOD_ADVICE,
        EXPENDABLES,
        YOU_FIRST,
        ERRAND
    }

    var level = 1
    var maxLevel = 3
    var isActive = true
    var modifier1: Modifier? = null
    var modifier2: Modifier? = null

    fun getModifier1Value() = modifier1?.resolve(level) ?: 0
    fun getModifier2Value() = modifier2?.resolve(level) ?: 0

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Defines the way various modifiers apply to skills
     */
    class Modifier(val type: Type, val value: Int) {

        enum class Type {
            STATIC, //Only modifier value is used
            PLUS_LEVEL, //Current skill level is added to modifier value
            MINUS_LEVEL, //Current skill level is subtracted from modifier value
            PLUS_DOUBLE_LEVEL //Current skill level is multiplied by 2 and added to the modifier value
        }

        /**
         * Get modifier value for specified level
         * @param level Level to check
         * @return Modifier value at this level
         */
        fun resolve(level: Int) = when (type) {
            Type.STATIC -> value
            Type.PLUS_LEVEL -> value + level
            Type.MINUS_LEVEL -> value - level
            Type.PLUS_DOUBLE_LEVEL -> value + level * 2
        }

    }


}