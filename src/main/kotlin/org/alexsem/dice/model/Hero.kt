package org.alexsem.dice.model

import java.util.*

/**
 * Describes one particular hero
 */
data class Hero(val type: Type) {

    /**
     * Hero type
     */
    enum class Type {
        BRAWLER, //Physical, Somatic
        HUNTER, //Somatic, Mental
        MYSTIC, //Mental
        PROPHET, //Divine, Mental, Verbal
        MENTOR //Verbal, Ally
    }

    var name = ""
    var isAlive = true
    var favoredDieType: Die.Type = Die.Type.ALLY
    val hand = Hand(0)
    val bag: Bag = Bag()
    val discardPile: Pile = Pile()

    private val diceLimits = mutableListOf<DiceLimit>()
    private val skills = mutableListOf<Skill>()
    private val dormantSkills = mutableListOf<Skill>()

    fun addDiceLimit(limit: DiceLimit) = diceLimits.add(limit)
    fun getDiceLimits(): List<DiceLimit> = Collections.unmodifiableList(diceLimits)
    fun addSkill(skill: Skill) = skills.add(skill)
    fun getSkills(): List<Skill> = Collections.unmodifiableList(skills)
    fun addDormantSkill(skill: Skill) = dormantSkills.add(skill)
    fun getDormantSkills(): List<Skill> = Collections.unmodifiableList(dormantSkills)

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Increase current value for specified die types limit
     * @param type Die type
     */
    fun increaseDiceLimit(type: Die.Type) {
        diceLimits.find { it.type == type }?.let {
            when {
                it.current < it.maximal -> it.current++
                else -> throw IllegalArgumentException("Already at maximum")
            }
        } ?: throw IllegalArgumentException("Incorrect type specified")
    }

    /**
     * Hide specific die to bag
     * @param die Die to hide
     */
    fun hideDieFromHand(die: Die) {
        bag.put(die)
        hand.removeDie(die)
    }

    /**
     * Discard specific die from hand
     * @param die Die to discard
     */
    fun discardDieFromHand(die: Die) {
        discardPile.put(die)
        hand.removeDie(die)
    }

    /**
     * Check whether hero has skill of specified type
     * @param type Skill type
     * @return true or false
     */
    fun hasSkill(type: Skill.Type) = skills.any { it.type == type }

    /**
     * Increase level of available skill or make one of the dormant skills available
     * @param type Type of skill to improve
     */
    fun improveSkill(type: Skill.Type) {
        dormantSkills
                .find { it.type == type }
                ?.let {
                    skills.add(it)
                    dormantSkills.remove(it)
                }
        skills
                .find { it.type == type }
                ?.let {
                    when {
                        it.level < it.maxLevel -> it.level += 1
                        else -> throw IllegalStateException("Skill already maxed out")
                    }
                } ?: throw IllegalArgumentException("Skill not found")
    }

}