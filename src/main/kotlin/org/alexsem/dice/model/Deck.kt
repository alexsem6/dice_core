package org.alexsem.dice.model

import java.util.*

/**
 * An ordered collection of cards (of the specific type)
 * @param <E> Type of cards
 */
class Deck<E: Threat> {

    private val cards = LinkedList<E>()
    val size
        get() = cards.size

    fun addToTop(card: E) = cards.addFirst(card)
    fun addToBottom(card: E) = cards.addLast(card)
    fun revealTop(): E = cards.first
    fun drawFromTop(): E = cards.removeFirst()
    fun shuffle() = cards.shuffle()
    fun clear() = cards.clear()
    fun examine() = cards.toList()

}