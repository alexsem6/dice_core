package org.alexsem.dice.model

/**
 * List of possible scenario rewards
 */
enum class ScenarioReward {
    GAIN_DIE, //Get one die of the chosen type
    UPGRADE_SKILL, //Upgrade existing skill
    LEARN_SKILL, //Learn new (dormant) skill
    INCREASE_DIE_LIMIT, //Increase limit for the chosen die type
    INCREASE_HAND_CAPACITY //Increase hand capacity by 1
}
