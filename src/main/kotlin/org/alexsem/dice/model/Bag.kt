package org.alexsem.dice.model

import java.util.*

/**
 * Bag with dice contained in it
 */
open class Bag {

    protected val dice = LinkedList<Die>()
    val size
        get() = dice.size

    /**
     * Add dice to bag
     * @param dice Dice to add
     */
    fun put(vararg dice: Die) {
        dice.forEach(this.dice::addLast)
        this.dice.shuffle()
    }

    /**
     * Draw die of particular type
     * @param type Type of die
     * @return die of specified type or null if no dice of this type
     */
    fun drawOfType(type: Die.Type) = dice.find { it.type == type }?.let { dice.remove(it); it }

    fun draw(): Die = dice.pollFirst()
    fun clear() = dice.clear()
    fun examine() = dice.sorted().toList()

}