package org.alexsem.dice.model

/**
 * Special class which described limitations for dice types
 */
class DiceLimit(val type: Die.Type, val initial: Int, val maximal: Int, var current: Int)
