package org.alexsem.dice.model

/**
 * Superclass for [Villain], [Enemy] and [Obstacle]
 */
sealed class Threat {
    lateinit var name: LocalizedString
    lateinit var description: LocalizedString
    private val traits = mutableListOf<Trait>()

    fun addTrait(trait: Trait) = traits.add(trait)
    fun getTraits(): List<Trait> = traits
}

/**
 * Obstacle cards appearing during exploration of locations
 */
class Obstacle(val tier: Int, vararg val dieTypes: Die.Type) : Threat()

/**
 * Villain cards appearing during exploration of locations
 */
class Villain : Threat()

/**
 * Enemy cards appearing during exploration of locations
 */
class Enemy : Threat()