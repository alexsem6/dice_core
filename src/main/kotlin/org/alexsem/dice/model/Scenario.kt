package org.alexsem.dice.model

import java.util.*

/**
 * Data data related to specific scenario
 */
class Scenario {

    lateinit var name: LocalizedString
    lateinit var description: LocalizedString
    lateinit var intro: LocalizedString
    lateinit var outro: LocalizedString

    var level = 0
    var initialTimer = 0

    private val allySkills = mutableListOf<AllySkill>()
    private val specialRules = mutableListOf<SpecialRule>()

    fun addAllySkill(skill: AllySkill) = allySkills.add(skill)
    fun getAllySkills(): List<AllySkill> = Collections.unmodifiableList(allySkills)
    fun addSpecialRule(rule: SpecialRule) = specialRules.add(rule)
    fun getSpecialRules(): List<SpecialRule> = Collections.unmodifiableList(specialRules)
}