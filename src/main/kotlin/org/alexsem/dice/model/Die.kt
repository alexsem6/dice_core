package org.alexsem.dice.model

class Die(val type: Type, val size: Int) : Comparable<Die> {

    enum class Type {
        PHYSICAL, //Blue
        SOMATIC, //Green
        MENTAL, //Purple
        VERBAL, //Yellow
        DIVINE, //Cyan
        WOUND, //Gray
        ENEMY, //Red
        VILLAIN, //Orange
        OBSTACLE, //Brown
        ALLY //White
    }

    /**
     * Produce random number within die size
     * @return Random die roll
     */
    fun roll() = (1.. size).random()

    override fun toString() = "d$size"

    override fun compareTo(other: Die): Int {
        return compareValuesBy(this, other, Die::type, { -it.size })
    }

}
