package org.alexsem.dice.model

/**
 * Class used for storing strings in different languages
 */
class LocalizedString(defaultValue: String, localizations: Map<String, String>) {

    private val default: String = defaultValue
    private val values: Map<String, String> = localizations.toMap()

    operator fun get(lang: String) = values.getOrDefault(lang, default)

    override fun equals(other: Any?) = when {
        this === other -> true
        other !is LocalizedString -> false
        else -> default == other.default
    }

    override fun hashCode(): Int {
        return default.hashCode()
    }

    /**
     * Get current string as list of language-value pairs
     * @return List of pairs
     */
    fun asList() = mutableListOf("" to default).apply {
        values.forEach { k, v -> this.add(k to v) }
    }.toList()
}

