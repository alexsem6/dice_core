package org.alexsem.dice.model

/**
 * All location-related information
 */
class Location {
    lateinit var name: LocalizedString
    lateinit var description: LocalizedString

    var isOpen = true
    var closingDifficulty = 0
    lateinit var bag: Bag
    var villain: Villain? = null
    lateinit var enemies: Deck<Enemy>
    lateinit var obstacles: Deck<Obstacle>

    private val specialRules = mutableListOf<SpecialRule>()
    fun addSpecialRule(rule: SpecialRule) = specialRules.add(rule)
    fun getSpecialRules() = specialRules

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as Location
        if (name != other.name) return false
        return true
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }


}