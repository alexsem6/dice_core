package org.alexsem.dice.model

import java.util.*

const val MAX_HAND_SIZE = 10
const val MAX_HAND_ALLY_SIZE = 6

/**
 * Defines dice present in hero's hand
 */
class Hand(var capacity: Int) {

    private val dice = LinkedList<Die>()
    private val allies = LinkedList<Die>()

    val dieCount
        get() = dice.size
    val allyDieCount
        get() = allies.size
    val woundCount
        get() = dice.filter { it.type == Die.Type.WOUND }.count()

    /**
     * Get die at specified index
     * @param index
     * @return Die at index or null
     */
    fun dieAt(index: Int) = when {
        (index in 0 until dieCount) -> dice[index]
        else -> null
    }

    /**
     * Get ally die at specified index
     * @param index
     * @return Die at index or null
     */
    fun allyDieAt(index: Int) = when {
        (index in 0 until allyDieCount) -> allies[index]
        else -> null
    }

    /**
     * Add new die (any type) to hand
     * @param die to add
     */
    fun addDie(die: Die) = when {
        die.type == Die.Type.ALLY -> allies.addLast(die)
        else -> dice.addLast(die)
    }

    /**
     * Remove specific die (any type) from hand
     * @param die Die to remove
     */
    fun removeDie(die: Die) = when {
        die.type == Die.Type.ALLY -> allies.remove(die)
        else -> dice.remove(die)
    }

    /**
     * Find die of specific type
     * @param type Die type
     * @return die which was found or null
     */
    fun findDieOfType(type: Die.Type): Die? = when (type) {
        Die.Type.ALLY -> if (allies.isNotEmpty()) allies.first else null
        else -> dice.firstOrNull { it.type == type }
    }

    /**
     * Remove all dice form hand
     */
    fun clear() {
        dice.clear()
        allies.clear()
    }

    /**
     * Return a sorted list of all dice currently present in the hand. Changes to this list will not affect the hand in
     * any way
     * @return List of all the dice in hand
     */
    fun examine(): List<Die> = (dice + allies).sorted()
}