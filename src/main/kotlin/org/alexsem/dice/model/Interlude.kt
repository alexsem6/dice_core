package org.alexsem.dice.model

class Interlude {

    /**
     * Various hints on what type of music to play
     */
    enum class Mood {
        NONE,
        SAD,
        SERENE,
        DETERMINED,
        OMINOUS,
        TRIUMPHANT,
        DANGEROUS,
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Actor for the screenplay
     */
    sealed class Actor {
        /**
         * Extension for [Actor] class where actor is an actual [org.alexsem.dice.model.Hero]
         */
        class Hero(val hero: org.alexsem.dice.model.Hero) : Actor()

        /**
         * Extension for [Actor] class where actor is an arbitrary NPC
         */
        class NPC(val index: Int, val name: LocalizedString) : Actor()
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * One single part of the screenplay
     */
    sealed class Event {

        /**
         * Extension for the [Event] which changes the mood of the screenplay
         */
        class Tune(val mood: Mood) : Event()
        /**
         * Extension for the [Event] which represents a title (headline)
         */
        class Headline(val text: LocalizedString) : Event()
        /**
         * Extension for the [Event] which represents some comment (serves exposition purposes)
         */
        class Narration(val text: LocalizedString) : Event()

        /**
         * Extension for the [Event] which certain implies [Actor] making a statement
         */
        class Phrase(val actor: Actor, val text: LocalizedString) : Event()
    }

    //------------------------------------------------------------------------------------------------------------------

    lateinit var name: LocalizedString
    private val script = mutableListOf<Event>()

    fun addEvent(event: Event) = script.add(event)
    fun getScript(): List<Event> = script
}

