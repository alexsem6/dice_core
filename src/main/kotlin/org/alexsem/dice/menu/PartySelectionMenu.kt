package org.alexsem.dice.menu

import org.alexsem.dice.ui.Action
import org.alexsem.dice.ui.ActionList
import org.alexsem.dice.ui.StatusMessage
import org.alexsem.dice.ui.UI
import org.alexsem.dice.ui.audio.Audio
import org.alexsem.dice.ui.audio.Sound
import org.alexsem.dice.ui.storage.PartyInfoFull
import org.alexsem.dice.ui.storage.PartyInfoShort
import java.io.IOException
import kotlin.math.max

class PartySelectionMenu(ui: UI, private val callback: (PartyInfoFull?) -> Unit) {

    private val renderer = ui.menuRenderer
    private val interactor = ui.menuInteractor
    private val storage = ui.storage

    private var currentPage: Int = 0
    private var pageCount: Int = 0
    private var parties = emptyList<PartyInfoShort>()
    private var pickedIndex: Int? = null

    private var phase: Phase = Phase.NO_PARTIES
    private var statusMessage = StatusMessage.EMPTY
    private var actions = ActionList.EMPTY

    fun start() {
        loadPartyList()
        processCycle()
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Various party selector phases
     */
    private enum class Phase {
        CHOOSE_PARTY,
        NO_PARTIES,
        LOAD_ERROR,
        DELETE_PARTY,
        DELETE_CONFIRM,
        DELETE_ERROR,
        SELECT_ERROR
    }

    /**
     * PHASE: Choose party from the list
     */
    private fun changePhaseChooseParty() {
        phase = Phase.CHOOSE_PARTY
        pickedIndex = null
        statusMessage = StatusMessage.CHOOSE_PARTY_TO_LOAD
        actions = ActionList()
        actions.add(Action.Type.DELETE_PARTY)
        if (pageCount > 1) {
            actions.add(Action.Type.INFO_PAGE_DOWN, currentPage < pageCount)
            actions.add(Action.Type.INFO_PAGE_UP, currentPage > 1)
        }
        actions.add(Action.Type.CANCEL)
    }

    /**
     * PHASE: No loadable parties available
     */
    private fun changePhaseNoParties() {
        phase = Phase.NO_PARTIES
        pickedIndex = null
        statusMessage = StatusMessage.ERROR_PARTY_LIST_EMPTY
        actions = ActionList().add(Action.Type.CANCEL)
    }

    /**
     * PHASE: Could not load list of parties
     */
    private fun changePhaseLoadError() {
        Audio.playSound(Sound.ERROR)
        phase = Phase.LOAD_ERROR
        statusMessage = StatusMessage.ERROR_PARTY_LIST_LOAD
        actions = ActionList()
        actions.add(Action.Type.RETRY)
        actions.add(Action.Type.CANCEL)
    }

    /**
     * PHASE: Select party for deletion
     */
    private fun changePhaseDeleteParty() {
        phase = Phase.DELETE_PARTY
        pickedIndex = null
        statusMessage = StatusMessage.CHOOSE_PARTY_TO_DELETE
        actions = ActionList().add(Action.Type.CANCEL)
    }

    /**
     * PHASE: Confirm party deletion
     */
    private fun changePhaseDeleteConfirm() {
        Audio.playSound(Sound.CONFIRM_PROMPT)
        phase = Phase.DELETE_CONFIRM
        statusMessage = StatusMessage.CONFIRM_DELETE_PARTY
        actions = ActionList()
        actions.add(Action.Type.CONFIRM)
        actions.add(Action.Type.CANCEL)
    }

    /**
     * PHASE: Could not delete party
     */
    private fun changePhaseDeleteError() {
        Audio.playSound(Sound.ERROR)
        phase = Phase.DELETE_ERROR
        statusMessage = StatusMessage.ERROR_PARTY_DELETE
        actions = ActionList.EMPTY
    }

    /**
     * PHASE: Could not load party
     */
    private fun changePhaseSelectError() {
        Audio.playSound(Sound.ERROR)
        phase = Phase.SELECT_ERROR
        statusMessage = StatusMessage.ERROR_PARTY_LOAD
        actions = ActionList.EMPTY
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Set the correct page count based on the number of parties
     */
    private fun adjustPageCount() {
        pageCount = max(1, parties.size / renderer.partiesPerPage + (if (parties.size % renderer.partiesPerPage > 0) 1 else 0))
    }

    /**
     * Number of parties on current page
     */
    private val currentPagePartyCount: Int
        get() = when (currentPage) {
            pageCount -> parties.size - (currentPage - 1) * renderer.partiesPerPage
            else -> renderer.partiesPerPage
        }

    /**
     * Load list of parties
     */
    private fun loadPartyList() {
        try {
            parties = storage.loadPartyList()
            adjustPageCount()
            currentPage = 1
            if (parties.isNotEmpty()) {
                changePhaseChooseParty()
            } else {
                changePhaseNoParties()
            }
        } catch (ex: IOException) {
            parties = emptyList()
            currentPage = 1
            pageCount = 1
            changePhaseLoadError()
        }
    }

    /**
     * Delete selected party
     * @param party Party to delete
     */
    private fun deleteParty(party: PartyInfoShort) {
        actions = ActionList.EMPTY
        statusMessage = StatusMessage.EMPTY
        drawScreen()
        try {
            storage.deleteParty(party.id)
            Audio.playSound(Sound.DIE_REMOVE)
            parties = parties - party
            adjustPageCount()
            if (currentPage > pageCount) {
                currentPage--
            }
            if (parties.isNotEmpty()) {
                changePhaseChooseParty()
            } else {
                changePhaseNoParties()
            }
        } catch (ex: IOException) {
            changePhaseDeleteError()
        }
    }

    //==================================================================================================================
    /**
     * Run main cycle
     */
    private fun processCycle() {
        while (true) {
            drawScreen()
            when (phase) {
                //----------------------------------------------------------------------------
                Phase.CHOOSE_PARTY -> {
                    val action = interactor.pickPartySelectorAction(actions, currentPagePartyCount)
                    when (action.type) {
                        Action.Type.DELETE_PARTY -> changePhaseDeleteParty()
                        Action.Type.INFO_PAGE_DOWN -> if (currentPage < pageCount) {
                            Audio.playSound(Sound.PAGE_NEXT)
                            currentPage++
                            actions[Action.Type.INFO_PAGE_DOWN]?.isEnabled = (currentPage < pageCount)
                            actions[Action.Type.INFO_PAGE_UP]?.isEnabled = (currentPage > 1)
                        }
                        Action.Type.INFO_PAGE_UP -> if (currentPage > 1) {
                            Audio.playSound(Sound.PAGE_PREVIOUS)
                            currentPage--
                            actions[Action.Type.INFO_PAGE_DOWN]?.isEnabled = (currentPage < pageCount)
                            actions[Action.Type.INFO_PAGE_UP]?.isEnabled = (currentPage > 1)
                        }
                        Action.Type.CANCEL -> {
                            Audio.playSound(Sound.PAGE_CLOSE)
                            callback(null)
                            return
                        }
                        Action.Type.LIST_ITEM -> {
                            actions = ActionList.EMPTY
                            statusMessage = StatusMessage.EMPTY
                            drawScreen()
                            try {
                                val index = (currentPage - 1) * renderer.partiesPerPage + action.data
                                val info = storage.loadParty(parties[index].id)
                                Audio.playSound(Sound.DIE_PICK)
                                callback(info)
                                return
                            } catch (ex: IOException) {
                                changePhaseSelectError()
                            }
                        }
                        else -> throw AssertionError("Should not happen")
                    }
                }
                //----------------------------------------------------------------------------
                Phase.NO_PARTIES, Phase.LOAD_ERROR -> when (interactor.pickAction(actions).type) {
                    Action.Type.RETRY -> loadPartyList()
                    Action.Type.CANCEL -> {
                        Audio.playSound(Sound.PAGE_CLOSE)
                        callback(null)
                        return
                    }
                    else -> throw AssertionError("Should not happen")
                }
                //----------------------------------------------------------------------------
                Phase.DELETE_PARTY -> {
                    val action = interactor.pickPartySelectorAction(actions, currentPagePartyCount)
                    when (action.type) {
                        Action.Type.CANCEL -> changePhaseChooseParty()
                        Action.Type.LIST_ITEM -> {
                            if (action.data < parties.size) {
                                pickedIndex = (currentPage - 1) * renderer.partiesPerPage + action.data
                                changePhaseDeleteConfirm()
                            }
                        }
                        else -> throw AssertionError("Should not happen")
                    }
                }
                //----------------------------------------------------------------------------
                Phase.DELETE_CONFIRM -> when (interactor.pickAction(actions).type) {
                    Action.Type.CONFIRM -> deleteParty(parties[pickedIndex!!])
                    Action.Type.CANCEL -> changePhaseChooseParty()
                    else -> throw AssertionError("Should not happen")
                }
                //----------------------------------------------------------------------------
                Phase.DELETE_ERROR, Phase.SELECT_ERROR -> {
                    interactor.anyInput()
                    changePhaseChooseParty()
                }
                //----------------------------------------------------------------------------
            }
        }
    }

    //==================================================================================================================

    /**
     * Draw current screen
     */
    private fun drawScreen() {
        renderer.drawPartyList(parties, pickedIndex, currentPage, pageCount, statusMessage, actions)
    }


}