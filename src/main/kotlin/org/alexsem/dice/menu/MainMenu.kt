package org.alexsem.dice.menu

import org.alexsem.dice.manager.PartyManager
import org.alexsem.dice.ui.Action
import org.alexsem.dice.ui.ActionList
import org.alexsem.dice.ui.UI
import org.alexsem.dice.ui.audio.Audio
import org.alexsem.dice.ui.audio.Music
import org.alexsem.dice.ui.audio.Sound
import org.alexsem.dice.ui.storage.PartyInfoFull
import java.io.IOException

/**
 * Starting interactive class
 */
class MainMenu(private val ui: UI) {

    private val renderer = ui.menuRenderer
    private val interactor = ui.menuInteractor
    private val storage = ui.storage

    private var actions = ActionList.EMPTY


    fun start() {
        Audio.playMusic(Music.MENU_MAIN)
        actions = ActionList()
        actions.add(Action.Type.NEW_ADVENTURE)
        actions.add(Action.Type.CONTINUE_ADVENTURE)
        actions.add(Action.Type.MANUAL, false)// TODO implement
        actions.add(Action.Type.EXIT)
        adjustSavedAdventuresAvailability()
        processCycle()
    }

    /**
     * Check whether saved adventures available and adjust respective button state
     */
    private fun adjustSavedAdventuresAvailability() {
        actions[Action.Type.CONTINUE_ADVENTURE]?.isEnabled = try {
            storage.isPartiesAvailable()
        } catch (ex: IOException) {
            false
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Start new adventure
     */
    private fun newAdventure() {
        Audio.playSound(Sound.DIE_PICK)
        AdventureSelectionMenu(ui) { adventure ->
            if (adventure != null) {
                PartyManager(ui) { y, name, heroes ->
                    if (y) {
                        try {
                            goToScenarioSelector(storage.createParty(name, heroes, adventure))
                        } catch (ex: IOException) {
                            Audio.playSound(Sound.ERROR)
                        }
                    }
                }.start()
            }
            adjustSavedAdventuresAvailability()
        }.start()
    }

    /**
     * Resume existing adventure
     */
    private fun continueAdventure() {
        Audio.playSound(Sound.DIE_PICK)
        PartySelectionMenu(ui) { party ->
            adjustSavedAdventuresAvailability()
            party?.let { goToScenarioSelector(it) }
        }.start()
    }

    /**
     * Navigate to scenario selection screen
     * @param party Party to use
     */
    private fun goToScenarioSelector(party: PartyInfoFull) {
        ScenarioSelectionMenu(ui, party).start()
        Audio.playMusic(Music.MENU_MAIN)
    }


    //==================================================================================================================

    /**
     * Run main cycle
     */
    private fun processCycle() {
        renderer.clearScreen()
        while (true) {
            drawScreen()
            when (interactor.pickAction(actions).type) {
                Action.Type.NEW_ADVENTURE -> newAdventure()
                Action.Type.CONTINUE_ADVENTURE -> continueAdventure()
                Action.Type.MANUAL -> TODO()
                Action.Type.EXIT -> {
                    Audio.stopMusic()
                    Audio.playSound(Sound.LEAVE)
                    renderer.clearScreen()
                    Thread.sleep(500)
                    return
                }
                else -> throw AssertionError("Should not happen")
            }
        }
    }

    //==================================================================================================================

    /**
     * Draw current screen
     */
    private fun drawScreen() {
        renderer.drawMainMenu(actions)
    }

}