package org.alexsem.dice.menu

import org.alexsem.dice.generator.generateAdventureDescription
import org.alexsem.dice.generator.generateAdventureName
import org.alexsem.dice.generator.template.adventure.AdventureTemplate
import org.alexsem.dice.model.LocalizedString
import org.alexsem.dice.ui.Action
import org.alexsem.dice.ui.ActionList
import org.alexsem.dice.ui.StatusMessage
import org.alexsem.dice.ui.UI
import org.alexsem.dice.ui.audio.Audio
import org.alexsem.dice.ui.audio.Sound
import org.reflections.Reflections

class AdventureSelectionMenu(ui: UI, private val callback: (AdventureTemplate?) -> Unit) {

    private val renderer = ui.menuRenderer
    private val interactor = ui.menuInteractor

    private var currentAdventureIndex: Int = 0

    private var adventureTemplates = emptyList<AdventureTemplate>()
    private var adventureNames = emptyList<LocalizedString>()
    private var adventureDescriptions = emptyList<LocalizedString>()

    private var phase: Phase = Phase.NO_ADVENTURES
    private var statusMessage = StatusMessage.EMPTY
    private var actions = ActionList.EMPTY

    fun start() {
        findAdventures()
        processCycle()
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Various adventure selector phases
     */
    private enum class Phase {
        CHOOSE_ADVENTURE,
        NO_ADVENTURES,
        LOAD_ERROR
    }

    /**
     * PHASE: Choose adventure from the list
     */
    private fun changePhaseChooseAdventure() {
        phase = Phase.CHOOSE_ADVENTURE
        statusMessage = StatusMessage.CHOOSE_ADVENTURE_TO_START
        actions = ActionList()
        actions.add(Action.Type.CONFIRM)
        if (adventureNames.size > 1) {
            actions.add(Action.Type.NEXT_ADVENTURE)
            actions.add(Action.Type.PREVIOUS_ADVENTURE)
        }
        actions.add(Action.Type.CANCEL)
    }

    /**
     * PHASE: No adventures available
     */
    private fun changePhaseNoAdventures() {
        phase = Phase.NO_ADVENTURES
        statusMessage = StatusMessage.ERROR_ADVENTURE_LIST_EMPTY
        actions = ActionList().add(Action.Type.CANCEL)
    }

    /**
     * PHASE: Could not load list of adventures
     */
    private fun changePhaseLoadError() {
        Audio.playSound(Sound.ERROR)
        phase = Phase.LOAD_ERROR
        statusMessage = StatusMessage.ERROR_ADVENTURE_LIST_LOAD
        actions = ActionList()
        actions.add(Action.Type.RETRY)
        actions.add(Action.Type.CANCEL)
    }


    //------------------------------------------------------------------------------------------------------------------

    /**
     * Load list of adventures
     */
    private fun findAdventures() {
        try {
            adventureTemplates = Reflections("org.alexsem.dice").getSubTypesOf(AdventureTemplate::class.java).map { it.newInstance() }
            adventureNames = adventureTemplates.map { generateAdventureName(it) }
            adventureDescriptions = adventureTemplates.map { generateAdventureDescription(it) }
            currentAdventureIndex = 0
            if (adventureTemplates.isNotEmpty()) {
                changePhaseChooseAdventure()
            } else {
                changePhaseNoAdventures()
            }
        } catch (ex: Exception) {
            adventureTemplates = emptyList()
            adventureNames = emptyList()
            adventureDescriptions = emptyList()
            currentAdventureIndex = 0
            changePhaseLoadError()
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Run main cycle
     */
    private fun processCycle() {
        while (true) {
            drawScreen()
            when (phase) {
                //----------------------------------------------------------------------------
                Phase.CHOOSE_ADVENTURE -> {
                    val action = interactor.pickAction(actions)
                    when (action.type) {
                        Action.Type.NEXT_ADVENTURE -> {
                            Audio.playSound(Sound.PAGE_NEXT)
                            currentAdventureIndex = ++currentAdventureIndex % adventureNames.size
                        }
                        Action.Type.PREVIOUS_ADVENTURE ->  {
                            Audio.playSound(Sound.PAGE_PREVIOUS)
                            currentAdventureIndex = (--currentAdventureIndex + adventureNames.size) % adventureNames.size
                        }
                        Action.Type.CANCEL -> {
                            Audio.playSound(Sound.PAGE_CLOSE)
                            callback(null)
                            return
                        }
                        Action.Type.CONFIRM -> {
                                Audio.playSound(Sound.DIE_PICK)
                                callback(adventureTemplates[currentAdventureIndex])
                                return
                        }
                        else -> throw AssertionError("Should not happen")
                    }
                }
                //----------------------------------------------------------------------------
                Phase.NO_ADVENTURES, Phase.LOAD_ERROR -> when (interactor.pickAction(actions).type) {
                    Action.Type.RETRY -> findAdventures()
                    Action.Type.CANCEL -> {
                        Audio.playSound(Sound.PAGE_CLOSE)
                        callback(null)
                        return
                    }
                    else -> throw AssertionError("Should not happen")
                }
                //----------------------------------------------------------------------------
            }
        }
    }

    //==================================================================================================================

    /**
     * Draw current screen
     */
    private fun drawScreen() {
        renderer.drawAdventureList(adventureNames, adventureDescriptions, currentAdventureIndex, statusMessage, actions)
    }

}