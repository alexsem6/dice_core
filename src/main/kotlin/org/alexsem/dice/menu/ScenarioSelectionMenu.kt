package org.alexsem.dice.menu

import org.alexsem.dice.game.Game
import org.alexsem.dice.game.GameCallback
import org.alexsem.dice.game.InterludePlayer
import org.alexsem.dice.game.ScenicTextPlayer
import org.alexsem.dice.generator.*
import org.alexsem.dice.generator.template.adventure.AdventureTemplate
import org.alexsem.dice.manager.DiceManager
import org.alexsem.dice.model.LocalizedString
import org.alexsem.dice.model.Pile
import org.alexsem.dice.model.ScenarioReward
import org.alexsem.dice.ui.Action
import org.alexsem.dice.ui.ActionList
import org.alexsem.dice.ui.StatusMessage
import org.alexsem.dice.ui.UI
import org.alexsem.dice.ui.audio.Audio
import org.alexsem.dice.ui.audio.Music
import org.alexsem.dice.ui.audio.Sound
import org.alexsem.dice.ui.storage.PartyInfoFull
import org.alexsem.dice.upgrade.HeroDiceLimitsUpgrader
import org.alexsem.dice.upgrade.HeroDieRewarder
import org.alexsem.dice.upgrade.HeroHandCapacityUpgrader
import org.alexsem.dice.upgrade.HeroSkillsUpgrader
import java.io.IOException
import kotlin.math.max

class ScenarioSelectionMenu(private val ui: UI, private val party: PartyInfoFull) {

    private val renderer = ui.menuRenderer
    private val interactor = ui.menuInteractor
    private val storage = ui.storage

    private var currentPage: Int = 0
    private var pageCount: Int = 0
    private var pickedIndex: Int? = null

    private var adventureName = generateAdventureName(party.adventure)
    private var phaseNames = emptyList<LocalizedString>()

    private var phase: Phase = Phase.NO_ADVENTURE_PHASES
    private var statusMessage = StatusMessage.EMPTY
    private var actions = ActionList.EMPTY

    fun start() {
        generateAdventurePhases()
        if (phaseNames.isNotEmpty() && storage.isGameSaveAvailable(party.id)) {
            launchAdventurePhase(party.progress)
        }
        Audio.playMusic(Music.MENU_ADVENTURE)
        processCycle()
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Various scenario selector phases
     */
    private enum class Phase {
        CHOOSE_ADVENTURE_PHASE,
        NO_ADVENTURE_PHASES,
        LOAD_ERROR,
        CONFIRM_SCENARIO_REPLAY,
        EXIT_IMMEDIATELY
    }

    /**
     * PHASE: Choose adventure phase from the list
     */
    private fun changePhaseChooseAdventurePhase() {
        phase = Phase.CHOOSE_ADVENTURE_PHASE
        pickedIndex = null
        actions = ActionList()
        if (party.progress < phaseNames.size) {
            statusMessage = StatusMessage.CHOOSE_ADVENTURE_PHASE_TO_START
            actions.add(Action.Type.CONFIRM)
        } else {
            statusMessage = StatusMessage.ALL_ADVENTURE_PHASES_COMPLETED
        }
        if (pageCount > 1) {
            actions.add(Action.Type.INFO_PAGE_DOWN)
            actions.add(Action.Type.INFO_PAGE_UP)
        }
        actions.add(Action.Type.MANAGE_DICE)
        actions.add(Action.Type.EXIT)
        adjustActionsEnabled()
    }

    /**
     * PHASE: Confirm replaying of already finished scenario
     */
    private fun changePhaseConfirmScenarioReplay() {
        Audio.playSound(Sound.CONFIRM_PROMPT)
        phase = Phase.CONFIRM_SCENARIO_REPLAY
        statusMessage = StatusMessage.CONFIRM_SCENARIO_REPLAY
        actions = ActionList()
        actions.add(Action.Type.CONFIRM)
        actions.add(Action.Type.PLAY_INTRO)
        actions.add(Action.Type.PLAY_OUTRO)
        actions.add(Action.Type.CANCEL)
    }

    /**
     * PHASE: No adventure phases available
     */
    private fun changePhaseNoAdventurePhases() {
        phase = Phase.NO_ADVENTURE_PHASES
        pickedIndex = null
        statusMessage = StatusMessage.ERROR_ADVENTURE_PHASE_LIST_EMPTY
        actions = ActionList().add(Action.Type.CANCEL)
    }

    /**
     * PHASE: Could not load list of parties
     */
    private fun changePhaseLoadError() {
        Audio.playSound(Sound.ERROR)
        pickedIndex = null
        phase = Phase.LOAD_ERROR
        statusMessage = StatusMessage.ERROR_ADVENTURE_PHASE_LIST_LOAD
        actions = ActionList()
        actions.add(Action.Type.RETRY)
        actions.add(Action.Type.CANCEL)
    }

    /**
     * PHASE: Close scenario selector
     */
    private fun changePhaseExitImmediately() {
        phase = Phase.EXIT_IMMEDIATELY
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Set the correct page count based on the number of phases
     */
    private fun adjustPageCount() {
        pageCount = max(1, phaseNames.size / renderer.scenariosPerPage + (if (phaseNames.size % renderer.scenariosPerPage > 0) 1 else 0))
    }

    /**
     * Set actions as enabled/disabled based on the current state
     */
    private fun adjustActionsEnabled() {
        actions[Action.Type.CONFIRM]?.isEnabled = party.progress < phaseNames.size
                && party.progress / renderer.scenariosPerPage + 1 == currentPage
                && party.adventure.phases[party.progress] !is AdventureTemplate.Phase.ToBeContinued
        actions[Action.Type.INFO_PAGE_DOWN]?.isEnabled = (currentPage < pageCount)
        actions[Action.Type.INFO_PAGE_UP]?.isEnabled = (currentPage > 1)
    }


    /**
     * Number of adventure phases on current page
     */
    private val currentPagePhaseCount: Int
        get() = when (currentPage) {
            pageCount -> phaseNames.size - (currentPage - 1) * renderer.partiesPerPage
            else -> renderer.partiesPerPage
        }

    /**
     * Generate list of adventure phase names
     */
    private fun generateAdventurePhases() {
        try {
            phaseNames = generateAdventurePhaseNames(party.adventure)
            adjustPageCount()
            currentPage = party.progress / renderer.scenariosPerPage + 1
            if (phaseNames.isNotEmpty()) {
                changePhaseChooseAdventurePhase()
            } else {
                changePhaseNoAdventurePhases()
            }
        } catch (ex: Exception) {
            phaseNames = emptyList()
            currentPage = 1
            pageCount = 1
            changePhaseLoadError()
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Callback for scenario finished phase
     */
    private val scenarioEndCallback: GameCallback = { result, heroes, pile, index ->
        if (result == Game.Result.EXIT) { //Just exit to main menu
            changePhaseExitImmediately()
        } else { //Process party results
            party.heroes = heroes
            if (result == Game.Result.WIN && index == party.progress) {
                party.progress++
                val phase = party.adventure.phases[index] as AdventureTemplate.Phase.Scenario
                when (phase.reward) {
                    ScenarioReward.GAIN_DIE -> HeroDieRewarder(ui, party.heroes, phase.level).start()
                    ScenarioReward.UPGRADE_SKILL -> HeroSkillsUpgrader(ui, party.heroes, HeroSkillsUpgrader.SkillSet.IMPROVE_EXISTING).start()
                    ScenarioReward.LEARN_SKILL -> HeroSkillsUpgrader(ui, party.heroes, HeroSkillsUpgrader.SkillSet.LEARN_NEW).start()
                    ScenarioReward.INCREASE_DIE_LIMIT -> HeroDiceLimitsUpgrader(ui, party.heroes).start()
                    ScenarioReward.INCREASE_HAND_CAPACITY -> HeroHandCapacityUpgrader(ui, party.heroes).start()
                }
            }
            launchDiceManager(pile)
            try {
                storage.deleteGame(party.id)
            } catch (ex: Exception) {
                //Do nothing
            }
            Audio.playMusic(Music.MENU_ADVENTURE)
        }
    }

    /**
     * Launch and process results of selected adventure phase
     */
    private fun launchAdventurePhase(index: Int) {
        val phase = party.adventure.phases[index]
        when (phase) {
            is AdventureTemplate.Phase.Scenario -> {
                val game: Game = when (storage.isGameSaveAvailable(party.id)) {
                    true -> try {
                        storage.loadGame(party.id)
                    } catch (ex: IOException) {
                        null
                    }
                    false -> null
                }?.let { Game(ui, it, scenarioEndCallback) }
                        ?: Game(ui, party.id, index,
                                generateScenario(phase.scenario, phase.level),
                                generateLocations(phase.scenario, phase.level, party.heroes.size),
                                party.heroes, scenarioEndCallback)
                Audio.stopMusic()
                game.start()
            }
            is AdventureTemplate.Phase.Interlude -> {
                InterludePlayer(ui, generateInterlude(phase.interlude, party.heroes)).start()
                Audio.playMusic(Music.MENU_ADVENTURE)
                if (index == party.progress) {
                    party.progress++
                    try {
                        storage.updateParty(party)
                    } catch (ex: IOException) {
                        //Do nothing
                    }
                }
            }
            is AdventureTemplate.Phase.ToBeContinued -> {
                //Do nothing
            }
        }
    }

    /**
     * Launch dice manager for current heroes
     * @param pile Common pool to use
     */
    private fun launchDiceManager(pile: Pile) {
        DiceManager(ui, party.heroes, pile).start()
        try {
            storage.updateParty(party)
        } catch (ex: IOException) {
            //Do nothing
        }
    }

    /**
     * Launch Scenario scenic text player
     * @param index Index of adventure phase
     * @param isIntro true to watch intro, false to watch outro
     */
    private fun launchScenicTextPlayer(index: Int, isIntro: Boolean) {
        val phase = party.adventure.phases[index] as AdventureTemplate.Phase.Scenario
        ScenicTextPlayer(ui, generateScenario(phase.scenario, phase.level), isIntro).start()
        Audio.playMusic(Music.MENU_ADVENTURE)
        changePhaseChooseAdventurePhase()
    }

    //==================================================================================================================

    /**
     * Run main cycle
     */
    private fun processCycle() {
        while (true) {
            drawScreen()
            when (phase) {
                //----------------------------------------------------------------------------
                Phase.CHOOSE_ADVENTURE_PHASE -> {
                    val currentPageScenarioCount = max(0, party.progress - (currentPage - 1) * renderer.scenariosPerPage)
                    val action = interactor.pickScenarioSelectorAction(actions, currentPageScenarioCount)
                    when (action.type) {
                        Action.Type.LIST_ITEM -> {
                            val index = (currentPage - 1) * renderer.scenariosPerPage + action.data
                            if (party.adventure.phases[index] is AdventureTemplate.Phase.Scenario) {
                                pickedIndex = index
                                changePhaseConfirmScenarioReplay()
                            } else {
                                launchAdventurePhase(index)
                            }
                        }
                        Action.Type.INFO_PAGE_DOWN -> if (currentPage < pageCount) {
                            Audio.playSound(Sound.PAGE_NEXT)
                            currentPage++
                            adjustActionsEnabled()
                        }
                        Action.Type.INFO_PAGE_UP -> if (currentPage > 1) {
                            Audio.playSound(Sound.PAGE_PREVIOUS)
                            currentPage--
                            adjustActionsEnabled()
                        }
                        Action.Type.MANAGE_DICE -> launchDiceManager(Pile())
                        Action.Type.EXIT -> {
                            Audio.playSound(Sound.PAGE_CLOSE)
                            return
                        }
                        Action.Type.CONFIRM -> launchAdventurePhase(party.progress)
                        else -> throw AssertionError("Should not happen")
                    }
                }
                //----------------------------------------------------------------------------
                Phase.NO_ADVENTURE_PHASES, Phase.LOAD_ERROR -> when (interactor.pickAction(actions).type) {
                    Action.Type.RETRY -> generateAdventurePhases()
                    Action.Type.CANCEL -> {
                        Audio.playSound(Sound.PAGE_CLOSE)
                        return
                    }
                    else -> throw AssertionError("Should not happen")
                }
                //----------------------------------------------------------------------------
                Phase.CONFIRM_SCENARIO_REPLAY -> when (interactor.pickAction(actions).type) {
                    Action.Type.CONFIRM -> {
                        val index = pickedIndex!!
                        changePhaseChooseAdventurePhase()
                        launchAdventurePhase(index)
                    }
                    Action.Type.PLAY_INTRO -> launchScenicTextPlayer(pickedIndex!!, true)
                    Action.Type.PLAY_OUTRO -> launchScenicTextPlayer(pickedIndex!!, false)
                    Action.Type.CANCEL -> changePhaseChooseAdventurePhase()
                    else -> throw AssertionError("Should not happen")
                }
                //----------------------------------------------------------------------------
                Phase.EXIT_IMMEDIATELY -> return
            }
        }
    }

    //==================================================================================================================

    /**
     * Draw current screen
     */
    private fun drawScreen() {
        renderer.drawScenarioList(adventureName, phaseNames, party.progress, party.heroes, pickedIndex, currentPage, pageCount, statusMessage, actions)
    }

//TODO b for back page conflicts with item
}
