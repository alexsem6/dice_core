package org.alexsem.dice.adventure

import org.alexsem.dice.generator.template.adventure.EnemyTemplate
import org.alexsem.dice.model.Trait

@Deprecated("Test")
class TestEnemyTemplate : EnemyTemplate {
    override val name = "Customs Officer"
    override val description = "Not much can be said about these 'people'. They have a job of protecting honest citizens from crooks and frauds, whether in reality they are the biggest crooks themselves. When one of them is at us, there is no point in showing any opposition. We better hand over all of our valuables and be on our way, than risk spending the rest of the week in some unpleasant places."
    override val traits = listOf(
            Trait.WIN_DRAW_DIE
    )

    override val nameLocalizations = mapOf(
            "ru" to "Таможенник"
    )
    override val descriptionLocalizations = mapOf(
            "ru" to "Что можно сказать об этих 'людях'? Их работа - защищать честных граждан от пройдох и мошенников, но в реальности они сами - большие мошенники. Вершат свои тёмные дела, прикрываясь буквой закона. Протестовать бесполезно - лучше мы добровольно сдадим им всё своё добро, чем проведём остаток недели в каком-нибудь неприятном месте."
    )
}
