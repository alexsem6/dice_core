package org.alexsem.dice.adventure

import org.alexsem.dice.generator.template.adventure.VillainTemplate
import org.alexsem.dice.model.Trait

@Deprecated("Test")
class TestVillainTemplate : VillainTemplate {

    override val name = "'Squeaky' Usher"
    override val description = "Jedaiah Usher comes from the poor family of Ortholan immigrants. He was born weak and frail, with one leg shorter than the other (qhich made him limping for the rest of his life). He was silent and inconspicuous, never participated in social activities and his favorite hobby was to perform 'various anatomic researches' on small animals (mostly cats and dogs) he found roaming the city dump in search of food. Eventually this became his occupation: when he grew up, he worked as either a butcher in a shop or a surgeon in a clinic - the sources disagree over this part of his biography. One is definite thought, that during the War he served as a field medic and reached a certain level of skill in his new profession. This is where he got his dubios nickname - not particularly known for what exactly.\n\nAfter the treaty was signed, doctor Usher continued working in the military hospital - up until one day, when his name was involved in a huge scandal (some dead bodies and transplanted organs were involved as well). He fled the region and came living to a small settlement of Hampstead where his peculiar skills came in huge demand by local authority figure Ezra Granov."
    override val traits = listOf(
            Trait.DAMAGE_WOUND_TWO,
            Trait.WIN_CLOSE_ATTEMPT
    )

    override val nameLocalizations = mapOf(
            "ru" to "'Скрипучий' Ашер"
    )
    override val descriptionLocalizations = mapOf<String, String>(    )
}