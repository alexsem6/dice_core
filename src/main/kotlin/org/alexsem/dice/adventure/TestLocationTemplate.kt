package org.alexsem.dice.adventure

import org.alexsem.dice.generator.template.adventure.LocationTemplate
import org.alexsem.dice.generator.template.bag.BagTemplate
import org.alexsem.dice.generator.template.die.SingleDieTypeFilter
import org.alexsem.dice.model.Die
import org.alexsem.dice.model.SpecialRule

@Deprecated("Test location")
class TestLocationTemplate : LocationTemplate {

    override val name = "Test Location"
    override val description = "Some Description"

    override val basicClosingDifficulty = 0
    override val enemyCardsCount = 1
    override val obstacleCardsCount = 1

    override val bagTemplate = BagTemplate().apply {
        addPlan(9, 9, SingleDieTypeFilter(Die.Type.ENEMY))
    }

    override val enemyCardPool = listOf(
            TestEnemyTemplate()
    )

    override val obstacleCardPool = listOf(
            TestObstacleTemplate()
    )

    override val specialRules = listOf(
            SpecialRule.CLAUSTROPHOBIA
    )

    override val nameLocalizations = mapOf<String, String>()
    override val descriptionLocalizations = mapOf<String, String>()
}
