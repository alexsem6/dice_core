package org.alexsem.dice.adventure

import org.alexsem.dice.generator.template.adventure.ObstacleTemplate
import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Trait

@Deprecated("Test class")
class TestObstacleTemplate : ObstacleTemplate {

    override val name = "Full stop reverse"

    override val description = "Just some random obstacle standing in your way.\nSecond line of it."

    override val tier = 1

    override val dieTypes = arrayOf(Die.Type.PHYSICAL, Die.Type.MENTAL)

    override val traits = listOf(Trait.DAMAGE_FINISH_TURN)

    override val nameLocalizations = mapOf(
            "ru" to "Преграда-перерада"
    )

    override val descriptionLocalizations = mapOf(
            "ru" to "Дешевая локализация"
    )

}