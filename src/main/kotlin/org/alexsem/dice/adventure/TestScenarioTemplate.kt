package org.alexsem.dice.adventure

import org.alexsem.dice.generator.template.adventure.LocationTemplate
import org.alexsem.dice.generator.template.adventure.ScenarioTemplate
import org.alexsem.dice.model.AllySkill.Type.*
import org.alexsem.dice.model.SpecialRule

@Deprecated("Test scenario")
class TestScenarioTemplate : ScenarioTemplate {

    override val name = "Test"
    override val description = "After a long journey we have finally arrived to small city of Elry. Here we are to meet the mayor Joseph Aldwyn who may provide us with jobs and place to live. There are several places he may be at this hour - we have to visit them all. It is dark already so we must hurry up."
    override val intro = "Devastated by the war, our homeland now lies in ruins. There is nowhere for us to return, so in order to survive we have to seek new opportunities. Leeroy Kole, local refugee coordinator, directed us to Greyholm area, to the small city of Elry, where our services may be of need. There we are to get in contact with city mayor Joseph Aldwyn, who should provide us with accommodations and job opportunities. Without second thoughts we set off, planning to reach our destination before the sunset.\nThe journey was hard and troublesome. Twice our bus was attacked by bandits, once we had to stop for repairs and in the end our progress was impeded by anomalic purple thunderstorm, which are said to occasionally happen in the area. No big surprise that we arrived to Elry late in the evening when most of the town is already asleep. Now it becomes way more difficult to find mayor Aldwyn.\nWe should obviously check the town hall (providing mayor is still there). If not, there is a couple of other places he may be at this hour. Central park is the favorite one, where he likes to relax in peace and quiet after yet another working day is over. Alternatively, in case he feels especially frisky, old Joseph is often seen at the 'Sharp Edge' tavern drinking, gambling, singing songs of dubious content, baldly participating in endless contests and having a great time in general.\nWe should move out immediately. There is no time to waste."
    override val outro = "Even though we spent half of the night searching for the mayor, we had no luck finding him. After visiting all the places he could probably be at and talking to dozens of people, the location of Joseph Aldwyn remains a mystery. The hope remains though is that he went on some private business and didn't want to be disturbed (which occasionally happened in the past). Cold comfort this is, but the best we have at the moment.\nThe remainder of our scant savings was spent on cheap meals and shabby rooms in 'The Fierce Horse Inn'. Ugly interior, squeaky beds, giant rats and roaches along with incessant noise from outside (as if someone is being brutally raped and murdered) will accompany us through the rest of the night. As soon as it dawns we'll resume our search, and hopefully this time luck will be on our side."

    override val initialTimer = 25
    override val staticLocations = listOf(
            TestLocationTemplate(),
            TestLocationTemplate2()
    )

    override val dynamicLocationsPool = listOf<LocationTemplate>(

    )

    override val villains = listOf(
            TestVillainTemplate()
    )

    override val allySkills = listOf(
            DARK_ENCHANTER,
            DEFENDER,
            ILLUSIONIST,
            COMBAT_ADVISOR,
            SCRYER,
            TRICKSTER
    )

    override val specialRules = listOf(
            SpecialRule.COVERT_OPERATION
    )

    override val nameLocalizations = mapOf(
            "ru" to "Test"
    )
    override val descriptionLocalizations = mapOf<String, String>()
    override val introLocalizations = mapOf<String, String>()
    override val outroLocalizations = mapOf<String, String>()

    override fun calculateDynamicLocationsCount(numberOfHeroes: Int) = 0
}