package org.alexsem.dice.adventure

import org.alexsem.dice.generator.template.adventure.ObstacleTemplate
import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Trait

@Deprecated("Test data")
class TestObstacleTemplate2 : ObstacleTemplate {

    override val name = "Unfriendly Host"
    override val description = "Usually owners are nice, well-mannered people, who are pleasant to talk to. Why wouldn't they be, when they serve dozens of customers every day? Sociability and hospitality are an important trait, whether you run a small grocery store, a bar or an underground casino.\nUnfortunately, sometimes the host is complete opposite to what he is expected to be. He frowns, grumbles and hates visitors for the sole reason of them being impudent enough to distract him from more important matters. And what makes things worse is that there is often nothing that can be done."
    override val tier = 2

    override val dieTypes = arrayOf(Die.Type.VERBAL)
    override val traits = listOf(Trait.DAMAGE_TRAVEL, Trait.IMMUNE_AVOID_ENCOUNTER)

    override val nameLocalizations = mapOf(
            "ru" to "Неприветливый Хозяин"
    )
    override val descriptionLocalizations = mapOf(
            "ru" to "Обычно владельцы милые, хорошо воспитанные люди, с которыми приятно общаться. Почему б им такими не быть, если каждый день им приходится обслуживать десятки посетителей. Общительность и гостеприимство очень важные навыки в работе, независимо от того, содержишь ты маленький продуктовый магазин, бар или подпольное казино.\nК сожалению, иногда хозяин - прямая противоположность тому, что от него ожидаешь. Он хмурится, ворчит и ненавидит посетителей лишь за то, что их хватило наглости отвлечь его от других, более важных дел. И что самое неприятное - зачастую с этим совершенно ничего нельзя поделать."
    )

}