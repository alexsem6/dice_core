package org.alexsem.dice.generator

import org.alexsem.dice.generator.template.adventure.EnemyTemplate
import org.alexsem.dice.generator.template.adventure.ObstacleTemplate
import org.alexsem.dice.generator.template.adventure.VillainTemplate
import org.alexsem.dice.model.*

/**
 * Generate [Villain] threat of the specific type
 * @param template Template used for generation
 * @return generated villain
 */
fun generateVillain(template: VillainTemplate) = Villain().apply {
    name = LocalizedString(template.name, template.nameLocalizations)
    description = LocalizedString(template.description, template.descriptionLocalizations)
    template.traits.forEach { addTrait(it) }
}

/**
 * Generate [Enemy] threat of the specific type
 * @param template Template used for generation
 * @return generated enemy
 */
fun generateEnemy(template: EnemyTemplate) = Enemy().apply {
    name = LocalizedString(template.name, template.nameLocalizations)
    description = LocalizedString(template.description, template.descriptionLocalizations)
    template.traits.forEach { addTrait(it) }
}

/**
 * Generate [Obstacle] threat of the specific type
 * @param template Template used for generation
 * @return generated obstacle
 */
fun generateObstacle(template: ObstacleTemplate) = Obstacle(template.tier, *template.dieTypes).apply {
    name = LocalizedString(template.name, template.nameLocalizations)
    description = LocalizedString(template.description, template.descriptionLocalizations)
    template.traits.forEach { addTrait(it) }
}

//----------------------------------------------------------------------------------------------------------------------

/**
 * Generate [Deck] of [Enemy] cards
 * @param types Pool of enemy types
 * @param limit How many cards to leave in the deck
 * @return generated enemy deck
 */
fun generateEnemyDeck(types: Collection<EnemyTemplate>, limit: Int?): Deck<Enemy> {
    val deck = types
            .map { generateEnemy(it) }
            .shuffled()
            .fold(Deck<Enemy>()) { d, c -> d.addToTop(c); d }
    limit?.let {
        while (deck.size > it) deck.drawFromTop()
    }
    return deck
}

/**
 * Generate [Deck] of [Obstacle] cards
 * @param templates Pool of obstacle templates
 * @param limit How many cards to leave in the deck
 * @return generated obstacle deck
 */
fun generateObstacleDeck(templates: Collection<ObstacleTemplate>, limit: Int?): Deck<Obstacle> {
    val deck = templates
            .map { generateObstacle(it) }
            .shuffled()
            .fold(Deck<Obstacle>()) { d, c -> d.addToTop(c); d }
    limit?.let {
        while (deck.size > it) deck.drawFromTop()
    }
    return deck
}