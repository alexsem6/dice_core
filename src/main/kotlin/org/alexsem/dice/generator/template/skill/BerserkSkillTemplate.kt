package org.alexsem.dice.generator.template.skill

import org.alexsem.dice.model.Skill

/**
 * Template for [Skill.Type.BERSERK] skill
 */
class BerserkSkillTemplate : SkillTemplate {

    override val type = Skill.Type.BERSERK
    override val maxLevel = 3
    override val modifier1 = Skill.Modifier(Skill.Modifier.Type.PLUS_LEVEL, 0)
    override val modifier2: Skill.Modifier? = null
}
