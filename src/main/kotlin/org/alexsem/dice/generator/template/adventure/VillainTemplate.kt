package org.alexsem.dice.generator.template.adventure

import org.alexsem.dice.model.Trait

/**
 * Template for various Villains heroes need to defeat
 */
interface VillainTemplate {
    /**
     * Obstacle name
     */
    val name: String
    /**
     * Obstacle description
     */
    val description: String
    /**
     * List of obstacle traits
     */
    val traits: List<Trait>
    /**
     * Various localized name strings (optional)
     */
    val nameLocalizations: Map<String, String>
    /**
     * Various localized description strings (optional)
     */
    val descriptionLocalizations: Map<String, String>
}