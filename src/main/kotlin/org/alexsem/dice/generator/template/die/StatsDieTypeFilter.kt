package org.alexsem.dice.generator.template.die

import org.alexsem.dice.model.Die

/**
 * Implementation of [DieTypeFilter] which includes only types for general hero stat parameters
 */
class StatsDieTypeFilter : DieTypeFilter {

    private val statTypes = setOf(Die.Type.PHYSICAL, Die.Type.SOMATIC, Die.Type.MENTAL, Die.Type.VERBAL)

    override fun test(type: Die.Type) = (type in statTypes)

}