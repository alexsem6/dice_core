package org.alexsem.dice.generator.template.adventure

interface InterludeTemplate {

    /**
     * Type of actor who participates in the event
     */
    enum class Subject(val index: Int) {
        MOOD(-1), HEADLINE(-1), NARRATION(-1), HERO1(0), HERO2(1), HERO3(2), HERO4(3), HERO5(4),
        NPC1(0), NPC2(1), NPC3(2), NPC4(3), NPC5(4), NPC6(5), NPC7(6), NPC8(7), NPC9(8)
    }

    /**
     * Non-playable character template
     */
    interface NPC {
        /**
         * NPC name
         */
        val name: String
        /**
         * Various localized name strings (optional)
         */
        val nameLocalizations: Map<String, String>
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Get interlude name
     */
    val name: String

    /**
     * Get list of NPCs
     */
    val npcs: List<NPC>

    /**
     * Ordered list of events
     */
    val events: List<Pair<Subject, String>>

    /**
     * Various localized name strings (optional)
     */
    val nameLocalizations: Map<String, String>

    /**
     * Various localized name strings (optional)
     */
    val eventsLocalizations: Map<String, List<Pair<Subject, String>>>
}