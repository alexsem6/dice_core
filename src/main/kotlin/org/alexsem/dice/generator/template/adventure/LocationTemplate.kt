package org.alexsem.dice.generator.template.adventure

import org.alexsem.dice.generator.template.bag.BagTemplate
import org.alexsem.dice.model.SpecialRule

/**
 * Template for all available Locations
 */
interface LocationTemplate {
    /**
     * Location name
     */
    val name: String
    /**
     * Location description
     */
    val description: String
    /**
     * Template for location bag
     */
    val bagTemplate: BagTemplate
    /**
     * Basic value of basic location closing difficulty
     */
    val basicClosingDifficulty: Int
    /**
     * Number of cards in the enemy deck
     */
    val enemyCardsCount: Int
    /**
     * Number of cards in the obstacle deck
     */
    val obstacleCardsCount: Int
    /**
     * Pool of enemy cards
     */
    val enemyCardPool: Collection<EnemyTemplate>
    /**
     * Pool of obstacle cards
     */
    val obstacleCardPool: Collection<ObstacleTemplate>
    /**
     * Get special rules for the location
     */
    val specialRules: List<SpecialRule>
    /**
     * Various localized name strings (optional)
     */
    val nameLocalizations: Map<String, String>
    /**
     * Various localized description strings (optional)
     */
    val descriptionLocalizations: Map<String, String>
}