package org.alexsem.dice.generator.template.die

import org.alexsem.dice.model.Die

/**
 * Implementation of [DieTypeFilter] which includes specified types
 */
class MultipleDieTypeFilter(vararg val types: Die.Type): DieTypeFilter {
    override fun test(type: Die.Type) = (type in types)
}