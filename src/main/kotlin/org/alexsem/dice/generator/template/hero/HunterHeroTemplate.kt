package org.alexsem.dice.generator.template.hero

import org.alexsem.dice.generator.template.skill.*
import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Die.Type.*
import org.alexsem.dice.model.Hero

/**
 * Template for [Hero.Type.HUNTER] hero generation
 */
class HunterHeroTemplate : HeroTemplate {

    override val type = Hero.Type.HUNTER
    override val favoredDieType = SOMATIC
    override val initialHandCapacity = 6

    override val initialDice = listOf(
            Die(PHYSICAL, 6),
            Die(PHYSICAL, 4),
            Die(PHYSICAL, 4),
            Die(PHYSICAL, 4),
            Die(PHYSICAL, 4),
            Die(SOMATIC, 6),
            Die(SOMATIC, 6),
            Die(SOMATIC, 4),
            Die(SOMATIC, 4),
            Die(SOMATIC, 4),
            Die(SOMATIC, 4),
            Die(SOMATIC, 4),
            Die(SOMATIC, 4),
            Die(SOMATIC, 4),
            Die(MENTAL, 6),
            Die(MENTAL, 4),
            Die(MENTAL, 4),
            Die(MENTAL, 4),
            Die(MENTAL, 4),
            Die(VERBAL, 4)
    )

    override fun getDiceCount(type: Die.Type) = when (type) {
        PHYSICAL -> 5 to 7
        SOMATIC -> 8 to 13
        MENTAL -> 5 to 8
        VERBAL -> 1 to 2
        else -> null
    }

    override val initialSkills = listOf(
            ShootSkillTemplate(),
            EvasionSkillTemplate(),
            StealthSkillTemplate(),
            ReadTracesSkillTemplate(),
            SurvivalistSkillTemplate()
    )

    override val dormantSkills = listOf(
            VolleySkillTemplate(),
            CoverFireSkillTemplate(),
            LayTrapSkillTemplate(),
            SelfBandageSkillTemplate(),
            OrienteeringSkillTemplate()
    )

}