package org.alexsem.dice.generator.template.die

import org.alexsem.dice.model.Die

/**
 * The simplest implementation of [DieTypeFilter] which represents single die type
 */
class SingleDieTypeFilter(val type: Die.Type) : DieTypeFilter {
    override fun test(type: Die.Type) = (this.type == type)
}