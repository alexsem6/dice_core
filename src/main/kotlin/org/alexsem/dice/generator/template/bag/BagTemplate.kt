package org.alexsem.dice.generator.template.bag

import org.alexsem.dice.generator.template.die.DieTypeFilter

class BagTemplate {

    class Plan(val minQuantity: Int, val maxQuantity: Int, val filter: DieTypeFilter)

    val plans = mutableListOf<Plan>()
    var fixedDieCount: Int? = null

    /**
     * Add new plan to the template
     * @param minQuantity Minimum number of dice of this type
     * @param maxQuantity Maximum number of dice of the type (inclusive)
     * @param filter Possible die types
     */
    fun addPlan(minQuantity: Int, maxQuantity: Int, filter: DieTypeFilter) = plans.add(Plan(minQuantity, maxQuantity, filter))

}