package org.alexsem.dice.generator.template.die

import org.alexsem.dice.model.Die

/**
 * Filters which check die to correspondence to various criteria
 */
@FunctionalInterface
interface DieTypeFilter {

    /**
     * Checks whether die type corresponds to specific check
     * @param type Die type to check
     * @return true if die passes the check, false otherwise
     */
    fun test(type: Die.Type): Boolean

}
