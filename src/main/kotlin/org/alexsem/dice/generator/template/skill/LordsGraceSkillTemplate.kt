package org.alexsem.dice.generator.template.skill

import org.alexsem.dice.model.Skill

/**
 * Template for [Skill.Type.CELESTIAL_OMEN] skill
 */
class LordsGraceSkillTemplate : SkillTemplate {

    override val type = Skill.Type.LORDS_GRACE
    override val maxLevel = 3
    override val modifier1 = Skill.Modifier(Skill.Modifier.Type.PLUS_DOUBLE_LEVEL, +2)
    override val modifier2: Skill.Modifier? = null
}
