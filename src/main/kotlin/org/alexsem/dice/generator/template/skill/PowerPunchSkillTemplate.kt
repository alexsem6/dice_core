package org.alexsem.dice.generator.template.skill

import org.alexsem.dice.model.Skill

/**
 * Template for [Skill.Type.POWER_PUNCH] skill
 */
class PowerPunchSkillTemplate : SkillTemplate {

    override val type = Skill.Type.POWER_PUNCH
    override val maxLevel = 3
    override val modifier1 = Skill.Modifier(Skill.Modifier.Type.PLUS_LEVEL, 0)
    override val modifier2 = Skill.Modifier(Skill.Modifier.Type.PLUS_DOUBLE_LEVEL, +1)
}
