package org.alexsem.dice.generator.template.skill

import org.alexsem.dice.model.Skill

/**
 * Template for [Skill.Type.DODGE] skill
 */
class DodgeSkillTemplate : SkillTemplate {

    override val type = Skill.Type.DODGE
    override val maxLevel = 1
    override val modifier1 = Skill.Modifier(Skill.Modifier.Type.STATIC, 2)
    override val modifier2: Skill.Modifier? = null
}
