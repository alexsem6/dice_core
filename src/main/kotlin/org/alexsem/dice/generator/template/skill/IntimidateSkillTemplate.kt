package org.alexsem.dice.generator.template.skill

import org.alexsem.dice.model.Skill

/**
 * Template for [Skill.Type.INTIMIDATE] skill
 */
class IntimidateSkillTemplate : SkillTemplate {

    override val type = Skill.Type.INTIMIDATE
    override val maxLevel = 3
    override val modifier1 = Skill.Modifier(Skill.Modifier.Type.MINUS_LEVEL, -1)
    override val modifier2: Skill.Modifier? = null
}
