package org.alexsem.dice.generator.template.hero

import org.alexsem.dice.generator.template.skill.*
import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Die.Type.*
import org.alexsem.dice.model.Hero

/**
 * Template for [Hero.Type.PROPHET] hero generation
 */
class ProphetHeroTemplate : HeroTemplate {

    override val type = Hero.Type.PROPHET
    override val favoredDieType = DIVINE
    override val initialHandCapacity = 6

    override val initialDice = listOf(
            Die(PHYSICAL, 4),
            Die(PHYSICAL, 4),
            Die(PHYSICAL, 4),
            Die(MENTAL, 4),
            Die(MENTAL, 4),
            Die(MENTAL, 4),
            Die(MENTAL, 4),
            Die(MENTAL, 4),
            Die(VERBAL, 6),
            Die(VERBAL, 4),
            Die(VERBAL, 4),
            Die(VERBAL, 4),
            Die(VERBAL, 4),
            Die(DIVINE, 6),
            Die(DIVINE, 6),
            Die(DIVINE, 6),
            Die(DIVINE, 4),
            Die(DIVINE, 4),
            Die(DIVINE, 4),
            Die(DIVINE, 4)
    )

    override fun getDiceCount(type: Die.Type) = when (type) {
        PHYSICAL -> 3 to 4
        MENTAL -> 5 to 7
        VERBAL -> 5 to 8
        DIVINE -> 7 to 11
        else -> null
    }

    override val initialSkills = listOf(
            HolyLightSkillTemplate(),
            PrayerSkillTemplate(),
            SermonSkillTemplate(),
            DivineAidSkillTemplate(),
            HealSkillTemplate()
    )

    override val dormantSkills = listOf(
            ForesightSkillTemplate(),
            BlessingSkillTemplate(),
            DisciplineSkillTemplate(),
            LordsGraceSkillTemplate(),
            CelestialOmenSkillTemplate()
    )

}
