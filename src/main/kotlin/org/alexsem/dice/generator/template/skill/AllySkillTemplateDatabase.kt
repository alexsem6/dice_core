package org.alexsem.dice.generator.template.skill

import org.alexsem.dice.model.AllySkill
import org.alexsem.dice.model.AllySkill.Type.*

object AllySkillTemplateDatabase {

    private val ALLY_MODIFIERS = mapOf(
            COMBAT_ADVISOR to arrayOf(-4),
            DARK_ENCHANTER to arrayOf(-4),
            DEFENDER to arrayOf(+0),
            DIPLOMAT to arrayOf(+3, +6),
            FALCON to arrayOf(),
            FIELD_MEDIC to arrayOf(+0),
            HERMIT to arrayOf(-1),
            INTERN to arrayOf(2),
            ILLUSIONIST to arrayOf(+1),
            LOCAL_DWELLER to arrayOf(2),
            MASTERFUL_DISTRACTION to arrayOf(+1),
            PATHFINDER to arrayOf(),
            PHYSICIAN to arrayOf(1, 0),
            PYROMANIAC to arrayOf(+2, +5),
            RETIRED_SOLDIER to arrayOf(+0, +3),
            SABOTEUR to arrayOf(),
            SCRYER to arrayOf(+2),
            TOUR_GUIDE to arrayOf(),
            TRICKSTER to arrayOf()
    )

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Get array of modifiers for specific [AllySkill] type
     * @param type Ally skill type
     * @return Array of modifiers (may be empty)
     */
    fun getAllySkillModifiers(type: AllySkill.Type) = ALLY_MODIFIERS[type]

}