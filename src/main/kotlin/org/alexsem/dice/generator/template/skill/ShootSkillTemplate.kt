package org.alexsem.dice.generator.template.skill

import org.alexsem.dice.model.Skill

/**
 * Template for [Skill.Type.SHOOT] skill
 */
class ShootSkillTemplate : SkillTemplate {

    override val type = Skill.Type.SHOOT
    override val maxLevel = 3
    override val modifier1 = Skill.Modifier(Skill.Modifier.Type.PLUS_LEVEL, -1)
    override val modifier2 = Skill.Modifier(Skill.Modifier.Type.PLUS_LEVEL, +1)
}
