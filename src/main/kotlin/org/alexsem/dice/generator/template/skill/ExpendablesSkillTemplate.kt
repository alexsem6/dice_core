package org.alexsem.dice.generator.template.skill

import org.alexsem.dice.model.Skill

/**
 * Template for [Skill.Type.EXPENDABLES] skill
 */
class ExpendablesSkillTemplate : SkillTemplate {

    override val type = Skill.Type.EXPENDABLES
    override val maxLevel = 3
    override val modifier1 = Skill.Modifier(Skill.Modifier.Type.PLUS_DOUBLE_LEVEL, 2)
    override val modifier2: Skill.Modifier? = null
}
