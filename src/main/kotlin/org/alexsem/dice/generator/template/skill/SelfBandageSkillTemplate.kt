package org.alexsem.dice.generator.template.skill

import org.alexsem.dice.model.Skill

/**
 * Template for [Skill.Type.SELF_BANDAGE] skill
 */
class SelfBandageSkillTemplate : SkillTemplate {

    override val type = Skill.Type.SELF_BANDAGE
    override val maxLevel = 3
    override val modifier1 = Skill.Modifier(Skill.Modifier.Type.PLUS_LEVEL, -1)
    override val modifier2: Skill.Modifier? = null
}
