package org.alexsem.dice.generator.template.skill

import org.alexsem.dice.model.Skill

/**
 * Template for [Skill.Type.DIVINE_AID] skill
 */
class DivineAidSkillTemplate : SkillTemplate {

    override val type = Skill.Type.DIVINE_AID
    override val maxLevel = 1
    override val modifier1: Skill.Modifier? = null
    override val modifier2: Skill.Modifier? = null
    override val isActive
        get() = false
}
