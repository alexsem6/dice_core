package org.alexsem.dice.generator.template.adventure

import org.alexsem.dice.model.ScenarioReward

/**
 * Classes which implement this interface serve as the templates for Adventure generation.
 */
interface AdventureTemplate {

    sealed class Phase {
        class Scenario(val scenario: ScenarioTemplate, val level: Int, val reward: ScenarioReward) : Phase()
        class Interlude(val interlude: InterludeTemplate) : Phase()
        class ToBeContinued(val message: String, val messageLocalizations: Map<String, String>) : Phase()
    }

    /**
     * Adventure name
     */
    val name: String
    /**
     * Adventure description
     */
    val description: String

    /**
     * List of adventure phases
     */
    val phases: List<Phase>

    /**
     * Various localized name strings (optional)
     */
    val nameLocalizations: Map<String, String>
    /**
     * Various localized description strings (optional)
     */
    val descriptionLocalizations: Map<String, String>
}