package org.alexsem.dice.generator.template.hero

import org.alexsem.dice.generator.template.skill.*
import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Die.Type.*
import org.alexsem.dice.model.Hero

/**
 * Template for [Hero.Type.MENTOR] hero generation
 */
class MentorHeroTemplate : HeroTemplate {

    override val type = Hero.Type.MENTOR
    override val favoredDieType = VERBAL
    override val initialHandCapacity = 5

    override val initialDice = listOf(
            Die(MENTAL, 6),
            Die(MENTAL, 4),
            Die(MENTAL, 4),
            Die(MENTAL, 4),
            Die(MENTAL, 4),
            Die(VERBAL, 6),
            Die(VERBAL, 6),
            Die(VERBAL, 4),
            Die(VERBAL, 4),
            Die(VERBAL, 4),
            Die(VERBAL, 4),
            Die(VERBAL, 4),
            Die(VERBAL, 4),
            Die(VERBAL, 4),
            Die(DIVINE, 4),
            Die(DIVINE, 4),
            Die(DIVINE, 4),
            Die(ALLY, 6),
            Die(ALLY, 4),
            Die(ALLY, 4)
    )

    override fun getDiceCount(type: Die.Type) = when (type) {
        MENTAL -> 5 to 8
        VERBAL -> 9 to 12
        DIVINE -> 3 to 4
        ALLY -> 3 to 6
        else -> null
    }

    override val initialSkills = listOf(
            NegotiationSkillTemplate(),
            RecruitmentSkillTemplate(),
            SummonSkillTemplate(),
            InspirerSkillTemplate(),
            PastExperienceSkillTemplate()
    )

    override val dormantSkills = listOf(
            ResuscitationSkillTemplate(),
            GoodAdviceSkillTemplate(),
            ExpendablesSkillTemplate(),
            YouFirstSkillTemplate(),
            ErrandSkillTemplate()
    )

}
