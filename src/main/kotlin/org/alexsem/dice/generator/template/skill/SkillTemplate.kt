package org.alexsem.dice.generator.template.skill

import org.alexsem.dice.model.Skill

interface SkillTemplate {

    /**
     * Type of the skill
     */
    val type: Skill.Type
    /**
     * Maximal possible skill level (usually 3)
     */
    val maxLevel: Int
    /**
     * First skill modifier (if available)
     */
    val modifier1: Skill.Modifier?
    /**
     * Second skill modifier (if available)
     */
    val modifier2: Skill.Modifier?
    /**
     * Defines whether skill is active or passive
     */
    val isActive
        get() = true
}

