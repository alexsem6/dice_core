package org.alexsem.dice.generator.template.skill

import org.alexsem.dice.model.Skill

/**
 * Template for [Skill.Type.PROTECT] skill
 */
class ProtectSkillTemplate : SkillTemplate {

    override val type = Skill.Type.PROTECT
    override val maxLevel = 3
    override val modifier1 = Skill.Modifier(Skill.Modifier.Type.PLUS_LEVEL, -2)
    override val modifier2: Skill.Modifier? = null
}
