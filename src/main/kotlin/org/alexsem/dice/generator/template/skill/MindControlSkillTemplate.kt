package org.alexsem.dice.generator.template.skill

import org.alexsem.dice.model.Skill

/**
 * Template for [Skill.Type.MIND_CONTROL] skill
 */
class MindControlSkillTemplate : SkillTemplate {

    override val type = Skill.Type.MIND_CONTROL
    override val maxLevel = 3
    override val modifier1 = Skill.Modifier(Skill.Modifier.Type.PLUS_LEVEL, 0)
    override val modifier2 = Skill.Modifier(Skill.Modifier.Type.PLUS_LEVEL, 1)
}
