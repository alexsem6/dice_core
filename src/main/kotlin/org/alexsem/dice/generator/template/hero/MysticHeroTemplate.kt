package org.alexsem.dice.generator.template.hero

import org.alexsem.dice.generator.template.skill.*
import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Die.Type.*
import org.alexsem.dice.model.Hero

/**
 * Template for [Hero.Type.MYSTIC] hero generation
 */
class MysticHeroTemplate : HeroTemplate {

    override val type = Hero.Type.MYSTIC
    override val favoredDieType = MENTAL
    override val initialHandCapacity = 7

    override val initialDice = listOf(
            Die(PHYSICAL, 4),
            Die(PHYSICAL, 4),
            Die(PHYSICAL, 4),
            Die(SOMATIC, 4),
            Die(SOMATIC, 4),
            Die(SOMATIC, 4),
            Die(MENTAL, 6),
            Die(MENTAL, 6),
            Die(MENTAL, 6),
            Die(MENTAL, 4),
            Die(MENTAL, 4),
            Die(MENTAL, 4),
            Die(MENTAL, 4),
            Die(MENTAL, 4),
            Die(MENTAL, 4),
            Die(MENTAL, 4),
            Die(VERBAL, 4),
            Die(VERBAL, 4),
            Die(VERBAL, 6),
            Die(VERBAL, 4)
    )

    override fun getDiceCount(type: Die.Type) = when (type) {
        PHYSICAL -> 3 to 4
        SOMATIC -> 4 to 6
        MENTAL -> 9 to 14
        VERBAL -> 4 to 6
        else -> null
    }

    override val initialSkills = listOf(
            MagicBoltSkillTemplate(),
            ElementalShieldSkillTemplate(),
            TeleportSkillTemplate(),
            MindControlSkillTemplate(),
            ConcentrationSkillTemplate()
    )

    override val dormantSkills = listOf(
            WarpSkillTemplate(),
            InvigorateSkillTemplate(),
            IngenuitySkillTemplate(),
            CauterizeSkillTemplate(),
            DevastationSkillTemplate()
    )
}
