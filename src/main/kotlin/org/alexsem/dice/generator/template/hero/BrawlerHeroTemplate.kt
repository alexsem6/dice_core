package org.alexsem.dice.generator.template.hero

import org.alexsem.dice.generator.template.skill.*
import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Die.Type.*
import org.alexsem.dice.model.Hero

/**
 * Template for [Hero.Type.BRAWLER] hero generation
 */
class BrawlerHeroTemplate : HeroTemplate {
    
    override val type = Hero.Type.BRAWLER
    override val favoredDieType = PHYSICAL
    override val initialHandCapacity = 5

    override val initialDice = listOf(
            Die(PHYSICAL, 6),
            Die(PHYSICAL, 6),
            Die(PHYSICAL, 6),
            Die(PHYSICAL, 4),
            Die(PHYSICAL, 4),
            Die(PHYSICAL, 4),
            Die(PHYSICAL, 4),
            Die(PHYSICAL, 4),
            Die(PHYSICAL, 4),
            Die(SOMATIC, 6),
            Die(SOMATIC, 4),
            Die(SOMATIC, 4),
            Die(SOMATIC, 4),
            Die(SOMATIC, 4),
            Die(SOMATIC, 4),
            Die(MENTAL, 4),
            Die(MENTAL, 4),
            Die(VERBAL, 4),
            Die(VERBAL, 4),
            Die(VERBAL, 4)
    )

    override fun getDiceCount(type: Die.Type) = when (type) {
        PHYSICAL -> 9 to 14
        SOMATIC -> 6 to 8
        MENTAL -> 2 to 3
        VERBAL -> 3 to 5
        else -> null
    }

    override val initialSkills = listOf(
            HitSkillTemplate(),
            DodgeSkillTemplate(),
            IntimidateSkillTemplate(),
            ProtectSkillTemplate(),
            SubmitSkillTemplate()
    )

    override val dormantSkills = listOf(
            PowerPunchSkillTemplate(),
            StoneSkinSkillTemplate(),
            WarcrySkillTemplate(),
            BerserkSkillTemplate(),
            InterveneSkillTemplate()
    )

}