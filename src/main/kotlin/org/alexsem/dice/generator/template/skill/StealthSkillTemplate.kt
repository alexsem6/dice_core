package org.alexsem.dice.generator.template.skill

import org.alexsem.dice.model.Skill

/**
 * Template for [Skill.Type.STEALTH] skill
 */
class StealthSkillTemplate : SkillTemplate {

    override val type = Skill.Type.STEALTH
    override val maxLevel = 3
    override val modifier1 = Skill.Modifier(Skill.Modifier.Type.PLUS_DOUBLE_LEVEL, 1)
    override val modifier2: Skill.Modifier? = null
}
