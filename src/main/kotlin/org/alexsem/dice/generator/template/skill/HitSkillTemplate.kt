package org.alexsem.dice.generator.template.skill

import org.alexsem.dice.model.Skill

/**
 * Template for [Skill.Type.HIT] skill
 */
class HitSkillTemplate : SkillTemplate {

    override val type = Skill.Type.HIT
    override val maxLevel = 3
    override val modifier1 = Skill.Modifier(Skill.Modifier.Type.PLUS_LEVEL, -1)
    override val modifier2 = Skill.Modifier(Skill.Modifier.Type.PLUS_LEVEL, +1)
}
