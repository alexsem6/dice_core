package org.alexsem.dice.generator.template.skill

import org.alexsem.dice.model.Skill

/**
 * Template for [Skill.Type.READ_TRACES] skill
 */
class ReadTracesSkillTemplate : SkillTemplate {

    override val type = Skill.Type.READ_TRACES
    override val maxLevel = 1
    override val modifier1: Skill.Modifier? = null
    override val modifier2: Skill.Modifier? = null
}
