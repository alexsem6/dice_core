package org.alexsem.dice.generator.template.adventure

import org.alexsem.dice.model.AllySkill
import org.alexsem.dice.model.Scenario
import org.alexsem.dice.model.SpecialRule

/**
 * Template for various [Scenario] generation
 */
interface ScenarioTemplate {
    /**
     * Get scenario name
     */
    val name: String
    /**
     * Get scenario description
     */
    val description: String
    /**
     * Get scenario intro
     */
    val intro: String
    /**
     * Get scenario outro
     */
    val outro: String
    /**
     * Starting timer value
     */
    val initialTimer: Int
    /**
     * List of static location templates for the scenario (may be empty).
     * All of these will be added in exact order to the beginning of location list.
     * Villains will not be dealt to these.
     */
    val staticLocations: List<LocationTemplate>
    /**
     * List of location templates for the scenario.
     * Some number of these will be added in random order to the end of location list.
     * Villains will be dealt to these.
     */
    val dynamicLocationsPool: List<LocationTemplate>
    /**
     * Villains for this specific scenario
     */
    val villains: List<VillainTemplate>
    /**
     * List of ally skills available for the scenario
     */
    val allySkills: List<AllySkill.Type>
    /**
     * List of special rules for the scenario
     */
    val specialRules: List<SpecialRule>

    /**
     * Various localized name strings (optional)
     */
    val nameLocalizations: Map<String, String>

    /**
     * Various localized description strings (optional)
     */
    val descriptionLocalizations: Map<String, String>

    /**
     * Various localized intro strings (optional)
     */
    val introLocalizations: Map<String, String>

    /**
     * Various localized outro strings (optional)
     */
    val outroLocalizations: Map<String, String>

    /**
     * Get the number of dynamic (random) locations
     * @param numberOfHeroes Number of heroes in the party
     * @return Number of locations for the scenario
     */
    fun calculateDynamicLocationsCount(numberOfHeroes: Int) = numberOfHeroes + 2
}
