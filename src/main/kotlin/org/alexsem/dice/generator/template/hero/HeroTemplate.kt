package org.alexsem.dice.generator.template.hero

import org.alexsem.dice.generator.template.skill.SkillTemplate
import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Hero

/**
 * Superclass for [Hero] generation templates
 */
interface HeroTemplate {
    /**
     * Type of the hero
     */
    val type: Hero.Type
    /**
     * Number of dice hero initially starts his hand with
     */
    val initialHandCapacity: Int
    /**
     * Favored die type (at least 1 die of this type will always be present in starting hand)
     */
    val favoredDieType: Die.Type
    /**
     * Initial dice available to hero
     */
    val initialDice: Collection<Die>

    /**
     * Initial skills available to hero
     */
    val initialSkills: List<SkillTemplate>
    /**
     * Skills that hero can learn later
     */
    val dormantSkills: List<SkillTemplate>

    /**
     * Get initial and maximal possible number of dice of each type
     * @param type Die type
     * @return initial and maximal number of dice for this type
     */
    fun getDiceCount(type: Die.Type): Pair<Int, Int>?


}