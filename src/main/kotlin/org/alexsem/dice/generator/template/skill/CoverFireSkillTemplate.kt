package org.alexsem.dice.generator.template.skill

import org.alexsem.dice.model.Skill

/**
 * Template for [Skill.Type.COVER_FIRE] skill
 */
class CoverFireSkillTemplate : SkillTemplate {

    override val type = Skill.Type.COVER_FIRE
    override val maxLevel = 3
    override val modifier1 = Skill.Modifier(Skill.Modifier.Type.PLUS_LEVEL, 0)
    override val modifier2: Skill.Modifier? = null
}
