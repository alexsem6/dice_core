package org.alexsem.dice.generator.template.adventure

import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Trait

/**
 * Template for various obstacles appearing on heroes' way
 */
interface ObstacleTemplate {
    /**
     * Obstacle name
     */
    val name: String
    /**
     * Obstacle description
     */
    val description: String
    /**
     * Obstacle difficulty (1 - 3)
     */
    val tier: Int
    /**
     * Die types that should be used to defeat the obstacle
     */
    val dieTypes: Array<Die.Type>
    /**
     * List of obstacle traits
     */
    val traits: List<Trait>
    /**
     * Various localized name strings (optional)
     */
    val nameLocalizations: Map<String, String>
    /**
     * Various localized description strings (optional)
     */
    val descriptionLocalizations: Map<String, String>
}