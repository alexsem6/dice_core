package org.alexsem.dice.generator

import org.alexsem.dice.generator.template.adventure.AdventureTemplate
import org.alexsem.dice.generator.template.adventure.InterludeTemplate
import org.alexsem.dice.generator.template.adventure.InterludeTemplate.Subject.*
import org.alexsem.dice.model.Hero
import org.alexsem.dice.model.Interlude
import org.alexsem.dice.model.LocalizedString


/**
 * Generate localized name of the Adventure from [AdventureTemplate]
 * @param template Template to use
 * @return Generated name
 */
fun generateAdventureName(template: AdventureTemplate) = LocalizedString(template.name, template.nameLocalizations)

/**
 * Generate localized description of the Adventure from [AdventureTemplate]
 * @param template Template to use
 * @return Generated description
 */
fun generateAdventureDescription(template: AdventureTemplate) = LocalizedString(template.description, template.descriptionLocalizations)

/**
 * Generate list of names for phases for the specific Adventure
 * @param template Template to use
 * @return LIst of localized phase names
 */
fun generateAdventurePhaseNames(template: AdventureTemplate) = template.phases.map { p ->
    when (p) {
        is AdventureTemplate.Phase.Scenario -> LocalizedString(p.scenario.name, p.scenario.nameLocalizations)
        is AdventureTemplate.Phase.Interlude -> LocalizedString(p.interlude.name, p.interlude.nameLocalizations)
        is AdventureTemplate.Phase.ToBeContinued -> LocalizedString(p.message, p.messageLocalizations)
    }
}

/**
 * Extract localized phrase text from [InterludeTemplate]
 * @param template Template to extract from
 * @param index Phrase index
 */
private fun getPhraseText(template: InterludeTemplate, index: Int) =
        LocalizedString(template.events[index].second, template.eventsLocalizations.mapValues { it.value[index].second })

/**
 * Generate [Interlude] from the specified [InterludeTemplate]
 * @param template Template for generation
 * @param heroes List of heroes in current party
 * @return Generated interlude
 */
fun generateInterlude(template: InterludeTemplate, heroes: List<Hero>) = Interlude().apply {
    name = LocalizedString(template.name, template.nameLocalizations)
    val npcActors = template.npcs.mapIndexed { i, n ->
        Interlude.Actor.NPC(i, LocalizedString(n.name, n.nameLocalizations))
    }
    template.events.mapIndexed { i, p ->
        when (p.first) {
            MOOD -> Interlude.Event.Tune(Interlude.Mood.valueOf(p.second.toUpperCase()))
            HEADLINE -> Interlude.Event.Headline(getPhraseText(template, i))
            NARRATION -> Interlude.Event.Narration(getPhraseText(template, i))
            HERO1, HERO2, HERO3, HERO4, HERO5 -> Interlude.Event.Phrase(Interlude.Actor.Hero(heroes[p.first.index % heroes.size]), getPhraseText(template, i))
            NPC1, NPC2, NPC3, NPC4, NPC5, NPC6, NPC7, NPC8, NPC9 -> Interlude.Event.Phrase(npcActors[p.first.index % npcActors.size], getPhraseText(template, i))
        }
    }.forEach { addEvent(it) }
}