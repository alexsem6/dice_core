package org.alexsem.dice.generator

import org.alexsem.dice.generator.template.hero.*
import org.alexsem.dice.model.DiceLimit
import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Hero
import org.alexsem.dice.model.Hero.Type.*

/**
 * Generate [Hero] of the specified [Hero.Type]
 * @param type Hero type
 * @param name Hero name (optional)
 * @return Generated hero
 */
fun generateHero(type: Hero.Type, name: String = ""): Hero {
    val template = when (type) {
        BRAWLER -> BrawlerHeroTemplate()
        HUNTER -> HunterHeroTemplate()
        MYSTIC -> MysticHeroTemplate()
        PROPHET -> ProphetHeroTemplate()
        MENTOR -> MentorHeroTemplate()
    }
    return Hero(type).apply {
        this.name = name
        isAlive = true
        favoredDieType = template.favoredDieType
        hand.capacity = template.initialHandCapacity
        template.initialDice.forEach { bag.put(it) }

        for ((t, l) in Die.Type.values().map { it to template.getDiceCount(it) }) {
            l?.let { addDiceLimit(DiceLimit(t, it.first, it.second, it.first)) }
        }
        template.initialSkills.map { generateSkill(it) }.forEach { addSkill(it) }
        template.dormantSkills.map { generateSkill(it, 0) }.forEach { addDormantSkill(it) }
    }
}

/**
 * Re-create hero from template with the same progress (needed when rules change during update)
 * @param hero Hero to copy
 * @return Regenerated hero
 */
fun regenerateHero(hero: Hero) = generateHero(hero.type, hero.name).apply {
    isAlive = hero.isAlive
    hand.capacity = hero.hand.capacity
    hand.clear()
    (0 until hero.hand.dieCount).mapNotNull { hero.hand.dieAt(it) }.forEach { hand.addDie(it) }
    (0 until hero.hand.allyDieCount).mapNotNull { hero.hand.allyDieAt(it) }.forEach { hand.addDie(it) }
    bag.clear()
    hero.bag.examine().forEach { bag.put(it) }
    discardPile.clear()
    hero.discardPile.examine().forEach { discardPile.put(it) }
    hero.getDiceLimits().forEach { l ->
        (l.initial until l.current).forEach { _ -> increaseDiceLimit(l.type) }
    }
    hero.getSkills().forEach { s ->
        ((if (hasSkill(s.type)) 1 else 0) until s.level).forEach { _ -> improveSkill(s.type) }
    }
}
