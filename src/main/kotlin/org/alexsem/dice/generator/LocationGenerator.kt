package org.alexsem.dice.generator

import org.alexsem.dice.generator.template.adventure.LocationTemplate
import org.alexsem.dice.model.LocalizedString
import org.alexsem.dice.model.Location

private val closingDifficultyRule: (Int, Int) -> Int =
        { basic, level -> basic + level * 2 }

/**
 * Generate [Location] from the [LocationTemplate]
 * @param template Template to use
 * @param level Scenario level
 * @return generated location
 */
fun generateLocation(template: LocationTemplate, level: Int) = Location().apply {
    name = LocalizedString(template.name, template.nameLocalizations)
    description = LocalizedString(template.description, template.descriptionLocalizations)
    bag = generateBag(template.bagTemplate, level)
    closingDifficulty = closingDifficultyRule(template.basicClosingDifficulty, level)
    enemies = generateEnemyDeck(template.enemyCardPool, if (template.enemyCardsCount > 0) template.enemyCardsCount else template.enemyCardPool.size)
    obstacles = generateObstacleDeck(template.obstacleCardPool, if (template.obstacleCardsCount > 0) template.obstacleCardsCount else template.obstacleCardPool.size)
    template.specialRules.forEach { addSpecialRule(it) }
}