package org.alexsem.dice.generator

import org.alexsem.dice.generator.template.skill.AllySkillTemplateDatabase
import org.alexsem.dice.generator.template.skill.SkillTemplate
import org.alexsem.dice.model.AllySkill
import org.alexsem.dice.model.Skill

/**
 * Generate hero [Skill] using [SkillTemplate]
 * @param template Template to use
 * @param initialLevel Skill level to set initially
 * @return Generated skill
 */
fun generateSkill(template: SkillTemplate, initialLevel: Int = 1): Skill {
    val skill = Skill(template.type)
    skill.isActive = template.isActive
    skill.level = initialLevel
    skill.maxLevel = template.maxLevel
    skill.modifier1 = template.modifier1
    skill.modifier2 = template.modifier2
    return skill
}

/**
 * Generate [AllySkill] of the specific type
 * @param type Skill type
 * @return generated skill
 */
fun generateAllySkill(type: AllySkill.Type): AllySkill {
    val skill = AllySkill(type)
    with(AllySkillTemplateDatabase.getAllySkillModifiers(type)!!) {
        if (this.isNotEmpty()) {
            skill.modifier1 = this[0]
        }
        if (this.size > 1) {
            skill.modifier2 = this[1]
        }
    }
    return skill
}