package org.alexsem.dice.generator

import org.alexsem.dice.generator.template.bag.BagTemplate
import org.alexsem.dice.model.Bag
import org.alexsem.dice.model.Die

/**
 * Generates array of [Die] instances based on the [BagTemplate.Plan]
 * @param plan  Plan to implement
 * @param level Scenario level
 * @return Array of dice
 */
private fun realizePlan(plan: BagTemplate.Plan, level: Int): Array<Die> {
    val count = (plan.minQuantity..plan.maxQuantity).shuffled().last()
    return (1..count).map { generateDie(plan.filter, level) }.toTypedArray()
}

/**
 * Generate [Bag] with dice
 * @param template Template to use
 * @param level Scenario level (affects die sizes)
 */
fun generateBag(template: BagTemplate, level: Int): Bag {
    val bag = template.plans.asSequence()
            .map { realizePlan(it, level) }
            .fold(Bag()) { b, d -> b.put(*d); b }
    template.fixedDieCount?.let { count ->
        if (bag.size >= count) { //Trim bag
            (1..bag.size - count).forEach { _ -> bag.draw() }
        } else { //Grow bag
            (1..count - bag.size).forEach { _ -> bag.put(generateDie(template.plans.random().filter, level)) }
        }
    }
    return bag
}


