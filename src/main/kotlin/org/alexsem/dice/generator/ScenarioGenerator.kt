package org.alexsem.dice.generator

import org.alexsem.dice.generator.template.die.SingleDieTypeFilter
import org.alexsem.dice.generator.template.adventure.ScenarioTemplate
import org.alexsem.dice.model.*

private fun Location.villainPresent() = this.getSpecialRules().none { it == SpecialRule.NOWHERE_TO_HIDE }

/**
 * Generate [Scenario] out of [ScenarioTemplate]
 * @param template ScenarioTemplate to use
 * @param level Required scenario level
 * @return generated scenario
 */
fun generateScenario(template: ScenarioTemplate, level: Int) = Scenario().apply {
    name = LocalizedString(template.name, template.nameLocalizations)
    description = LocalizedString(template.description, template.descriptionLocalizations)
    intro = LocalizedString(template.intro, template.introLocalizations)
    outro = LocalizedString(template.outro, template.outroLocalizations)
    this.level = level
    initialTimer = template.initialTimer
    template.allySkills.map { generateAllySkill(it) }.forEach { addAllySkill(it) }
    template.specialRules.forEach { addSpecialRule(it) }
}

/**
 * Generate list of [Location]s for specific [Scenario]
 * @param template Scenario template
 * @param level Required scenario level
 * @param numberOfHeroes Number of heroes in the party
 * @return list of locations
 */
fun generateLocations(template: ScenarioTemplate, level: Int, numberOfHeroes: Int): List<Location> {
    val locations = template.staticLocations.map { generateLocation(it, level) } +
            template.dynamicLocationsPool
                    .map { generateLocation(it, level) }
                    .shuffled()
                    .take(template.calculateDynamicLocationsCount(numberOfHeroes))
    val villains = template.villains
            .map(::generateVillain)
            .shuffled()
    val acceptable = locations.filter(Location::villainPresent)
    if (template.specialRules.contains(SpecialRule.ONE_SHOULD_SUFFICE)) {
       acceptable.random().let { location->
           location.villain = villains[0]
           location.bag.put(generateDie(SingleDieTypeFilter(Die.Type.VILLAIN), level))
       }
    } else {
        acceptable.forEachIndexed { index, location ->
            if (index < villains.size) {
                location.villain = villains[index]
                location.bag.put(generateDie(SingleDieTypeFilter(Die.Type.VILLAIN), level))
            }
        }
    }
    return locations
}