package org.alexsem.dice.generator

import org.alexsem.dice.generator.template.die.DieTypeFilter
import org.alexsem.dice.model.Die

private val DISTRIBUTION_LEVEL1 = intArrayOf(4, 4, 4, 4, 6, 6, 6, 6, 8)
private val DISTRIBUTION_LEVEL2 = intArrayOf(4, 6, 6, 6, 6, 8, 8, 8, 8, 10)
private val DISTRIBUTION_LEVEL3 = intArrayOf(6, 8, 8, 8, 10, 10, 10, 10, 12, 12, 12)
private val DISTRIBUTIONS = arrayOf(
        intArrayOf(4),
        DISTRIBUTION_LEVEL1,
        DISTRIBUTION_LEVEL2,
        DISTRIBUTION_LEVEL3
)

/**
 * Get currently maximal [Die] level
 * @return Max level
 */
fun getMaxLevel() = DISTRIBUTIONS.size - 1

/**
 * Generate random [Die] based on filter and level
 * @param filter Allowed die types
 * @param level Die level
 * @return Random die
 */
fun generateDie(filter: DieTypeFilter, level: Int) = Die(generateDieType(filter), generateDieSize(level))

/**
 * Generate random [Die.Type]
 * @param filter Allowed die types
 * @return Random type
 */
private fun generateDieType(filter: DieTypeFilter): Die.Type {
    var type: Die.Type
    do {
        type = Die.Type.values().random()
    } while (!filter.test(type))
    return type
}

/**
 * Generate random [Die] size from specific level
 * @param level Die level
 * @return Some random die size
 */
private fun generateDieSize(level: Int) =
        DISTRIBUTIONS[if (level < 1 || level > getMaxLevel()) 0 else level].random()

