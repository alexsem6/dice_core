package org.alexsem.dice.ui

/**
 * Actions are used to pass input between user and app
 */

class Action(
        val type: Type,
        var isEnabled: Boolean = true,
        val data: Int = 0
) {

    enum class Type {
        NONE, //Blank type
        CONFIRM, //Confirm some action
        CANCEL, //Cancel action
        MENU, //Bring up the menu
        EXIT, //Exit something
        LIST_ITEM, //Some position in custom list

        HAND_POSITION, //Some position in hand
        HAND_ALLY_POSITION, //Some ally position in hand
        PILE_POSITION, //Some position in pile
        EXPLORE_LOCATION, //Explore current location
        EXPLORE_LOCATION_AGAIN, //Explore current location again
        CLOSE_LOCATION, //Close current location
        USE_SKILL, //Use current hero's skill
        ASSIST, //Get assistance from other heroes
        TRAVEL, //Travel to another location
        GIVE_DIE, //Give die to another hero
        FLEE, //Flee the encounter
        INFO, //Show various information screens
        INFO_GENERAL, //Show general info page
        INFO_SPECIAL_RULES, //Show list of special rules (scenario or location)
        INFO_PAGE_UP, //Go to previous page
        INFO_PAGE_DOWN, //Go to next page
        INFO_ENEMY, //Show info about encountered enemy
        INFO_VILLAIN, //Show info about encountered villain
        INFO_OBSTACLE, //Show info about encountered obstacle
        INFO_MODIFIERS, //Show info about current hero pending modifiers
        INFO_SCENARIO, //Show info about current scenario
        INFO_SCENARIO_ALLIES, //Show info about available ally skills
        INFO_LOCATION, //Show info about location
        INFO_HERO, //Show information about hero
        INFO_HERO_DICE, //Show hero dice limits
        INFO_HERO_SKILLS, //Show hero skills
        FINISH_TURN, //Finish current turn
        ACQUIRE, //Acquire (DIVINE) die
        ENDURE, //Submissively take wound or damage
        LEAVE, //Return (ALLY) die to location bag
        FORFEIT, //Remove die from game
        HIDE, //Put die into bag
        DISCARD, //Put die to discard pile
        DETER, //Put die to deterrent pile

        PHYSICAL, //Pick PHYSICAL die type
        SOMATIC, //Pick SOMATIC die type
        MENTAL, //Pick MENTAL die type
        VERBAL, //Pick VERBAL die type
        DIVINE, //Pick DIVINE die type
        ALLY, //Pick ALLY die type
        NEXT_HERO, //Navigate to next hero
        PREVIOUS_HERO, //Navigate to previous hero
        FINISH, //Finish whatever is currently happening

        ADD_HERO, //Add new hero to party
        DELETE_HERO, //Remove hero from party
        RENAME_HERO, //Change hero's name
        RENAME_PARTY, //Change current party name

        RETRY, //Retry whatever was attempted
        DELETE_PARTY, //Delete party from storage
        NEXT_ADVENTURE, //Show next adventure
        PREVIOUS_ADVENTURE, //Show previous adventure

        MANAGE_DICE, //Open dice manager

        NEW_ADVENTURE, //Start new adventure
        CONTINUE_ADVENTURE, //Load existing adventure
        MANUAL, //Read manual
        PLAY_INTRO, //Show scenario intro text
        PLAY_OUTRO, //Show scenario outro text
    }

}