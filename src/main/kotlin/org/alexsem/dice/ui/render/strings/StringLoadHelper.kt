package org.alexsem.dice.ui.render.strings

import org.alexsem.dice.ui.Action
import org.alexsem.dice.ui.StatusMessage
import org.alexsem.dice.model.*

/**
 * Helper class for loading specific types of strings
 */
class StringLoadHelper(private val loader: StringLoader) {

    /**
     * Load key (if present) and name of specific [Action]
     * @param action Action to load
     * @return array of loaded strings
     */
    fun loadActionData(action: Action): Array<String> {
        val key = load("action_${action.type}_key")
        val name = load("action_${action.type}_name")
        return if (key.isNotEmpty()) arrayOf(key, name) else arrayOf(name)
    }

    /**
     * Transform [StatusMessage] object to String representation
     * @param message Message to resolve
     * @return respective String
     */
    fun loadStatusMessage(message: StatusMessage) = when (message) {
        StatusMessage.EMPTY -> ""
        else -> with(load(message)) {
            message.data?.let { String.format(this, it) } ?: this
        }
    }

    /**
     * Load localized name of specific [Die] type
     * @param type Type to load
     * @return die type name
     */
    fun loadDieType(type: Die.Type) = load(type)

    /**
     * Load localized name of specific [Skill]
     * @param skill Skill to load
     * @return skill name
     */
    fun loadSkillName(skill: Skill) = load("skill_${skill.type}_name")

    /**
     * Load localized name of specific [Skill]
     * @param type Type of skill to load
     * @return skill name
     */
    fun loadSkillName(type: Skill.Type) = load("skill_${type}_name")

    /**
     * Load localized description of specific [Skill]
     * @param skill Skill to load
     * @param level Level to load description for
     * @return skill description
     */
    fun loadSkillDescription(skill: Skill, level: Int) = with(load("skill_${skill.type}_description")) {
        val size = (if (skill.modifier1 != null) 1 else 0) + if (skill.modifier2 != null) 1 else 0
        val values = arrayOfNulls<Any>(size)
        skill.modifier1?.let { values[0] = it.resolve(level) }
        skill.modifier2?.let { values[1] = it.resolve(level) }
        String.format(this, *values).noZeros()
    }

    /**
     * Load localized name of specific [AllySkill]
     * @param skill Ally skill to load
     * @return skill name
     */
    fun loadAllySkillName(skill: AllySkill) = load("ally_${skill.type}_name")

    /**
     * Load localized name of specific [AllySkill]
     * @param type Type of ally skill to load
     * @return skill name
     */
    fun loadAllySkillName(type: AllySkill.Type) = load("ally_${type}_name")

    /**
     * Load localized description of specific [AllySkill]
     * @param skill Ally skill to load
     * @return skill description
     */
    fun loadAllySkillDescription(skill: AllySkill): String {
        val format = load("ally_${skill.type}_description")
        return String.format(format, skill.modifier1, skill.modifier2).noZeros()
    }

    /**
     * Load localized name of specific [SpecialRule]
     * @param rule Special rule to load
     * @return rule name
     */
    fun loadSpecialRuleName(rule: SpecialRule) = load("rule_${rule}_name")

    /**
     * Load localized description of specific [SpecialRule]
     * @param rule Special rule to load
     * @return rule description
     */
    fun loadSpecialRuleDescription(rule: SpecialRule) = load("rule_${rule}_description")

    /**
     * Load localized class name for the specific [Hero]
     * @param type Class to load name for
     * @return hero class name
     */
    fun loadHeroClassName(type: Hero.Type) = load(type)

    /**
     * Load localized class description for the specific [Hero]
     * @param type Class to load description for
     * @return hero class description
     */
    fun loadHeroClassDescription(type: Hero.Type) = load("${type}_description")

    /**
     * Load localized class description for the specific [Trait]
     * @param trait Rule to load description for
     * @param cardName Name of the encountered threat (enemy, villain etc.)
     * @return trait description
     */
    fun loadTraitDescription(trait: Trait, cardName: String) = String.format(load("trait_$trait"), cardName)

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Helper function for resource loading
     */
    private fun load(key: String) = loader.loadString(key.toLowerCase().replace(' ', '_'))

    /**
     * Helper function for resource loading
     */
    private fun load(key: Any) = load(key.toString())

    /**
     * Remove all zeroes in description string
     */
    private fun String.noZeros() = replace("\\+0".toRegex(), "")

}
