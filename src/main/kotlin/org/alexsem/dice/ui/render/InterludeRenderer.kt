package org.alexsem.dice.ui.render

import org.alexsem.dice.model.Interlude

interface InterludeRenderer: Renderer {

    /**
     * Draw one [Interlude.Event.Headline] of the [Interlude]
     * @param interlude Interlude in question
     * @param headline Headline to render
     */
    fun drawHeadline(interlude: Interlude, headline: Interlude.Event.Headline)

    /**
     * Draw one [Interlude.Event.Narration] of the [Interlude]
     * @param interlude Interlude in question
     * @param text Narration to render
     */
    fun drawNarration(interlude: Interlude, text: Interlude.Event.Narration)

    /**
     * Draw single [Interlude.Event.Phrase] of the [Interlude]
     * @param interlude Interlude in question
     * @param phrase Phrase to render
     */
    fun drawPhrase(interlude: Interlude, phrase: Interlude.Event.Phrase)
}