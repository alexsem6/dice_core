package org.alexsem.dice.ui.render.strings

/**
 * Implementations of this interface load strings from respective resources (localizing, if needed)
 */
interface StringLoader {

    /**
     * Transforms some key to exact string
     * @param key Key to load String for
     * @return respective String resource
     */
    fun loadString(key: String): String

}
