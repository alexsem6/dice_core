package org.alexsem.dice.ui.render

import org.alexsem.dice.game.*
import org.alexsem.dice.ui.ActionList
import org.alexsem.dice.model.*
import org.alexsem.dice.ui.StatusMessage

/**
 * Interface which defines the way current game state is displayed
 */
interface GameRenderer: Renderer {

    /**
     * Draw the screen for specified hero turn start
     * @param hero Current hero
     */
    fun drawHeroTurnStart(hero: Hero)

    /**
     * Draw the screen for specified hero death
     * @param hero Deceased hero
     */
    fun drawHeroDeath(hero: Hero)

    /**
     * Draw location interior screen
     * @param location Location to draw
     * @param heroesAtLocation List of heroes at current location
     * @param timer Current timer value
     * @param currentHero Current hero
     * @param battleCheck Current battle check (if in progress) or null
     * @param encounteredDie Current exploration die (if available) or null
     * @param encounteredThreat Currently encountered enemy, villain or obstacle object (if available) or null
     * @param pickedDice Positions in hand which were picked
     * @param activePositions Positions in hand that are active
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawLocationInteriorScreen(location: Location, heroesAtLocation: List<Hero>, timer: Int, currentHero: Hero, battleCheck: DieBattleCheck?, encounteredDie: DiePair?, encounteredThreat: Threat?, pickedDice: HandMask, activePositions: HandMask, statusMessage: StatusMessage, actions: ActionList)

    /**
     * Draw travel animation
     * @param departureLocation Location which hero left behind
     * @param destinationLocation Location hero travels towards
     * @param hero Current hero
     * @param progress Travel progress (percentage)
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawLocationTravelScreen(departureLocation: Location, destinationLocation: Location, hero: Hero, progress: Int, statusMessage: StatusMessage, actions: ActionList)

    /**
     * Draw selection dialog for a list of heroes
     * @param heroes List of heroes to pick from
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawHeroSelectionDialog(heroes: List<Hero>, statusMessage: StatusMessage, actions: ActionList)

    /**
     * Draw selection dialog for a list of heroes
     * @param skills List of skills to pick from
     * @param allySkills List of ally skills to pick from
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawSkillSelectionDialog(skills: List<Skill>, allySkills: List<AllySkill>, statusMessage: StatusMessage, actions: ActionList)

    /**
     * Draw location exterior will all available locations
     * @param scenario Current scenario
     * @param timer Current timer value
     * @param locations List of locations
     * @param heroPlacement Mapping between heroes and locations
     * @param currentHero Current hero
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawLocationSelectionDialog(scenario: Scenario, timer: Int, locations: List<Location>, heroPlacement: Map<Location, List<Hero>>, currentHero: Hero, statusMessage: StatusMessage, actions: ActionList)

    /**
     * Draw contents of some pile for picking dice from it
     * @param pile Pile to draw
     * @param pickedDice Positions in pile which were picked
     * @param activePositions Positions in pile that are active
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawPileDiceSelectionDialog(pile: Pile, pickedDice: PileMask, activePositions: PileMask, statusMessage: StatusMessage, actions: ActionList)

    /**
     * Draw general info about current scenario
     * @param scenario Current scenario
     * @param deterrentPile Dice being deterred
     * @param page Page number
     * @param totalPages Total number of pages
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawScenarioInfoGeneral(scenario: Scenario, deterrentPile: Pile, page: Int, totalPages: Int, statusMessage: StatusMessage, actions: ActionList)

    /**
     * Get the number of pages (screens) needed to render scenario description
     * @param scenario Current scenario
     * @return Number of pages needed to display general page
     */
    fun calculateScenarioInfoGeneralPageCount(scenario: Scenario): Int

    /**
     * Draw info about current scenario special rules
     * @param scenario Current scenario
     * @param page Page number
     * @param totalPages Total number of pages
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawScenarioInfoRules(scenario: Scenario, page: Int, totalPages: Int, statusMessage: StatusMessage, actions: ActionList)

    /**
     * Get the number of pages (screens) needed to render the full list of special rules
     * @param scenario Current scenario
     * @return Number of pages needed to display special rules
     */
    fun calculateScenarioInfoRulesPageCount(scenario: Scenario): Int

    /**
     * Get the number of pages (screens) needed to render the full list of ally skills
     * @param scenario Current scenario
     * @return Number of pages needed to display ally skills
     */
    fun calculateScenarioInfoAlliesPageCount(scenario: Scenario): Int

    /**
     * Draw info about current scenario ally skills
     * @param scenario Current scenario
     * @param page Page number
     * @param totalPages Total number of pages
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawScenarioInfoAllies(scenario: Scenario, page: Int, totalPages: Int, statusMessage: StatusMessage, actions: ActionList)

    /**
     * Get the number of pages (screens) needed to render location description
     * @param location Location to show info about
     * @return Number of pages needed to display general page
     */
    fun calculateLocationInfoGeneralPageCount(location: Location): Int

    /**
     * Draw general info about specified location
     * @param location Location to show info about
     * @param totalPages Total number of pages
     * @param statusMessage Status message
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawLocationInfoGeneral(location: Location, page: Int, totalPages: Int, statusMessage: StatusMessage, actions: ActionList)

    /**
     * Draw info about specified location special rules
     * @param location Location to show info about
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawLocationInfoRules(location: Location, page: Int, totalPages: Int, statusMessage: StatusMessage, actions: ActionList)

    /**
     * Get the number of pages (screens) needed to render the full list of special rules
     * @param location Current location
     * @return Number of pages needed to display special rules
     */
    fun calculateLocationInfoRulesPageCount(location: Location): Int

    /**
     * Draw general info about selected hero
     * @param hero Hero to show
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawHeroInfoGeneral(hero: Hero, statusMessage: StatusMessage, actions: ActionList)

    /**
     * Draw info about selected hero dice limitations
     * @param hero Hero to show
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawHeroInfoDice(hero: Hero, statusMessage: StatusMessage, actions: ActionList)

    /**
     * Draw info about selected hero skills
     * @param hero Hero to show
     * @param page Page number
     * @param totalPages Total number of pages
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawHeroInfoSkills(hero: Hero, page: Int, totalPages: Int, statusMessage: StatusMessage, actions: ActionList)

    /**
     * Get the number of pages (screens) needed to render the full list of hero skills
     * @param hero Hero to calculate skills for
     * @return Number of pages needed to display skills
     */
    fun calculateHeroInfoSkillsPageCount(hero: Hero): Int

    /**
     * Draw info about encountered enemy
     * @param enemy Enemy to show
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawEnemyInfo(enemy: Enemy, statusMessage: StatusMessage, actions: ActionList)

    /**
     * Draw info about encountered obstacle
     * @param obstacle Obstacle to show
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawObstacleInfo(obstacle: Obstacle, statusMessage: StatusMessage, actions: ActionList)

    /**
     * Draw info about encountered villain
     * @param villain Villain to show
     * @param page Page number
     * @param totalPages Total number of pages
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawVillainInfo(villain: Villain, page: Int, totalPages: Int, statusMessage: StatusMessage, actions: ActionList)

    /**
     * Get the number of pages (screens) needed to render villain description
     * @param villain Villain to draw
     * @return Number of pages needed to display info
     */
    fun calculateVillainInfoPageCount(villain: Villain): Int

    /**
     * Draw info about pending modifiers
     * @param hero Current hero
     * @param skillModifiers Modifiers from [Skill]s
     * @param allyModifiers Modifiers from [AllySkill]s
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawModifiersInfo(hero: Hero, skillModifiers: List<PendingSkillModifier>, allyModifiers: List<PendingAllyModifier>, statusMessage: StatusMessage, actions: ActionList)

    /**
     * Draw victory screen
     * @param message Victory message
     */
    fun drawGameVictory(message: StatusMessage)

    /**
     * Draw loss screen
     * @param message Loss message
     */
    fun drawGameLoss(message: StatusMessage)

    /**
     * Draw pause menu
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawGamePauseMenu(statusMessage: StatusMessage, actions: ActionList)

    /**
     * Draw scenario text (intro/outro) on several pages (if needed)
     * @param scenario Scenario in question
     * @param isOutro true for outro, false for intro
     * @param page Page number
     * @param totalPages Total number of pages
     * @param actions List of actions
     */
    fun drawScenarioScenicText(scenario: Scenario, isOutro: Boolean, page: Int, totalPages: Int, actions: ActionList)

    /**
     * Calculate number of pages needed to display scenario text
     * @param scenario Scenario in question
     * @param isOutro true for outro, false for intro
     * @return required number of pages
     */
    fun calculateScenarioScenicTextPageCount(scenario: Scenario, isOutro: Boolean): Int
}
