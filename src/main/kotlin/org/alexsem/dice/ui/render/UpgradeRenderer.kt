package org.alexsem.dice.ui.render

import org.alexsem.dice.ui.ActionList
import org.alexsem.dice.ui.StatusMessage
import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Hero
import org.alexsem.dice.model.Skill

/**
 * Interface which defines the way various upgrade screens are displayed
 */
interface UpgradeRenderer: Renderer {

    /**
     * Draw info about selected hero dice limitations
     * @param hero Hero to show
     * @param plusOne The type which is currently increased by 1 (or null, if none)
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawHeroDiceLimits(hero: Hero, plusOne: Die.Type?, statusMessage: StatusMessage, actions: ActionList)

    /**
     * Draw list of selected hero skills
     * @param hero Hero to show
     * @param skills List of skills
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawHeroSkills(hero: Hero, skills: List<Skill>, statusMessage: StatusMessage, actions: ActionList)

    /**
     * Show name and description of the specific skill
     * @param skill Skill to show
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawSkillInfo(skill: Skill, statusMessage: StatusMessage, actions: ActionList)

    /**
     * Show message about increase in hero hand capacity
     * @param hero Hero to show
     * @param statusMessage Status message
     */
    fun drawHeroHandCapacityIncrease(hero: Hero, statusMessage: StatusMessage)

    /**
     * Draw dialog with die types
     * @param hero Hero to draw dialog for
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawDieTypeSelectionDialog(hero: Hero, statusMessage: StatusMessage, actions: ActionList)

    /**
     * Draw message about hero receiving new die
     * @param hero Hero who received the die
     * @param die Die which was received
     */
    fun drawNewDieReceived(hero: Hero, die: Die)
}
