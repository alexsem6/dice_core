package org.alexsem.dice.ui.render

import org.alexsem.dice.model.Hero
import org.alexsem.dice.ui.ActionList
import org.alexsem.dice.ui.StatusMessage
import org.alexsem.dice.ui.storage.PartyInfoShort
import org.alexsem.dice.model.LocalizedString

/**
 * Interface which defines how various menus are displayed
 */
interface MenuRenderer: Renderer {

    /**
     * Number of parties per page in Party selector
     */
    val partiesPerPage: Int

    /**
     * Number of scenarios per page in Scenario selector
     */
    val scenariosPerPage: Int

    /**
     * Draw list of stored parties
     * @param parties Parties to draw
     * @param pickedIndex Selected party index (may be null)
     * @param page Page number
     * @param totalPages Total number of pages
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawPartyList(parties: List<PartyInfoShort>, pickedIndex: Int?, page: Int, totalPages: Int, statusMessage: StatusMessage, actions: ActionList)

    /**
     * Draw list of adventures (with one being currently selected)
     * @param names List of adventure names
     * @param descriptions List of adventure descriptions
     * @param selectedIndex Number of currently selected adventure
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawAdventureList(names: List<LocalizedString>, descriptions: List<LocalizedString>, selectedIndex: Int, statusMessage: StatusMessage, actions: ActionList)

    /**
     * Draw list of adventure phases
     * @param adventureName Name of Adventure
     * @param phaseNames List of adventure phase names
     * @param progress Number of completed scenarios
     * @param heroes List of heroes
     * @param pickedIndex Scenario index to highlight (may be null)
     * @param page Page number
     * @param totalPages Total number of pages
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawScenarioList(adventureName: LocalizedString, phaseNames: List<LocalizedString>, progress: Int, heroes: List<Hero>, pickedIndex: Int?, page: Int, totalPages: Int, statusMessage: StatusMessage, actions: ActionList)

    /**
     * Draw staring screen
     * @param actions List of actions
     */
    fun drawMainMenu(actions: ActionList)

}