package org.alexsem.dice.ui.render

import org.alexsem.dice.ui.ActionList
import org.alexsem.dice.ui.StatusMessage
import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Hero

/**
 * Interface which defines the way various upgrade screens are displayed
 */
interface ManagerRenderer: Renderer {

    /**
     * Maximum number of common pool dice displayed on each page
     */
    val commonPoolDicePerPage: Int

    /**
     * Draw one page of the dice manager
     * @param hero Current hero
     * @param heroDice Map of current hero dice
     * @param pickedType Type of die which was picked (if was)
     * @param commonPool Common die pool
     * @param page Current page of the common pool
     * @param totalPages Total number of pages of the common pool
     * @param activePositions Number of positions which can be selected
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawDiceManagerScreen(hero: Hero, heroDice: Map<Die.Type, List<Die>>, pickedType: Die.Type?, commonPool: List<Die>, page: Int, totalPages: Int, activePositions: Set<Int>, statusMessage: StatusMessage, actions: ActionList)

    /**
     * Draw general page of party manager
     * @param partyName Name of current party
     * @param heroes LIst of heroes added so far
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawPartyManagerGeneral(partyName: String, heroes: List<Hero>, statusMessage: StatusMessage, actions: ActionList)

    /**
     * Draw text edit field
     * @param currentText Currently entered text
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawPartyNameEditScreen(currentText: String, statusMessage: StatusMessage, actions: ActionList)

    /**
     * Draw dialog for [Hero] class selection
     * @param partyName Name of current party
     * @param heroes List of heroes
     * @param classes List of available hero classes
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawHeroClassSelectionDialog(partyName: String, heroes: List<Hero>, classes: List<Hero.Type>, statusMessage: StatusMessage, actions: ActionList)

    /**
     * Draw [Hero] name edit field
     * @param partyName Name of current party
     * @param heroes List of heroes
     * @param heroIndex Index of hero whose name is being edited
     * @param heroType Class of hero whose name is being edited
     * @param currentText Currently entered text
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawHeroNameEditScreen(partyName: String, heroes: List<Hero>, heroIndex:Int, heroType: Hero.Type, currentText: String, statusMessage: StatusMessage, actions: ActionList)

    /**
     * Draw general page of party manager
     * @param partyName Name of current party
     * @param heroes List of heroes added so far
     * @param statusMessage Status message
     * @param actions List of actions
     */
    fun drawPartyManagerHeroSelection(partyName: String, heroes: List<Hero>, statusMessage: StatusMessage, actions: ActionList)

    /**
     * Maximal length of party name
     */
    val maxPartyNameLength: Int

    /**
     * Maximal length of hero name
     */
    val maxHeroNameLength: Int

}
