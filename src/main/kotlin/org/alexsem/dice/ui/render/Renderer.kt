package org.alexsem.dice.ui.render

/**
 * Common superinterface for all renderers
 */
interface Renderer {
    /**
     * Clear screen
     */
    fun clearScreen()
}