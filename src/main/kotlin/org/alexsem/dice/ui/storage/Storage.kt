package org.alexsem.dice.ui.storage

import org.alexsem.dice.generator.template.adventure.AdventureTemplate
import org.alexsem.dice.model.Hero
import java.io.IOException
import java.util.*

interface Storage {

    /**
     * Function to generate identifier for party name
     * @param name Party name
     * @return Unique identifier
     */
    fun generatePartyIdentifier(name: String) = name.hashCode().toString() + Date().time.toString()

    /**
     * Crate and save new party to storage
     * @param name Party name
     * @param heroes List of heroes
     * @param adventure Template of adventure for which party is created
     * @return Generated party
     * @throws IOException in case writing fails
     */
    @Throws(IOException::class)
    fun createParty(name: String, heroes: List<Hero>, adventure: AdventureTemplate): PartyInfoFull

    /**
     * Update existing (and saved) party
     * @param info Full party info
     * @throws IOException in case writing fails
     */
    @Throws(IOException::class)
    fun updateParty(info: PartyInfoFull)

    /**
     * Remove existing party
     * @param id Party identifier
     * @throws IOException in case writing fails
     */
    @Throws(IOException::class)
    fun deleteParty(id:String)

    /**
     * Load saved party
     * @param id Party identifier
     * @return Full party info
     * @throws IOException in case reading fails
     */
    @Throws(IOException::class)
    fun loadParty(id: String): PartyInfoFull

    /**
     * Load list of saved parties
     * @return List of short party data
     * @throws IOException in case reading fails
     */
    @Throws(IOException::class)
    fun loadPartyList(): List<PartyInfoShort>

    /**
     * Checks whether there are any stored parties
     * @return true or false
     */
    fun isPartiesAvailable(): Boolean

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Saves game data
     * @param info Game data to save
     * @throws IOException in case writing fails
     */
    @Throws(IOException::class)
    fun saveGame(info: GameInfo)

    /**
     * Loads previously saved game data
     * @param id Party identifier
     * @return Saved game data
     * @throws IOException in case reading fails
     */
    @Throws(IOException::class)
    fun loadGame(id: String): GameInfo

    /**
     * Deletes stored game data
     * @param id Party identifier
     * @throws IOException in case deleting fails
     */
    @Throws(IOException::class)
    fun deleteGame(id: String)

    /**
     * Checks whether there is game data stored for specific party
     * @param id Party identifier
     * @return true or false
     */
    fun isGameSaveAvailable(id: String): Boolean
}