package org.alexsem.dice.ui.storage

import org.alexsem.dice.game.*
import org.alexsem.dice.model.*
import org.alexsem.dice.ui.ActionList
import org.alexsem.dice.ui.StatusMessage
import org.alexsem.dice.ui.audio.Music

class GameInfo(val partyId: String) {

    //Mandatory data
    var phaseIndex: Int = -1
    lateinit var scenario: Scenario
    lateinit var locations: List<Location>
    lateinit var heroes: List<Hero>

    //Current game state
    var timer = 0
    var currentHeroIndex = -1
    var currentLocationIndex = -1
    var deterrentPile = Pile()
    val heroPlacement = mutableMapOf<Int, List<Int>>()

    //Turn details
    var traveledThisTurn = false
    var gaveDieThisTurn = false
    var exploredThisTurn = false
    var exploredAgainThisTurn = false
    var attemptedCloseThisTurn = false
    var encounteredDie: DiePair? = null
    var encounteredThreat: Threat? = null
    lateinit var pendingHeroModifiers1: List<PendingSkillModifier>
    lateinit var pendingHeroModifiers2: List<PendingAllyModifier>
    lateinit var pendingEncounterModifiers: List<PendingSkillModifier>

    //Current game phase
    var phase: GamePhase = GamePhase.SCENARIO_START
    var currentMusic: Music? = null

    //Display details
    var screen = GameScreen.SCENARIO_INTRO
    var statusMessage = StatusMessage.EMPTY
    var actions: ActionList = ActionList.EMPTY

}