package org.alexsem.dice.ui.storage

import org.alexsem.dice.game.*
import org.alexsem.dice.generator.regenerateHero
import org.alexsem.dice.generator.template.adventure.AdventureTemplate
import org.alexsem.dice.model.*
import org.alexsem.dice.ui.Action
import org.alexsem.dice.ui.ActionList
import org.alexsem.dice.ui.StatusMessage
import org.alexsem.dice.ui.audio.Music
import java.io.*
import java.util.*
import kotlin.math.min

/**
 * Write party info to the stream
 * @param stream Stream to write to
 * @param info Data to write
 * @throws IOException in case writing fails
 */
@Throws(IOException::class)
fun writePartyInfoShort(stream: OutputStream, info: PartyInfoShort) {
    val out = DataOutputStream(BufferedOutputStream(stream))
    out.writeUTF(info.id)
    out.writeLong(info.timestamp.time)
    out.writeUTF(info.partyName)
    val heroCount = min(info.heroTypes.size, info.heroNames.size)
    out.writeInt(heroCount)
    (0 until heroCount).forEach { out.writeUTF(info.heroNames[it]) }
    (0 until heroCount).forEach { out.writeUTF(info.heroTypes[it].toString()) }
    out.writeUTF(info.adventure.javaClass.name)
    out.flush()
}

/**
 * Read party info from the stream
 * @param stream Stream to read from
 * @return Parsed data
 * @throws IOException in case reading or parsing fails
 */
@Throws(IOException::class)
fun readPartyInfoShort(stream: InputStream): PartyInfoShort {
    val inp = DataInputStream(stream)
    try {
        val info = PartyInfoShort(inp.readUTF())
        info.timestamp = Date(inp.readLong())
        info.partyName = inp.readUTF()
        val heroCount = inp.readInt()
        info.heroNames = (0 until heroCount).map { inp.readUTF() }
        info.heroTypes = (0 until heroCount).map { Hero.Type.valueOf(inp.readUTF()) }
        val adventureTemplate = Class.forName(inp.readUTF())
        info.adventure = adventureTemplate.newInstance() as AdventureTemplate
        return info
    } catch (ex: IOException) {
        throw ex
    } catch (ex: Exception) {
        throw IOException(ex)
    }
}

//----------------------------------------------------------------------------------------------------------------------

/**
 * Write party info to the stream
 * @param stream Stream to write to
 * @param info Data to write
 * @throws IOException in case writing fails
 */
@Throws(IOException::class)
fun writePartyInfoFull(stream: OutputStream, info: PartyInfoFull) {
    val out = DataOutputStream(BufferedOutputStream(stream))
    out.writeUTF(info.id)
    out.writeUTF(info.partyName)
    out.writeInt(info.heroes.size)
    info.heroes.forEach { writeHeroInfo(out, it) }
    out.writeUTF(info.adventure.javaClass.name)
    out.writeInt(info.progress)
    out.flush()
}

/**
 * Read party info from the stream
 * @param stream Stream to read from
 * @return Parsed data
 * @throws IOException in case reading or parsing fails
 */
@Throws(IOException::class)
fun readPartyInfoFull(stream: InputStream): PartyInfoFull {
    val inp = DataInputStream(stream)
    try {
        val info = PartyInfoFull()
        info.id = inp.readUTF()
        info.partyName = inp.readUTF()
        info.heroes = (0 until inp.readInt()).map { readHeroInfo(inp) }
        val adventureTemplate = Class.forName(inp.readUTF())
        info.adventure = adventureTemplate.newInstance() as AdventureTemplate
        info.progress = inp.readInt()
        return info
    } catch (ex: IOException) {
        throw ex
    } catch (ex: Exception) {
        throw IOException(ex)
    }
}

//----------------------------------------------------------------------------------------------------------------------

/**
 * Write game info to the stream
 * @param stream Stream to write to
 * @param info Data to write
 * @throws IOException in case writing fails
 */
@Throws(IOException::class)
fun writeGameInfo(stream: OutputStream, info: GameInfo) {
    val out = DataOutputStream(BufferedOutputStream(stream))
    out.writeUTF(info.partyId)
    out.writeInt(info.phaseIndex)
    writeScenarioInfo(out, info.scenario)
    out.writeInt(info.locations.size)
    info.locations.forEach { writeLocationInfo(out, it) }
    out.writeInt(info.heroes.size)
    info.heroes.forEach { writeHeroInfo(out, it) }

    out.writeInt(info.timer)
    out.writeInt(info.currentHeroIndex)
    out.writeInt(info.currentLocationIndex)
    out.writeInt(info.deterrentPile.size)
    info.deterrentPile.examine().forEach { writeDieInfo(out, it) }
    out.writeInt(info.heroPlacement.size)
    info.heroPlacement.forEach { k, v ->
        out.writeInt(k)
        out.writeInt(v.size)
        v.forEach { out.writeInt(it) }
    }

    out.writeBoolean(info.traveledThisTurn)
    out.writeBoolean(info.gaveDieThisTurn)
    out.writeBoolean(info.exploredThisTurn)
    out.writeBoolean(info.exploredAgainThisTurn)
    out.writeBoolean(info.attemptedCloseThisTurn)
    out.writeBoolean(info.encounteredDie != null)
    info.encounteredDie?.let { pair ->
        writeDieInfo(out, pair.die)
        out.writeInt(pair.modifier)
    }
    out.writeBoolean(info.encounteredThreat != null)
    info.encounteredThreat?.let {
        out.writeUTF(it.javaClass.name)
        when (it) {
            is Enemy -> writeEnemyInfo(out, it)
            is Villain -> writeVillainInfo(out, it)
            is Obstacle -> writeObstacleInfo(out, it)
        }
    }
    out.writeInt(info.pendingHeroModifiers1.size)
    info.pendingHeroModifiers1.forEach {
        out.writeUTF(it.type.toString())
        out.writeInt(it.value)
    }
    out.writeInt(info.pendingHeroModifiers2.size)
    info.pendingHeroModifiers2.forEach {
        out.writeUTF(it.type.toString())
        out.writeInt(it.value)
    }
    out.writeInt(info.pendingEncounterModifiers.size)
    info.pendingEncounterModifiers.forEach {
        out.writeUTF(it.type.toString())
        out.writeInt(it.value)
    }

    out.writeUTF(info.phase.toString())
    out.writeBoolean(info.currentMusic != null)
    info.currentMusic?.let { out.writeUTF(it.toString()) }

    out.writeUTF(info.screen.toString())
    out.writeUTF(info.statusMessage.toString())
    out.writeBoolean(info.statusMessage.data != null)
    info.statusMessage.data?.let { out.writeInt(it) }
    out.writeInt(info.actions.size)
    info.actions.forEach {
        out.writeUTF(it.type.toString())
        out.writeBoolean(it.isEnabled)
        out.writeInt(it.data)
    }
    out.flush()
}

/**
 * Read game info from the stream
 * @param stream Stream to read from
 * @return Parsed data
 * @throws IOException in case reading or parsing fails
 */
@Throws(IOException::class)
fun readGameInfo(stream: InputStream): GameInfo {
    val inp = DataInputStream(stream)
    try {
        val info = GameInfo(inp.readUTF())
        info.phaseIndex = inp.readInt()
        info.scenario = readScenarioInfo(inp)
        info.locations = (0 until inp.readInt()).map { readLocationInfo(inp) }
        info.heroes = (0 until inp.readInt()).map { readHeroInfo(inp) }

        info.timer = inp.readInt()
        info.currentHeroIndex = inp.readInt()
        info.currentLocationIndex = inp.readInt()
        info.deterrentPile = (0 until inp.readInt()).map { readDieInfo(inp) }.fold(Pile()) { p, d -> p.put(d); p }

        (0 until inp.readInt()).map {
            val key = inp.readInt()
            val value = (0 until inp.readInt()).map { inp.readInt() }.toList()
            key to value
        }.forEach {
            info.heroPlacement[it.first] = it.second
        }

        info.traveledThisTurn = inp.readBoolean()
        info.gaveDieThisTurn = inp.readBoolean()
        info.exploredThisTurn = inp.readBoolean()
        info.exploredAgainThisTurn = inp.readBoolean()
        info.attemptedCloseThisTurn = inp.readBoolean()
        if (inp.readBoolean()) {
            info.encounteredDie = DiePair(readDieInfo(inp), inp.readInt())
        }
        if (inp.readBoolean()) {
            val threat = Class.forName(inp.readUTF())
            info.encounteredThreat = when (threat) {
                Enemy::class.java -> readEnemyInfo(inp)
                Villain::class.java -> readVillainInfo(inp)
                Obstacle::class.java -> readObstacleInfo(inp)
                else -> throw IOException("Unknown threat class")
            }
        }
        info.pendingHeroModifiers1 = (0 until inp.readInt()).map { PendingSkillModifier(Skill.Type.valueOf(inp.readUTF()), inp.readInt()) }
        info.pendingHeroModifiers2 = (0 until inp.readInt()).map { PendingAllyModifier(AllySkill.Type.valueOf(inp.readUTF()), inp.readInt()) }
        info.pendingEncounterModifiers = (0 until inp.readInt()).map { PendingSkillModifier(Skill.Type.valueOf(inp.readUTF()), inp.readInt()) }

        info.phase = GamePhase.valueOf(inp.readUTF())
        if (inp.readBoolean()) {
            info.currentMusic = Music.valueOf(inp.readUTF())
        }

        info.screen = GameScreen.valueOf(inp.readUTF())
        info.statusMessage = StatusMessage.valueOf(inp.readUTF())
        if (inp.readBoolean()) {
            info.statusMessage.data = inp.readInt()
        }
        info.actions = (0 until inp.readInt())
                .map { Action(Action.Type.valueOf(inp.readUTF()), inp.readBoolean(), inp.readInt()) }
                .fold(ActionList()) { l, a -> l.add(a) }

        return info
    } catch (ex: IOException) {
        throw ex
    } catch (ex: Exception) {
        throw IOException(ex)
    }
}

//----------------------------------------------------------------------------------------------------------------------

/**
 * Write scenario data to stream
 * @param out Stream to write to
 * @param scenario Scenario to write
 * @throws IOException in case writing fails
 */
@Throws(IOException::class)
private fun writeScenarioInfo(out: DataOutputStream, scenario: Scenario) {
    writeLocalizedString(out, scenario.name)
    writeLocalizedString(out, scenario.description)
    writeLocalizedString(out, scenario.intro)
    writeLocalizedString(out, scenario.outro)

    out.writeInt(scenario.level)
    out.writeInt(scenario.initialTimer)

    out.writeInt(scenario.getAllySkills().size)
    scenario.getAllySkills().forEach {
        out.writeUTF(it.type.toString())
        out.writeInt(it.modifier1)
        out.writeInt(it.modifier2)
    }
    out.writeInt(scenario.getSpecialRules().size)
    scenario.getSpecialRules().forEach { out.writeUTF(it.toString()) }
}

/**
 * Read scenario info from the stream
 * @param stream Stream to read from
 * @return Parsed data
 * @throws IOException in case reading or parsing fails
 */
@Throws(IOException::class)
private fun readScenarioInfo(stream: InputStream): Scenario {
    val inp = DataInputStream(stream)
    try {
        val scenario = Scenario()
        scenario.name = readLocalizedString(inp)
        scenario.description = readLocalizedString(inp)
        scenario.intro = readLocalizedString(inp)
        scenario.outro = readLocalizedString(inp)

        scenario.level = inp.readInt()
        scenario.initialTimer = inp.readInt()

        (0 until inp.readInt()).map {
            AllySkill(AllySkill.Type.valueOf(inp.readUTF())).apply {
                modifier1 = inp.readInt()
                modifier2 = inp.readInt()
            }
        }.forEach { scenario.addAllySkill(it) }
        (0 until inp.readInt()).map { SpecialRule.valueOf(inp.readUTF()) }.forEach { scenario.addSpecialRule(it) }

        return scenario
    } catch (ex: IOException) {
        throw ex
    } catch (ex: Exception) {
        throw IOException(ex)
    }
}
//----------------------------------------------------------------------------------------------------------------------

/**
 * Write location data to stream
 * @param out Stream to write to
 * @param location Location to write
 * @throws IOException in case writing fails
 */
@Throws(IOException::class)
private fun writeLocationInfo(out: DataOutputStream, location: Location) {
    writeLocalizedString(out, location.name)
    writeLocalizedString(out, location.description)

    out.writeBoolean(location.isOpen)
    out.writeInt(location.closingDifficulty)
    out.writeInt(location.bag.size)
    location.bag.examine().forEach { writeDieInfo(out, it) }

    out.writeBoolean(location.villain != null)
    location.villain?.let { writeVillainInfo(out, it) }
    out.writeInt(location.enemies.size)
    location.enemies.examine().forEach { writeEnemyInfo(out, it) }
    out.writeInt(location.obstacles.size)
    location.obstacles.examine().forEach { writeObstacleInfo(out, it) }

    out.writeInt(location.getSpecialRules().size)
    location.getSpecialRules().forEach { out.writeUTF(it.toString()) }
}

/**
 * Read location info from the stream
 * @param stream Stream to read from
 * @return Parsed data
 * @throws IOException in case reading or parsing fails
 */
@Throws(IOException::class)
private fun readLocationInfo(stream: InputStream): Location {
    val inp = DataInputStream(stream)
    try {
        val location = Location()
        location.name = readLocalizedString(inp)
        location.description = readLocalizedString(inp)

        location.isOpen = inp.readBoolean()
        location.closingDifficulty = inp.readInt()
        location.bag = (0 until inp.readInt()).map { readDieInfo(inp) }.fold(Bag()) { b, d -> b.put(d); b }

        if (inp.readBoolean()) {
            location.villain = readVillainInfo(inp)
        }
        location.enemies = Deck()
        (0 until inp.readInt()).map { readEnemyInfo(inp) }.forEach { location.enemies.addToTop(it) }
        location.enemies.shuffle()
        location.obstacles = Deck()
        (0 until inp.readInt()).map { readObstacleInfo(inp) }.forEach { location.obstacles.addToTop(it) }
        location.obstacles.shuffle()

        (0 until inp.readInt()).map { SpecialRule.valueOf(inp.readUTF()) }.forEach { location.addSpecialRule(it) }

        return location
    } catch (ex: IOException) {
        throw ex
    } catch (ex: Exception) {
        throw IOException(ex)
    }
}

//----------------------------------------------------------------------------------------------------------------------

/**
 * Write hero data to stream
 * @param out Stream to write to
 * @param hero Hero to write
 * @throws IOException in case writing fails
 */
@Throws(IOException::class)
private fun writeHeroInfo(out: DataOutputStream, hero: Hero) {
    out.writeUTF(hero.type.toString())
    out.writeUTF(hero.name)
    out.writeBoolean(hero.isAlive)
    out.writeUTF(hero.favoredDieType.toString())
    out.writeInt(hero.hand.capacity)
    out.writeInt(hero.hand.dieCount + hero.hand.allyDieCount)
    hero.hand.examine().forEach { writeDieInfo(out, it) }
    out.writeInt(hero.bag.size)
    hero.bag.examine().forEach { writeDieInfo(out, it) }
    out.writeInt(hero.discardPile.size)
    hero.discardPile.examine().forEach { writeDieInfo(out, it) }
    out.writeInt(hero.getDiceLimits().size)
    hero.getDiceLimits().forEach { l ->
        out.writeUTF(l.type.toString())
        out.writeInt(l.initial)
        out.writeInt(l.maximal)
        out.writeInt(l.current)
    }
    out.writeInt(hero.getSkills().size)
    hero.getSkills().forEach { writeSkillInfo(out, it) }
    out.writeInt(hero.getDormantSkills().size)
    hero.getDormantSkills().forEach { writeSkillInfo(out, it) }
}

/**
 * Read hero data from the stream
 * @param inp Stream to read from
 * @return Parsed data
 * @throws Exception in case reading or parsing fails
 */
@Throws(Exception::class)
private fun readHeroInfo(inp: DataInputStream): Hero {
    val hero = Hero(Hero.Type.valueOf(inp.readUTF()))
    hero.name = inp.readUTF()
    hero.isAlive = inp.readBoolean()
    hero.favoredDieType = Die.Type.valueOf(inp.readUTF())
    hero.hand.capacity = inp.readInt()
    (0 until inp.readInt()).map { readDieInfo(inp) }.forEach { hero.hand.addDie(it) }
    (0 until inp.readInt()).map { readDieInfo(inp) }.forEach { hero.bag.put(it) }
    (0 until inp.readInt()).map { readDieInfo(inp) }.forEach { hero.discardPile.put(it) }
    (0 until inp.readInt()).map {
        DiceLimit(Die.Type.valueOf(inp.readUTF()), inp.readInt(), inp.readInt(), inp.readInt())
    }.forEach { hero.addDiceLimit(it) }
    (0 until inp.readInt()).map { readSkillInfo(inp) }.forEach { hero.addSkill(it) }
    (0 until inp.readInt()).map { readSkillInfo(inp) }.forEach { hero.addDormantSkill(it) }
    return regenerateHero(hero)
}

//----------------------------------------------------------------------------------------------------------------------

/**
 * Write die data to stream
 * @param out Stream to write to
 * @param die Die to write
 * @throws IOException in case writing fails
 */
@Throws(IOException::class)
private fun writeDieInfo(out: DataOutputStream, die: Die) {
    out.writeUTF(die.type.toString())
    out.writeInt(die.size)
}

/**
 * Read die data from the stream
 * @param inp Stream to read from
 * @return Parsed data
 * @throws Exception in case reading or parsing fails
 */
@Throws(Exception::class)
private fun readDieInfo(inp: DataInputStream) =
        Die(Die.Type.valueOf(inp.readUTF()), inp.readInt())

//----------------------------------------------------------------------------------------------------------------------

/**
 * Write skill data to stream
 * @param out Stream to write to
 * @param skill Skill to write
 * @throws IOException in case writing fails
 */
@Throws(IOException::class)
private fun writeSkillInfo(out: DataOutputStream, skill: Skill) {
    out.writeUTF(skill.type.toString())
    out.writeInt(skill.level)
    out.writeInt(skill.maxLevel)
    out.writeBoolean(skill.isActive)
    fun writeModifier(modifier: Skill.Modifier?) {
        out.writeBoolean(modifier != null)
        modifier?.let {
            out.writeUTF(it.type.toString())
            out.writeInt(it.value)
        }
    }
    writeModifier(skill.modifier1)
    writeModifier(skill.modifier2)
}

/**
 * Read skill data from the stream
 * @param inp Stream to read from
 * @return Parsed data
 * @throws Exception in case reading or parsing fails
 */
@Throws(Exception::class)
private fun readSkillInfo(inp: DataInputStream): Skill {
    val skill = Skill(Skill.Type.valueOf(inp.readUTF()))
    skill.level = inp.readInt()
    skill.maxLevel = inp.readInt()
    skill.isActive = inp.readBoolean()
    fun readModifier() = when (inp.readBoolean()) {
        true -> Skill.Modifier(Skill.Modifier.Type.valueOf(inp.readUTF()), inp.readInt())
        false -> null
    }
    skill.modifier1 = readModifier()
    skill.modifier2 = readModifier()
    return skill
}

//----------------------------------------------------------------------------------------------------------------------

/**
 * Write enemy data to stream
 * @param out Stream to write to
 * @param enemy Enemy to write
 * @throws IOException in case writing fails
 */
@Throws(IOException::class)
private fun writeEnemyInfo(out: DataOutputStream, enemy: Enemy) {
    writeLocalizedString(out, enemy.name)
    writeLocalizedString(out, enemy.description)
    out.writeInt(enemy.getTraits().size)
    enemy.getTraits().forEach { out.writeUTF(it.toString()) }
}

/**
 * Read enemy data from the stream
 * @param inp Stream to read from
 * @return Parsed data
 * @throws Exception in case reading or parsing fails
 */
@Throws(Exception::class)
private fun readEnemyInfo(inp: DataInputStream): Enemy {
    val enemy = Enemy()
    enemy.name = readLocalizedString(inp)
    enemy.description = readLocalizedString(inp)
    (0 until inp.readInt()).map { Trait.valueOf(inp.readUTF()) }.forEach { enemy.addTrait(it) }
    return enemy
}

/**
 * Write villain data to stream
 * @param out Stream to write to
 * @param villain Villain to write
 * @throws IOException in case writing fails
 */
@Throws(IOException::class)
private fun writeVillainInfo(out: DataOutputStream, villain: Villain) {
    writeLocalizedString(out, villain.name)
    writeLocalizedString(out, villain.description)
    out.writeInt(villain.getTraits().size)
    villain.getTraits().forEach { out.writeUTF(it.toString()) }
}

/**
 * Read villain data from the stream
 * @param inp Stream to read from
 * @return Parsed data
 * @throws Exception in case reading or parsing fails
 */
@Throws(Exception::class)
private fun readVillainInfo(inp: DataInputStream): Villain {
    val villain = Villain()
    villain.name = readLocalizedString(inp)
    villain.description = readLocalizedString(inp)
    (0 until inp.readInt()).map { Trait.valueOf(inp.readUTF()) }.forEach { villain.addTrait(it) }
    return villain
}

/**
 * Write obstacle data to stream
 * @param out Stream to write to
 * @param obstacle Obstacle to write
 * @throws IOException in case writing fails
 */
@Throws(IOException::class)
private fun writeObstacleInfo(out: DataOutputStream, obstacle: Obstacle) {
    out.writeInt(obstacle.tier)
    out.writeInt(obstacle.dieTypes.size)
    obstacle.dieTypes.forEach { out.writeUTF(it.toString()) }
    writeLocalizedString(out, obstacle.name)
    writeLocalizedString(out, obstacle.description)
    out.writeInt(obstacle.getTraits().size)
    obstacle.getTraits().forEach { out.writeUTF(it.toString()) }
}

/**
 * Read obstacle data from the stream
 * @param inp Stream to read from
 * @return Parsed data
 * @throws Exception in case reading or parsing fails
 */
@Throws(Exception::class)
private fun readObstacleInfo(inp: DataInputStream): Obstacle {
    val obstacle = Obstacle(inp.readInt(), *(0 until inp.readInt()).map { Die.Type.valueOf(inp.readUTF()) }.toTypedArray())
    obstacle.name = readLocalizedString(inp)
    obstacle.description = readLocalizedString(inp)
    (0 until inp.readInt()).map { Trait.valueOf(inp.readUTF()) }.forEach { obstacle.addTrait(it) }
    return obstacle
}

//----------------------------------------------------------------------------------------------------------------------

/**
 * Write localized string to stream
 * @param out Stream to write to
 * @param string String to write
 * @throws IOException in case writing fails
 */
@Throws(IOException::class)
private fun writeLocalizedString(out: DataOutputStream, string: LocalizedString) {
    with(string.asList()) {
        out.writeInt(this.size)
        this.forEach {
            out.writeUTF(it.first)
            out.writeUTF(it.second)
        }
    }
}

/**
 * Read localized string from the stream
 * @param inp Stream to read from
 * @return Parsed data
 * @throws Exception in case reading or parsing fails
 */
@Throws(Exception::class)
private fun readLocalizedString(inp: DataInputStream): LocalizedString {
    val list = (0 until inp.readInt()).map { inp.readUTF() to inp.readUTF() }.toMutableList()
    val default = list.first { it.first == "" }
    list.remove(default)
    return LocalizedString(default.second, list.toMap())
}

