package org.alexsem.dice.ui.storage

import org.alexsem.dice.generator.generateAdventureName
import org.alexsem.dice.generator.template.adventure.AdventureTemplate
import org.alexsem.dice.model.Hero
import org.alexsem.dice.model.LocalizedString
import java.util.*

class PartyInfoShort(val id: String) {

    lateinit var timestamp: Date
    lateinit var partyName: String
    lateinit var heroNames: List<String>
    lateinit var heroTypes: List<Hero.Type>
    private lateinit var _template: AdventureTemplate
    var adventure: AdventureTemplate
        get() = _template
        set(value) {
            _template = value
            adventureName = generateAdventureName(value)
        }
    lateinit var adventureName: LocalizedString
        private set
}
