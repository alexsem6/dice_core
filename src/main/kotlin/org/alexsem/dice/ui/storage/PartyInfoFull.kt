package org.alexsem.dice.ui.storage

import org.alexsem.dice.generator.template.adventure.AdventureTemplate
import org.alexsem.dice.model.Hero

class PartyInfoFull {

    var id: String = ""
    lateinit var partyName: String
    lateinit var heroes: List<Hero>
    lateinit var adventure: AdventureTemplate
    var progress = 0
}
