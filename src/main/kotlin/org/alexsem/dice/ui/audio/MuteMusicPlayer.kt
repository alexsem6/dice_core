package org.alexsem.dice.ui.audio

/**
 * Basic implementation of [MusicPlayer] interface, which does not play any sounds
 */
class MuteMusicPlayer : MusicPlayer {

    override fun play(music: Music) {
        //Do nothing
    }

    override fun stop() {
        //Do nothing
    }
}
