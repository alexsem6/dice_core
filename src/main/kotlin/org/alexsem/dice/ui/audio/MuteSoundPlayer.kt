package org.alexsem.dice.ui.audio

/**
 * Basic implementation of [SoundPlayer] interface, which does not play any sounds
 */
class MuteSoundPlayer : SoundPlayer {

    override fun play(sound: Sound) {
        //Do nothing
    }
}
