package org.alexsem.dice.ui.audio

/**
 * Singleton object to play both [Sound] samples and [Music] pieces
 */
object Audio {

    private var soundPlayer: SoundPlayer = MuteSoundPlayer()
    private var musicPlayer: MusicPlayer = MuteMusicPlayer()

    /**
     * Initialize audio system
     * @param soundPlayer Player to use for playing sounds
     * @param musicPlayer Player to use for playing music
     */
    fun init(soundPlayer: SoundPlayer, musicPlayer: MusicPlayer) {
        this.soundPlayer = soundPlayer
        this.musicPlayer = musicPlayer
    }

    /**
     * Play specific [Sound] sample once
     */
    fun playSound(sound: Sound) = this.soundPlayer.play(sound)

    /**
     * Start playing specific [Music] piece in a loop
     */
    fun playMusic(music: Music) = this.musicPlayer.play(music)

    /**
     * Stop music playback
     */
    fun stopMusic() = this.musicPlayer.stop()

}