package org.alexsem.dice.ui.audio

/**
 * Constants for various sounds used by the application
 */
enum class Sound {

    TURN_START, //Hero starts the turn
    BATTLE_CHECK_ROLL, //Perform check, type
    BATTLE_CHECK_SUCCESS, //Check was successful
    BATTLE_CHECK_FAILURE, //Check failed
    DIE_DRAW, //Draw die from bag
    DIE_HIDE, //Remove die to bag
    DIE_DISCARD, //Remove die to pile
    DIE_REMOVE, //Remove die entirely
    DIE_PICK, //Check/uncheck the die
    SKILL_PREPARE, //Hero prepares to use skill
    TRAVEL, //Move hero to another location
    LEAVE, //Forfeit the encounter
    ENCOUNTER_STAT, //Hero encounters STAT die
    ENCOUNTER_DIVINE, //Hero encounters DIVINE die
    ENCOUNTER_ALLY, //Hero encounters ALLY die
    ENCOUNTER_WOUND, //Hero encounters WOUND die
    ENCOUNTER_OBSTACLE, //Hero encounters OBSTACLE die
    ENCOUNTER_ENEMY, //Hero encounters ENEMY die
    ENCOUNTER_VILLAIN, //Hero encounters VILLAIN die
    DEFEAT_OBSTACLE, //Hero defeats OBSTACLE die
    DEFEAT_ENEMY, //Hero defeats ENEMY die
    DEFEAT_VILLAIN, //Hero defeats VILLAIN die
    PAGE_NEXT, //Navigate to next page
    PAGE_PREVIOUS, //Navigate to previous page
    PAGE_CLOSE, //Close page (usually info)
    TAKE_DAMAGE, //Hero takes damage
    HERO_DEATH, //Hero death //TODO change
    CLOSE_LOCATION,  //Location closed
    GAME_VICTORY, //Scenario completed
    GAME_LOSS, //Scenario failed
    UPGRADE, //Improve hero in some way
    ERROR, //When something unexpected happens
    CONFIRM_PROMPT, //When about to confirm something
    EXPLOSION, //Self-explanatory

}
