package org.alexsem.dice.ui.audio

/**
 * Implementations of this interface should play background music ([Music])
 */
interface MusicPlayer {

    /**
     * Start playing music
     * @param music Music to play
     */
    fun play(music: Music)

    /**
     * Stop playing music
     */
    fun stop()
}