package org.alexsem.dice.ui.audio

/**
 * Implementations of this interface are used to play various sound
 */
interface SoundPlayer {

    /**
     * Play specified sound once
     * @param sound Sound to play
     */
    fun play(sound: Sound)

}
