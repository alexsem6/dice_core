package org.alexsem.dice.ui

import org.alexsem.dice.ui.input.*
import org.alexsem.dice.ui.render.*
import org.alexsem.dice.ui.storage.Storage

/**
 * Common interface for all user interactions.
 * Implementations provide concrete classes for [org.alexsem.dice.ui.render.Renderer]s, [org.alexsem.dice.ui.input.Interactor]s and [Storage]
 */
interface UI {
    /**
     * Renders [org.alexsem.dice.game.Game]-related screens
     */
    val gameRenderer: GameRenderer
    /**
     * Renders [org.alexsem.dice.game.InterludePlayer]-related screens
     */
    val interludeRenderer: InterludeRenderer
    /**
     * Renders screens related to various menus
     */
    val menuRenderer: MenuRenderer
    /**
     * Renders screens related to various managers
     */
    val managerRenderer: ManagerRenderer
    /**
     * Renders screens related to various upgrade routines
     */
    val upgradeRenderer: UpgradeRenderer

    /**
     * Provides user interaction during [org.alexsem.dice.game.Game] sessions
     */
    val gameInteractor: GameInteractor
    /**
     * Provides user interaction during [org.alexsem.dice.game.InterludePlayer] scenes
     */
    val interludeInteractor: InterludeInteractor
    /**
     * Provides user interactions for various menus
     */
    val menuInteractor: MenuInteractor
    /**
     * Provides user interactions for various managers
     */
    val managerInteractor: ManagerInteractor
    /**
    Provides user interactions for various upgrade routines
     */
    val upgradeInteractor: UpgradeInteractor

    /**
     * Provides access to places where save files are stored
     */
    val storage: Storage
}