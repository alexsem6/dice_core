package org.alexsem.dice.ui.input

/**
 * Implementations of this interface are used to receive input from users during hero upgrades
 */
interface UpgradeInteractor : Interactor
