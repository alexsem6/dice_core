package org.alexsem.dice.ui.input

import org.alexsem.dice.ui.Action

interface InterludeInteractor: Interactor {

    fun advanceScript(): Action
}