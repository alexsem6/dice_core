package org.alexsem.dice.ui.input

import org.alexsem.dice.ui.Action
import org.alexsem.dice.game.HandMask
import org.alexsem.dice.game.PileMask
import org.alexsem.dice.ui.ActionList

/**
 * Implementations of this interface are used to receive input from users during gameplay
 */
interface GameInteractor : Interactor {

    /**
     * Interacts with dice in some hand by selecting/deselecting them
     * @param activePositions Currently active positions
     * @param actions List of actions
     * @return interaction result
     */
    fun pickDiceFromHand(activePositions: HandMask, actions: ActionList): Action

    /**
     * Interacts with dice in some pile by selecting/deselecting them
     * @param activePositions Currently active positions
     * @param actions List of actions
     * @return interaction result
     */
    fun pickDiceFromPile(activePositions: PileMask, actions: ActionList): Action

}
