package org.alexsem.dice.ui.input

import org.alexsem.dice.ui.Action
import org.alexsem.dice.ui.ActionList

/**
 * [Interactor] for working with various managers
 */
interface ManagerInteractor : Interactor {

    /**
     * Pick dice manager action or a die from the common pool
     * @param actions Regular actions to pick from
     * @param commonPoolPositions Active positions (on current page)
     */
    fun pickDiceManagerAction(actions: ActionList, commonPoolPositions: Set<Int>): Action

    /**
     * Enter some name (only letters without spaces)
     * @param initialText Text to start from
     * @param maxLength Maximum length of a string
     * @param allowSpaces true to allow entering spaces
     * @param allowDigits true to allow entering digits
     * @param textChangeListener input callback
     * @return Entered string or null if canceled
     */
    fun inputText(initialText: String, maxLength: Int, allowSpaces: Boolean = true, allowDigits: Boolean = true, textChangeListener: ((String) -> Unit)? = null): String?
}