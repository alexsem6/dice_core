package org.alexsem.dice.ui.input

import org.alexsem.dice.ui.Action
import org.alexsem.dice.ui.ActionList

/**
 * Superinterface for all interactors, defines the most common methods
 */
interface Interactor {

    /**
     * Waits for any input from the user
     */
    fun anyInput()

    /**
     * Select one item from some list
     * @param listSize Number of items in list
     * @return picked position (0-based)
     */
    fun pickItemFromList(listSize: Int): Action

    /**
     * Pick an action from the list
     * @param list List of possible actions
     * @return Action which was picked
     */
    fun pickAction(list: ActionList): Action


}
