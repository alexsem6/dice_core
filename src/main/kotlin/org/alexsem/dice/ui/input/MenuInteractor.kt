package org.alexsem.dice.ui.input

import org.alexsem.dice.ui.Action
import org.alexsem.dice.ui.ActionList

/**
 * Implementations of this interface are used to receive input from users during various menu interactions
 */
interface MenuInteractor: Interactor {

    /**
     * Pick party from a list or perform other action
     * @param actions Regular actions to pick from
     * @param currentPagePartyCount Number of parties on current page
     */
    fun pickPartySelectorAction(actions: ActionList, currentPagePartyCount: Int): Action

    /**
     * Pick scenario from a list or perform other action
     * @param actions Regular actions to pick from
     * @param currentPageScenarioCount Number of scenarios on current page
     */
    fun pickScenarioSelectorAction(actions: ActionList, currentPageScenarioCount: Int): Action
}