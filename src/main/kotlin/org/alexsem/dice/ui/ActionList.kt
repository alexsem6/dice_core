package org.alexsem.dice.ui

class ActionList : Iterable<Action> {

    companion object {
        val EMPTY
            get() = ActionList()
    }

    private val actions = mutableListOf<Action>()
    val size
        get() = actions.size

    /**
     * Add existing action to the list
     * @param action Action to add
     * @return self reference
     */
    fun add(action: Action): ActionList {
        actions.add(action)
        return this
    }

    /**
     * Add new action to the list
     * @param type Action type
     * @param enabled true if action is enabled, false if disabled
     * @return self reference
     */
    fun add(type: Action.Type, enabled: Boolean = true): ActionList {
        add(Action(type, enabled))
        return this
    }

    /**
     * Add list of existing actions action to the list
     * @param actions ActionList to add
     * @return self reference
     */
    fun addAll(actions: ActionList): ActionList {
        actions.forEach { add(it) }
        return this
    }

    /**
     * Remove action from the list
     * @param type Action type
     * @return self reference
     */
    fun remove(type: Action.Type): ActionList {
        actions.removeIf { it.type == type }
        return this
    }

    /**
     * Get action by index
     * @param index Action index
     * @return action or null;
     */
    operator fun get(index: Int) = actions[index]

    /**
     * Get action by type
     * @param type Action type
     * @return action or null
     */
    operator fun get(type: Action.Type) = actions.find { it.type == type }

    //------------------------------------------------------------------------------------------------------------------

    override fun iterator(): Iterator<Action> = ActionListIterator()

    private inner class ActionListIterator : Iterator<Action> {
        private var position = -1
        override fun hasNext() = (actions.size > position + 1)
        override fun next() = actions[++position]
    }

}