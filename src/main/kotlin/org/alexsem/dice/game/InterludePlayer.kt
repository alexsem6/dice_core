package org.alexsem.dice.game

import org.alexsem.dice.model.Interlude
import org.alexsem.dice.model.Interlude.Mood.*
import org.alexsem.dice.ui.Action
import org.alexsem.dice.ui.UI
import org.alexsem.dice.ui.audio.Audio
import org.alexsem.dice.ui.audio.Music

class InterludePlayer(ui: UI, private val interlude: Interlude) {

    private val renderer = ui.interludeRenderer
    private val interactor = ui.interludeInteractor

    /**
     * Start interaction
     */
    fun start() {
        processCycle()
    }

    /**
     * Change mood
     */
    private fun changeMood(mood: Interlude.Mood) {
        when (mood) {
            NONE -> Audio.stopMusic()
            SAD -> Audio.playMusic(Music.INTERLUDE_SAD)
            SERENE -> Audio.playMusic(Music.INTERLUDE_SERENE)
            DETERMINED -> Audio.playMusic(Music.INTERLUDE_DETERMINED)
            OMINOUS -> Audio.playMusic(Music.INTERLUDE_OMINOUS)
            TRIUMPHANT -> Audio.playMusic(Music.INTERLUDE_TRIUMPHANT)
            DANGEROUS -> Audio.playMusic(Music.INTERLUDE_DANGEROUS)
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Draw script
     */
    private fun processCycle() {
        loop@ for (event in interlude.getScript()) {
            when (event) {
                is Interlude.Event.Tune -> {
                    changeMood(event.mood)
                    continue@loop
                }
                is Interlude.Event.Headline -> renderer.drawHeadline(interlude, event)
                is Interlude.Event.Narration -> renderer.drawNarration(interlude, event)
                is Interlude.Event.Phrase -> renderer.drawPhrase(interlude, event)
            }
            when (interactor.advanceScript().type) {
                Action.Type.INFO_PAGE_DOWN -> continue@loop
                Action.Type.CANCEL -> break@loop
                else -> throw AssertionError("Should not happen")
            }
        }
        Audio.stopMusic()
    }


}