package org.alexsem.dice.game.rule.pile

import org.alexsem.dice.game.PileMask
import org.alexsem.dice.model.Pile

/**
 * Implementation of [PileMaskRule] where dice of any type up to certain number can be picked
 */
class AnyDicePileMaskRule(pile: Pile, private val maxDiceCount: Int)
    : PileMaskRule(pile) {

    override fun checkMask(mask: PileMask) = (mask.positionCount in 1..maxDiceCount)

    override fun isPositionActive(mask: PileMask, position: Int) =
            mask.checkPosition(position) || mask.positionCount < maxDiceCount

}