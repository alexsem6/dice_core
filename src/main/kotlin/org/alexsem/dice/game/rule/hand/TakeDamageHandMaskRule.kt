package org.alexsem.dice.game.rule.hand

import org.alexsem.dice.game.HandMask
import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Hand
import kotlin.math.min

/**
 * Implementation of [HandMaskRule] which participates in die discard process at the end of the turn
 */
class TakeDamageHandMaskRule(hand: Hand, amount: Int) : HandMaskRule(hand) {

    private val diceToDiscard = min(amount, hand.dieCount + hand.allyDieCount - hand.woundCount)

    override fun checkMask(mask: HandMask) = (mask.positionCount + mask.allyPositionCount == diceToDiscard)

    override fun isPositionActive(mask: HandMask, position: Int) = when {
        mask.checkPosition(position) -> true
        hand.dieAt(position) === null -> false
        hand.dieAt(position)!!.type === Die.Type.WOUND -> false
        mask.positionCount + mask.allyPositionCount < diceToDiscard -> true
        else -> false
    }

    override fun isAllyPositionActive(mask: HandMask, position: Int) = when {
        mask.checkAllyPosition(position) -> true
        hand.allyDieAt(position) === null -> false
        mask.positionCount + mask.allyPositionCount < diceToDiscard -> true
        else -> false
    }

}