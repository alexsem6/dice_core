package org.alexsem.dice.game.rule.hand

import org.alexsem.dice.game.HandMask
import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Hand
import kotlin.math.min

/**
 * Implementation of [HandMaskRule] which participates in die discard process at the end of the turn
 */
class DiscardExtraDiceHandMaskRule(hand: Hand) : HandMaskRule(hand) {

    private val minDiceToDiscard = if (hand.dieCount > hand.capacity) min(hand.dieCount - hand.woundCount, hand.dieCount - hand.capacity) else 0
    private val maxDiceToDiscard = hand.dieCount - hand.woundCount

    override fun checkMask(mask: HandMask) =
            (mask.positionCount in minDiceToDiscard..maxDiceToDiscard) &&
                    (mask.allyPositionCount in 0..hand.allyDieCount)

    override fun isPositionActive(mask: HandMask, position: Int) = when {
        mask.checkPosition(position) -> true
        hand.dieAt(position) == null -> false
        hand.dieAt(position)!!.type == Die.Type.WOUND -> false
        mask.positionCount < maxDiceToDiscard -> true
        else -> false
    }

    override fun isAllyPositionActive(mask: HandMask, position: Int) = hand.allyDieAt(position) != null

}
