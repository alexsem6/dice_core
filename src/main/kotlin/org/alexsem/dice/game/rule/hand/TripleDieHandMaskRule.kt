package org.alexsem.dice.game.rule.hand

import org.alexsem.dice.game.HandMask
import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Die.Type.*
import org.alexsem.dice.model.Hand

/**
 * Implementation of [HandMaskRule] which involves picking two dice each of which has own list of possible types
 */
open class TripleDieHandMaskRule(
        hand: Hand,
        types1: Array<Die.Type>,
        types2: Array<Die.Type>,
        types3: Array<Die.Type>)
    : HandMaskRule(hand) {

    private val types1 = types1.toSet()
    private val types2 = types2.toSet()
    private val types3 = types3.toSet()

    override fun checkMask(mask: HandMask): Boolean {
        if (mask.positionCount + mask.allyPositionCount != 3) {
            return false
        }
        return getCheckedDice(mask).asSequence()
                .filter { it.type in types1 }
                .any { d1 ->
                    getCheckedDice(mask)
                            .filter { d2 -> d2 !== d1 }
                            .filter { it.type in types2 }
                            .any { d2 ->
                                getCheckedDice(mask)
                                        .filter { d3 -> d3 !== d1 }
                                        .filter { d3 -> d3 !== d2 }
                                        .any { it.type in types3 }
                            }
                }
    }

    override fun isPositionActive(mask: HandMask, position: Int): Boolean {
        if (mask.checkPosition(position)) {
            return true
        }
        val die = hand.dieAt(position) ?: return false
        return when (mask.positionCount + mask.allyPositionCount) {
            0 -> die.type in types1 || die.type in types2 || die.type in types3
            1 -> with(getCheckedDice(mask).first()) {
                (this.type in types1 && (die.type in types2 || die.type in types3))
                        || (this.type in types2 && (die.type in types1 || die.type in types3))
                        || (this.type in types3 && (die.type in types1 || die.type in types2))
            }
            2 -> with(getCheckedDice(mask)) {
                val d1 = this[0]
                val d2 = this[1]
                (d1.type in types1 && d2.type in types2 && die.type in types3) ||
                        (d2.type in types1 && d1.type in types2 && die.type in types3) ||
                        (d1.type in types1 && d2.type in types3 && die.type in types2) ||
                        (d2.type in types1 && d1.type in types3 && die.type in types2) ||
                        (d1.type in types2 && d2.type in types3 && die.type in types1) ||
                        (d2.type in types2 && d1.type in types3 && die.type in types1)
            }
            3 -> false
            else -> false
        }
    }

    override fun isAllyPositionActive(mask: HandMask, position: Int): Boolean {
        if (mask.checkAllyPosition(position)) {
            return true
        }
        if (hand.allyDieAt(position) == null) {
            return false
        }
        return when (mask.positionCount + mask.allyPositionCount) {
            0 -> ALLY in types1 || ALLY in types2 || ALLY in types3
            1 -> with(getCheckedDice(mask).first()) {
                (this.type in types1 && (ALLY in types2 || ALLY in types3))
                        || (this.type in types2 && (ALLY in types1 || ALLY in types3))
                        || (this.type in types3 && (ALLY in types1 || ALLY in types2))
            }
            2 -> with(getCheckedDice(mask)) {
                val d1 = this[0]
                val d2 = this[1]
                (d1.type in types1 && d2.type in types2 && ALLY in types3) ||
                        (d2.type in types1 && d1.type in types2 && ALLY in types3) ||
                        (d1.type in types1 && d2.type in types3 && ALLY in types2) ||
                        (d2.type in types1 && d1.type in types3 && ALLY in types2) ||
                        (d1.type in types2 && d2.type in types3 && ALLY in types1) ||
                        (d2.type in types2 && d1.type in types3 && ALLY in types1)
            }
            3 -> false
            else -> false
        }
    }

}