package org.alexsem.dice.game.rule.hand

import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Hand

/**
 * Implementation of [HandMaskRule] for dealing with Tier-2 Obstacles (which require 1 specific die)
 */
class SpecificDieObstacleHandMaskRule(hand: Hand, type: Die.Type) : SingleDieHandMaskRule(hand, type, Die.Type.DIVINE)
