package org.alexsem.dice.game.rule.pile

import org.alexsem.dice.game.PileMask
import org.alexsem.dice.model.Pile

/**
 * Special interface which defines rules for picking dice from piles
 */
abstract class PileMaskRule(pile: Pile) {
    val dice = pile.examine()

    /**
     * Checks whether specified dice (mask) picked from the pile correspond to the rules
     * @param mask Mask to check
     * @return true if mask picked correctly, false otherwise
     */
    abstract fun checkMask(mask: PileMask): Boolean

    /**
     * Additionally check whether specified position is active
     * @param mask Mask to check
     * @param position Position to check
     * @return true if position can be interacted with, false otherwise
     */
    abstract fun isPositionActive(mask: PileMask, position: Int): Boolean

}