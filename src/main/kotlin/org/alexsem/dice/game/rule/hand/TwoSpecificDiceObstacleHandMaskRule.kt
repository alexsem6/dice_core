package org.alexsem.dice.game.rule.hand

import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Hand

/**
 * Implementation of [HandMaskRule] for dealing with Tier-3 Obstacles (which require 2 dice of specific types)
 */
class TwoSpecificDiceObstacleHandMaskRule(hand: Hand, type1: Die.Type, type2: Die.Type) : DoubleDieHandMaskRule(hand, arrayOf(type1, Die.Type.DIVINE), arrayOf(type2, Die.Type.DIVINE))
