package org.alexsem.dice.game.rule.hand

import org.alexsem.dice.game.HandMask
import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Hand

/**
 * Implementation of [HandMaskRule] which participates in forming of the battle check when acquiring stat dice
 */
class StatDieAcquireHandMaskRule(hand: Hand, private val requiredType: Die.Type) : HandMaskRule(hand) {

    /**
     * Define how many dice of specified type are currently checked
     */
    private fun checkedDieCount(mask: HandMask) =
            (0 until hand.dieCount).filter(mask::checkPosition).mapNotNull(hand::dieAt).count { it.type === requiredType }

    override fun checkMask(mask: HandMask) = (mask.allyPositionCount == 0 && checkedDieCount(mask) == 1)

    override fun isPositionActive(mask: HandMask, position: Int) = with(hand.dieAt(position)) {
        when {
            mask.checkPosition(position) -> true
            this == null -> false
            this.type === Die.Type.DIVINE -> true
            this.type === requiredType && checkedDieCount(mask) < 1 -> true
            else -> false
        }
    }

    override fun isAllyPositionActive(mask: HandMask, position: Int) = false


}