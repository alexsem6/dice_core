package org.alexsem.dice.game.rule.hand

import org.alexsem.dice.model.Die.Type.*
import org.alexsem.dice.model.Hand

class SingleNonWoundDieHandMaskRule(hand: Hand) :
        SingleDieHandMaskRule(hand, PHYSICAL, SOMATIC, MENTAL, VERBAL, DIVINE, ALLY)