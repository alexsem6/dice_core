package org.alexsem.dice.game.rule.hand

import org.alexsem.dice.game.HandMask
import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Die.Type.ALLY
import org.alexsem.dice.model.Hand

/**
 * Implementation of [HandMaskRule] which involves picking single die of one of the specified types
 */
open class SingleDieHandMaskRule(hand: Hand, vararg types: Die.Type) : HandMaskRule(hand) {

    private val types = types.toSet()

    override fun checkMask(mask: HandMask) = when {
        mask.positionCount + mask.allyPositionCount != 1 -> false
        mask.positionCount == 1 && getCheckedDice(mask).filter { it.type in types }.count() == 1 -> true
        mask.allyPositionCount == 1 && getCheckedDice(mask).filter { ALLY in types }.count() == 1 -> true
        else -> false
    }

    override fun isPositionActive(mask: HandMask, position: Int) = when {
        mask.checkPosition(position) -> true
        hand.dieAt(position) == null -> false
        mask.positionCount + mask.allyPositionCount >= 1 -> false
        hand.dieAt(position)!!.type in types -> true
        else -> false
    }

    override fun isAllyPositionActive(mask: HandMask, position: Int) = when {
        mask.checkAllyPosition(position) -> true
        hand.allyDieAt(position) == null -> false
        mask.positionCount + mask.allyPositionCount >= 1 -> false
        ALLY in types -> true
        else -> false
    }
}
