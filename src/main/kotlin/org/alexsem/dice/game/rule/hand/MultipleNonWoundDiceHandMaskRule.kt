package org.alexsem.dice.game.rule.hand

import org.alexsem.dice.game.HandMask
import org.alexsem.dice.model.Die.Type.*
import org.alexsem.dice.model.Hand

class MultipleNonWoundDiceHandMaskRule(hand: Hand, private val minCount: Int, private val maxCount: Int)
    : HandMaskRule(hand) {

    override fun checkMask(mask: HandMask) =
            mask.positionCount + mask.allyPositionCount in (minCount..maxCount)

    override fun isPositionActive(mask: HandMask, position: Int) = when {
        mask.checkPosition(position) -> true
        mask.positionCount + mask.allyPositionCount >= maxCount -> false
        hand.dieAt(position) == null -> false
        hand.dieAt(position)?.type == WOUND -> false
        else -> true
    }

    override fun isAllyPositionActive(mask: HandMask, position: Int) = when {
        mask.checkAllyPosition(position) -> true
        mask.positionCount + mask.allyPositionCount >= maxCount -> false
        hand.allyDieAt(position) == null -> false
        else -> true
    }

}
