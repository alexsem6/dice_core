package org.alexsem.dice.game.rule.hand

import org.alexsem.dice.game.HandMask
import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Die.Type.*
import org.alexsem.dice.model.Hand

/**
 * Implementation of [HandMaskRule] which involves picking two dice each of which has own list of possible types
 */
open class DoubleDieHandMaskRule(hand: Hand, types1: Array<Die.Type>, types2: Array<Die.Type>) : HandMaskRule(hand) {

    private val types1 = types1.toSet()
    private val types2 = types2.toSet()

    override fun checkMask(mask: HandMask): Boolean {
        if (mask.positionCount + mask.allyPositionCount != 2) {
            return false
        }
        return getCheckedDice(mask)
                .filter { it.type in types1 }
                .any { d1 ->
                    getCheckedDice(mask)
                            .filter { d2 -> d2 !== d1 }
                            .any { it.type in types2 }
                }
    }

    override fun isPositionActive(mask: HandMask, position: Int): Boolean {
        if (mask.checkPosition(position)) {
            return true
        }
        val die = hand.dieAt(position) ?: return false
        return when (mask.positionCount + mask.allyPositionCount) {
            0 -> die.type in types1 || die.type in types2
            1 -> with(getCheckedDice(mask).first()) {
                (this.type in types1 && die.type in types2) || (this.type in types2 && die.type in types1)
            }
            2 -> false
            else -> false
        }
    }

    override fun isAllyPositionActive(mask: HandMask, position: Int): Boolean {
        if (mask.checkAllyPosition(position)) {
            return true
        }
        if (hand.allyDieAt(position)== null) {
            return false
        }
        return when (mask.positionCount + mask.allyPositionCount) {
            0 -> ALLY in types1 || ALLY in types2
            1 -> with (getCheckedDice(mask).first()) {
                (this.type in types1 && ALLY in types2) || (this.type in types2 && ALLY in types1)
            }
            2 -> false
            else -> false
        }
    }


}