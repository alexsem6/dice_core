package org.alexsem.dice.game.rule.hand

import org.alexsem.dice.game.HandMask
import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Hand
import org.alexsem.dice.model.MAX_HAND_ALLY_SIZE
import org.alexsem.dice.model.MAX_HAND_SIZE
import kotlin.math.min

/**
 * Implementation of [HandMaskRule] which participates in die transfer process from one hero to another
 */
class GiveDiceHandMaskRule(hand: Hand, recipientHand: Hand) : HandMaskRule(hand) {

    private val maxDiceToGive = min(hand.dieCount - hand.woundCount, MAX_HAND_SIZE - recipientHand.dieCount)
    private val maxAllyDiceToGive = min(hand.allyDieCount, MAX_HAND_ALLY_SIZE - recipientHand.allyDieCount)


    override fun checkMask(mask: HandMask) = mask.positionCount + mask.allyPositionCount > 0
            && mask.positionCount in 0..maxDiceToGive
            && mask.allyPositionCount in 0..maxAllyDiceToGive

    override fun isPositionActive(mask: HandMask, position: Int) = when {
        mask.checkPosition(position) -> true
        hand.dieAt(position) == null -> false
        hand.dieAt(position)!!.type === Die.Type.WOUND -> false
        mask.positionCount < maxDiceToGive -> true
        else -> false
    }

    override fun isAllyPositionActive(mask: HandMask, position: Int) = when {
        mask.checkAllyPosition(position) -> true
        hand.allyDieAt(position) == null -> false
        mask.allyPositionCount < maxAllyDiceToGive -> true
        else -> false
    }

}