package org.alexsem.dice.game.rule.hand

import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Hand

/**
 * Implementation of [HandMaskRule] for dealing with Tier-1 Obstacles (which require 1 die of two possible types)
 */
class EitherOfTwoDiceObstacleHandMaskRule(hand: Hand, type1: Die.Type, type2: Die.Type)
    : SingleDieHandMaskRule(hand, type1, type2, Die.Type.DIVINE)
