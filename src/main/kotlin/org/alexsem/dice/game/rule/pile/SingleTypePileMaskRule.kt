package org.alexsem.dice.game.rule.pile

import org.alexsem.dice.game.PileMask
import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Pile

/**
 * Implementation of [PileMaskRule] where dice of specific requiredType up to certain number can be picked
 */
class SingleTypePileMaskRule(pile: Pile, private val requiredType: Die.Type, private val minDieCount: Int, private val maxDiceCount: Int)
    : PileMaskRule(pile) {

    override fun checkMask(mask: PileMask) = (mask.positionCount in minDieCount..maxDiceCount) && checkedDiceCount(mask) == mask.positionCount

    override fun isPositionActive(mask: PileMask, position: Int) = when {
        mask.checkPosition(position)-> true
        dice[position].type != requiredType -> false
        checkedDiceCount(mask) >= maxDiceCount -> false
        else -> true
    }

    private fun checkedDiceCount(mask: PileMask) =
            (0 until dice.size).filter(mask::checkPosition).map { dice[it] }.count { it.type === requiredType }

}