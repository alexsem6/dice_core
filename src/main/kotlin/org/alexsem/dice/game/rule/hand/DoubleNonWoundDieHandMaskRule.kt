package org.alexsem.dice.game.rule.hand

import org.alexsem.dice.model.Die.Type.*
import org.alexsem.dice.model.Hand

class DoubleNonWoundDieHandMaskRule(hand: Hand) : DoubleDieHandMaskRule(
        hand,
        arrayOf(PHYSICAL, SOMATIC, MENTAL, VERBAL, DIVINE, ALLY),
        arrayOf(PHYSICAL, SOMATIC, MENTAL, VERBAL, DIVINE, ALLY)
)