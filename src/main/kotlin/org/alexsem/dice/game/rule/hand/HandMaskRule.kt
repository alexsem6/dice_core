package org.alexsem.dice.game.rule.hand

import org.alexsem.dice.game.HandMask
import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Hand

/**
 * Special interface which defines rules for picking dice from hand
 */
abstract class HandMaskRule(val hand: Hand) {

    /**
     * Checks whether specified dice (mask) picked from the hand correspond to the rules
     * @param mask Mask to check
     * @return true if mask picked correctly, false otherwise
     */
    abstract fun checkMask(mask: HandMask): Boolean

    /**
     * Additionally check whether specified position is active
     * @param mask Mask to check
     * @param position Position to check
     * @return true if position can be interacted with, false otherwise
     */
    abstract fun isPositionActive(mask: HandMask, position: Int): Boolean

    /**
     * Additionally check whether specified ally position is active
     * @param mask Mask to check
     * @param position Ally position to check
     * @return true if ally position can be interacted with, false otherwise
     */
    abstract fun isAllyPositionActive(mask: HandMask, position: Int): Boolean

    /**
     * Get all dice from [Hand] which correspond to specified [HandMask]
     * @param mask Mask to check against
     * @return combined stream
     */
    fun getCheckedDice(mask: HandMask): List<Die> {
        return ((0 until hand.dieCount).filter(mask::checkPosition).map(hand::dieAt))
                .plus((0 until hand.allyDieCount).filter(mask::checkAllyPosition).map(hand::allyDieAt))
                .filterNotNull()
    }

}