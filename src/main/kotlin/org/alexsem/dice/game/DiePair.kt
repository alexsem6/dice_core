package org.alexsem.dice.game

import org.alexsem.dice.model.Die

/**
 * Die and modifier pair
 */
class DiePair(val die: Die, var modifier: Int = 0)
