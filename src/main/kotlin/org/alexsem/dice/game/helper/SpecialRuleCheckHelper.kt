package org.alexsem.dice.game.helper

import org.alexsem.dice.game.GamePhase
import org.alexsem.dice.model.Location
import org.alexsem.dice.model.Scenario
import org.alexsem.dice.model.SpecialRule
import org.alexsem.dice.model.SpecialRule.*

/**
 * Helper class for [org.alexsem.dice.game.Game] to perform [SpecialRule]-related checks
 */
object SpecialRuleCheckHelper {

    //------------------------------------------------------------------------------------------------------------------
    private val rulesScenarioStart = listOf(
            BADLY_HURT,
            DEEPLY_WOUNDED,
            FRIENDLY_NEIGHBOURHOOD,
            THOROUGH_INVESTIGATION,
            DISPERSION,
            TRUSTED_COMPANIONS,
            GREAT_BETRAYAL,
            NOWHERE_TO_HIDE,
            SAFE_LOCATION,
            ONE_SHOULD_SUFFICE,
            UNIMPEDED_PROGRESS,
            PLENTIFUL_SUPPLIES
    )
    private val rulesEncounterEnemy = listOf(
            SERENITY,
            STATE_OF_EMERGENCY,
            VICIOUS_INHABITANTS,
            YOU_ARE_NOT_WELCOME_HERE
    )
    private val rulesEncounterVillain = listOf(
            STATE_OF_EMERGENCY
    )
    private val rulesEncounterObstacle = listOf(
            STATE_OF_EMERGENCY,
            AGGRESSIVE_ENVIRONMENT
    )
    private val rulesLocationClosed = listOf(
            JUST_A_SCRATCH,
            OCCASIONAL_ACCOMPLICES,
            HIDDEN_STASH,
            DONT_FORGET_TO_PAY_UP,
            HEAVY_CONSEQUENCES,
            GODS_BLESSING,
            MAY_THE_GOD_BE_WITH_YOU,
            MUCH_BETTER,
            I_NEEDED_THIS
    )
    private val rulesHeroTurnStart = listOf(
            THWARTED_PLANS,
            HOLD_ONTO_YOUR_BELONGINGS,
            THEY_ARE_EVERYWHERE,
            OVERPREPARED,
            UTTER_MESS
    )
    private val rulesHeroTurnEnd = listOf(
            IMPOSSIBLE_TO_STAY
    )
    private val rulesHeroArrivesAtLocation = listOf(
            MONEY_UP_FRONT
    )
    private val rules = mapOf(
            GamePhase.SCENARIO_START to rulesScenarioStart,
            GamePhase.LOCATION_ENCOUNTER_ENEMY to rulesEncounterEnemy,
            GamePhase.LOCATION_ENCOUNTER_VILLAIN to rulesEncounterVillain,
            GamePhase.LOCATION_ENCOUNTER_OBSTACLE to rulesEncounterObstacle,
            GamePhase.LOCATION_CLOSED to rulesLocationClosed,
            GamePhase.HERO_TURN_START to rulesHeroTurnStart,
            GamePhase.HERO_TURN_END to rulesHeroTurnEnd,
            GamePhase.HERO_ARRIVES_AT_LOCATION to rulesHeroArrivesAtLocation
    )

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Get rules of current scenario and select ones relevant to the current [GamePhase]
     * @param phase Current game phase
     * @param scenario Current scenario
     * @return Relevant rules
     */
    fun getApplicableScenarioSpecialRules(phase: GamePhase, scenario: Scenario) = with(rules[phase] ?: listOf()) {
        scenario.getSpecialRules().filter { it in this }
    }

    /**
     * Get rules of specific location and select ones relevant to the current [GamePhase]
     * @param phase Current game phase
     * @param location Specific location
     * @return Relevant rules
     */
    fun getApplicableLocationSpecialRules(phase: GamePhase, location: Location) =
            with(rules[phase] ?: listOf()) {
                location.getSpecialRules().filter { it in this }
            }
}