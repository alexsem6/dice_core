package org.alexsem.dice.game.helper

import org.alexsem.dice.game.DieBattleCheck
import org.alexsem.dice.game.DieBattleCheck.Method.*
import org.alexsem.dice.game.GamePhase
import org.alexsem.dice.game.distribute.*
import org.alexsem.dice.game.filter.bag.EmptyBagFilter
import org.alexsem.dice.game.filter.bag.NoMoreThanNDiceBagFilter
import org.alexsem.dice.game.filter.bag.SingleAnyDieBagFilter
import org.alexsem.dice.game.filter.hand.DoubleDieHandFilter
import org.alexsem.dice.game.filter.hand.HandFilter
import org.alexsem.dice.game.filter.hand.SingleDieHandFilter
import org.alexsem.dice.game.rule.hand.*
import org.alexsem.dice.game.rule.pile.AnyDicePileMaskRule
import org.alexsem.dice.game.rule.pile.PileMaskRule
import org.alexsem.dice.model.*
import org.alexsem.dice.model.AllySkill.Type.*
import org.alexsem.dice.model.Die.Type.*

/**
 * Helper class for [org.alexsem.dice.game.Game] to perform [AllySkill]-related checks
 */
object AllySkillCheckHelper {

    //------------------------------------------------------------------------------------------------------------------
    private val skillsBeforeExploration = setOf(
            FALCON,
            FIELD_MEDIC,
            HERMIT,
            INTERN,
            LOCAL_DWELLER,
            MASTERFUL_DISTRACTION,
            PATHFINDER,
            PHYSICIAN,
            SABOTEUR,
            SCRYER,
            TOUR_GUIDE,
            TRICKSTER
    )
    private val skillsEncounterEnemy = setOf(
            COMBAT_ADVISOR,
            DARK_ENCHANTER,
            DIPLOMAT,
            PYROMANIAC,
            RETIRED_SOLDIER,
            TRICKSTER
    )
    private val skillsEncounterVillain = setOf(
            COMBAT_ADVISOR,
            DARK_ENCHANTER,
            DIPLOMAT,
            PYROMANIAC,
            RETIRED_SOLDIER,
            TRICKSTER
    )
    private val skillsEncounterAlly = setOf(
            DIPLOMAT,
            TRICKSTER
    )
    private val skillsTakeDamage = setOf(
            DEFENDER,
            ILLUSIONIST,
            TRICKSTER
    )
    private val skillsAfterExploration = setOf(
            FALCON,
            FIELD_MEDIC,
            HERMIT,
            INTERN,
            LOCAL_DWELLER,
            MASTERFUL_DISTRACTION,
            PATHFINDER,
            PHYSICIAN,
            SABOTEUR,
            TOUR_GUIDE,
            TRICKSTER
    )

    private val skills = mapOf(
            GamePhase.LOCATION_BEFORE_EXPLORATION to skillsBeforeExploration,
            GamePhase.LOCATION_ENCOUNTER_ENEMY to skillsEncounterEnemy,
            GamePhase.LOCATION_ENCOUNTER_VILLAIN to skillsEncounterVillain,
            GamePhase.LOCATION_ENCOUNTER_ALLY to skillsEncounterAlly,
            GamePhase.HERO_TAKES_DAMAGE to skillsTakeDamage,
            GamePhase.LOCATION_AFTER_EXPLORATION to skillsAfterExploration
    )

    //------------------------------------------------------------------------------------------------------------------
    private val handFiltersUser = mapOf(
            COMBAT_ADVISOR to SingleDieHandFilter(ALLY),
            DARK_ENCHANTER to SingleDieHandFilter(ALLY),
            DEFENDER to SingleDieHandFilter(ALLY),
            DIPLOMAT to SingleDieHandFilter(ALLY),
            FALCON to SingleDieHandFilter(ALLY),
            FIELD_MEDIC to SingleDieHandFilter(ALLY),
            HERMIT to SingleDieHandFilter(ALLY),
            INTERN to SingleDieHandFilter(ALLY),
            ILLUSIONIST to SingleDieHandFilter(ALLY),
            LOCAL_DWELLER to SingleDieHandFilter(ALLY),
            MASTERFUL_DISTRACTION to DoubleDieHandFilter(arrayOf(ALLY), arrayOf(ALLY)),
            PATHFINDER to SingleDieHandFilter(ALLY),
            PHYSICIAN to SingleDieHandFilter(ALLY),
            PYROMANIAC to SingleDieHandFilter(ALLY),
            RETIRED_SOLDIER to SingleDieHandFilter(ALLY),
            SABOTEUR to SingleDieHandFilter(ALLY),
            SCRYER to SingleDieHandFilter(ALLY),
            TOUR_GUIDE to SingleDieHandFilter(ALLY),
            TRICKSTER to SingleDieHandFilter(ALLY)
    )

    private val handFiltersTarget = mapOf<AllySkill.Type, HandFilter>(
            FIELD_MEDIC to SingleDieHandFilter(WOUND)
    )

    //------------------------------------------------------------------------------------------------------------------
    private val battleCheckMethods = mapOf(
            COMBAT_ADVISOR to arrayOf(MAX),
            DARK_ENCHANTER to arrayOf(MAX),
            DEFENDER to arrayOf(MAX),
            DIPLOMAT to arrayOf(MAX),
            FIELD_MEDIC to arrayOf(MAX),
            HERMIT to arrayOf(MAX),
            ILLUSIONIST to arrayOf(MAX),
            MASTERFUL_DISTRACTION to arrayOf(MAX),
            PHYSICIAN to arrayOf(MAX),
            PYROMANIAC to arrayOf(MAX),
            RETIRED_SOLDIER to arrayOf(MAX),
            SCRYER to arrayOf(MAX)
    )

    //------------------------------------------------------------------------------------------------------------------
    private val dieDistributors = mapOf(
            COMBAT_ADVISOR to arrayOf(discardAllDieDistributor),
            DARK_ENCHANTER to arrayOf(discardAllDieDistributor),
            DEFENDER to arrayOf(discardAllDieDistributor),
            DIPLOMAT to arrayOf(hideAllDieDistributor, discardAllDieDistributor),
            FIELD_MEDIC to arrayOf(discardAllDieDistributor),
            HERMIT to arrayOf(discardAllDieDistributor),
            ILLUSIONIST to arrayOf(discardAllDieDistributor),
            MASTERFUL_DISTRACTION to arrayOf(discardAllDieDistributor),
            PHYSICIAN to arrayOf(deterAllDieDistributor),
            PYROMANIAC to arrayOf(hideAllDieDistributor, discardAllDieDistributor),
            RETIRED_SOLDIER to arrayOf(hideAllDieDistributor, discardAllDieDistributor),
            SCRYER to arrayOf(discardAllDieDistributor)
    )

    //------------------------------------------------------------------------------------------------------------------
    private val bagFilters = mapOf(
            FALCON to SingleAnyDieBagFilter(),
            INTERN to SingleAnyDieBagFilter(),
            MASTERFUL_DISTRACTION to NoMoreThanNDiceBagFilter(5),
            PHYSICIAN to SingleAnyDieBagFilter(),
            SABOTEUR to EmptyBagFilter(),
            SCRYER to SingleAnyDieBagFilter(),
            TOUR_GUIDE to SingleAnyDieBagFilter(),
            TRICKSTER to SingleAnyDieBagFilter()
    )

    //------------------------------------------------------------------------------------------------------------------

    private val traitsDenial = mapOf(
            DARK_ENCHANTER to arrayOf(Trait.IMMUNE_MODIFIERS),
            DEFENDER to arrayOf(Trait.DAMAGE_MAGICAL, Trait.IMMUNE_REDUCTION),
            DIPLOMAT to arrayOf(Trait.IMMUNE_AVOID_ENCOUNTER),
            ILLUSIONIST to arrayOf(Trait.IMMUNE_REDUCTION),
            PYROMANIAC to arrayOf(Trait.IMMUNE_MAGICAL),
            RETIRED_SOLDIER to arrayOf(Trait.IMMUNE_PHYSICAL)
    )

    private val traitsRequire = mapOf<AllySkill.Type, Array<Trait>>()

    //------------------------------------------------------------------------------------------------------------------
    private val modifiersEncounterStat = setOf(
            HERMIT
    )
    private val modifiersEncounterAlly = setOf(
            HERMIT
    )
    private val modifiersEncounterObstacle = setOf(
            HERMIT
    )
    private val modifiersEncounterEnemy = setOf(
            COMBAT_ADVISOR,
            HERMIT
    )
    private val modifiersEncounterVillain = setOf(
            COMBAT_ADVISOR,
            HERMIT
    )
    private val modifiersClosingLocation = setOf(
            HERMIT,
            LOCAL_DWELLER
    )

    private val pendingModifiers = mapOf(
            GamePhase.LOCATION_ENCOUNTER_STAT to modifiersEncounterStat,
            GamePhase.LOCATION_ENCOUNTER_ALLY to modifiersEncounterAlly,
            GamePhase.LOCATION_ENCOUNTER_OBSTACLE to modifiersEncounterObstacle,
            GamePhase.LOCATION_ENCOUNTER_ENEMY to modifiersEncounterEnemy,
            GamePhase.LOCATION_ENCOUNTER_VILLAIN to modifiersEncounterVillain,
            GamePhase.LOCATION_CLOSE_ATTEMPT to modifiersClosingLocation
    )

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Pick correct [HandMaskRule] for using [AllySkill] (primary usage)
     * @param skill Ally skill to use
     * @param hand Hero's hand
     * @return correct [HandMaskRule] or null
     */
    fun getHandMaskRulePrimary(skill: AllySkill, hand: Hand) = when (skill.type) {
        COMBAT_ADVISOR -> SingleDieHandMaskRule(hand, ALLY)
        DARK_ENCHANTER -> SingleDieHandMaskRule(hand, ALLY)
        DEFENDER -> SingleDieHandMaskRule(hand, ALLY)
        DIPLOMAT -> SingleDieHandMaskRule(hand, ALLY)
        FALCON -> SingleDieHandMaskRule(hand, ALLY)
        FIELD_MEDIC -> SingleDieHandMaskRule(hand, WOUND)
        HERMIT -> SingleDieHandMaskRule(hand, ALLY)
        INTERN -> SingleDieHandMaskRule(hand, ALLY)
        ILLUSIONIST -> SingleDieHandMaskRule(hand, ALLY)
        LOCAL_DWELLER -> SingleDieHandMaskRule(hand, ALLY)
        MASTERFUL_DISTRACTION -> DoubleDieHandMaskRule(hand, arrayOf(ALLY), arrayOf(ALLY))
        PATHFINDER -> SingleDieHandMaskRule(hand, ALLY)
        PHYSICIAN -> SingleDieHandMaskRule(hand, ALLY)
        PYROMANIAC -> SingleDieHandMaskRule(hand, ALLY)
        RETIRED_SOLDIER -> SingleDieHandMaskRule(hand, ALLY)
        SABOTEUR -> SingleDieHandMaskRule(hand, ALLY)
        SCRYER -> SingleDieHandMaskRule(hand, ALLY)
        TOUR_GUIDE -> SingleDieHandMaskRule(hand, ALLY)
        TRICKSTER -> SingleDieHandMaskRule(hand, ALLY)
    }

    /**
     * Pick correct [HandMaskRule] for using [AllySkill] (subsequent usage, if applicable)
     * @param skill Ally skill to use
     * @param hand Hero's hand
     * @return correct [HandMaskRule]
     */
    fun getHandMaskRuleSecondary(skill: AllySkill, hand: Hand) = when (skill.type) {
        FIELD_MEDIC -> SingleDieHandMaskRule(hand, ALLY)
        PHYSICIAN -> SingleDieHandMaskRule(hand, ALLY)
        else -> throw AssertionError("Should not happen")
    }

    /**
     * Pick correct [PileMaskRule] for using [AllySkill], if applicable
     * @param skill Ally skill to use
     * @param pile Pile to operate on
     * @param power Rule level, where applicable
     * @return correct PileMaskRule
     */
    fun getPileMaskRule(skill: AllySkill, pile: Pile, power: Int) = when (skill.type) {
        INTERN -> AnyDicePileMaskRule(pile, power)
        PHYSICIAN -> AnyDicePileMaskRule(pile, power)
        else -> throw AssertionError("Should not happen")
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Check whether specified [Hero] can use specific [AllySkill] in current [GamePhase]
     * @param phase Current game phase
     * @param hero Hero to check
     * @param skill Ally skill type to check
     * @param opponentTraits List of traits of the encountered threat (if applicable), or null
     * @return true or false
     */
    private fun checkHeroCanUseAllySkill(phase: GamePhase, hero: Hero, location: Location,
                                         skill: AllySkill, opponentTraits: List<Trait>?) = when {
        skill.type !in (skills[phase] ?: setOf()) -> false
        !hero.isAlive -> false
        (!(handFiltersUser[skill.type]?.test(hero.hand) ?: true)) -> false
        (!(handFiltersTarget[skill.type]?.test(hero.hand) ?: true)) -> false
        opponentTraits != null && (traitsDenial[skill.type]?.any { it in opponentTraits } ?: false) -> false
        traitsRequire[skill.type] != null && !traitsRequire.getValue(skill.type).any {
            opponentTraits?.contains(it) ?: false
        } -> false
        else -> when (skill.type) {
            FALCON, MASTERFUL_DISTRACTION, SABOTEUR, SCRYER, TOUR_GUIDE -> location.isOpen && bagFilters[skill.type]?.test(location.bag) ?: false
            INTERN, PHYSICIAN -> bagFilters[skill.type]?.test(hero.discardPile) ?: false
            TRICKSTER -> bagFilters[skill.type]?.test(hero.bag) ?: false
            else -> true
        }
    }

    /**
     * Get list of [AllySkill]s specified [Hero] can use at current point of time
     * @param scenario Current scenario
     * @param phase Current game phase
     * @param hero Hero to check
     * @param location Location hero is placed in
     * @param opponentTraits List of traits of the encountered threat (if applicable), or null
     * @return list of skills
     */
    fun getAllySkillsHeroCanUse(scenario: Scenario, phase: GamePhase, hero: Hero, location: Location, opponentTraits: List<Trait>?) =
            scenario.getAllySkills().filter { checkHeroCanUseAllySkill(phase, hero, location, it, opponentTraits) }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Check whether pending modifier of specified ([AllySkill]) type is applicable in the current [GamePhase]
     * @param phase Current game phase
     * @param type Type of pending modifier (skill)
     * @return true or false
     */
    fun checkModifierApplicable(phase: GamePhase, type: AllySkill.Type) =
            pendingModifiers[phase]?.contains(type) ?: false

    /**
     * Return correct [DieBattleCheck.Method] when using specific ally skill
     * @param skill Ally skill to use
     * @param advancedVariant true for using advanced skill variant, false for regular variant
     * @return Method to use or null
     */
    fun getBattleCheckMethod(skill: AllySkill, advancedVariant: Boolean = false): DieBattleCheck.Method {
        val methods = battleCheckMethods[skill.type]
        return when (methods?.size) {
            1 -> methods[0]
            2 -> if (advancedVariant) methods[1] else methods[0]
            else -> throw AssertionError("This should never happen!")
        }
    }

    /**
     * Return correct [DieDistributor] when using specific ally skill
     * @param skill Ally skill to use
     * @param advancedVariant true for using advanced skill variant, false for regular variant
     * @return Method to use or null
     */
    fun getDieDistributor(skill: AllySkill, advancedVariant: Boolean = false): DieDistributor {
        val distributors = dieDistributors[skill.type]
        return when (distributors?.size) {
            1 -> distributors[0]
            2 -> if (advancedVariant) distributors[1] else distributors[0]
            else -> throw AssertionError("This should never happen!")
        }
    }


}