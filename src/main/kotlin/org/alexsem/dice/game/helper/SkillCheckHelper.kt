package org.alexsem.dice.game.helper

import org.alexsem.dice.game.DieBattleCheck
import org.alexsem.dice.game.DieBattleCheck.Method.*
import org.alexsem.dice.game.GamePhase
import org.alexsem.dice.game.distribute.*
import org.alexsem.dice.game.filter.bag.SingleAnyDieBagFilter
import org.alexsem.dice.game.filter.bag.SingleDieBagFilter
import org.alexsem.dice.game.filter.bag.SingleNotSpecificDieBagFilter
import org.alexsem.dice.game.filter.hand.*
import org.alexsem.dice.game.rule.hand.*
import org.alexsem.dice.game.rule.pile.AnyDicePileMaskRule
import org.alexsem.dice.game.rule.pile.PileMaskRule
import org.alexsem.dice.game.rule.pile.SingleTypePileMaskRule
import org.alexsem.dice.model.*
import org.alexsem.dice.model.Die.Type.*
import org.alexsem.dice.model.Skill.Type.*

/**
 * Helper class for [org.alexsem.dice.game.Game] to perform [Skill]-related checks
 */
object SkillCheckHelper {

    //------------------------------------------------------------------------------------------------------------------
    //--- Skills ---
    private val skillsBeforeExploration = setOf(
            BERSERK,
            READ_TRACES,
            LAY_TRAP,
            SELF_BANDAGE,
            ORIENTEERING,
            CONCENTRATION,
            TELEPORT,
            INVIGORATE,
            INGENUITY,
            CAUTERIZE,
            PRAYER,
            HEAL,
            FORESIGHT,
            BLESSING,
            DISCIPLINE,
            LORDS_GRACE,
            CELESTIAL_OMEN,
            SUMMON,
            PAST_EXPERIENCE,
            RESUSCITATION,
            EXPENDABLES
    )
    private val skillsEncounterEnemy = setOf(
            HIT,
            POWER_PUNCH,
            INTIMIDATE,
            WARCRY,
            SHOOT,
            STEALTH,
            VOLLEY,
            MAGIC_BOLT,
            INGENUITY,
            DEVASTATION,
            HOLY_LIGHT,
            NEGOTIATION
    )
    private val skillsEncounterVillain = setOf(
            HIT,
            POWER_PUNCH,
            INTIMIDATE,
            WARCRY,
            SHOOT,
            STEALTH,
            VOLLEY,
            MAGIC_BOLT,
            INGENUITY,
            DEVASTATION,
            HOLY_LIGHT,
            NEGOTIATION
    )
    private val skillsEncounterAlly = setOf(
            INTIMIDATE,
            SUBMIT,
            STEALTH,
            MIND_CONTROL,
            INGENUITY,
            SERMON,
            RECRUITMENT
    )
    private val skillsTakeDamage = setOf(
            DODGE,
            STONE_SKIN,
            EVASION,
            ELEMENTAL_SHIELD,
            WARP,
            INGENUITY,
            YOU_FIRST
    )
    private val skillsAfterExploration = setOf(
            BERSERK,
            READ_TRACES,
            SELF_BANDAGE,
            ORIENTEERING,
            CONCENTRATION,
            TELEPORT,
            INGENUITY,
            CAUTERIZE,
            HEAL,
            FORESIGHT,
            DISCIPLINE,
            LORDS_GRACE,
            CELESTIAL_OMEN,
            PAST_EXPERIENCE,
            RESUSCITATION,
            EXPENDABLES
    )
    //--- Assist ---
    private val assistBeforeExploration = setOf(
            LAY_TRAP,
            INVIGORATE,
            CAUTERIZE,
            HEAL,
            BLESSING,
            ERRAND
    )
    private val assistEncounterEnemy = setOf(
            WARCRY,
            INTERVENE,
            COVER_FIRE,
            GOOD_ADVICE
    )
    private val assistEncounterVillain = setOf(
            WARCRY,
            INTERVENE,
            COVER_FIRE,
            GOOD_ADVICE
    )
    private val assistEncounterAlly = setOf(
            GOOD_ADVICE
    )
    private val assistTakeDamage = setOf(
            PROTECT,
            ELEMENTAL_SHIELD
    )
    private val assistAfterExploration = setOf(
            CAUTERIZE,
            HEAL,
            ERRAND
    )

    private val skills = mapOf(
            GamePhase.LOCATION_BEFORE_EXPLORATION to skillsBeforeExploration,
            GamePhase.LOCATION_ENCOUNTER_ENEMY to skillsEncounterEnemy,
            GamePhase.LOCATION_ENCOUNTER_VILLAIN to skillsEncounterVillain,
            GamePhase.LOCATION_ENCOUNTER_ALLY to skillsEncounterAlly,
            GamePhase.HERO_TAKES_DAMAGE to skillsTakeDamage,
            GamePhase.LOCATION_AFTER_EXPLORATION to skillsAfterExploration
    )
    private val assist = mapOf(
            GamePhase.LOCATION_BEFORE_EXPLORATION to assistBeforeExploration,
            GamePhase.LOCATION_ENCOUNTER_ENEMY to assistEncounterEnemy,
            GamePhase.LOCATION_ENCOUNTER_VILLAIN to assistEncounterVillain,
            GamePhase.LOCATION_ENCOUNTER_ALLY to assistEncounterAlly,
            GamePhase.HERO_TAKES_DAMAGE to assistTakeDamage,
            GamePhase.LOCATION_AFTER_EXPLORATION to assistAfterExploration
    )

    //------------------------------------------------------------------------------------------------------------------
    private val handFiltersUser = mapOf(
            HIT to SingleDieHandFilter(PHYSICAL),
            DODGE to SingleDieHandFilter(SOMATIC),
            INTIMIDATE to SingleDieHandFilter(VERBAL),
            PROTECT to DoubleDieHandFilter(arrayOf(PHYSICAL), arrayOf(SOMATIC)),
            SUBMIT to DoubleDieHandFilter(arrayOf(PHYSICAL), arrayOf(VERBAL, MENTAL)),
            POWER_PUNCH to DoubleDieHandFilter(arrayOf(PHYSICAL), arrayOf(PHYSICAL, SOMATIC)),
            STONE_SKIN to DoubleDieHandFilter(arrayOf(PHYSICAL), arrayOf(PHYSICAL)),
            WARCRY to SingleDieHandFilter(VERBAL),
            BERSERK to SingleDieHandFilter(MENTAL),
            INTERVENE to DoubleDieHandFilter(arrayOf(PHYSICAL), arrayOf(PHYSICAL, SOMATIC)),
            SHOOT to SingleDieHandFilter(SOMATIC),
            EVASION to DoubleDieHandFilter(arrayOf(PHYSICAL), arrayOf(PHYSICAL, SOMATIC, MENTAL)),
            STEALTH to SingleDieHandFilter(SOMATIC, MENTAL),
            READ_TRACES to DoubleDieHandFilter(arrayOf(SOMATIC), arrayOf(MENTAL, VERBAL)),
            VOLLEY to SingleDieHandFilter(SOMATIC),
            COVER_FIRE to DoubleDieHandFilter(arrayOf(SOMATIC), arrayOf(PHYSICAL)),
            LAY_TRAP to DoubleDieHandFilter(arrayOf(MENTAL), arrayOf(SOMATIC)),
            SELF_BANDAGE to TripleDieHandFilter(arrayOf(PHYSICAL), arrayOf(SOMATIC), arrayOf(MENTAL)),
            ORIENTEERING to DoubleDieHandFilter(arrayOf(MENTAL), arrayOf(PHYSICAL, SOMATIC, MENTAL)),
            MAGIC_BOLT to DoubleDieHandFilter(arrayOf(MENTAL), arrayOf(MENTAL)),
            ELEMENTAL_SHIELD to DoubleDieHandFilter(arrayOf(MENTAL), arrayOf(SOMATIC, MENTAL)),
            WARP to DoubleDieHandFilter(arrayOf(MENTAL), arrayOf(SOMATIC)),
            MIND_CONTROL to DoubleDieHandFilter(arrayOf(MENTAL), arrayOf(VERBAL)),
            CONCENTRATION to TripleDieHandFilter(arrayOf(PHYSICAL), arrayOf(SOMATIC), arrayOf(VERBAL)),
            TELEPORT to DoubleDieHandFilter(arrayOf(MENTAL), arrayOf(MENTAL, VERBAL)),
            INVIGORATE to DoubleDieHandFilter(arrayOf(VERBAL), arrayOf(MENTAL)),
            INGENUITY to SingleNonWoundDieHandFilter(),
            CAUTERIZE to DoubleDieHandFilter(arrayOf(MENTAL), arrayOf(PHYSICAL)),
            DEVASTATION to TripleDieHandFilter(arrayOf(MENTAL), arrayOf(PHYSICAL, SOMATIC, MENTAL, VERBAL), arrayOf(PHYSICAL, SOMATIC, MENTAL, VERBAL)),
            HOLY_LIGHT to DoubleDieHandFilter(arrayOf(DIVINE), arrayOf(PHYSICAL, MENTAL, VERBAL)),
            SERMON to SingleDieHandFilter(VERBAL),
            HEAL to DoubleDieHandFilter(arrayOf(DIVINE), arrayOf(MENTAL, VERBAL)),
            FORESIGHT to DoubleDieHandFilter(arrayOf(MENTAL), arrayOf(DIVINE)),
            BLESSING to DoubleDieHandFilter(arrayOf(DIVINE), arrayOf(VERBAL)),
            DISCIPLINE to SingleDieHandFilter(PHYSICAL),
            LORDS_GRACE to FreeSpaceHandFilter(),
            CELESTIAL_OMEN to DoubleDieHandFilter(arrayOf(DIVINE), arrayOf(DIVINE)),
            NEGOTIATION to SingleDieHandFilter(VERBAL),
            RECRUITMENT to SingleDieHandFilter(MENTAL, VERBAL),
            SUMMON to SingleDieHandFilter(VERBAL),
            PAST_EXPERIENCE to SingleDieHandFilter(MENTAL),
            RESUSCITATION to SingleDieHandFilter(DIVINE),
            GOOD_ADVICE to DoubleDieHandFilter(arrayOf(VERBAL), arrayOf(MENTAL)),
            EXPENDABLES to SingleDieHandFilter(ALLY),
            YOU_FIRST to SingleDieHandFilter(ALLY),
            ERRAND to DoubleDieHandFilter(arrayOf(VERBAL, DIVINE, ALLY), arrayOf(PHYSICAL, SOMATIC, MENTAL, VERBAL, DIVINE, ALLY))
    )

    private val handFiltersTarget = mapOf(
            BERSERK to SingleDieHandFilter(WOUND),
            DISCIPLINE to SingleDieHandFilter(WOUND),
            CAUTERIZE to SingleDieHandFilter(WOUND),
            ERRAND to FreeSpaceHandFilter()
    )

    //------------------------------------------------------------------------------------------------------------------
    private val battleCheckMethods = mapOf(
            HIT to arrayOf(MAX),
            PROTECT to arrayOf(MAX),
            SUBMIT to arrayOf(MAX),
            POWER_PUNCH to arrayOf(MAX),
            STONE_SKIN to arrayOf(AVG_DOWN),
            BERSERK to arrayOf(MAX),
            WARCRY to arrayOf(MAX),
            INTERVENE to arrayOf(AVG_UP),
            SHOOT to arrayOf(MAX),
            EVASION to arrayOf(MIN),
            STEALTH to arrayOf(MAX),
            COVER_FIRE to arrayOf(AVG_UP),
            VOLLEY to arrayOf(MAX),
            LAY_TRAP to arrayOf(MIN),
            SELF_BANDAGE to arrayOf(MIN),
            ORIENTEERING to arrayOf(AVG_DOWN),
            MAGIC_BOLT to arrayOf(MIN, MAX),
            ELEMENTAL_SHIELD to arrayOf(AVG_UP),
            WARP to arrayOf(MIN),
            INVIGORATE to arrayOf(MIN),
            CAUTERIZE to arrayOf(MAX),
            MIND_CONTROL to arrayOf(AVG_UP, MAX),
            DEVASTATION to arrayOf(MAX),
            HOLY_LIGHT to arrayOf(AVG_UP),
            SERMON to arrayOf(MAX),
            HEAL to arrayOf(MIN),
            FORESIGHT to arrayOf(MAX),
            BLESSING to arrayOf(MIN),
            DISCIPLINE to arrayOf(MAX),
            CELESTIAL_OMEN to arrayOf(AVG_DOWN),
            NEGOTIATION to arrayOf(MAX),
            RECRUITMENT to arrayOf(MAX),
            RESUSCITATION to arrayOf(MAX),
            GOOD_ADVICE to arrayOf(MIN),
            EXPENDABLES to arrayOf(MAX),
            YOU_FIRST to arrayOf(MAX)
    )


    //------------------------------------------------------------------------------------------------------------------
    private val dieDistributors = mapOf(
            HIT to arrayOf(hideAllDieDistributor, discardAllDieDistributor),
            INTIMIDATE to arrayOf(hideAllDieDistributor),
            PROTECT to arrayOf(hideAllDieDistributor),
            SUBMIT to arrayOf(hideAllDieDistributor, discardSmallestPhysicalHideRestDieDistributor),
            POWER_PUNCH to arrayOf(hideAllDieDistributor, discardSmallestPhysicalHideRestDieDistributor),
            STONE_SKIN to arrayOf(discardSmallestPhysicalHideRestDieDistributor),
            WARCRY to arrayOf(discardAllDieDistributor),
            BERSERK to arrayOf(discardAllDieDistributor),
            INTERVENE to arrayOf(hideAllDieDistributor),
            SHOOT to arrayOf(hideAllDieDistributor, discardAllDieDistributor),
            EVASION to arrayOf(discardSmallestSomaticHideRestDieDistributor),
            STEALTH to arrayOf(hideAllDieDistributor),
            COVER_FIRE to arrayOf(hideAllDieDistributor),
            VOLLEY to arrayOf(hideAllDieDistributor),
            LAY_TRAP to arrayOf(discardSmallestMentalHideRestDieDistributor),
            SELF_BANDAGE to arrayOf(hideAllDieDistributor),
            ORIENTEERING to arrayOf(deterSmallestMentalDiscardRestDieDistributor),
            MAGIC_BOLT to arrayOf(hideAllDieDistributor, discardSmallestMentalHideRestDieDistributor),
            ELEMENTAL_SHIELD to arrayOf(discardSmallestMentalHideRestDieDistributor),
            WARP to arrayOf(discardSmallestMentalHideRestDieDistributor),
            INVIGORATE to arrayOf(hideAllDieDistributor),
            CAUTERIZE to arrayOf(discardSmallestMentalHideRestDieDistributor),
            MIND_CONTROL to arrayOf(hideAllDieDistributor, discardSmallestMentalHideRestDieDistributor),
            DEVASTATION to arrayOf(discardSmallestMentalHideRestDieDistributor),
            HOLY_LIGHT to arrayOf(discardSmallestDivineHideRestDieDistributor),
            SERMON to arrayOf(hideAllDieDistributor, discardAllDieDistributor),
            HEAL to arrayOf(discardSmallestDivineHideRestDieDistributor),
            FORESIGHT to arrayOf(discardAllDieDistributor),
            BLESSING to arrayOf(hideAllDieDistributor),
            DISCIPLINE to arrayOf(discardAllDieDistributor),
            CELESTIAL_OMEN to arrayOf(deterAllDieDistributor),
            NEGOTIATION to arrayOf(hideAllDieDistributor, discardAllDieDistributor),
            RECRUITMENT to arrayOf(hideAllDieDistributor, discardAllDieDistributor),
            RESUSCITATION to arrayOf(hideAllDieDistributor, discardAllDieDistributor),
            GOOD_ADVICE to arrayOf(hideAllDieDistributor),
            EXPENDABLES to arrayOf(deterAllDieDistributor),
            YOU_FIRST to arrayOf(discardAllDieDistributor)
            )

    //------------------------------------------------------------------------------------------------------------------
    private val bagFilters = mapOf(
            READ_TRACES to SingleAnyDieBagFilter(),
            LAY_TRAP to SingleAnyDieBagFilter(),
            SELF_BANDAGE to SingleAnyDieBagFilter(),
            ORIENTEERING to SingleNotSpecificDieBagFilter(VILLAIN),
            CONCENTRATION to SingleDieBagFilter(MENTAL),
            PRAYER to SingleDieBagFilter(DIVINE),
            HEAL to SingleAnyDieBagFilter(),
            FORESIGHT to SingleAnyDieBagFilter(),
            LORDS_GRACE to SingleNotSpecificDieBagFilter(DIVINE),
            SUMMON to SingleAnyDieBagFilter(),
            PAST_EXPERIENCE to SingleDieBagFilter(VERBAL),
            RESUSCITATION to SingleDieBagFilter(ALLY),
            EXPENDABLES to SingleAnyDieBagFilter()
    )

    //------------------------------------------------------------------------------------------------------------------
    @Suppress("BooleanLiteralArgument")
    private val assistRangeNearFar = mapOf(
            PROTECT to arrayOf(true, false),
            WARCRY to arrayOf(true, false),
            INTERVENE to arrayOf(true, false),
            COVER_FIRE to arrayOf(false, true),
            LAY_TRAP to arrayOf(true, false),
            ELEMENTAL_SHIELD to arrayOf(true, false),
            INVIGORATE to arrayOf(true, false),
            CAUTERIZE to arrayOf(true, false),
            HEAL to arrayOf(true, false),
            BLESSING to arrayOf(true, true),
            GOOD_ADVICE to arrayOf(true, false),
            ERRAND to arrayOf(true, true)
    )

    //------------------------------------------------------------------------------------------------------------------

    private val traitsDenial = mapOf(
            HIT to arrayOf(Trait.IMMUNE_PHYSICAL),
            DODGE to arrayOf(Trait.IMMUNE_REDUCTION),
            INTIMIDATE to arrayOf(Trait.IMMUNE_MODIFIERS),
            PROTECT to arrayOf(Trait.DAMAGE_MAGICAL),
            POWER_PUNCH to arrayOf(Trait.IMMUNE_PHYSICAL),
            STONE_SKIN to arrayOf(Trait.DAMAGE_MAGICAL, Trait.IMMUNE_REDUCTION),
            SHOOT to arrayOf(Trait.IMMUNE_PHYSICAL),
            EVASION to arrayOf(Trait.IMMUNE_REDUCTION),
            STEALTH to arrayOf(Trait.IMMUNE_AVOID_ENCOUNTER),
            COVER_FIRE to arrayOf(Trait.IMMUNE_PHYSICAL),
            LAY_TRAP to arrayOf(Trait.IMMUNE_MODIFIERS),
            VOLLEY to arrayOf(Trait.IMMUNE_PHYSICAL),
            MAGIC_BOLT to arrayOf(Trait.IMMUNE_MAGICAL),
            DEVASTATION to arrayOf(Trait.IMMUNE_MAGICAL),
            ELEMENTAL_SHIELD to arrayOf(Trait.IMMUNE_REDUCTION),
            WARP to arrayOf(Trait.IMMUNE_REDUCTION),
            HOLY_LIGHT to arrayOf(Trait.IMMUNE_MAGICAL),
            NEGOTIATION to arrayOf(Trait.IMMUNE_AVOID_ENCOUNTER),
            YOU_FIRST to arrayOf(Trait.IMMUNE_REDUCTION)
    )

    private val traitsRequire = mapOf(
            ELEMENTAL_SHIELD to arrayOf(Trait.DAMAGE_MAGICAL)
    )

    //------------------------------------------------------------------------------------------------------------------
    private val modifiersEncounterStat = setOf(
            INVIGORATE,
            BLESSING
    )
    private val modifiersEncounterAlly = setOf(
            BLESSING,
            GOOD_ADVICE
    )
    private val modifiersEncounterObstacle = setOf(
            SURVIVALIST,
            INVIGORATE,
            BLESSING
    )
    private val modifiersEncounterEnemy = setOf(
            WARCRY,
            LAY_TRAP,
            BLESSING,
            GOOD_ADVICE
    )
    private val modifiersEncounterVillain = setOf(
            WARCRY,
            LAY_TRAP,
            BLESSING,
            GOOD_ADVICE
    )
    private val modifiersClosingLocation = setOf(
            INVIGORATE,
            BLESSING
    )

    private val pendingModifiers = mapOf(
            GamePhase.LOCATION_ENCOUNTER_STAT to modifiersEncounterStat,
            GamePhase.LOCATION_ENCOUNTER_ALLY to modifiersEncounterAlly,
            GamePhase.LOCATION_ENCOUNTER_OBSTACLE to modifiersEncounterObstacle,
            GamePhase.LOCATION_ENCOUNTER_ENEMY to modifiersEncounterEnemy,
            GamePhase.LOCATION_ENCOUNTER_VILLAIN to modifiersEncounterVillain,
            GamePhase.LOCATION_CLOSE_ATTEMPT to modifiersClosingLocation
    )

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Pick correct [HandMaskRule] for using hero's [Skill] (primary usage)
     * @param skill Skill to use
     * @param hand Hero's hand
     * @return correct [HandMaskRule] or null
     */
    fun getHandMaskRulePrimary(skill: Skill, hand: Hand) = when (skill.type) {
        HIT -> SingleDieHandMaskRule(hand, PHYSICAL)
        DODGE -> SingleDieHandMaskRule(hand, SOMATIC)
        INTIMIDATE -> SingleDieHandMaskRule(hand, VERBAL)
        PROTECT -> DoubleDieHandMaskRule(hand, arrayOf(PHYSICAL), arrayOf(SOMATIC))
        SUBMIT -> DoubleDieHandMaskRule(hand, arrayOf(PHYSICAL), arrayOf(MENTAL, VERBAL))
        POWER_PUNCH -> DoubleDieHandMaskRule(hand, arrayOf(PHYSICAL), arrayOf(PHYSICAL, SOMATIC))
        STONE_SKIN -> DoubleDieHandMaskRule(hand, arrayOf(PHYSICAL), arrayOf(PHYSICAL))
        WARCRY -> SingleDieHandMaskRule(hand, VERBAL)
        BERSERK -> SingleDieHandMaskRule(hand, WOUND)
        INTERVENE -> DoubleDieHandMaskRule(hand, arrayOf(PHYSICAL), arrayOf(PHYSICAL, SOMATIC))
        SHOOT -> SingleDieHandMaskRule(hand, SOMATIC)
        EVASION -> DoubleDieHandMaskRule(hand, arrayOf(PHYSICAL), arrayOf(PHYSICAL, SOMATIC, MENTAL))
        READ_TRACES -> DoubleDieHandMaskRule(hand, arrayOf(SOMATIC), arrayOf(MENTAL, VERBAL))
        STEALTH -> SingleDieHandMaskRule(hand, SOMATIC, MENTAL)
        VOLLEY -> SingleDieHandMaskRule(hand, SOMATIC)
        COVER_FIRE -> DoubleDieHandMaskRule(hand, arrayOf(SOMATIC), arrayOf(PHYSICAL))
        LAY_TRAP -> DoubleDieHandMaskRule(hand, arrayOf(MENTAL), arrayOf(SOMATIC))
        SELF_BANDAGE -> TripleDieHandMaskRule(hand, arrayOf(PHYSICAL), arrayOf(SOMATIC), arrayOf(MENTAL))
        ORIENTEERING -> DoubleDieHandMaskRule(hand, arrayOf(MENTAL), arrayOf(PHYSICAL, SOMATIC, MENTAL))
        MAGIC_BOLT -> DoubleDieHandMaskRule(hand, arrayOf(MENTAL), arrayOf(MENTAL))
        ELEMENTAL_SHIELD -> DoubleDieHandMaskRule(hand, arrayOf(MENTAL), arrayOf(MENTAL))
        WARP -> DoubleDieHandMaskRule(hand, arrayOf(MENTAL), arrayOf(SOMATIC))
        MIND_CONTROL -> DoubleDieHandMaskRule(hand, arrayOf(MENTAL), arrayOf(VERBAL))
        CONCENTRATION -> TripleDieHandMaskRule(hand, arrayOf(PHYSICAL), arrayOf(SOMATIC), arrayOf(VERBAL))
        TELEPORT -> DoubleDieHandMaskRule(hand, arrayOf(MENTAL), arrayOf(MENTAL, VERBAL))
        INVIGORATE -> DoubleDieHandMaskRule(hand, arrayOf(VERBAL), arrayOf(MENTAL))
        INGENUITY -> MultipleNonWoundDiceHandMaskRule(hand, 1, skill.getModifier1Value())
        CAUTERIZE -> SingleDieHandMaskRule(hand, WOUND)
        DEVASTATION -> TripleDieHandMaskRule(hand, arrayOf(MENTAL), arrayOf(PHYSICAL, SOMATIC, MENTAL, VERBAL), arrayOf(PHYSICAL, SOMATIC, MENTAL, VERBAL))
        HOLY_LIGHT -> DoubleDieHandMaskRule(hand, arrayOf(DIVINE), arrayOf(PHYSICAL, MENTAL, VERBAL))
        SERMON -> SingleDieHandMaskRule(hand, VERBAL)
        HEAL -> DoubleDieHandMaskRule(hand, arrayOf(DIVINE), arrayOf(MENTAL, VERBAL))
        FORESIGHT -> SingleDieHandMaskRule(hand, MENTAL)
        BLESSING -> DoubleDieHandMaskRule(hand, arrayOf(DIVINE), arrayOf(VERBAL))
        DISCIPLINE -> SingleDieHandMaskRule(hand, WOUND)
        CELESTIAL_OMEN -> DoubleDieHandMaskRule(hand, arrayOf(DIVINE), arrayOf(DIVINE))
        NEGOTIATION -> SingleDieHandMaskRule(hand, VERBAL)
        RECRUITMENT -> SingleDieHandMaskRule(hand, MENTAL, VERBAL)
        SUMMON -> SingleDieHandMaskRule(hand, VERBAL)
        PAST_EXPERIENCE -> SingleDieHandMaskRule(hand, MENTAL)
        RESUSCITATION -> SingleDieHandMaskRule(hand, DIVINE)
        GOOD_ADVICE -> DoubleDieHandMaskRule(hand, arrayOf(VERBAL), arrayOf(MENTAL))
        EXPENDABLES -> SingleDieHandMaskRule(hand, ALLY)
        YOU_FIRST -> SingleDieHandMaskRule(hand, ALLY)
        ERRAND -> SingleDieHandMaskRule(hand, VERBAL, DIVINE, ALLY)
        else -> throw AssertionError("Should not happen")
    }

    /**
     * Pick correct [HandMaskRule] for using hero's [Skill] (subsequent usage, if applicable)
     * @param skill Skill to use
     * @param hand Hero's hand
     * @return correct [HandMaskRule]
     */
    fun getHandMaskRuleSecondary(skill: Skill, hand: Hand) = when (skill.type) {
        BERSERK -> SingleDieHandMaskRule(hand, MENTAL)
        VOLLEY -> SingleDieHandMaskRule(hand, PHYSICAL, SOMATIC)
        CAUTERIZE -> DoubleDieHandMaskRule(hand, arrayOf(MENTAL), arrayOf(PHYSICAL))
        FORESIGHT -> SingleDieHandMaskRule(hand, DIVINE)
        DISCIPLINE -> SingleDieHandMaskRule(hand, PHYSICAL)
        else -> throw AssertionError("Should not happen")
    }

    /**
     * Pick correct [PileMaskRule] for using hero's [Skill], if applicable
     * @param skill Skill to use
     * @param pile Pile to operate on
     * @param power Rule level, where applicable
     * @return correct [PileMaskRule]
     */
    fun getPileMaskRule(skill: Skill, pile: Pile, power: Int) = when (skill.type) {
        SELF_BANDAGE -> AnyDicePileMaskRule(pile, power)
        CONCENTRATION -> SingleTypePileMaskRule(pile, MENTAL, 1, power + 1)
        PRAYER -> SingleTypePileMaskRule(pile, DIVINE, 1, power)
        HEAL -> AnyDicePileMaskRule(pile, power)
        PAST_EXPERIENCE -> SingleTypePileMaskRule(pile, VERBAL, 1, power)
        RESUSCITATION -> SingleTypePileMaskRule(pile, ALLY, 1, power)
        else -> throw AssertionError("Should not happen")
    }

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Check whether specified [Hero] can use specific [Skill] in current [GamePhase]
     * @param phase Current game phase
     * @param hero Hero to check
     * @param skill Skill to check
     * @param opponentTraits List of traits of the encountered threat (if applicable), or null
     * @return true or false
     */
    private fun checkHeroCanUseSkill(phase: GamePhase, hero: Hero, location: Location,
                                     skill: Skill, opponentTraits: List<Trait>?) = when {
        skill.type !in (skills[phase] ?: setOf()) -> false
        !hero.isAlive -> false
        (!(handFiltersUser[skill.type]?.test(hero.hand) ?: true)) -> false
        (!(handFiltersTarget[skill.type]?.test(hero.hand) ?: true)) -> false
        !checkTraitsAllowToUseSkill(skill, opponentTraits) -> false
        else -> when (skill.type) {
            READ_TRACES -> location.isOpen && bagFilters[skill.type]?.test(location.bag) ?: false
            LAY_TRAP -> location.isOpen && bagFilters[skill.type]?.test(location.bag) ?: false
            SELF_BANDAGE -> bagFilters[skill.type]?.test(hero.discardPile) ?: false
            ORIENTEERING -> location.isOpen && SpecialRule.TOO_DANGEROUS_HERE !in location.getSpecialRules() && bagFilters[skill.type]?.test(location.bag) ?: false
            CONCENTRATION -> bagFilters[skill.type]?.test(hero.discardPile) ?: false
            PRAYER -> bagFilters[skill.type]?.test(hero.discardPile) ?: false
            HEAL -> bagFilters[skill.type]?.test(hero.discardPile) ?: false
            FORESIGHT -> location.isOpen && bagFilters[skill.type]?.test(location.bag) ?: false
            LORDS_GRACE -> bagFilters[skill.type]?.test(hero.bag) ?: false
            SUMMON -> hero.hand.dieCount < MAX_HAND_SIZE && (bagFilters[skill.type]?.test(hero.bag) ?: false)
            RESUSCITATION -> bagFilters[skill.type]?.test(hero.discardPile) ?: false
            EXPENDABLES -> location.isOpen && bagFilters[skill.type]?.test(location.bag) ?: false
            else -> true
        }
    }

    /**
     * Check whether hero can be assisted by other hero with specified skill
     * @param phase Current game phase
     * @param currentHero Hero whose turn is
     * @param assistingHero Hero who is trying to assist
     * @param isSameLocation true if heroes locate at the same location, false otherwise
     * @param skill Skill to check
     * @return true or false
     */
    private fun checkHeroCanAssistWithSkill(phase: GamePhase, currentHero: Hero, assistingHero: Hero, isSameLocation: Boolean,
                                            skill: Skill, opponentTraits: List<Trait>?) = when {
        skill.type !in (assist[phase] ?: setOf()) -> false
        !currentHero.isAlive -> false
        !assistingHero.isAlive -> false
        (!(handFiltersUser[skill.type]?.test(assistingHero.hand) ?: true)) -> false
        (!(handFiltersTarget[skill.type]?.test(currentHero.hand) ?: true)) -> false
        opponentTraits != null && (traitsDenial[skill.type]?.any { it in opponentTraits } ?: false) -> false
        traitsRequire[skill.type] != null && !traitsRequire.getValue(skill.type).any {
            opponentTraits?.contains(it) ?: false
        } -> false
        isSameLocation && !(assistRangeNearFar[skill.type]?.get(0) ?: false) -> false
        !isSameLocation && !(assistRangeNearFar[skill.type]?.get(1) ?: false) -> false
        else -> when (skill.type) {
            HEAL -> bagFilters[skill.type]?.test(currentHero.discardPile) ?: false
            else -> true
        }
    }

    /**
     * Get list of [Skill]s specified [Hero] can use at current point of time
     * @param phase Current game phase
     * @param hero Hero to check
     * @param location Location hero is placed in
     * @param opponentTraits List of traits of the encountered threat (if applicable), or null
     * @return list of skills
     */
    fun getSkillsHeroCanUse(phase: GamePhase, hero: Hero, location: Location, opponentTraits: List<Trait>?) =
            hero.getSkills().filter { checkHeroCanUseSkill(phase, hero, location, it, opponentTraits) }

    /**
     * Get list of [Skill]s hero can be assisted with by other hero
     * @param phase Current game phase
     * @param currentHero Hero whose turn is
     * @param assistingHero Hero who is trying to assist
     * @param isSameLocation true if heroes locate at the same location, false otherwise
     * @return true or false
     */
    fun getSkillsHeroCanAssistWith(phase: GamePhase, currentHero: Hero, assistingHero: Hero, isSameLocation: Boolean, opponentTraits: List<Trait>?) =
            assistingHero.getSkills().filter { checkHeroCanAssistWithSkill(phase, currentHero, assistingHero, isSameLocation, it, opponentTraits) }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Check whether pending modifier of specified ([Skill]) type is applicable in the current [GamePhase]
     * @param phase Current game phase
     * @param type Type of pending modifier (skill)
     * @return true or false
     */
    fun checkModifierApplicable(phase: GamePhase, type: Skill.Type) =
            pendingModifiers[phase]?.contains(type) ?: false

    /**
     * Checks whether current skill can be used when encountered [Threat] with specific [Trait]s
     * @param skill Skill to check
     * @param traits List of traits
     * @return true is skill can be used, false otherwise
     */
    fun checkTraitsAllowToUseSkill(skill: Skill, traits: List<Trait>?) = when {
        traits != null && traitsDenial[skill.type]?.any { it in traits } ?: false -> false
        traitsRequire[skill.type] != null && !traitsRequire.getValue(skill.type).any { traits?.contains(it) ?: false } -> false
        else -> true
    }

    /**
     * Return correct [DieBattleCheck.Method] when using specific skill
     * @param skill Skill to use
     * @param advancedVariant true for using advanced skill variant, false for regular variant
     * @return Method to use or null
     */
    fun getBattleCheckMethod(skill: Skill, advancedVariant: Boolean = false): DieBattleCheck.Method {
        val methods = battleCheckMethods[skill.type]
        return when (methods?.size) {
            1 -> methods[0]
            2 -> if (advancedVariant) methods[1] else methods[0]
            else -> throw AssertionError("This should never happen!")
        }
    }

    /**
     * Return correct [DieDistributor] when using specific skill
     * @param skill Skill to use
     * @param advancedVariant true for using advanced skill variant, false for regular variant
     * @return Method to use or null
     */
    fun getDieDistributor(skill: Skill, advancedVariant: Boolean = false): DieDistributor {
        val distributors = dieDistributors[skill.type]
        return when (distributors?.size) {
            1 -> distributors[0]
            2 -> if (advancedVariant) distributors[1] else distributors[0]
            else -> throw AssertionError("This should never happen!")
        }
    }


}