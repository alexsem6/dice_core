package org.alexsem.dice.game.helper

import org.alexsem.dice.game.filter.hand.EitherOfTwoDiceObstacleHandFilter
import org.alexsem.dice.game.filter.hand.SpecificDieObstacleHandFilter
import org.alexsem.dice.game.filter.hand.TwoSpecificDiceObstacleHandFilter
import org.alexsem.dice.game.rule.hand.EitherOfTwoDiceObstacleHandMaskRule
import org.alexsem.dice.game.rule.hand.HandMaskRule
import org.alexsem.dice.game.rule.hand.SpecificDieObstacleHandMaskRule
import org.alexsem.dice.game.rule.hand.TwoSpecificDiceObstacleHandMaskRule
import org.alexsem.dice.model.Hand
import org.alexsem.dice.model.Hero
import org.alexsem.dice.model.Obstacle

/**
 * Helper class for [org.alexsem.dice.game.Game] to perform [Obstacle]-related checks
 */
object ObstacleCheckHelper {

    private fun getObstacleHandFilter(obstacle: Obstacle) = when (obstacle.tier) {
        1 -> EitherOfTwoDiceObstacleHandFilter(obstacle.dieTypes[0], obstacle.dieTypes[1])
        2 -> SpecificDieObstacleHandFilter(obstacle.dieTypes[0])
        3 -> TwoSpecificDiceObstacleHandFilter(obstacle.dieTypes[0], obstacle.dieTypes[1])
        else -> throw AssertionError("Should not happen")
    }

    /**
     * Checks whether specified [Hero] has means to attempt the check to defeat [Obstacle]
     * @param hero Hero to check
     * @param obstacle Obstacle to check for
     * @return true or false
     */
    fun checkHeroCanAttemptObstacleCheck(hero: Hero, obstacle: Obstacle) =
            hero.isAlive && getObstacleHandFilter(obstacle).test(hero.hand)

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Pick correct [HandMaskRule] for passing check to defeat [Obstacle]
     * @param obstacle Obstacle to defeat
     * @param hand Hero's hand
     * @return correct HandMaskRule or null
     */
    fun getObstacleHandMaskRule(obstacle: Obstacle, hand: Hand) = when (obstacle.tier) {
        1 -> EitherOfTwoDiceObstacleHandMaskRule(hand, obstacle.dieTypes[0], obstacle.dieTypes[1])
        2 -> SpecificDieObstacleHandMaskRule(hand, obstacle.dieTypes[0])
        3 -> TwoSpecificDiceObstacleHandMaskRule(hand, obstacle.dieTypes[0], obstacle.dieTypes[1])
        else -> throw AssertionError("Should not happen")
    }

}