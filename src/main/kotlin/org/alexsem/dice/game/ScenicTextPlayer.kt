package org.alexsem.dice.game

import org.alexsem.dice.ui.Action
import org.alexsem.dice.ui.UI
import org.alexsem.dice.ui.audio.Audio
import org.alexsem.dice.ui.audio.Music
import org.alexsem.dice.ui.audio.Sound
import org.alexsem.dice.model.Scenario
import org.alexsem.dice.ui.ActionList

class ScenicTextPlayer(ui: UI, private val scenario: Scenario, private val isIntro: Boolean) {

    private val renderer = ui.gameRenderer
    private val interactor = ui.gameInteractor

    private var currentPage: Int = 1
    private var pageCount: Int = 1
    private var actions: ActionList = ActionList.EMPTY


    /**
     * Start interaction
     */
    fun start() {
        Audio.playSound(Sound.PAGE_NEXT)
        Audio.playMusic(when (isIntro) {
            true -> Music.SCENARIO_INTRO
            false -> Music.SCENARIO_OUTRO
        })
        currentPage = 1
        pageCount = renderer.calculateScenarioScenicTextPageCount(scenario, !isIntro)
        actions = ActionList()
        if (pageCount > 1) {
            actions.add(Action.Type.INFO_PAGE_DOWN, currentPage < pageCount)
            actions.add(Action.Type.INFO_PAGE_UP, currentPage > 1)
        }
        actions.add(Action.Type.CONFIRM)
        processCycle()
    }

    /**
     * Move to the next info page (if possible)
     */
    private fun pageDown() {
        if (currentPage < pageCount) {
            Audio.playSound(Sound.PAGE_NEXT)
            currentPage++
            actions[Action.Type.INFO_PAGE_DOWN]?.isEnabled = (currentPage < pageCount)
            actions[Action.Type.INFO_PAGE_UP]?.isEnabled = (currentPage > 1)
        }
    }

    /**
     * Move to the previous info page (if possible)
     */
    private fun pageUp() {
        if (pageCount > 1) {
            Audio.playSound(Sound.PAGE_PREVIOUS)
            currentPage--
            actions[Action.Type.INFO_PAGE_DOWN]?.isEnabled = (currentPage < pageCount)
            actions[Action.Type.INFO_PAGE_UP]?.isEnabled = (currentPage > 1)
        }
    }

    //==================================================================================================================

    /**
     * Draw script
     */
    private fun processCycle() {
        while (true) {
            drawScreen()
            when (interactor.pickAction(actions).type) {
                Action.Type.INFO_PAGE_DOWN -> pageDown()
                Action.Type.INFO_PAGE_UP -> pageUp()
                Action.Type.CONFIRM -> {
                    Audio.stopMusic()
                    return
                }
                else -> throw AssertionError("Should not happen")
            }
        }
    }

    //==================================================================================================================

    /**
     * Draws current screen
     */
    private fun drawScreen() {
        renderer.drawScenarioScenicText(scenario, !isIntro, currentPage, pageCount, actions)
    }


}