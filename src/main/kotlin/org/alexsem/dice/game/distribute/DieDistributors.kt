package org.alexsem.dice.game.distribute

import org.alexsem.dice.game.DieBattleCheck
import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Die.Type.*
import org.alexsem.dice.game.DiePair
import org.alexsem.dice.model.Hero
import org.alexsem.dice.model.Pile
import org.alexsem.dice.model.Skill.Type.DIVINE_AID

/**
 * Special action that regulates the way dice participated in [DieBattleCheck] are distributed as a result
 */
typealias DieDistributor = (DieBattleCheck, Hero, deterrentPile: Pile?) -> Unit

val hideAllDieDistributor: DieDistributor = { dbc, hero, _ -> uniform(dbc) { hide(hero, it) } }
val discardAllDieDistributor: DieDistributor = { dbc, hero, _ -> uniform(dbc) { discard(hero, it) } }
val deterAllDieDistributor: DieDistributor = { dbc, hero, pile -> uniform(dbc) { deter(hero, pile, it) } }
val removeAllDieDistributor: DieDistributor = { _, _, _ -> }
val deterDivineHideRestDieDistributor: DieDistributor = { dbc, hero, pile ->
    uniform(dbc) {
        when (it.type) {
            DIVINE -> deter(hero, pile, it)
            else -> hide(hero, it)
        }
    }
}
val deterDivineDiscardRestDieDistributor: DieDistributor = { dbc, hero, pile ->
    uniform(dbc) {
        when (it.type) {
            DIVINE -> deter(hero, pile, it)
            else -> discard(hero, it)
        }
    }
}
val discardSmallestHideRestDieDistributor: DieDistributor =
        { dbc, hero, _ -> discardSmallestAnyHideRest(dbc, hero) }
val discardSmallestPhysicalHideRestDieDistributor: DieDistributor =
        { dbc, hero, _ -> discardSmallestSpecificHideRest(dbc, hero, PHYSICAL) }
val discardSmallestSomaticHideRestDieDistributor: DieDistributor =
        { dbc, hero, _ -> discardSmallestSpecificHideRest(dbc, hero, SOMATIC) }
val discardSmallestMentalHideRestDieDistributor: DieDistributor =
        { dbc, hero, _ -> discardSmallestSpecificHideRest(dbc, hero, MENTAL) }
val discardSmallestVerbalHideRestDieDistributor: DieDistributor =
        { dbc, hero, _ -> discardSmallestSpecificHideRest(dbc, hero, VERBAL) }
val discardSmallestDivineHideRestDieDistributor: DieDistributor =
        { dbc, hero, _ -> discardSmallestSpecificHideRest(dbc, hero, DIVINE) }
val deterSmallestMentalDiscardRestDieDistributor: DieDistributor =
        { dbc, hero, pile -> deterSmallestSpecificDiscardRest(dbc, hero, pile ?: Pile(), MENTAL) }

//----------------------------------------------------------------------------------------------------------------------

/**
 * Simplified distribution method for applying the same procedure to each die
 * @param battleCheck Battle check that was performed
 * @param action Procedure to apply to each die
 */
private fun uniform(battleCheck: DieBattleCheck, action: (d: Die) -> Unit) {
    (0 until battleCheck.heroPairCount).map(battleCheck::getHeroPairAt).map(DiePair::die).forEach(action)
}

/**
 * Find index of the smallest [Die] of any type
 * @param battleCheck Battle check to search
 *
 */
private fun smallestIndex(battleCheck: DieBattleCheck) =
        (0 until battleCheck.heroPairCount).minBy { battleCheck.getHeroPairAt(it).die.size } ?: -1

/**
 * Find index of the smallest [Die] of specified type in [DieBattleCheck]
 * @param battleCheck Battle check to search
 * @type Required die type
 *
 */
private fun smallestIndexByType(battleCheck: DieBattleCheck, type: Die.Type) =
        (0 until battleCheck.heroPairCount)
                .filter { battleCheck.getHeroPairAt(it).die.type == type }
                .minBy { battleCheck.getHeroPairAt(it).die.size } ?: -1

/**
 * Discard smallest [Die] of specific type in [DieBattleCheck] and hide the rest
 * @param battleCheck Battle check that was performed
 * @param hero Hero who performed the check
 * @param type Die type
 */
private fun discardSmallestSpecificHideRest(battleCheck: DieBattleCheck, hero: Hero, type: Die.Type) {
    val smallestIndex = smallestIndexByType(battleCheck, type)
    if (smallestIndex > -1) {
        discard(hero, battleCheck.getHeroPairAt(smallestIndex).die)
    }
    (0 until battleCheck.heroPairCount)
            .filter { it != smallestIndex }
            .map { battleCheck.getHeroPairAt(it).die }
            .forEach { hide(hero, it) }
}

/**
 * Deter smallest [Die] of specific type in [DieBattleCheck] and discard the rest
 * @param battleCheck Battle check that was performed
 * @param hero Hero who performed the check
 * @param type Die type
 * @param deterrentPile Pile to deter to
 */
private fun deterSmallestSpecificDiscardRest(battleCheck: DieBattleCheck, hero: Hero, deterrentPile: Pile, type: Die.Type) {
    val smallestIndex = smallestIndexByType(battleCheck, type)
    if (smallestIndex > -1) {
        deter(hero, deterrentPile, battleCheck.getHeroPairAt(smallestIndex).die)
    }
    (0 until battleCheck.heroPairCount)
            .filter { it != smallestIndex }
            .map { battleCheck.getHeroPairAt(it).die }
            .forEach { discard(hero, it) }
}

/**
 * Discard smallest [Die] of specific type in [DieBattleCheck] and hide the rest
 * @param battleCheck Battle check that was performed
 * @param hero Hero who performed the check
 */
private fun discardSmallestAnyHideRest(battleCheck: DieBattleCheck, hero: Hero) {
    val smallestIndex = smallestIndex(battleCheck)
    if (smallestIndex > -1) {
        discard(hero, battleCheck.getHeroPairAt(smallestIndex).die)
    }
    (0 until battleCheck.heroPairCount)
            .filter { it != smallestIndex }
            .map { battleCheck.getHeroPairAt(it).die }
            .forEach { hide(hero, it) }
}

private fun hide(hero: Hero, die: Die) = hero.bag.put(die)
private fun discard(hero: Hero, die: Die) = hero.discardPile.put(die)
private fun deter(hero: Hero, pile: Pile?, die: Die) = if (hero.hasSkill(DIVINE_AID)) discard(hero, die) else pile?.put(die)

