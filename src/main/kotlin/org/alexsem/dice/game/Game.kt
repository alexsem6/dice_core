package org.alexsem.dice.game

import org.alexsem.dice.game.distribute.*
import org.alexsem.dice.game.filter.bag.SingleAnyDieBagFilter
import org.alexsem.dice.game.filter.bag.SingleNonAllyDieBagFilter
import org.alexsem.dice.game.filter.hand.*
import org.alexsem.dice.game.helper.AllySkillCheckHelper
import org.alexsem.dice.game.helper.ObstacleCheckHelper
import org.alexsem.dice.game.helper.SkillCheckHelper
import org.alexsem.dice.game.helper.SpecialRuleCheckHelper
import org.alexsem.dice.game.rule.hand.*
import org.alexsem.dice.game.rule.pile.AnyDicePileMaskRule
import org.alexsem.dice.game.rule.pile.PileMaskRule
import org.alexsem.dice.generator.generateDie
import org.alexsem.dice.generator.template.die.SingleDieTypeFilter
import org.alexsem.dice.generator.template.die.StatsDieTypeFilter
import org.alexsem.dice.model.*
import org.alexsem.dice.model.AllySkill.Type.*
import org.alexsem.dice.model.Skill.Type.*
import org.alexsem.dice.model.SpecialRule.*
import org.alexsem.dice.ui.Action
import org.alexsem.dice.ui.Action.Type.*
import org.alexsem.dice.ui.ActionList
import org.alexsem.dice.ui.StatusMessage
import org.alexsem.dice.ui.StatusMessage.*
import org.alexsem.dice.ui.UI
import org.alexsem.dice.ui.audio.Audio
import org.alexsem.dice.ui.audio.Music
import org.alexsem.dice.ui.audio.Sound
import org.alexsem.dice.ui.input.GameInteractor
import org.alexsem.dice.ui.render.GameRenderer
import org.alexsem.dice.ui.storage.GameInfo
import org.alexsem.dice.ui.storage.Storage
import kotlin.math.max
import kotlin.math.min

typealias GameCallback = (isWon: Game.Result, heroes: List<Hero>, deterrentPile: Pile, phaseIndex: Int) -> Unit

/**
 * All data related to current game. Processes gaming cycle.
 */
@Suppress("ForEachParameterNotUsed")
class Game {

    /**
     * Different game outcomes
     */
    enum class Result { WIN, LOSE, EXIT }

    /**
     * Main constructor for starting a new game
     * @param ui User interaction class
     * @param partyId Identifier of a current party
     * @param phaseIndex Index of the current adventure phase
     * @param scenario Scenario to start
     * @param locations Locations to play in
     * @param heroes Heroes to use
     * @param callback Callback to invoke when game finishes
     */
    constructor(ui: UI, partyId: String, phaseIndex: Int, scenario: Scenario, locations: List<Location>, heroes: List<Hero>, callback: GameCallback) {
        this.renderer = ui.gameRenderer
        this.interactor = ui.gameInteractor
        this.storage = ui.storage
        this.realTimeSave = RealTimeSave(storage)

        this.partyId = partyId
        this.phaseIndex = phaseIndex
        this.scenario = scenario
        this.locations = locations
        this.heroes = heroes
        this.callback = callback
    }

    /**
     * Additional constructor for resuming previously unfinished game
     * @param ui User interaction class
     * @param info Stored game info
     * @param callback Callback to invoke when game finishes
     */
    constructor(ui: UI, info: GameInfo, callback: GameCallback) {
        this.renderer = ui.gameRenderer
        this.interactor = ui.gameInteractor
        this.storage = ui.storage
        this.realTimeSave = RealTimeSave(storage)

        this.partyId = info.partyId
        this.phaseIndex = info.phaseIndex
        this.scenario = info.scenario
        this.heroes = info.heroes
        this.locations = info.locations
        this.callback = callback

        this.timer = info.timer
        this.currentHeroIndex = info.currentHeroIndex
        this.currentHero = heroes[info.currentHeroIndex]
        this.currentLocation = locations[info.currentLocationIndex]
        info.deterrentPile.examine().forEach { this.deterrentPile.put(it) }
        info.heroPlacement.forEach { k, v -> this.heroPlacement[locations[k]] = v.map { heroes[it] }.toMutableList() }

        this.traveledThisTurn = info.traveledThisTurn
        this.gaveDieThisTurn = info.gaveDieThisTurn
        this.exploredThisTurn = info.exploredThisTurn
        this.exploredAgainThisTurn = info.exploredAgainThisTurn
        this.attemptedCloseThisTurn = info.attemptedCloseThisTurn
        this.encounteredDie = info.encounteredDie
        this.encounteredThreat = info.encounteredThreat
        info.pendingHeroModifiers1.forEach { this.pendingHeroModifiers1.add(it) }
        info.pendingHeroModifiers2.forEach { this.pendingHeroModifiers2.add(it) }
        info.pendingEncounterModifiers.forEach { this.pendingEncounterModifiers.add(it) }

        this.phase = info.phase
        this.currentMusic = info.currentMusic

        this.screen = info.screen
        this.statusMessage = info.statusMessage
        this.actions = info.actions
    }


    //------------------------------------------------------------------------------------------------------------------

    //UI
    private val renderer: GameRenderer
    private val interactor: GameInteractor
    private val storage: Storage
    private val realTimeSave: RealTimeSave

    //Main data
    private val partyId: String
    private val phaseIndex: Int
    private val scenario: Scenario
    private val locations: List<Location>
    private val heroes: List<Hero>
    private val callback: GameCallback

    //Current game state
    private var timer = 0
    private var currentHeroIndex = -1
    private lateinit var currentHero: Hero
    private lateinit var currentLocation: Location
    private val deterrentPile = Pile()
    private val heroPlacement = mutableMapOf<Location, MutableList<Hero>>()

    //Turn details
    private var traveledThisTurn = false
    private var gaveDieThisTurn = false
    private var exploredThisTurn = false
    private var exploredAgainThisTurn = false
    private var attemptedCloseThisTurn = false
    private var encounteredDie: DiePair? = null
    private var encounteredThreat: Threat? = null
    private var battleCheck: DieBattleCheck? = null
    private var currentDamage: Damage? = null
    private val pendingHeroModifiers1 = mutableListOf<PendingSkillModifier>()
    private val pendingHeroModifiers2 = mutableListOf<PendingAllyModifier>()
    private val pendingEncounterModifiers = mutableListOf<PendingSkillModifier>()

    //Hand and pile operation details
    private val activeHandPositions = HandMask()
    private val pickedHandPositions = HandMask()
    private val activePilePositions = PileMask()
    private val pickedPilePositions = PileMask()

    //Current game phase
    private var phase: GamePhase = GamePhase.SCENARIO_START
    private var currentMusic: Music? = null

    //Dialogs
    private var heroSelectionDialog: List<Hero> = emptyList()
    private var locationSelectionDialog: List<Location> = emptyList()
    private var skillSelectionDialog: List<Skill> = emptyList()
    private var allySkillSelectionDialog: List<AllySkill> = emptyList()
    private var pilePickDialog: Pile? = null

    //Info screens
    private var infoHero: Hero? = null
    private var infoLocation: Location? = null
    private var infoCurrentPage: Int = 1
    private var infoPageCount: Int = 1

    //Travel screen
    private var travelDestination: Location? = null
    private var travelProgress: Int = 0

    //Display details
    private var screen = GameScreen.SCENARIO_INTRO
    private var statusMessage = EMPTY
    private var actions: ActionList = ActionList.EMPTY

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Start gaming cycle
     */
    fun start() {
        if (heroes.isEmpty()) throw IllegalStateException("Heroes list is empty!")
        if (locations.isEmpty()) throw IllegalStateException("Location list is empty!")
        realTimeSave.start()

        if (phase == GamePhase.SCENARIO_START) { //New game
            savedStates.clear()

            heroes.forEach { it.isAlive = true }
            timer = scenario.initialTimer

            //Place all heroes onto the first location
            locations.forEach { location -> heroPlacement[location] = mutableListOf() }
            heroPlacement[locations[0]]?.addAll(heroes)

            //Draw initial hand for each hero
            heroes.forEach(::drawInitialHand)

            //Apply special rules
            phase = GamePhase.SCENARIO_START
            applyScenarioSpecialRules()
            locations.forEach(::applyLocationSpecialRules)

            //First hero turn
            currentHeroIndex = -1
            changePhaseScenarioIntro()
        } else { //Resuming game
            currentMusic?.let { Audio.playMusic(it) }
        }

        processCycle()
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Pick some dice from hand
     * @param rule        Rule to check selection against
     * @param allowCancel true to add CANCEL action
     * @param onEachLoop  some actions to be performed each time user selects/deselects a die
     * @return true if selection was confirmed, false if canceled
     */
    private fun pickDiceFromHand(rule: HandMaskRule, allowCancel: Boolean = true, onEachLoop: (() -> Unit)? = null): Boolean {
        //Preparations
        pickedHandPositions.clear()
        actions = ActionList().add(CONFIRM, false)
        if (allowCancel) {
            actions.add(CANCEL)
        }
        val hand = rule.hand
        while (true) {
            //Recurring action
            onEachLoop?.invoke()
            //Define success condition
            val canProceed = rule.checkMask(pickedHandPositions)
            actions[CONFIRM]?.isEnabled = canProceed
            //Prepare active hand commands
            activeHandPositions.clear()
            (0 until hand.dieCount)
                    .filter { rule.isPositionActive(pickedHandPositions, it) }
                    .forEach { activeHandPositions.addPosition(it) }
            (0 until hand.allyDieCount)
                    .filter { rule.isAllyPositionActive(pickedHandPositions, it) }
                    .forEach { activeHandPositions.addAllyPosition(it) }
            //Draw current phase
            drawScreen()
            //Process interaction result
            val result = interactor.pickDiceFromHand(activeHandPositions, actions)
            when (result.type) {
                CONFIRM -> if (canProceed) {
                    activeHandPositions.clear()
                    return true
                }
                CANCEL -> if (allowCancel) {
                    activeHandPositions.clear()
                    pickedHandPositions.clear()
                    return false
                }
                HAND_POSITION -> {
                    Audio.playSound(Sound.DIE_PICK)
                    pickedHandPositions.switchPosition(result.data)
                }
                HAND_ALLY_POSITION -> {
                    Audio.playSound(Sound.DIE_PICK)
                    pickedHandPositions.switchAllyPosition(result.data)
                }
                else -> throw AssertionError("Should not happen")
            }
        }
    }

    /**
     * Take some dice from some pile
     * @param rule        Rule to check selection against
     * @param allowCancel true to add CANCEL action
     */
    private fun pickDiceFromPile(rule: PileMaskRule, allowCancel: Boolean = true): Boolean {
        //Preparations
        pickedPilePositions.clear()
        actions = ActionList().add(CONFIRM, false)
        if (allowCancel) {
            actions.add(CANCEL)
        }
        val dice = rule.dice

        while (true) {
            //Define success condition
            val canProceed = rule.checkMask(pickedPilePositions)
            actions[CONFIRM]?.isEnabled = canProceed

            //Prepare active pile commands
            activePilePositions.clear()
            (0 until dice.size)
                    .filter { rule.isPositionActive(pickedPilePositions, it) }
                    .forEach { activePilePositions.addPosition(it) }

            //Draw current phase
            drawScreen()

            //Process interaction result
            val result = interactor.pickDiceFromPile(activePilePositions, actions)
            when (result.type) {
                CONFIRM -> if (canProceed) {
                    activePilePositions.clear()
                    return true
                }
                CANCEL -> {
                    if (allowCancel) {
                        activePilePositions.clear()
                        pickedPilePositions.clear()
                        return false
                    }
                    Audio.playSound(Sound.DIE_PICK)
                    pickedPilePositions.switchPosition(result.data)
                }
                PILE_POSITION -> {
                    Audio.playSound(Sound.DIE_PICK)
                    pickedPilePositions.switchPosition(result.data)
                }
                else -> throw AssertionError("Should not happen")
            }
        }
    }

    /**
     * Simplified routine for picking action from the list
     * @param message Status message to show
     * @param actions List of possible Action types
     * @return type of picked action
     */
    private fun quickActionPick(message: StatusMessage, vararg actions: Action.Type): Action.Type {
        saveState()
        //Show message to user
        this.statusMessage = message
        this.actions = actions.fold(ActionList()) { al, a -> al.add(a); al }
        drawScreen()
        //Interact with user
        val result = interactor.pickAction(this.actions).type
        restoreState()
        return result
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Select hero from some pre-defined list
     * @param heroes  List of heroes
     * @param message Status message
     * @return picked hero, or null if canceled
     */
    private fun selectHeroFromList(heroes: List<Hero>, message: StatusMessage): Hero? {
        saveState()
        //Show dialog
        screen = GameScreen.DIALOG_HERO_SELECT
        statusMessage = message
        actions = ActionList().add(CANCEL)
        heroSelectionDialog = heroes
        var result: Hero? = null
        drawScreen()
        //Process input
        val pickedAction = interactor.pickItemFromList(heroSelectionDialog.size)
        if (pickedAction.type == LIST_ITEM) {
            result = heroSelectionDialog[pickedAction.data]
        }
        restoreState()
        //Return result
        heroSelectionDialog = emptyList()
        return result
    }

    /**
     * Select location from some pre-defined list
     * @param locations List of locations
     * @param message   Status message
     * @param allowCancel true to allow canceling, false otherwise
     * @return picked location, or null if canceled
     */
    private fun selectLocationFromList(locations: List<Location>, message: StatusMessage, allowCancel: Boolean = true): Location? {
        saveState()
        //Show dialog
        screen = GameScreen.DIALOG_LOCATION_SELECT
        statusMessage = message
        actions = ActionList()
        if (allowCancel) {
            actions.add(CANCEL)
        }
        locationSelectionDialog = locations
        var result: Location? = null
        drawScreen()
        //Process input
        loop@ while (true) {
            val pickedAction = interactor.pickItemFromList(locationSelectionDialog.size)
            when (pickedAction.type) {
                LIST_ITEM -> {
                    result = locationSelectionDialog[pickedAction.data]
                    break@loop
                }
                CANCEL -> if (allowCancel) {
                    break@loop
                }
                else -> throw AssertionError("Should not happen")
            }
        }
        restoreState()
        //Return result
        locationSelectionDialog = emptyList()
        return result
    }

    /**
     * Select skill from some pre-defined list
     * @param skills  List of skills
     * @param message Status message
     * @return picked skill, or null if canceled
     */
    private fun selectSkillFromList(skills: List<Skill>, allySkills: List<AllySkill>, message: StatusMessage): Any? {
        saveState()
        //Show dialog
        screen = GameScreen.DIALOG_SKILL_SELECT
        statusMessage = message
        actions = ActionList().add(CANCEL)
        skillSelectionDialog = skills
        allySkillSelectionDialog = allySkills
        var result: Any? = null
        drawScreen()
        //Process input
        val pickedAction = interactor.pickItemFromList(skills.size + allySkills.size)
        if (pickedAction.type === LIST_ITEM) {
            result = if (pickedAction.data < skills.size) {
                skills[pickedAction.data]
            } else {
                allySkills[pickedAction.data - skills.size]
            }
        }
        restoreState()
        //Return result
        skillSelectionDialog = emptyList()
        allySkillSelectionDialog = emptyList()
        return result
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Calculate the sum of (applicable) hero modifiers
     * @return sum of hero modifiers
     */
    private fun resolveHeroModifiers() = pendingHeroModifiers1.asSequence()
            .filter { SkillCheckHelper.checkModifierApplicable(phase, it.type) }
            .sumBy(PendingSkillModifier::value) +
            pendingHeroModifiers2.asSequence()
                    .filter { AllySkillCheckHelper.checkModifierApplicable(phase, it.type) }
                    .sumBy(PendingAllyModifier::value)

    /**
     * Clear lists of pending hero modifiers
     */
    private fun clearPendingHeroModifiers() {
        pendingHeroModifiers1.clear()
        pendingHeroModifiers2.clear()
    }

    /**
     * Apply pending encountered modifiers (to the newly encountered die) and clear the list
     */
    private fun applyEncounterModifiers() {
        val modifier = pendingEncounterModifiers.asSequence()
                .filter { SkillCheckHelper.checkModifierApplicable(phase, it.type) }
                .sumBy(PendingSkillModifier::value)
        encounteredDie!!.modifier += modifier
        pendingEncounterModifiers.clear()
    }


    /**
     * Perform battle check which involves taking dice from performer hand, rolling, distributing a nd processing roll result
     * @param performer Hero who perform the check
     * @param method         Aggregation method to use
     * @param opponent       Opposing die with modifier, or null if no opponent needed
     * @param promptMessage  StatusMessage to display while hero picks dice
     * @param handMaskRule   HandMaskRule to regulate die picking process
     * @param heroModifier   Modifier to apply to performer dice
     * @param dieDistributor DieDistributor to define what happens with dice participating in the check
     * @param successMessage StatusMessage to show in case of passing check successfully
     * @param failureMessage StatusMessage to show in case of failing the check
     * @param successActions Actions to perform after successfully passing the check
     * @param failureActions Actions to perform after failing he check
     * @param allowCancel Whether cancelling check is allowed (true by default)
     * @return true of check was performed, false otherwise
     */
    private fun performGenericBattleCheck(performer: Hero, method: DieBattleCheck.Method, opponent: DiePair?, promptMessage: StatusMessage, handMaskRule: HandMaskRule,
                                          heroModifier: Int, dieDistributor: DieDistributor, successMessage: StatusMessage, failureMessage: StatusMessage,
                                          successActions: (result: Int) -> Unit, failureActions: (result: Int) -> Unit, allowCancel: Boolean = true): Boolean {
        saveState()
        //Prepare check
        currentHero = performer
        currentLocation = findLocationForHero(performer)
        screen = GameScreen.LOCATION_INTERIOR
        battleCheck = DieBattleCheck(method, opponent)
        pickedHandPositions.clear()
        statusMessage = promptMessage
        val hand = handMaskRule.hand
        //Try to pick dice from performer's hand
        if (!pickDiceFromHand(handMaskRule, allowCancel) {
                    battleCheck!!.clearHeroPairs()
                    (collectPickedDice(hand) + collectPickedAllyDice(hand))
                            .map { DiePair(it, heroModifier) }
                            .forEach(battleCheck!!::addHeroPair)
                }) {
            restoreState()
            battleCheck = null
            pickedHandPositions.clear()
            return false
        }
        //Remove dice from hand
        collectPickedDice(hand).forEach { hand.removeDie(it) }
        collectPickedAllyDice(hand).forEach { hand.removeDie(it) }
        pickedHandPositions.clear()
        //Perform check
        Audio.playSound(Sound.BATTLE_CHECK_ROLL)
        for (i in 0..7) {
            battleCheck!!.roll()
            drawScreen()
            slowBit()
        }
        battleCheck!!.calculateResult()
        val result = battleCheck?.result ?: -1
        val success = result >= 0
        //Process dice which participated in the check
        dieDistributor(battleCheck!!, currentHero, deterrentPile)
        //Show message to user
        Audio.playSound(if (success) Sound.BATTLE_CHECK_SUCCESS else Sound.BATTLE_CHECK_FAILURE)
        statusMessage = if (success) successMessage else failureMessage
        actions = ActionList.EMPTY
        drawScreen()
        interactor.anyInput()
        //Clean up
        battleCheck = null
        opponent?.let {
            clearPendingHeroModifiers()
            pendingEncounterModifiers.clear()
        }
        restoreState()
        //Resolve consequences of the check
        if (success) {
            successActions(result)
        } else {
            failureActions(result)
        }
        return true
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Give die to other hero
     * @param source Hero who gives the dice
     * @param recipient Hero to dive dice to
     * @param setDiceGivenFlag true to set gaveDieThisTurn flag, false to ignore
     */
    private fun giveDieToOtherHero(source: Hero, recipient: Hero, setDiceGivenFlag: Boolean = true) {
        saveState()
        //Perform transfer process
        currentHero = source
        screen = GameScreen.LOCATION_INTERIOR
        statusMessage = CHOOSE_DICE_TO_GIVE with (MAX_HAND_SIZE - recipient.hand.dieCount)
        drawScreen()
        val currentHand = currentHero.hand
        val recipientHand = recipient.hand
        val result = pickDiceFromHand(GiveDiceHandMaskRule(currentHand, recipientHand))
        statusMessage = EMPTY
        actions = ActionList.EMPTY
        if (result) {
            val diceToGive = collectPickedDice(currentHand)
            val allyDiceToGive = collectPickedAllyDice(currentHand)
            pickedHandPositions.clear()
            (diceToGive + allyDiceToGive).forEach {
                Audio.playSound(Sound.DIE_DISCARD)
                currentHand.removeDie(it)
                recipientHand.addDie(it)
                drawScreen()
                slowDown()
            }
            if (setDiceGivenFlag) {
                gaveDieThisTurn = true
            }
        }
        restoreState()
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Attempt to use skill by current hero
     */
    private fun tryUseSkill() {
        val skills = SkillCheckHelper.getSkillsHeroCanUse(phase, currentHero, currentLocation, encounteredThreat?.getTraits())
        val allySkills = AllySkillCheckHelper.getAllySkillsHeroCanUse(scenario, phase, currentHero, currentLocation, encounteredThreat?.getTraits())
        selectSkillFromList(skills, allySkills, SELECT_SKILL_TO_USE)?.let {
            when (it) {
                is Skill -> useSkill(it)
                is AllySkill -> useAllySkill(it)
            }
            actions[USE_SKILL]?.isEnabled = checkHeroCanUseSkills(currentHero)
            actions[ASSIST]?.isEnabled = checkHeroCanBeAssisted(currentHero)
        }
    }

    /**
     * Attempt to assist current hero with some skill
     */
    private fun tryAssistCurrentHero() {
        fun isAtTheSameLocation(hero1: Hero, hero2: Hero) = (findLocationForHero(hero1) === findLocationForHero(hero2))
        val list = heroes.asSequence()
                .filter(Hero::isAlive)
                .filter { it !== currentHero }
                .filter { SkillCheckHelper.getSkillsHeroCanAssistWith(phase, currentHero, it, isAtTheSameLocation(currentHero, it), encounteredThreat?.getTraits()).isNotEmpty() }
                .toList()
        val assistingHero = selectHeroFromList(list, SELECT_HERO_TO_GET_ASSIST)
        if (assistingHero != null) {
            val skills = SkillCheckHelper.getSkillsHeroCanAssistWith(phase, currentHero, assistingHero, isAtTheSameLocation(currentHero, assistingHero), encounteredThreat?.getTraits())
            selectSkillFromList(skills, listOf(), SELECT_SKILL_TO_ASSIST_WITH)?.let {
                assistWithSkill(assistingHero, it as Skill)
                actions[USE_SKILL]?.isEnabled = checkHeroCanUseSkills(currentHero)
                actions[ASSIST]?.isEnabled = checkHeroCanBeAssisted(currentHero)
            }
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Travel to other location
     * @param hero Hero to travel
     * @param allowCancel true to allow cancel location selection, false to disallow
     * @param needToSetTraveledFlag true to set [traveledThisTurn] to true, false to not set
     */
    private fun travelToOtherLocation(hero: Hero, allowCancel: Boolean = true, needToSetTraveledFlag: Boolean = true) {
        saveState()
        currentHero = hero
        currentLocation = findLocationForHero(hero)
        //Travel
        val location = selectLocationFromList(locations.filter { it !== currentLocation }, SELECT_LOCATION_TO_TRAVEL, allowCancel)
        if (location != null) {
            Audio.playSound(Sound.TRAVEL)
            this.travelDestination = location
            screen = GameScreen.LOCATION_TRAVEL
            statusMessage = LOCATION_TRAVELING
            actions = ActionList.EMPTY
            for (i in 0..100 step 10) {
                travelProgress = i
                drawScreen()
                slowBit()
            }
            heroPlacement[currentLocation]?.removeIf { it === currentHero }
            heroPlacement[location]?.add(currentHero)
            currentLocation = location
            phase = GamePhase.HERO_ARRIVES_AT_LOCATION
            applyLocationSpecialRules(currentLocation)
            restoreState()
            if (hero == currentHero) {
                currentLocation = findLocationForHero(hero)
                gaveDieThisTurn = true
                actions.remove(GIVE_DIE)
                if (needToSetTraveledFlag) {
                    traveledThisTurn = true
                    actions.remove(TRAVEL)
                }
                if (!exploredThisTurn) {
                    actions[EXPLORE_LOCATION]?.isEnabled = checkLocationCanBeExplored(currentLocation)
                } else if (!exploredAgainThisTurn && checkLocationCanBeExplored(currentLocation)) {
                    actions[EXPLORE_LOCATION_AGAIN]?.isEnabled = checkHeroCanExploreAgain(currentHero)
                }
            }
        } else {
            restoreState()
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Close specific location
     * @param location Location to close
     */
    private fun closeLocation(location: Location) {
        Audio.playSound(Sound.CLOSE_LOCATION)
        phase = GamePhase.LOCATION_CLOSED
        location.isOpen = false
        location.bag.clear()
        location.villain = null
        location.enemies.clear()
        location.obstacles.clear()
        applyScenarioSpecialRules()
        applyLocationSpecialRules(location)
        location.getSpecialRules().clear()
        //Change phase
        if (checkWinningConditions()) { //Heroes already won
            changePhaseScenarioOutro()
        } else {
            changePhaseLocationCorrectExploration()
        }
    }

    /**
     * Close current location with a message
     */
    private fun autoCloseLocation() {
        encounteredDie = null
        statusMessage = LOCATION_CLOSE_AUTO
        actions = ActionList.EMPTY
        drawScreen()
        interactor.anyInput()
        closeLocation(currentLocation)
    }

    /**
     * Attempt to close current location
     */
    private fun attemptToCloseLocation() {
        loop@ while (true) {
            //Choose action
            val shouldDiscard =
                    when (quickActionPick(CHOOSE_HOW_CLOSE_LOCATION with getClosingDifficulty(currentLocation), HIDE, DISCARD, CANCEL)) {
                        CANCEL -> break@loop
                        DISCARD -> true
                        else -> false
                    }
            //Prepare check
            saveState()
            phase = GamePhase.LOCATION_CLOSE_ATTEMPT
            //Perform check
            val checkPerformed = performGenericBattleCheck(
                    currentHero,
                    DieBattleCheck.Method.SUM,
                    null,
                    CHOOSE_DICE_CLOSE_LOCATION with getClosingDifficulty(currentLocation),
                    LocationClosingHandMaskRule(currentHero.hand),
                    -getClosingDifficulty(currentLocation) + resolveHeroModifiers() + if (shouldDiscard) 2 else 0,
                    if (shouldDiscard) deterDivineDiscardRestDieDistributor else deterDivineHideRestDieDistributor,
                    LOCATION_CLOSE_SUCCESS,
                    LOCATION_CLOSE_FAILURE,
                    {
                        clearPendingHeroModifiers()
                        attemptedCloseThisTurn = true
                        exploredThisTurn = true
                        closeLocation(currentLocation)
                    },
                    {
                        clearPendingHeroModifiers()
                        attemptedCloseThisTurn = true
                        exploredThisTurn = true
                        changePhaseLocationCorrectExploration()
                    }
            )
            if (checkPerformed) {
                break@loop
            } else {
                restoreState()
            }
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Show info pages about current scenario
     */
    private fun showScenarioInfo() {
        saveState()
        //Show info
        statusMessage = SELECT_INFO_PAGE
        fun openGeneralPage() {
            Audio.playSound(Sound.PAGE_NEXT)
            screen = GameScreen.INFO_SCENARIO_GENERAL
            infoCurrentPage = 1
            infoPageCount = renderer.calculateScenarioInfoGeneralPageCount(scenario)
            actions = ActionList()
            if (infoPageCount > 1) {
                actions.add(INFO_PAGE_DOWN, infoCurrentPage < infoPageCount)
                actions.add(INFO_PAGE_UP, infoCurrentPage > 1)
            }
            actions.add(INFO_SPECIAL_RULES, scenario.getSpecialRules().isNotEmpty())
            actions.add(INFO_SCENARIO_ALLIES, scenario.getAllySkills().isNotEmpty())
            actions.add(CANCEL)
        }
        openGeneralPage()
        loop@ while (true) {
            drawScreen()
            //Process input
            val pickedAction = interactor.pickAction(actions)
            when (pickedAction.type) {
                INFO_GENERAL -> openGeneralPage()
                INFO_SPECIAL_RULES -> {
                    Audio.playSound(Sound.PAGE_NEXT)
                    screen = GameScreen.INFO_SCENARIO_RULES
                    infoCurrentPage = 1
                    infoPageCount = renderer.calculateScenarioInfoRulesPageCount(scenario)
                    actions = ActionList()
                    if (infoPageCount > 1) {
                        actions.add(INFO_PAGE_DOWN, infoCurrentPage < infoPageCount)
                        actions.add(INFO_PAGE_UP, infoCurrentPage > 1)
                    }
                    actions.add(INFO_GENERAL)
                    actions.add(INFO_SCENARIO_ALLIES, scenario.getAllySkills().isNotEmpty())
                    actions.add(CANCEL)
                }
                INFO_SCENARIO_ALLIES -> {
                    Audio.playSound(Sound.PAGE_NEXT)
                    screen = GameScreen.INFO_SCENARIO_ALLIES
                    infoCurrentPage = 1
                    infoPageCount = renderer.calculateScenarioInfoAlliesPageCount(scenario)
                    actions = ActionList()
                    if (infoPageCount > 1) {
                        actions.add(INFO_PAGE_DOWN, infoCurrentPage < infoPageCount)
                        actions.add(INFO_PAGE_UP, infoCurrentPage > 1)
                    }
                    actions.add(INFO_GENERAL)
                    actions.add(INFO_SPECIAL_RULES, scenario.getSpecialRules().isNotEmpty())
                    actions.add(CANCEL)
                }
                INFO_PAGE_UP -> infoPageUp()
                INFO_PAGE_DOWN -> infoPageDown()
                CANCEL -> {
                    Audio.playSound(Sound.PAGE_CLOSE)
                    break@loop
                }
                else -> throw AssertionError("Should not happen")
            }
        }
        restoreState()
    }

    /**
     * Show info pages about current scenario
     */
    private fun showLocationInfo() {
        saveState()
        //Show info
        statusMessage = SELECT_INFO_PAGE
        fun openGeneralPage() {
            Audio.playSound(Sound.PAGE_NEXT)
            screen = GameScreen.INFO_LOCATION_GENERAL
            infoCurrentPage = 1
            infoPageCount = renderer.calculateLocationInfoGeneralPageCount(infoLocation!!)
            actions = ActionList()
            if (infoPageCount > 1) {
                actions.add(INFO_PAGE_DOWN, infoCurrentPage < infoPageCount)
                actions.add(INFO_PAGE_UP, infoCurrentPage > 1)
            }
            actions.add(INFO_SPECIAL_RULES, infoLocation!!.getSpecialRules().isNotEmpty())
            actions.add(CANCEL)
        }
        openGeneralPage()
        loop@ while (true) {
            drawScreen()
            //Process input
            val pickedAction = interactor.pickAction(actions)
            when (pickedAction.type) {
                INFO_GENERAL -> openGeneralPage()
                INFO_SPECIAL_RULES -> {
                    Audio.playSound(Sound.PAGE_NEXT)
                    screen = GameScreen.INFO_LOCATION_RULES
                    infoCurrentPage = 1
                    infoPageCount = renderer.calculateLocationInfoRulesPageCount(infoLocation!!)
                    actions = ActionList()
                    if (infoPageCount > 1) {
                        actions.add(INFO_PAGE_DOWN, infoCurrentPage < infoPageCount)
                        actions.add(INFO_PAGE_UP, infoCurrentPage > 1)
                    }
                    actions.add(INFO_GENERAL)
                    actions.add(CANCEL)
                }
                INFO_PAGE_UP -> infoPageUp()
                INFO_PAGE_DOWN -> infoPageDown()
                CANCEL -> {
                    Audio.playSound(Sound.PAGE_CLOSE)
                    break@loop
                }
                else -> throw AssertionError("Should not happen")
            }
        }
        restoreState()
    }

    /**
     * Show hero info page
     */
    private fun showHeroInfo() {
        saveState()
        //Show info
        Audio.playSound(Sound.PAGE_NEXT)
        screen = GameScreen.INFO_HERO_GENERAL
        statusMessage = SELECT_INFO_PAGE
        actions = ActionList()
        actions.add(INFO_HERO_DICE)
        actions.add(INFO_HERO_SKILLS)
        actions.add(CANCEL)
        loop@ while (true) {
            drawScreen()
            //Process input
            val pickedAction = interactor.pickAction(actions)
            when (pickedAction.type) {
                INFO_GENERAL -> {
                    Audio.playSound(Sound.PAGE_NEXT)
                    screen = GameScreen.INFO_HERO_GENERAL
                    actions = ActionList()
                    actions.add(INFO_HERO_DICE)
                    actions.add(INFO_HERO_SKILLS)
                    actions.add(CANCEL)
                }
                INFO_HERO_DICE -> {
                    Audio.playSound(Sound.PAGE_NEXT)
                    screen = GameScreen.INFO_HERO_DICE
                    actions = ActionList()
                    actions.add(INFO_GENERAL)
                    actions.add(INFO_HERO_SKILLS)
                    actions.add(CANCEL)
                }
                INFO_HERO_SKILLS -> {
                    Audio.playSound(Sound.PAGE_NEXT)
                    screen = GameScreen.INFO_HERO_SKILLS
                    infoCurrentPage = 1
                    infoPageCount = renderer.calculateHeroInfoSkillsPageCount(infoHero!!)
                    actions = ActionList()
                    actions.add(INFO_PAGE_DOWN, infoCurrentPage < infoPageCount)
                    actions.add(INFO_PAGE_UP, infoCurrentPage > 1)
                    actions.add(INFO_GENERAL)
                    actions.add(INFO_HERO_DICE)
                    actions.add(CANCEL)
                }
                INFO_PAGE_UP -> infoPageUp()
                INFO_PAGE_DOWN -> infoPageDown()
                CANCEL -> {
                    Audio.playSound(Sound.PAGE_CLOSE)
                    break@loop
                }
                else -> throw AssertionError("Should not happen")
            }
        }
        restoreState()
    }

    /**
     * Show info pages about encountered enemy
     */
    private fun showEnemyInfo() {
        saveState()
        //Show info
        Audio.playSound(Sound.PAGE_NEXT)
        screen = GameScreen.INFO_ENEMY
        statusMessage = EMPTY
        actions = ActionList()
        actions.add(CANCEL)
        while (true) {
            drawScreen()
            if (interactor.pickAction(actions).type === CANCEL) {
                Audio.playSound(Sound.PAGE_CLOSE)
                break
            }
        }
        restoreState()
    }

    /**
     * Show info pages about encountered villain
     */
    private fun showVillainInfo() {
        saveState()
        //Show info
        Audio.playSound(Sound.PAGE_NEXT)
        screen = GameScreen.INFO_VILLAIN
        infoCurrentPage = 1
        infoPageCount = renderer.calculateVillainInfoPageCount(encounteredThreat as Villain)
        statusMessage = SELECT_INFO_PAGE
        actions = ActionList()
        if (infoPageCount > 1) {
            actions.add(INFO_PAGE_DOWN, infoCurrentPage < infoPageCount)
            actions.add(INFO_PAGE_UP, infoCurrentPage > 1)
        }
        actions.add(CANCEL)
        loop@ while (true) {
            drawScreen()
            when (interactor.pickAction(actions).type) {
                INFO_PAGE_UP -> infoPageUp()
                INFO_PAGE_DOWN -> infoPageDown()
                CANCEL -> {
                    Audio.playSound(Sound.PAGE_CLOSE)
                    break@loop
                }
                else -> throw AssertionError("Should not happen")
            }
        }
        restoreState()
    }

    /**
     * Show info pages about encountered obstacle
     */
    private fun showObstacleInfo() {
        saveState()
        //Show info
        Audio.playSound(Sound.PAGE_NEXT)
        screen = GameScreen.INFO_OBSTACLE
        statusMessage = EMPTY
        actions = ActionList()
        actions.add(CANCEL)
        while (true) {
            drawScreen()
            if (interactor.pickAction(actions).type === CANCEL) {
                Audio.playSound(Sound.PAGE_CLOSE)
                break
            }
        }
        restoreState()
    }

    /**
     * Show info pages about pending modifiers
     */
    private fun showModifiersInfo() {
        saveState()
        //Show info
        Audio.playSound(Sound.PAGE_NEXT)
        screen = GameScreen.INFO_MODIFIERS
        statusMessage = EMPTY
        actions = ActionList()
        actions.add(CANCEL)
        while (true) {
            drawScreen()
            if (interactor.pickAction(actions).type === CANCEL) {
                Audio.playSound(Sound.PAGE_CLOSE)
                break
            }
        }
        restoreState()
    }

    /**
     * Process showing of various info screens
     */
    private fun showInfoScreens() {
        saveState()
        //Show dialog
        actions = ActionList()
        when (encounteredDie?.die?.type) {
            Die.Type.ENEMY -> actions.add(INFO_ENEMY)
            Die.Type.VILLAIN -> actions.add(INFO_VILLAIN)
            Die.Type.OBSTACLE -> actions.add(INFO_OBSTACLE)
            else -> {
            }
        }
        actions.add(INFO_SCENARIO)
        actions.add(INFO_LOCATION)
        actions.add(INFO_HERO)
        actions.add(INFO_MODIFIERS, pendingHeroModifiers1.isNotEmpty() || pendingHeroModifiers2.isNotEmpty())
        actions.add(CANCEL)
        statusMessage = SELECT_INFO_TO_SHOW
        drawScreen()
        //Process input
        val pickedAction = interactor.pickAction(actions)
        when (pickedAction.type) {
            INFO_ENEMY -> showEnemyInfo()
            INFO_VILLAIN -> showVillainInfo()
            INFO_OBSTACLE -> showObstacleInfo()
            INFO_SCENARIO -> showScenarioInfo()
            INFO_LOCATION -> {
                infoLocation = selectLocationFromList(locations, SELECT_LOCATION_TO_SHOW)
                infoLocation?.let { showLocationInfo() }
            }
            INFO_HERO -> {
                infoHero = selectHeroFromList(heroes, SELECT_HERO_TO_SHOW)
                if (infoHero != null) {
                    showHeroInfo()
                }
            }
            INFO_MODIFIERS -> showModifiersInfo()
            CANCEL -> {
            }
            else -> throw AssertionError("Should not happen")
        }
        restoreState()
    }

    /**
     * Move to the next info page (if possible)
     */
    private fun infoPageDown() {
        if (infoCurrentPage < infoPageCount) {
            Audio.playSound(Sound.PAGE_NEXT)
            infoCurrentPage++
            actions[INFO_PAGE_DOWN]?.isEnabled = (infoCurrentPage < infoPageCount)
            actions[INFO_PAGE_UP]?.isEnabled = (infoCurrentPage > 1)
        }
    }

    /**
     * Move to the previous info page (if possible)
     */
    private fun infoPageUp() {
        if (infoCurrentPage > 1) {
            Audio.playSound(Sound.PAGE_PREVIOUS)
            infoCurrentPage--
            actions[INFO_PAGE_DOWN]?.isEnabled = (infoCurrentPage < infoPageCount)
            actions[INFO_PAGE_UP]?.isEnabled = (infoCurrentPage > 1)
        }
    }


    //------------------------------------------------------------------------------------------------------------------

    /**
     * Do some action to the specified [Hand] (including picking dice)
     * @param hero Hero whose hand to pick from
     * @param prompt Message to show when picking dice
     * @param rule Rule to use when picking dice
     * @param allowCancel true to allow canceling the selection (default), false otherwise
     * @param action Action to perform on each picked die
     * @return true if action was performed, false otherwise
     */
    private fun performActionWithHand(hero: Hero, prompt: StatusMessage, rule: HandMaskRule, allowCancel: Boolean = true, action: (die: Die) -> Unit): Boolean {
        saveState()
        //Setup
        screen = GameScreen.LOCATION_INTERIOR
        currentHero = hero
        currentLocation = findLocationForHero(hero)
        statusMessage = prompt
        //Process result
        val result = pickDiceFromHand(rule, allowCancel)
        statusMessage = EMPTY
        actions = ActionList.EMPTY
        if (result) {
            val pickedDice = collectPickedDice(hero.hand)
            val pickedAllyDice = collectPickedAllyDice(hero.hand)
            pickedHandPositions.clear()
            (pickedDice + pickedAllyDice).forEach(action)
        }
        pickedHandPositions.clear()
        //Return result
        restoreState()
        return result
    }

    /**
     * Do some action to the specified [Pile] (including picking dice)
     * @param pile Pile to perform actions on
     * @param prompt Message to show when picking dice
     * @param rule Rule to use when picking dice
     * @param allowCancel true to allow canceling the selection (default), false otherwise
     * @param action Action to perform on each picked die
     * @return true if action was performed, false otherwise
     */
    private fun performActionWithPile(pile: Pile, prompt: StatusMessage, rule: PileMaskRule, allowCancel: Boolean = true, action: (die: Die) -> Unit): Boolean {
        saveState()
        //Setup
        screen = GameScreen.DIALOG_PILE_PICK
        statusMessage = prompt
        pilePickDialog = pile
        //Process result
        val result = pickDiceFromPile(rule, allowCancel)
        statusMessage = EMPTY
        actions = ActionList.EMPTY
        if (result) {
            val pickedDice = collectPickedDice(pile)
            pickedPilePositions.clear()
            pickedDice.forEach(action)
        }
        pickedPilePositions.clear()
        pilePickDialog = null
        //Return result
        restoreState()
        return result
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Hide non-wound die from hero hand back to bag
     * @param hero Hero to affect
     */
    private fun hideNonWoundDie(hero: Hero) {
        if (!SingleNonWoundDieHandFilter().test(hero.hand)) {
            return
        }
        performActionWithHand(hero, CHOOSE_DICE_HIDE with 1, SingleNonWoundDieHandMaskRule(hero.hand), false) {
            Audio.playSound(Sound.DIE_HIDE)
            hero.hideDieFromHand(it)
        }
    }

    /**
     * Discard non-wound die from hero hand
     * @param hero Hero to affect
     */
    private fun discardNonWoundDie(hero: Hero) {
        if (!SingleNonWoundDieHandFilter().test(hero.hand)) {
            return
        }
        performActionWithHand(hero, CHOOSE_DICE_DISCARD with 1, SingleNonWoundDieHandMaskRule(hero.hand), false) {
            Audio.playSound(Sound.DIE_DISCARD)
            hero.discardDieFromHand(it)
        }
    }

    /**
     * Deter non-wound die from hero hand
     * @param hero Hero to affect
     */
    private fun deterNonWoundDie(hero: Hero) {
        if (!SingleNonWoundDieHandFilter().test(hero.hand)) {
            return
        }
        performActionWithHand(hero, CHOOSE_DICE_DETER with 1, SingleNonWoundDieHandMaskRule(hero.hand), false) {
            Audio.playSound(Sound.DIE_DISCARD)
            deterrentPile.put(it)
            hero.hand.removeDie(it)
        }
    }

    /**
     * Discard all dice of specific type from hero hand
     */
    private fun discardAllDiceOfSpecificType(hero: Hero, type: Die.Type) {
        saveState()
        currentHero = hero
        screen = GameScreen.LOCATION_INTERIOR
        currentLocation = findLocationForHero(hero)
        statusMessage = when (type) {
            Die.Type.PHYSICAL -> DISCARD_PHYSICAL_DICE
            Die.Type.SOMATIC -> DISCARD_SOMATIC_DICE
            Die.Type.MENTAL -> DISCARD_MENTAL_DICE
            Die.Type.VERBAL -> DISCARD_VERBAL_DICE
            Die.Type.DIVINE -> DISCARD_DIVINE_DICE
            Die.Type.ALLY -> DISCARD_ALLY_DICE
            else -> EMPTY
        }
        actions = ActionList.EMPTY
        Audio.playSound(Sound.TAKE_DAMAGE)
        drawScreen()
        interactor.anyInput()
        statusMessage = EMPTY
        val filter = SingleDieHandFilter(type)
        while (filter.test(hero.hand)) {
            Audio.playSound(Sound.DIE_DISCARD)
            hero.hand.findDieOfType(type)?.let { hero.discardDieFromHand(it) }
            drawScreen()
            slowDown()
        }
        restoreState()
    }

    /**
     * Draws die from hero's bag to hero's hand
     * @param hero Hero to draw for     *
     */
    private fun drawDieFromHeroBag(hero: Hero) {
        if (hero.bag.size > 0) {
            val die = hero.bag.draw()
            Audio.playSound(if (die.type == Die.Type.WOUND) Sound.ENCOUNTER_WOUND else Sound.DIE_DRAW)
            hero.hand.addDie(die)
            drawScreen()
            slowDown()
        }
    }

    /**
     * Decrease scenario timer by 1
     */
    private fun decreaseTimer() {
        saveState()
        Audio.playSound(Sound.ENCOUNTER_DIVINE)
        timer--
        statusMessage = TIMER_DECREASES
        actions = ActionList.EMPTY
        drawScreen()
        interactor.anyInput()
        restoreState()
    }

    /**
     * Increase scenario timer by 1
     */
    private fun increaseTimer() {
        saveState()
        Audio.playSound(Sound.ENCOUNTER_DIVINE)
        timer++
        statusMessage = TIMER_INCREASES
        actions = ActionList.EMPTY
        drawScreen()
        interactor.anyInput()
        restoreState()
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Perform wound taking process
     * @param hero  Hero who takes damage
     * @param count Number of wounds to take
     * @param size  Size of WOUND dice to generate
     */
    private fun takeWounds(hero: Hero, count: Int, size: Int) {
        if (!hero.isAlive) {
            return
        }
        saveState()
        //Prepare display
        currentHero = hero
        currentLocation = findLocationForHero(currentHero)
        phase = GamePhase.HERO_TAKES_DAMAGE
        screen = GameScreen.LOCATION_INTERIOR
        //Loop
        for (i in 1..count) {
            //Update screen
            Audio.playSound(Sound.ENCOUNTER_WOUND)
            statusMessage = ENCOUNTER_WOUND
            actions = ActionList()
            actions.add(ENDURE)
            drawScreen()
            //Interaction
            when (interactor.pickAction(actions).type) {
                ENDURE -> {
                    Audio.playSound(Sound.DIE_DRAW)
                    if (!checkHeroCanAcquireDie(currentHero, Die.Type.WOUND)) {
                        currentHero.discardDieFromHand(currentHero.hand.dieAt(0)!!)
                    }
                    currentHero.hand.addDie(Die(Die.Type.WOUND, size))
                    drawScreen()
                    slowDown()
                }
                else -> throw AssertionError("Should not happen")
            }
        }
        restoreState()
    }

    /**
     * Perform damage taking process (with skill usage etc.)
     * @param hero   Hero who takes damage
     * @param damage Damage taken
     */
    private fun takeDamage(hero: Hero, damage: Damage) {
        if (!hero.isAlive) {
            return
        }
        saveState()
        //Prepare display
        currentHero = hero
        currentLocation = findLocationForHero(currentHero)
        phase = GamePhase.HERO_TAKES_DAMAGE
        screen = GameScreen.LOCATION_INTERIOR
        currentDamage = damage
        Audio.playSound(Sound.TAKE_DAMAGE)
        //Resolve scenario and location special rules
        applyScenarioSpecialRules()
        applyLocationSpecialRules(currentLocation)
        //Loop
        while (damage.amount > 0) {
            //Update screen
            statusMessage = when (damage.type) {
                Damage.Type.PHYSICAL -> TAKE_DAMAGE_PHYSICAL with damage.amount
                Damage.Type.MAGICAL -> TAKE_DAMAGE_MAGICAL with damage.amount
            }
            actions = ActionList()
            actions.add(USE_SKILL, checkHeroCanUseSkills(currentHero))
            actions.add(ASSIST, checkHeroCanBeAssisted(currentHero))
            actions.add(ENDURE)
            actions.add(INFO)
            drawScreen()
            //Interaction
            when (interactor.pickAction(actions).type) {
                USE_SKILL -> tryUseSkill()
                ASSIST -> tryAssistCurrentHero()
                ENDURE -> if (pickDiceFromHand(TakeDamageHandMaskRule(currentHero.hand, damage.amount)) {
                            val damageLeft = damage.amount - pickedHandPositions.positionCount - pickedHandPositions.allyPositionCount
                            statusMessage = DAMAGE_LEFT with damageLeft
                        }) {
                    actions = ActionList.EMPTY
                    val discardDice = collectPickedDice(currentHero.hand)
                    val discardAllyDice = collectPickedAllyDice(currentHero.hand)
                    pickedHandPositions.clear()
                    (discardDice + discardAllyDice).forEach {
                        Audio.playSound(Sound.DIE_DISCARD)
                        currentHero.discardDieFromHand(it)
                        damage.reduce(1)
                        statusMessage = DAMAGE_LEFT with damage.amount
                        drawScreen()
                        slowDown()
                    }
                    while (damage.amount > 0) {
                        if (currentHero.bag.size == 0) {
                            damage.prevent()
                            break
                        }
                        Audio.playSound(Sound.DIE_DISCARD)
                        currentHero.discardPile.put(currentHero.bag.draw())
                        damage.reduce(1)
                        statusMessage = DAMAGE_LEFT with damage.amount
                        drawScreen()
                        slowDown()
                    }
                }
                INFO -> showInfoScreens()
                else -> throw AssertionError("Should not happen")
            }
        }
        restoreState()
        currentDamage = null
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Go through a list of scenario special rules to apply ones relevant to the current phase
     */
    private fun applyScenarioSpecialRules() {
        fun removeDiceOfType(bag: Bag, type: Die.Type) {
            var die: Die?
            do {
                die = bag.drawOfType(type)
            } while (die != null)
        }
        SpecialRuleCheckHelper.getApplicableScenarioSpecialRules(phase, scenario).forEach { rule ->
            when (rule) {
                BADLY_HURT -> heroes.forEach { hero ->
                    (1..2).forEach { hero.discardPile.put(hero.bag.draw()) }
                }
                DEEPLY_WOUNDED -> {
                    val filter = SingleDieTypeFilter(Die.Type.WOUND)
                    heroes.forEach { h -> h.hand.addDie(generateDie(filter, scenario.level)) }
                }
                FRIENDLY_NEIGHBOURHOOD -> locations.forEach { location ->
                    location.villain = null
                    removeDiceOfType(location.bag, Die.Type.VILLAIN)
                }
                SERENITY -> encounteredDie!!.modifier -= 1
                THOROUGH_INVESTIGATION -> locations.forEach { location ->
                    val dice = location.bag.examine().filter { it.type != Die.Type.VILLAIN }
                    if (dice.isNotEmpty()) {
                        (1..3).forEach { location.bag.put(Die(dice.random().type, dice.random().size)) }
                    }
                }
                DISPERSION -> {
                    heroPlacement.values.forEach(MutableList<Hero>::clear)
                    heroes.forEach { h -> heroPlacement[locations.random()]?.add(h) }
                }
                TRUSTED_COMPANIONS -> {
                    val filter = SingleDieTypeFilter(Die.Type.ALLY)
                    heroes.forEach { h -> (1..2).forEach { h.hand.addDie(generateDie(filter, scenario.level)) } }
                }
                PLENTIFUL_SUPPLIES -> heroes.forEach { h ->
                    h.hand.addDie(generateDie(SingleDieTypeFilter(h.favoredDieType), scenario.level))
                }
                GREAT_BETRAYAL -> {
                    locations.forEach { l -> removeDiceOfType(l.bag, Die.Type.ALLY) }
                    heroes.forEach { h ->
                        var die: Die?
                        do {
                            die = h.bag.drawOfType(Die.Type.ALLY)
                            die?.let { deterrentPile.put(it) }
                        } while (die != null)
                    }
                }
                JUST_A_SCRATCH -> if (SingleDieHandFilter(Die.Type.WOUND).test(currentHero.hand)) {
                    performActionWithHand(currentHero, CHOOSE_WOUND_FROM_HAND,
                            SingleDieHandMaskRule(currentHero.hand, Die.Type.WOUND), true) { d ->
                        Audio.playSound(Sound.DIE_REMOVE)
                        currentHero.hand.removeDie(d)
                        drawScreen()
                        slowDown()
                    }
                }
                OCCASIONAL_ACCOMPLICES -> {
                    statusMessage = GAIN_NEW_DIE
                    actions = ActionList.EMPTY
                    drawScreen()
                    interactor.anyInput()
                    Audio.playSound(Sound.DIE_DRAW)
                    currentHero.hand.addDie(generateDie(SingleDieTypeFilter(Die.Type.ALLY), scenario.level))
                    statusMessage = EMPTY
                    drawScreen()
                    slowDown()
                }
                ONE_SHOULD_SUFFICE -> {
                    val maxSize = locations.asSequence().map { it.bag.size }.max() ?: 0
                    locations.map { it.bag }.forEach { bag ->
                        while (bag.size < maxSize) {
                            bag.put(generateDie(SingleDieTypeFilter(Die.Type.ENEMY), scenario.level))
                        }
                    }
                }
                STATE_OF_EMERGENCY, VICIOUS_INHABITANTS -> encounteredDie!!.modifier += 1
                UTTER_MESS -> hideNonWoundDie(currentHero)
                else -> throw AssertionError("Should not happen")
            }
        }
    }

    /**
     * Go through a list of special rules of each open location to apply ones relevant to the current phase
     * @param location Location in question
     */
    private fun applyLocationSpecialRules(location: Location) {
        //        val isCurrent = if (phase == GamePhase.SCENARIO_START) false else location === currentLocation
        fun removeDice(type: Die.Type) {
            var die: Die?
            do {
                die = location.bag.drawOfType(type)
            } while (die != null)
        }
        SpecialRuleCheckHelper.getApplicableLocationSpecialRules(phase, location).forEach { rule ->
            when (rule) {
                NOWHERE_TO_HIDE -> removeDice(Die.Type.VILLAIN)
                SAFE_LOCATION -> removeDice(Die.Type.ENEMY)
                UNIMPEDED_PROGRESS -> removeDice(Die.Type.OBSTACLE)
                THWARTED_PLANS -> {
                    val hand = currentHero.hand
                    screen = GameScreen.LOCATION_INTERIOR
                    statusMessage = EMPTY
                    actions = ActionList.EMPTY
                    drawScreen()
                    slowDown()
                    while (hand.dieCount > hand.woundCount) {
                        Audio.playSound(Sound.DIE_HIDE)
                        currentHero.hideDieFromHand(hand.examine().first { it.type != Die.Type.WOUND })
                        drawScreen()
                        slowDown()
                    }
                    while (hand.allyDieCount > 0) {
                        Audio.playSound(Sound.DIE_HIDE)
                        currentHero.hideDieFromHand(hand.allyDieAt(0)!!)
                        drawScreen()
                        slowDown()
                    }
                    refillHeroHand(currentHero)
                }
                HOLD_ONTO_YOUR_BELONGINGS -> hideNonWoundDie(currentHero)
                OVERPREPARED -> {
                    saveState()
                    screen = GameScreen.LOCATION_INTERIOR
                    currentLocation = findLocationForHero(currentHero)
                    actions = ActionList.EMPTY
                    statusMessage = EMPTY
                    drawScreen()
                    slowDown()
                    drawDieFromHeroBag(currentHero)
                    restoreState()
                }
                THEY_ARE_EVERYWHERE, DONT_FORGET_TO_PAY_UP, MONEY_UP_FRONT -> discardNonWoundDie(currentHero)
                IMPOSSIBLE_TO_STAY -> travelToOtherLocation(currentHero, false)
                HIDDEN_STASH -> with(StatsDieTypeFilter()) {
                    actions = ActionList.EMPTY
                    statusMessage = EMPTY
                    (1..3).forEach {
                        Audio.playSound(Sound.DIE_DRAW)
                        currentHero.hand.addDie(generateDie(this, scenario.level))
                        drawScreen()
                        slowDown()
                    }
                }
                MAY_THE_GOD_BE_WITH_YOU -> with(SingleDieTypeFilter(Die.Type.DIVINE)) {
                    actions = ActionList.EMPTY
                    statusMessage = EMPTY
                    (1..2).forEach {
                        Audio.playSound(Sound.DIE_HIDE)
                        currentHero.bag.put(generateDie(this, scenario.level))
                        drawScreen()
                        slowDown()
                    }
                }
                GODS_BLESSING -> {
                    actions = ActionList.EMPTY
                    statusMessage = EMPTY
                    Audio.playSound(Sound.ENCOUNTER_DIVINE)
                    currentHero.hand.addDie(generateDie(SingleDieTypeFilter(Die.Type.DIVINE), scenario.level))
                    drawScreen()
                    slowDown()
                }
                MUCH_BETTER -> if (SingleAnyDieBagFilter().test(currentHero.discardPile)) {
                    performActionWithPile(currentHero.discardPile, CHOOSE_DICE_RETURN_BAG with 2,
                            AnyDicePileMaskRule(currentHero.discardPile, 2)) {
                        Audio.playSound(Sound.DIE_HIDE)
                        currentHero.discardPile.removeDie(it)
                        currentHero.bag.put(it)
                        drawScreen()
                        slowDown()
                    }
                }
                I_NEEDED_THIS -> if (SingleAnyDieBagFilter().test(currentHero.bag)) {
                    performActionWithPile(currentHero.discardPile, CHOOSE_DICE_RETURN_BAG with 1,
                            AnyDicePileMaskRule(currentHero.discardPile, 1)) {
                        Audio.playSound(Sound.DIE_HIDE)
                        currentHero.discardPile.removeDie(it)
                        currentHero.bag.put(it)
                        drawScreen()
                        slowDown()
                    }
                }
                AGGRESSIVE_ENVIRONMENT, YOU_ARE_NOT_WELCOME_HERE -> encounteredDie!!.modifier += 1
                HEAVY_CONSEQUENCES -> heroPlacement[currentLocation]?.filter(Hero::isAlive)?.forEach { deterNonWoundDie(it) }
                else -> throw AssertionError("Should not happen")
            }
        }
    }

    /**
     * Apply [Enemy], [Obstacle] or [Villain] traits when encountering respective dice
     * @param traits List of traits to resolve
     */
    private fun applyCardEncounterTraits(traits: List<Trait>) {
        fun rulesToDamageType() = when (traits.contains(Trait.DAMAGE_MAGICAL)) {
            true -> Damage.Type.MAGICAL
            false -> Damage.Type.PHYSICAL
        }

        var damage: Damage? = null
        var needToHideDie = false
        var needToDiscardDie = false
        var needToTakeWound = false
        val targets = mutableListOf(currentHero)
        traits.forEach { t ->
            when (t) {
                Trait.MODIFIER_MINUS_ONE -> encounteredDie!!.modifier -= 1
                Trait.MODIFIER_MINUS_TWO -> encounteredDie!!.modifier -= 2
                Trait.MODIFIER_PLUS_ONE -> encounteredDie!!.modifier += 1
                Trait.MODIFIER_PLUS_TWO -> encounteredDie!!.modifier += 2
                Trait.MODIFIER_PLUS_THREE -> encounteredDie!!.modifier += 3
                Trait.ENCOUNTER_HIDE_DIE -> needToHideDie = true
                Trait.ENCOUNTER_DISCARD_DIE -> needToDiscardDie = true
                Trait.ENCOUNTER_DAMAGE_ONE -> damage = Damage(rulesToDamageType(), 1)
                Trait.ENCOUNTER_DAMAGE_TWO -> damage = Damage(rulesToDamageType(), 2)
                Trait.ENCOUNTER_TAKE_WOUND -> needToTakeWound = true
                Trait.ENCOUNTER_TIMER_MINUS_ONE -> decreaseTimer()
                Trait.ENCOUNTER_ENTIRE_LOCATION ->
                    heroPlacement[currentLocation]?.asSequence()?.filter { it !== currentHero }?.filter(Hero::isAlive)?.forEach { targets.add(it) }
                else -> {
                }
            }
        }
        targets.forEach { target ->
            if (needToDiscardDie) {
                discardNonWoundDie(target)
            }
            if (needToHideDie) {
                hideNonWoundDie(target)
            }
            if (needToTakeWound) {
                takeWounds(target, 1, generateDie(SingleDieTypeFilter(Die.Type.WOUND), scenario.level).size)
            }
            damage?.let { takeDamage(target, it) }
        }
    }

    /**
     * Implement successful events happening after defeating enemy, villain or obstacle
     * @param traits List of traits to resolve
     */
    private fun applyCardSuccessConditions(traits: List<Trait>) {
        traits.forEach { trait ->
            when (trait) {
                Trait.WIN_DRAW_DIE -> drawDieFromHeroBag(currentHero)
                Trait.WIN_TAKE_STAT_DIE -> {
                    Audio.playSound(Sound.DIE_DRAW)
                    currentHero.hand.addDie(generateDie(StatsDieTypeFilter(), scenario.level))
                    drawScreen()
                    slowDown()
                }
                Trait.WIN_HIDE_STAT_DIE -> {
                    Audio.playSound(Sound.DIE_HIDE)
                    currentHero.bag.put(generateDie(StatsDieTypeFilter(), scenario.level))
                    drawScreen()
                    slowDown()
                }
                Trait.WIN_TAKE_DIVINE_DIE -> {
                    Audio.playSound(Sound.DIE_DRAW)
                    currentHero.hand.addDie(generateDie(SingleDieTypeFilter(Die.Type.DIVINE), scenario.level))
                    drawScreen()
                    slowDown()
                }
                Trait.WIN_HIDE_DIVINE_DIE -> {
                    Audio.playSound(Sound.DIE_HIDE)
                    currentHero.bag.put(generateDie(SingleDieTypeFilter(Die.Type.DIVINE), scenario.level))
                    drawScreen()
                    slowDown()
                }
                Trait.WIN_TAKE_ALLY_DIE -> {
                    Audio.playSound(Sound.DIE_DRAW)
                    currentHero.hand.addDie(generateDie(SingleDieTypeFilter(Die.Type.ALLY), scenario.level))
                    drawScreen()
                    slowDown()
                }
                Trait.WIN_TIMER_PLUS_ONE -> increaseTimer()
                Trait.WIN_TIMER_PLUS_TWO -> (1..2).forEach { increaseTimer() }
                Trait.WIN_DISCARD_DIE -> discardNonWoundDie(currentHero)
                Trait.WIN_DETER_DIE -> deterNonWoundDie(currentHero)
                Trait.WIN_DISCARD_ALL -> {
                    saveState()
                    Audio.playSound(Sound.EXPLOSION)
                    statusMessage = DISCARD_HAND_ALL_HEROES
                    actions = ActionList.EMPTY
                    drawScreen()
                    interactor.anyInput()
                    heroPlacement[currentLocation]?.forEach { discardHeroHand(it) }
                    restoreState()
                }
                else -> {
                }
            }
        }
    }

    /**
     * Calculate, alternate, modify and perform damage dealing by encountered enemy, villain or obstacle (with all the
     * rules which are needed to apply here).
     * @param traits       List of traits to resolve
     * @param damageAmount Amount of damage to be dealt (modifiers applied but rules did not)
     */
    private fun applyCardFailureConditions(traits: List<Trait>, damageAmount: Int) {
        clearPendingHeroModifiers()
        var amount = damageAmount
        var damageType = Damage.Type.PHYSICAL
        var additionalWounds = 0
        val targets = mutableListOf(currentHero)
        var needToHideHand = false
        var needToDiscardHand = false
        var needToHideDie = false
        var needToDiscardDie = false
        var needToDeterDie = false
        var needToTravel = false
        var needToDiscardAllType: Die.Type? = null
        var needToDiscardRandomStatType = false
        if (HEAVY_HITTERS in currentLocation.getSpecialRules() && encounteredThreat !is Obstacle) {
            amount++
        }
        if (PULLING_PUNCHES in scenario.getSpecialRules() && encounteredThreat !is Obstacle) {
            amount = max(0, amount - 1)
        }
        for (trait in traits) {
            when (trait) {
                Trait.DAMAGE_ONE -> amount = 1
                Trait.DAMAGE_TWO -> amount = 2
                Trait.DAMAGE_THREE -> amount = 3
                Trait.DAMAGE_PLUS_ONE -> amount += 1
                Trait.DAMAGE_PLUS_TWO -> amount += 2
                Trait.DAMAGE_MINUS_ONE -> amount -= 1
                Trait.DAMAGE_MINUS_TWO -> amount -= 2
                Trait.DAMAGE_ZERO -> amount = 0
                Trait.DAMAGE_MAGICAL -> damageType = Damage.Type.MAGICAL
                Trait.DAMAGE_WOUND_ONE -> additionalWounds = 1
                Trait.DAMAGE_WOUND_TWO -> additionalWounds = 2
                Trait.DAMAGE_ENTIRE_LOCATION ->
                    heroPlacement[currentLocation]?.asSequence()?.filter { it !== currentHero }?.filter(Hero::isAlive)?.forEach { targets.add(it) }
                Trait.DAMAGE_EVERYONE ->
                    heroes.asSequence().filter(Hero::isAlive).filter { it !== currentHero }.forEach { targets.add(it) }
                Trait.DAMAGE_HIDE_HAND -> needToHideHand = true
                Trait.DAMAGE_DISCARD_HAND -> needToDiscardHand = true
                Trait.DAMAGE_HIDE_DIE -> needToHideDie = true
                Trait.DAMAGE_DISCARD_DIE -> needToDiscardDie = true
                Trait.DAMAGE_DETER_DIE -> needToDeterDie = true
                Trait.DAMAGE_TRAVEL -> needToTravel = true
                Trait.DAMAGE_DISCARD_PHYSICAL -> needToDiscardAllType = Die.Type.PHYSICAL
                Trait.DAMAGE_DISCARD_SOMATIC -> needToDiscardAllType = Die.Type.SOMATIC
                Trait.DAMAGE_DISCARD_MENTAL -> needToDiscardAllType = Die.Type.MENTAL
                Trait.DAMAGE_DISCARD_VERBAL -> needToDiscardAllType = Die.Type.VERBAL
                Trait.DAMAGE_DISCARD_DIVINE -> needToDiscardAllType = Die.Type.DIVINE
                Trait.DAMAGE_DISCARD_ALLY -> needToDiscardAllType = Die.Type.ALLY
                Trait.DAMAGE_DISCARD_RANDOM_STAT -> needToDiscardRandomStatType = true
                Trait.DAMAGE_TIMER_MINUS_ONE -> decreaseTimer()
                Trait.DAMAGE_TIMER_MINUS_TWO -> (1..2).forEach { _ -> decreaseTimer() }
                Trait.DAMAGE_PREVENT_EXPLORATION -> {
                    exploredThisTurn = true
                    exploredAgainThisTurn = true
                }
                else -> {
                }
            }
        }
        targets.forEach { target ->
            needToDiscardAllType?.let { discardAllDiceOfSpecificType(target, it) }
            if (needToDiscardRandomStatType) {
                discardAllDiceOfSpecificType(target, when ((1..4).random()) {
                    1 -> Die.Type.PHYSICAL
                    2 -> Die.Type.SOMATIC
                    3 -> Die.Type.MENTAL
                    else -> Die.Type.VERBAL
                })
            }
            if (needToDiscardDie) {
                discardNonWoundDie(target)
            }
            if (needToDeterDie) {
                deterNonWoundDie(target)
            }
            if (needToHideDie) {
                hideNonWoundDie(target)
            }
            if (needToDiscardHand) {
                discardHeroHand(target)
            }
            if (needToHideHand) {
                hideHeroHand(target)
            }
            //Regular damage
            takeDamage(target, Damage(damageType, amount))
            if (additionalWounds > 0) {
                takeWounds(target, additionalWounds, encounteredDie!!.die.size)
            }
            if (needToTravel) {
                travelToOtherLocation(target, false)
            }
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * PHASE: Finalize hero's turn
     * Discard extra dice (or any dice, optionally) from hand and refill hand
     * @param preventHandModifications Do not discard extra (or optional) dice and do not refill hand
     */
    private fun changePhaseHeroTurnEnd(preventHandModifications: Boolean = false) {
        startScenarioMusic()
        battleCheck = null
        encounteredDie = null
        phase = GamePhase.HERO_TURN_END
        //Resolve special rules
        applyScenarioSpecialRules()
        applyLocationSpecialRules(currentLocation)
        //Hand modifications
        val hand = currentHero.hand
        if (!preventHandModifications) {
            //Discard extra dice (or optional dice)
            pickedHandPositions.clear()
            activeHandPositions.clear()
            val allowCancel =
                    if (hand.dieCount > hand.capacity) {
                        statusMessage = END_OF_TURN_DISCARD_EXTRA
                        false
                    } else {
                        statusMessage = END_OF_TURN_DISCARD_OPTIONAL
                        true
                    }
            val result = pickDiceFromHand(DiscardExtraDiceHandMaskRule(hand), allowCancel)
            statusMessage = EMPTY
            actions = ActionList.EMPTY
            if (result) {
                val discardDice = collectPickedDice(hand)
                val discardAllyDice = collectPickedAllyDice(hand)
                pickedHandPositions.clear()
                (discardDice + discardAllyDice).forEach { die ->
                    Audio.playSound(Sound.DIE_DISCARD)
                    currentHero.discardDieFromHand(die)
                    drawScreen()
                    slowDown()
                }
            }
            pickedHandPositions.clear()
            //Replenish hand
            if (LIMITED_RESOURCES !in currentLocation.getSpecialRules()) {
                refillHeroHand(currentHero)
            }
        }
        //Check death
        if (hand.woundCount >= hand.capacity || (currentHero.bag.size == 0 && hand.dieCount <= hand.woundCount)) {
            changePhaseHeroDeath()
        } else {
            changePhaseHeroTurnStart()
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * PHASE: Show scenario introduction page
     */
    private fun changePhaseScenarioIntro() {
        phase = GamePhase.SCENARIO_DISPLAY_INTRO
        Audio.playSound(Sound.PAGE_NEXT)
        startSpecificMusic(Music.SCENARIO_INTRO)
        screen = GameScreen.SCENARIO_INTRO
        infoCurrentPage = 1
        infoPageCount = renderer.calculateScenarioScenicTextPageCount(scenario, false)
        actions = ActionList()
        if (infoPageCount > 1) {
            actions.add(INFO_PAGE_DOWN, infoCurrentPage < infoPageCount)
            actions.add(INFO_PAGE_UP, infoCurrentPage > 1)
        }
        actions.add(CONFIRM)
    }

    /**
     * PHASE: Show scenario finalization page
     */
    private fun changePhaseScenarioOutro() {
        phase = GamePhase.SCENARIO_DISPLAY_OUTRO
        Audio.playSound(Sound.PAGE_NEXT)
        startSpecificMusic(Music.SCENARIO_OUTRO)
        screen = GameScreen.SCENARIO_OUTRO
        infoCurrentPage = 1
        infoPageCount = renderer.calculateScenarioScenicTextPageCount(scenario, true)
        actions = ActionList()
        if (infoPageCount > 1) {
            actions.add(INFO_PAGE_DOWN, infoCurrentPage < infoPageCount)
            actions.add(INFO_PAGE_UP, infoCurrentPage > 1)
        }
        actions.add(CONFIRM)
    }

    /**
     * PHASE: Prepare all variables for next hero turn
     */
    private fun changePhaseHeroTurnStart() {
        phase = GamePhase.HERO_TURN_START
        screen = GameScreen.HERO_TURN_START
        //Tick timer
        timer--
        if (timer < 0) {
            changePhaseGameLost(GAME_LOSS_OUT_OF_TIME)
            return
        }
        //Pick next hero
        do {
            currentHeroIndex = ++currentHeroIndex % heroes.size
            currentHero = heroes[currentHeroIndex]
        } while (!currentHero.isAlive)
        currentLocation = findLocationForHero(currentHero)
        //Check death
        if ((currentHero.bag.size == 0 && currentHero.hand.dieCount <= currentHero.hand.woundCount)) {
            changePhaseHeroDeath()
            return
        }
        //Setup
        Audio.playSound(Sound.TURN_START)
        startScenarioMusic()
        traveledThisTurn = false
        gaveDieThisTurn = false
        exploredThisTurn = false
        exploredAgainThisTurn = false
        attemptedCloseThisTurn = false
        clearPendingHeroModifiers()
        pendingEncounterModifiers.clear()
        //Save game
        storeGameData()
    }

    /**
     * PHASE: Prepare all variables to display location
     */
    private fun changePhaseLocationBeforeExploration() {
        startScenarioMusic()
        phase = GamePhase.LOCATION_BEFORE_EXPLORATION
        screen = GameScreen.LOCATION_INTERIOR
        currentLocation = findLocationForHero(currentHero)
        encounteredDie = null
        encounteredThreat = null
        battleCheck = null
        pickedHandPositions.clear()
        activeHandPositions.clear()
        statusMessage = CHOOSE_ACTION_BEFORE_EXPLORATION
        actions = ActionList()
        actions.add(EXPLORE_LOCATION, checkLocationCanBeExplored(currentLocation))
        if (checkLocationCanBeClosed(currentLocation)) {
            actions.add(CLOSE_LOCATION, checkHeroCanCloseLocation(currentHero))
        }
        actions.add(USE_SKILL, checkHeroCanUseSkills(currentHero))
        actions.add(ASSIST, checkHeroCanBeAssisted(currentHero))
        if (!traveledThisTurn) {
            actions.add(TRAVEL, checkHeroCanTravel(currentHero))
        }
        if (!gaveDieThisTurn) {
            actions.add(GIVE_DIE, checkHeroCanGiveDie(currentHero))
        }
        actions.add(INFO)
        actions.add(FINISH_TURN)
        actions.add(MENU)
        //Save game
        storeGameData()
    }

    /**
     * PHASE: Pick correct phase after certain [Die.Type] was encountered
     * @param die Encountered die
     */
    private fun changePhaseLocationCorrectEncounter(die: Die) {
        encounteredDie = DiePair(die, 0)
        when (die.type) {
            Die.Type.PHYSICAL, Die.Type.SOMATIC, Die.Type.MENTAL, Die.Type.VERBAL -> changePhaseLocationEncounterStatDie()
            Die.Type.DIVINE -> changePhaseLocationEncounterDivineDie()
            Die.Type.ALLY -> changePhaseLocationEncounterAllyDie()
            Die.Type.WOUND -> changePhaseLocationEncounterWoundDie()
            Die.Type.OBSTACLE -> changePhaseLocationEncounterObstacleDie()
            Die.Type.ENEMY -> changePhaseLocationEncounterEnemyDie()
            Die.Type.VILLAIN -> changePhaseLocationEncounterVillainDie()
        }
    }

    /**
     * PHASE: Display STAT die being encountered
     */
    private fun changePhaseLocationEncounterStatDie() {
        Audio.playSound(Sound.ENCOUNTER_STAT)
        phase = GamePhase.LOCATION_ENCOUNTER_STAT
        screen = GameScreen.LOCATION_INTERIOR
        encounteredThreat = null
        battleCheck = null
        pickedHandPositions.clear()
        activeHandPositions.clear()
        applyEncounterModifiers()
        statusMessage = when (encounteredDie!!.die.type) {
            Die.Type.PHYSICAL -> ENCOUNTER_PHYSICAL
            Die.Type.SOMATIC -> ENCOUNTER_SOMATIC
            Die.Type.MENTAL -> ENCOUNTER_MENTAL
            Die.Type.VERBAL -> ENCOUNTER_VERBAL
            else -> throw AssertionError("Should not happen")
        }
        val canAttemptCheck = checkHeroCanAttemptStatCheck(currentHero, encounteredDie!!.die.type)
        actions = ActionList()
        actions.add(HIDE, canAttemptCheck)
        actions.add(DISCARD, canAttemptCheck)
        actions.add(FORFEIT)
        //Save game
        storeGameData()
    }

    /**
     * PHASE: Display STAT die being encountered
     */
    private fun changePhaseLocationEncounterDivineDie() {
        Audio.playSound(Sound.ENCOUNTER_DIVINE)
        phase = GamePhase.LOCATION_ENCOUNTER_DIVINE
        screen = GameScreen.LOCATION_INTERIOR
        encounteredThreat = null
        battleCheck = null
        pickedHandPositions.clear()
        activeHandPositions.clear()
        applyEncounterModifiers()
        statusMessage = ENCOUNTER_DIVINE
        actions = ActionList()
        actions.add(ACQUIRE, checkHeroCanAcquireDie(currentHero, Die.Type.DIVINE))
        actions.add(FORFEIT)
        //Save game
        storeGameData()
    }

    /**
     * PHASE: Display STAT die being encountered
     */
    private fun changePhaseLocationEncounterAllyDie() {
        Audio.playSound(Sound.ENCOUNTER_ALLY)
        phase = GamePhase.LOCATION_ENCOUNTER_ALLY
        screen = GameScreen.LOCATION_INTERIOR
        encounteredThreat = null
        battleCheck = null
        pickedHandPositions.clear()
        activeHandPositions.clear()
        applyEncounterModifiers()
        statusMessage = ENCOUNTER_ALLY
        actions = ActionList()
        actions.add(USE_SKILL, checkHeroCanUseSkills(currentHero))
        actions.add(ASSIST, checkHeroCanBeAssisted(currentHero))
        actions.add(LEAVE)
        //Save game
        storeGameData()
    }

    /**
     * PHASE: Display WOUND die being encountered
     */
    private fun changePhaseLocationEncounterWoundDie() {
        Audio.playSound(Sound.ENCOUNTER_WOUND)
        phase = GamePhase.LOCATION_ENCOUNTER_WOUND
        screen = GameScreen.LOCATION_INTERIOR
        encounteredThreat = null
        battleCheck = null
        pickedHandPositions.clear()
        activeHandPositions.clear()
        applyEncounterModifiers()
        statusMessage = ENCOUNTER_WOUND
        actions = ActionList()
        actions.add(ENDURE)
        //Save game
        storeGameData()
    }


    /**
     * PHASE: Display OBSTACLE die being encountered
     */
    private fun changePhaseLocationEncounterObstacleDie() {
        Audio.playSound(Sound.ENCOUNTER_OBSTACLE)
        phase = GamePhase.LOCATION_ENCOUNTER_OBSTACLE
        screen = GameScreen.LOCATION_INTERIOR
        battleCheck = null
        pickedHandPositions.clear()
        activeHandPositions.clear()
        applyEncounterModifiers()
        //Pick obstacle threat
        val obstacles = currentLocation.obstacles
        obstacles.shuffle()
        val obstacle = obstacles.revealTop()
        encounteredThreat = obstacle
        //Resolve special rules
        applyCardEncounterTraits(obstacle.getTraits())
        applyScenarioSpecialRules()
        applyLocationSpecialRules(currentLocation)
        //Resolve additional rules
        currentHero.getSkills().firstOrNull { it.type === SURVIVALIST }?.let {
            pendingHeroModifiers1.add(PendingSkillModifier(it.type, it.getModifier1Value()))
        }
        //Setup actions
        statusMessage = ENCOUNTER_OBSTACLE
        actions = ActionList()
        val canAttemptCheck = ObstacleCheckHelper.checkHeroCanAttemptObstacleCheck(currentHero, obstacle)
        actions.add(HIDE, canAttemptCheck)
        actions.add(DISCARD, canAttemptCheck)
        actions.add(INFO)
        if (Trait.IMMUNE_AVOID_ENCOUNTER in obstacle.getTraits()) {
            actions.add(ENDURE)
        } else {
            actions.add(LEAVE)
        }
        //Save game
        storeGameData()
    }

    /**
     * PHASE: Display ENEMY die being encountered
     */
    private fun changePhaseLocationEncounterEnemyDie() {
        Audio.playSound(Sound.ENCOUNTER_ENEMY)
        phase = GamePhase.LOCATION_ENCOUNTER_ENEMY
        screen = GameScreen.LOCATION_INTERIOR
        battleCheck = null
        pickedHandPositions.clear()
        activeHandPositions.clear()
        applyEncounterModifiers()
        //Pick enemy threat
        val enemies = currentLocation.enemies
        enemies.shuffle()
        val enemy = enemies.revealTop()
        encounteredThreat = enemy
        //Resolve special rules
        applyScenarioSpecialRules()
        applyLocationSpecialRules(currentLocation)
        applyCardEncounterTraits(enemy.getTraits())
        //Setup actions
        statusMessage = ENCOUNTER_ENEMY
        actions = ActionList()
        actions.add(USE_SKILL, checkHeroCanUseSkills(currentHero))
        actions.add(ASSIST, checkHeroCanBeAssisted(currentHero))
        actions.add(INFO)
        actions.add(FLEE)
        //Save game
        storeGameData()
    }

    /**
     * PHASE: Display VILLAIN die being encountered
     */
    private fun changePhaseLocationEncounterVillainDie() {
        Audio.playSound(Sound.ENCOUNTER_VILLAIN)
        startVillainMusic()
        phase = GamePhase.LOCATION_ENCOUNTER_VILLAIN
        screen = GameScreen.LOCATION_INTERIOR
        battleCheck = null
        pickedHandPositions.clear()
        activeHandPositions.clear()
        applyEncounterModifiers()
        //Pick villain threat
        val villain = currentLocation.villain
        encounteredThreat = villain
        //Apply special rules
        applyCardEncounterTraits(villain!!.getTraits())
        applyScenarioSpecialRules()
        applyLocationSpecialRules(currentLocation)
        //Setup actions
        statusMessage = ENCOUNTER_VILLAIN
        actions = ActionList()
        actions.add(USE_SKILL, checkHeroCanUseSkills(currentHero))
        actions.add(ASSIST, checkHeroCanBeAssisted(currentHero))
        actions.add(INFO)
        actions.add(FLEE)
        //Save game
        storeGameData()
    }

    /**
     * PHASE: Prepare all variables to display location
     */
    private fun changePhaseLocationAfterExploration() {
        startScenarioMusic()
        phase = GamePhase.LOCATION_AFTER_EXPLORATION
        screen = GameScreen.LOCATION_INTERIOR
        currentLocation = findLocationForHero(currentHero)
        traveledThisTurn = true
        encounteredDie = null
        encounteredThreat = null
        battleCheck = null
        pickedHandPositions.clear()
        activeHandPositions.clear()
        statusMessage = CHOOSE_ACTION_AFTER_EXPLORATION
        actions = ActionList()
        if (exploredThisTurn && !exploredAgainThisTurn && checkLocationCanBeExploredAgain(currentLocation)) {
            actions.add(EXPLORE_LOCATION_AGAIN, checkHeroCanExploreAgain(currentHero))
        }
        if (checkLocationCanBeClosed(currentLocation) && !attemptedCloseThisTurn) {
            actions.add(CLOSE_LOCATION, checkHeroCanCloseLocation(currentHero))
        }
        actions.add(USE_SKILL, checkHeroCanUseSkills(currentHero))
        actions.add(ASSIST, checkHeroCanBeAssisted(currentHero))
        actions.add(INFO)
        actions.add(FINISH_TURN)
        actions.add(MENU)
        //Save game
        storeGameData()
    }

    /**
     * PHASE: Correctly display current exploration status
     */
    private fun changePhaseLocationCorrectExploration() = when (exploredThisTurn) {
        true -> changePhaseLocationAfterExploration()
        false -> changePhaseLocationBeforeExploration()
    }

    /**
     * PHASE: Display death of current hero
     */
    private fun changePhaseHeroDeath() {
        muteMusic()
        Audio.playSound(Sound.HERO_DEATH)
        phase = GamePhase.HERO_DEATH
        screen = GameScreen.HERO_DEATH
        currentHero.isAlive = false
        heroPlacement[findLocationForHero(currentHero)]?.remove(currentHero)
        //Save game
        storeGameData()
    }

    /**
     * PHASE: Display loss screen
     * @param message StatusMessage to display
     */
    private fun changePhaseGameLost(message: StatusMessage) {
        muteMusic()
        Audio.playSound(Sound.GAME_LOSS)
        phase = GamePhase.GAME_LOSS
        screen = GameScreen.GAME_LOSS
        statusMessage = message
        //Save game
        storeGameData()
    }

    /**
     * PHASE: Display loss screen
     */
    private fun changePhaseGameWon() {
        muteMusic()
        Audio.playSound(Sound.GAME_VICTORY)
        phase = GamePhase.GAME_VICTORY
        screen = GameScreen.GAME_VICTORY
        statusMessage = GAME_VICTORY
        //Save game
        storeGameData()
    }

    /**
     * PHASE: Pause the game
     */
    private fun changePhaseGamePause() {
        phase = GamePhase.PAUSE
        screen = GameScreen.PAUSE_MENU
        statusMessage = GAME_PAUSED
        actions = ActionList()
        actions.add(FORFEIT)
        actions.add(EXIT)
        actions.add(CANCEL)
    }

    //==================================================================================================================

    /**
     * Run main game cycle
     */
    private fun processCycle() {
        while (true) {
            drawScreen()
            when (phase) {
                //------------------------------------------------------------------------------------------------------
                GamePhase.SCENARIO_DISPLAY_INTRO ->
                    when (interactor.pickAction(actions).type) {
                        INFO_PAGE_DOWN -> infoPageDown()
                        INFO_PAGE_UP -> infoPageUp()
                        CONFIRM -> {
                            changePhaseHeroTurnStart()
                        }
                        else -> throw AssertionError("Should not happen")
                    }
                //------------------------------------------------------------------------------------------------------
                GamePhase.SCENARIO_DISPLAY_OUTRO ->
                    when (interactor.pickAction(actions).type) {
                        INFO_PAGE_DOWN -> infoPageDown()
                        INFO_PAGE_UP -> infoPageUp()
                        CONFIRM -> changePhaseGameWon()
                        else -> throw AssertionError("Should not happen")
                    }
                //------------------------------------------------------------------------------------------------------
                GamePhase.HERO_TURN_START -> {
                    interactor.anyInput()
                    applyScenarioSpecialRules()
                    applyLocationSpecialRules(currentLocation)
                    changePhaseLocationBeforeExploration()
                }
                //------------------------------------------------------------------------------------------------------
                GamePhase.HERO_DEATH -> {
                    interactor.anyInput()
                    if (heroes.none(Hero::isAlive)) {
                        changePhaseGameLost(GAME_LOSS_EVERYBODY_DIED)
                    } else {
                        changePhaseHeroTurnStart()
                    }
                }
                //------------------------------------------------------------------------------------------------------
                GamePhase.GAME_LOSS -> {
                    interactor.anyInput()
                    realTimeSave.shutdown()
                    callback(Result.LOSE, heroes, deterrentPile, phaseIndex)
                    return
                }
                //------------------------------------------------------------------------------------------------------
                GamePhase.GAME_VICTORY -> {
                    interactor.anyInput()
                    realTimeSave.shutdown()
                    callback(Result.WIN, heroes, deterrentPile, phaseIndex)
                    return
                }
                //------------------------------------------------------------------------------------------------------
                GamePhase.LOCATION_BEFORE_EXPLORATION ->
                    when (interactor.pickAction(actions).type) {
                        EXPLORE_LOCATION -> {
                            exploredThisTurn = true
                            changePhaseLocationCorrectEncounter(currentLocation.bag.draw())
                        }
                        CLOSE_LOCATION -> attemptToCloseLocation()
                        USE_SKILL -> tryUseSkill()
                        ASSIST -> tryAssistCurrentHero()
                        TRAVEL -> when (NOT_SO_FAST in currentLocation.getSpecialRules()) {
                            true -> if (performActionWithHand(currentHero, CHOOSE_DICE_DISCARD with 2,
                                            DoubleNonWoundDieHandMaskRule(currentHero.hand)) {
                                        Audio.playSound(Sound.DIE_DISCARD)
                                        currentHero.discardDieFromHand(it)
                                        drawScreen()
                                        slowDown()
                                    }) {
                                travelToOtherLocation(currentHero)
                            }
                            false -> travelToOtherLocation(currentHero)
                        }
                        GIVE_DIE -> (selectHeroFromList(getHeroesToGiveDieTo(currentHero), SELECT_HERO_TO_GIVE))?.let {
                            giveDieToOtherHero(currentHero, it)
                            changePhaseLocationCorrectExploration()
                        }
                        INFO -> showInfoScreens()
                        FINISH_TURN -> changePhaseHeroTurnEnd()
                        MENU -> changePhaseGamePause()
                        else -> throw AssertionError("Should not happen")
                    }
                //------------------------------------------------------------------------------------------------------
                GamePhase.LOCATION_ENCOUNTER_STAT -> {
                    val type = interactor.pickAction(actions).type
                    when (type) {
                        DISCARD, HIDE -> {
                            val shouldDiscard = type === DISCARD
                            performGenericBattleCheck(
                                    currentHero,
                                    DieBattleCheck.Method.SUM,
                                    encounteredDie,
                                    CHOOSE_DICE_PERFORM_CHECK,
                                    StatDieAcquireHandMaskRule(currentHero.hand, encounteredDie!!.die.type),
                                    resolveHeroModifiers() + if (shouldDiscard) 1 else 0,
                                    if (shouldDiscard) deterDivineDiscardRestDieDistributor else deterDivineHideRestDieDistributor,
                                    DIE_ACQUIRE_SUCCESS,
                                    DIE_ACQUIRE_FAILURE,
                                    {
                                        Audio.playSound(Sound.DIE_DRAW)
                                        currentHero.hand.addDie(encounteredDie!!.die)
                                        changePhaseLocationCorrectExploration()
                                    },
                                    { changePhaseLocationCorrectExploration() })
                        }
                        FORFEIT -> {
                            Audio.playSound(Sound.DIE_REMOVE)
                            changePhaseLocationCorrectExploration()
                        }
                        else -> throw AssertionError("Should not happen")
                    }
                }
                //------------------------------------------------------------------------------------------------------
                GamePhase.LOCATION_ENCOUNTER_DIVINE ->
                    when (interactor.pickAction(actions).type) {
                        ACQUIRE -> {
                            Audio.playSound(Sound.DIE_DRAW)
                            currentHero.hand.addDie(encounteredDie!!.die)
                            changePhaseLocationCorrectExploration()
                        }
                        FORFEIT -> {
                            Audio.playSound(Sound.DIE_REMOVE)
                            changePhaseLocationCorrectExploration()
                        }
                        else -> throw AssertionError("Should not happen")
                    }
                //------------------------------------------------------------------------------------------------------
                GamePhase.LOCATION_ENCOUNTER_ALLY ->
                    when (interactor.pickAction(actions).type) {
                        USE_SKILL -> tryUseSkill()
                        ASSIST -> tryAssistCurrentHero()
                        LEAVE -> {
                            Audio.playSound(Sound.LEAVE)
                            changePhaseLocationCorrectExploration()
                        }
                        else -> throw AssertionError("Should not happen")
                    }
                //------------------------------------------------------------------------------------------------------
                GamePhase.LOCATION_ENCOUNTER_WOUND ->
                    when (interactor.pickAction(actions).type) {
                        ENDURE -> {
                            Audio.playSound(Sound.DIE_DRAW)
                            if (!checkHeroCanAcquireDie(currentHero, Die.Type.WOUND)) {
                                currentHero.discardDieFromHand(currentHero.hand.dieAt(0)!!)
                            }
                            currentHero.hand.addDie(encounteredDie!!.die)
                            changePhaseLocationCorrectExploration()
                        }
                        else -> throw AssertionError("Should not happen")
                    }
                //------------------------------------------------------------------------------------------------------
                GamePhase.LOCATION_ENCOUNTER_OBSTACLE -> {
                    val type = interactor.pickAction(actions).type
                    when (type) {
                        DISCARD, HIDE -> {
                            val shouldDiscard = type === DISCARD
                            performGenericBattleCheck(
                                    currentHero,
                                    DieBattleCheck.Method.MAX,
                                    encounteredDie,
                                    CHOOSE_DICE_PERFORM_CHECK,
                                    ObstacleCheckHelper.getObstacleHandMaskRule(encounteredThreat as Obstacle, currentHero.hand),
                                    resolveHeroModifiers() + if (shouldDiscard) 2 else 0,
                                    if (shouldDiscard) discardSmallestHideRestDieDistributor else hideAllDieDistributor,
                                    OBSTACLE_OVERCOME_SUCCESS,
                                    OBSTACLE_OVERCOME_FAILURE,
                                    { battleResultSuccess(0) },
                                    { battleResultFailure(0) })
                        }
                        LEAVE -> {
                            Audio.playSound(Sound.LEAVE)
                            hideEncounteredDieToLocationBag()
                            changePhaseLocationCorrectExploration()
                        }
                        ENDURE -> battleResultFailure(0)
                        INFO -> showInfoScreens()
                        else -> throw AssertionError("Should not happen")
                    }
                }
                //------------------------------------------------------------------------------------------------------
                GamePhase.LOCATION_ENCOUNTER_ENEMY ->
                    when (interactor.pickAction(actions).type) {
                        USE_SKILL -> tryUseSkill()
                        ASSIST -> tryAssistCurrentHero()
                        FLEE -> fleeFromEncounter()
                        INFO -> showInfoScreens()
                        else -> throw AssertionError("Should not happen")
                    }
                //------------------------------------------------------------------------------------------------------
                GamePhase.LOCATION_ENCOUNTER_VILLAIN ->
                    when (interactor.pickAction(actions).type) {
                        USE_SKILL -> tryUseSkill()
                        ASSIST -> tryAssistCurrentHero()
                        FLEE -> fleeFromEncounter()
                        INFO -> showInfoScreens()
                        else -> throw AssertionError("Should not happen")
                    }
                //------------------------------------------------------------------------------------------------------
                GamePhase.LOCATION_AFTER_EXPLORATION ->
                    when (interactor.pickAction(actions).type) {
                        EXPLORE_LOCATION_AGAIN -> when (SpecialRule.TONGUE_IN_A_HEAD in scenario.getSpecialRules()) {
                            true -> if (performActionWithHand(currentHero,
                                            CHOOSE_DICE_HIDE with 1,
                                            SingleNonWoundDieHandMaskRule(currentHero.hand)) {
                                        Audio.playSound(Sound.DIE_HIDE)
                                        currentHero.hideDieFromHand(it)
                                    }) {
                                exploredThisTurn = true
                                exploredAgainThisTurn = true
                                changePhaseLocationCorrectEncounter(currentLocation.bag.draw())
                            }
                            false -> if (performActionWithHand(currentHero,
                                            CHOOSE_DICE_DISCARD with 1,
                                            SingleNonWoundDieHandMaskRule(currentHero.hand)) {
                                        Audio.playSound(Sound.DIE_DISCARD)
                                        currentHero.discardDieFromHand(it)
                                    }) {
                                exploredThisTurn = true
                                exploredAgainThisTurn = true
                                changePhaseLocationCorrectEncounter(currentLocation.bag.draw())
                            }
                        }
                        CLOSE_LOCATION -> attemptToCloseLocation()
                        USE_SKILL -> tryUseSkill()
                        ASSIST -> tryAssistCurrentHero()
                        INFO -> showInfoScreens()
                        FINISH_TURN -> changePhaseHeroTurnEnd()
                        MENU -> changePhaseGamePause()
                        else -> throw AssertionError("Should not happen")
                    }
                //------------------------------------------------------------------------------------------------------
                GamePhase.PAUSE ->
                    when (interactor.pickAction(actions).type) {
                        FORFEIT -> {
                            Audio.playSound(Sound.CONFIRM_PROMPT)
                            statusMessage = CONFIRM_FORFEIT_GAME
                            actions = ActionList().add(CONFIRM).add(CANCEL)
                            drawScreen()
                            when (interactor.pickAction(actions).type) {
                                CONFIRM -> {
                                    Audio.playSound(Sound.DIE_DISCARD)
                                    screen = GameScreen.LOCATION_INTERIOR
                                    statusMessage = EMPTY
                                    actions = ActionList.EMPTY
                                    drawScreen()
                                    slowDown()
                                    while (timer > 0) {
                                        Audio.playSound(Sound.DIE_DISCARD)
                                        timer--
                                        drawScreen()
                                        if (timer < 4) {
                                            slowDown()
                                        } else {
                                            slowBit()
                                        }
                                    }
                                    timer--
                                    changePhaseGameLost(GAME_LOSS_OUT_OF_TIME)
                                }
                                CANCEL -> changePhaseGamePause()
                                else -> throw AssertionError("Should not happen")
                            }
                        }
                        EXIT -> {
                            Audio.playSound(Sound.PAGE_CLOSE)
                            changePhaseLocationCorrectExploration()
                            realTimeSave.shutdown()
                            callback(Result.EXIT, heroes, deterrentPile, phaseIndex)
                            muteMusic()
                            return
                        }
                        CANCEL -> changePhaseLocationCorrectExploration()
                        else -> throw AssertionError("Should not happen")
                    }
                //------------------------------------------------------------------------------------------------------
                else -> throw AssertionError("Should not happen")
            }
            //----------------------------------------------------------------------------------------------------------
        }
    }

    //==================================================================================================================

    /**
     * Checks whether heroes should lose when villain escaped
     */
    private fun checkGameLossVillainEscaped() =
            (encounteredThreat is Villain && SpecialRule.DONT_LET_THEM_ESCAPE in scenario.getSpecialRules())

    /**
     * What to do when ENEMY or VILLAIN is defeated
     */
    private val battleResultSuccess = { result: Int ->
        when (encounteredThreat!!) {
            is Enemy -> Audio.playSound(Sound.DEFEAT_ENEMY)
            is Villain -> Audio.playSound(Sound.DEFEAT_VILLAIN)
            is Obstacle -> Audio.playSound(Sound.DEFEAT_OBSTACLE)
        }
        val traits = encounteredThreat!!.getTraits()
        val thresholdPassed = when {
            Trait.DEFEAT_THRESHOLD_ONE in traits && result < 1 -> false
            Trait.DEFEAT_THRESHOLD_TWO in traits && result < 2 -> false
            Trait.DEFEAT_THRESHOLD_THREE in traits && result < 3 -> false
            Trait.DEFEAT_THRESHOLD_EVEN in traits && result % 2 != 0 -> false
            Trait.DEFEAT_THRESHOLD_ODD in traits && result % 2 != 1 -> false
            Trait.DEFEAT_THRESHOLD_INFINITE in traits -> false
            else -> true
        }
        if (thresholdPassed) { //Threat defeated
            applyCardSuccessConditions(traits)
            startScenarioMusic()
            when {
                Trait.WIN_CLOSE_AUTO in traits && currentLocation.isOpen -> autoCloseLocation()
                Trait.WIN_CLOSE_ATTEMPT in traits && currentLocation.isOpen && checkHeroCanCloseLocation(currentHero) -> {
                    encounteredDie = null
                    val temp = attemptedCloseThisTurn
                    attemptToCloseLocation()
                    if (!attemptedCloseThisTurn) {
                        attemptedCloseThisTurn = temp
                        changePhaseLocationCorrectExploration()
                    }
                }
                Trait.WIN_EXPLORE_AGAIN in traits && checkLocationCanBeExplored(currentLocation) -> {
                    encounteredDie = null
                    when (quickActionPick(CONFIRM_EXPLORE_AGAIN, CONFIRM, CANCEL)) {
                        CONFIRM -> changePhaseLocationCorrectEncounter(currentLocation.bag.draw())
                        else -> changePhaseLocationCorrectExploration()
                    }
                }
                else -> changePhaseLocationCorrectExploration()
            }
        } else { //Threat not defeated
            if (checkGameLossVillainEscaped()) {
                changePhaseGameLost(GAME_LOSS_VILLAIN_ESCAPED)
            } else {
                hideEncounteredDieToLocationBag()
                changePhaseLocationCorrectExploration()
            }
        }
    }

    /**
     * What to do when battle check failed
     */
    private val battleResultFailure = { result: Int ->
        if (checkGameLossVillainEscaped()) {
            changePhaseGameLost(GAME_LOSS_VILLAIN_ESCAPED)
        } else {
            val traits = encounteredThreat!!.getTraits()
            val bag = currentLocation.bag
            applyCardFailureConditions(traits, -result)
            hideEncounteredDieToLocationBag(bag)
            when {
                Trait.DAMAGE_FINISH_TURN_IMMEDIATELY in traits -> changePhaseHeroTurnEnd(true)
                Trait.DAMAGE_FINISH_TURN in traits -> changePhaseHeroTurnEnd()
                else -> changePhaseLocationCorrectExploration()
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Use skill by current hero
     * @param skill Skill to use
     */
    private fun useSkill(skill: Skill) {
        Audio.playSound(Sound.SKILL_PREPARE)
        when (skill.type) {
            HIT, POWER_PUNCH, SHOOT, MAGIC_BOLT -> {
                val type = quickActionPick(CHOOSE_HOW_USE_SKILL, HIDE, DISCARD, CANCEL)
                when (type) {
                    HIDE, DISCARD -> {
                        val shouldDiscard = type === DISCARD
                        performGenericBattleCheck(
                                currentHero,
                                SkillCheckHelper.getBattleCheckMethod(skill, shouldDiscard),
                                encounteredDie,
                                CHOOSE_DICE_USE_SKILL,
                                SkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand),
                                resolveHeroModifiers() + if (shouldDiscard) skill.getModifier2Value() else skill.getModifier1Value(),
                                SkillCheckHelper.getDieDistributor(skill, shouldDiscard),
                                ENEMY_DEFEAT_SUCCESS,
                                ENEMY_DEFEAT_FAILURE,
                                battleResultSuccess,
                                battleResultFailure)
                    }
                    CANCEL -> { //Do nothing
                    }
                    else -> throw AssertionError("Should not happen")
                }
            }
            DODGE -> performActionWithHand(currentHero, CHOOSE_DICE_USE_SKILL,
                    SkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand)) {
                Audio.playSound(Sound.DIE_HIDE)
                currentHero.hideDieFromHand(it)
                currentDamage?.reduce(2)
            }
            INTIMIDATE -> performActionWithHand(currentHero, CHOOSE_DICE_USE_SKILL,
                    SkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand)) {
                Audio.playSound(Sound.DIE_HIDE)
                currentHero.hideDieFromHand(it)
                encounteredDie!!.modifier += skill.getModifier1Value()
            }
            SUBMIT, SERMON, MIND_CONTROL, RECRUITMENT -> {
                val type = quickActionPick(CHOOSE_HOW_USE_SKILL, HIDE, DISCARD, CANCEL)
                when (type) {
                    HIDE, DISCARD -> {
                        val shouldDiscard = type === DISCARD
                        performGenericBattleCheck(
                                currentHero,
                                SkillCheckHelper.getBattleCheckMethod(skill, shouldDiscard),
                                encounteredDie,
                                CHOOSE_DICE_USE_SKILL,
                                SkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand),
                                resolveHeroModifiers() + if (shouldDiscard) skill.getModifier2Value() else skill.getModifier1Value(),
                                SkillCheckHelper.getDieDistributor(skill, shouldDiscard),
                                DIE_ACQUIRE_SUCCESS,
                                DIE_ACQUIRE_FAILURE,
                                {
                                    Audio.playSound(Sound.DIE_DRAW)
                                    currentHero.hand.addDie(encounteredDie!!.die)
                                    changePhaseLocationCorrectExploration()
                                },
                                {
                                    Audio.playSound(Sound.LEAVE)
                                    changePhaseLocationCorrectExploration()
                                })
                    }
                    CANCEL -> { //Do nothing
                    }
                    else -> throw AssertionError("Should not happen")
                }
            }
            STONE_SKIN, EVASION, YOU_FIRST -> performGenericBattleCheck(
                    currentHero,
                    SkillCheckHelper.getBattleCheckMethod(skill),
                    null,
                    CHOOSE_DICE_USE_SKILL,
                    SkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand),
                    skill.getModifier1Value(),
                    SkillCheckHelper.getDieDistributor(skill),
                    EMPTY,
                    EMPTY,
                    { result ->
                        fun showStatus() {
                            statusMessage = DAMAGE_LEFT with currentDamage!!.amount
                            drawScreen()
                            slowDown()
                        }
                        Audio.playSound(Sound.DIE_DISCARD)
                        actions = ActionList.EMPTY
                        showStatus()
                        val amount = min(if (skill.type == Skill.Type.YOU_FIRST) (result / 2) else result, currentDamage!!.amount)
                        (1..amount).forEach {
                            currentDamage?.reduce(1)
                            showStatus()
                        }
                    },
                    {})
            WARCRY -> performGenericBattleCheck(
                    currentHero,
                    SkillCheckHelper.getBattleCheckMethod(skill),
                    null,
                    CHOOSE_DICE_USE_SKILL,
                    SkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand),
                    skill.getModifier1Value(),
                    SkillCheckHelper.getDieDistributor(skill),
                    EMPTY,
                    EMPTY,
                    { result ->
                        if (result > 0) {
                            Audio.playSound(Sound.UPGRADE)
                            pendingHeroModifiers1.add(PendingSkillModifier(skill.type, result))
                        }
                    },
                    {})
            BERSERK, CAUTERIZE, DISCIPLINE -> if (performActionWithHand(currentHero, CHOOSE_WOUND_FROM_HAND,
                            SkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand)) { die ->
                        encounteredDie = DiePair(die, 0)
                        performGenericBattleCheck(
                                currentHero,
                                SkillCheckHelper.getBattleCheckMethod(skill),
                                encounteredDie,
                                CHOOSE_DICE_USE_SKILL,
                                SkillCheckHelper.getHandMaskRuleSecondary(skill, currentHero.hand),
                                resolveHeroModifiers() + skill.getModifier1Value(),
                                SkillCheckHelper.getDieDistributor(skill),
                                WOUND_REMOVE_SUCCESS,
                                WOUND_REMOVE_FAILURE,
                                {
                                    Audio.playSound(Sound.DIE_REMOVE)
                                    currentHero.hand.removeDie(die)
                                },
                                {})
                        encounteredDie = null
                    }) {
                changePhaseLocationCorrectExploration()
            }
            STEALTH, NEGOTIATION -> performGenericBattleCheck(
                    currentHero,
                    SkillCheckHelper.getBattleCheckMethod(skill),
                    encounteredDie,
                    CHOOSE_DICE_USE_SKILL,
                    SkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand),
                    resolveHeroModifiers() + skill.getModifier1Value(),
                    SkillCheckHelper.getDieDistributor(skill),
                    ENCOUNTER_EVADE_SUCCESS,
                    ENCOUNTER_EVADE_FAILURE,
                    {
                        Audio.playSound(Sound.DIE_HIDE)
                        hideEncounteredDieToLocationBag()
                        changePhaseLocationCorrectExploration()
                    },
                    {}
            )
            READ_TRACES -> if (performActionWithHand(currentHero, CHOOSE_DICE_USE_SKILL,
                            SkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand)) { currentHero.hideDieFromHand(it) }) {
                changePhaseLocationCorrectEncounter(currentLocation.bag.draw())
            }
            VOLLEY -> {
                fun check(rule: HandMaskRule, allowCancel: Boolean) {
                    performGenericBattleCheck(
                            currentHero,
                            SkillCheckHelper.getBattleCheckMethod(skill),
                            encounteredDie,
                            CHOOSE_DICE_USE_SKILL,
                            rule,
                            resolveHeroModifiers() + skill.getModifier1Value(),
                            SkillCheckHelper.getDieDistributor(skill),
                            ENEMY_DEFEAT_SUCCESS,
                            ENEMY_DEFEAT_FAILURE,
                            battleResultSuccess,
                            { result ->
                                if (SingleDieHandFilter(Die.Type.PHYSICAL, Die.Type.SOMATIC).test(currentHero.hand)
                                        && quickActionPick(CHOOSE_HOW_USE_SKILL, HIDE, CANCEL) == HIDE) {
                                    check(SkillCheckHelper.getHandMaskRuleSecondary(skill, currentHero.hand), false)
                                } else {
                                    battleResultFailure(result)
                                }
                            }, allowCancel)
                }
                check(SkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand), true)
            }
            LAY_TRAP ->
                performGenericBattleCheck(
                        currentHero,
                        SkillCheckHelper.getBattleCheckMethod(skill),
                        null,
                        CHOOSE_DICE_USE_SKILL,
                        SkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand),
                        skill.getModifier1Value(),
                        SkillCheckHelper.getDieDistributor(skill),
                        EMPTY,
                        EMPTY,
                        { result ->
                            exploredThisTurn = true
                            changePhaseLocationCorrectEncounter(currentLocation.bag.draw())
                            if (result > 0 && encounteredDie!!.die.type in setOf(Die.Type.ENEMY, Die.Type.VILLAIN)
                                    && SkillCheckHelper.checkTraitsAllowToUseSkill(skill, encounteredThreat?.getTraits())) {
                                Audio.playSound(Sound.UPGRADE)
                                encounteredDie!!.modifier -= result
                            }
                        },
                        {
                            exploredThisTurn = true
                            changePhaseLocationCorrectEncounter(currentLocation.bag.draw())
                        })
            SELF_BANDAGE, HEAL -> performGenericBattleCheck(
                    currentHero,
                    SkillCheckHelper.getBattleCheckMethod(skill),
                    null,
                    CHOOSE_DICE_USE_SKILL,
                    SkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand),
                    skill.getModifier1Value(),
                    SkillCheckHelper.getDieDistributor(skill),
                    EMPTY,
                    EMPTY,
                    { result ->
                        if (result > 0) {
                            performActionWithPile(currentHero.discardPile, CHOOSE_DICE_RETURN_BAG with result,
                                    SkillCheckHelper.getPileMaskRule(skill, currentHero.discardPile, result)) {
                                Audio.playSound(Sound.DIE_HIDE)
                                currentHero.discardPile.removeDie(it)
                                currentHero.bag.put(it)
                                drawScreen()
                                slowDown()
                            }
                        }
                    },
                    {})
            ORIENTEERING -> performGenericBattleCheck(
                    currentHero,
                    SkillCheckHelper.getBattleCheckMethod(skill),
                    null,
                    CHOOSE_DICE_USE_SKILL,
                    SkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand),
                    -getClosingDifficulty(currentLocation) + resolveHeroModifiers() + skill.getModifier1Value(),
                    SkillCheckHelper.getDieDistributor(skill),
                    LOCATION_CLOSE_SUCCESS,
                    LOCATION_CLOSE_FAILURE,
                    {
                        clearPendingHeroModifiers()
                        attemptedCloseThisTurn = true
                        exploredThisTurn = true
                        exploredAgainThisTurn = true
                        closeLocation(currentLocation)
                    },
                    {
                        clearPendingHeroModifiers()
                        attemptedCloseThisTurn = true
                        exploredThisTurn = true
                        exploredAgainThisTurn = true
                        changePhaseLocationCorrectExploration()
                    }
            )
            WARP -> performGenericBattleCheck(
                    currentHero,
                    SkillCheckHelper.getBattleCheckMethod(skill),
                    null,
                    CHOOSE_DICE_USE_SKILL,
                    SkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand),
                    skill.getModifier1Value() - currentDamage!!.amount,
                    SkillCheckHelper.getDieDistributor(skill),
                    DAMAGE_PREVENT_SUCCESS,
                    DAMAGE_PREVENT_FAILURE,
                    { currentDamage!!.prevent() },
                    { Audio.playSound(Sound.TAKE_DAMAGE) })
            CONCENTRATION -> {
                var counter = 0
                if (performActionWithHand(currentHero, CHOOSE_DICE_USE_SKILL,
                                SkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand)) {
                            if (counter == 0) {
                                Audio.playSound(Sound.DIE_DISCARD)
                                currentHero.discardDieFromHand(it)
                            } else {
                                Audio.playSound(Sound.DIE_HIDE)
                                currentHero.hideDieFromHand(it)
                            }
                            statusMessage = EMPTY
                            actions = ActionList.EMPTY
                            drawScreen()
                            slowDown()
                            counter++
                        }) {
                    counter = 0
                    if (performActionWithPile(currentHero.discardPile, CHOOSE_DICE_RETURN_HAND with skill.getModifier1Value() + 1,
                                    SkillCheckHelper.getPileMaskRule(skill, currentHero.discardPile, skill.getModifier1Value()), false) {
                                currentHero.discardPile.removeDie(it)
                                if (counter == 0) {
                                    Audio.playSound(Sound.DIE_DRAW)
                                    currentHero.hand.addDie(it)
                                } else {
                                    Audio.playSound(Sound.DIE_HIDE)
                                    currentHero.bag.put(it)
                                }
                                drawScreen()
                                slowDown()
                                counter++
                            }) {
                        changePhaseLocationCorrectExploration()
                    }
                }
            }
            TELEPORT -> if (performActionWithHand(currentHero, CHOOSE_DICE_USE_SKILL,
                            SkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand)) {
                        Audio.playSound(Sound.DIE_HIDE)
                        currentHero.hideDieFromHand(it)
                        statusMessage = EMPTY
                        actions = ActionList.EMPTY
                        drawScreen()
                        slowDown()
                    }) {
                travelToOtherLocation(currentHero, allowCancel = true, needToSetTraveledFlag = false)
            }
            INGENUITY -> {
                saveState()
                var count = -1
                if (performActionWithHand(currentHero, CHOOSE_DICE_HIDE with skill.getModifier1Value(),
                                SkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand)) { die ->
                            Audio.playSound(Sound.DIE_HIDE)
                            currentHero.hideDieFromHand(die)
                            statusMessage = EMPTY
                            actions = ActionList.EMPTY
                            drawScreen()
                            slowDown()
                            count++
                        }) {
                    statusMessage = EMPTY
                    actions = ActionList.EMPTY
                    (0 until count).forEach { drawDieFromHeroBag(currentHero) }
                }
                restoreState()
            }
            DEVASTATION ->
                if (performGenericBattleCheck(
                                currentHero,
                                SkillCheckHelper.getBattleCheckMethod(skill),
                                encounteredDie,
                                CHOOSE_DICE_USE_SKILL,
                                SkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand),
                                resolveHeroModifiers() + skill.getModifier1Value(),
                                SkillCheckHelper.getDieDistributor(skill),
                                ENEMY_DEFEAT_SUCCESS,
                                ENEMY_DEFEAT_FAILURE,
                                battleResultSuccess,
                                battleResultFailure)) {
                    Audio.playSound(Sound.EXPLOSION)
                    heroPlacement[currentLocation]?.filter(Hero::isAlive)?.forEach { h ->
                        takeDamage(h, Damage(Damage.Type.MAGICAL, 1))
                    }
                }
            HOLY_LIGHT ->
                performGenericBattleCheck(
                        currentHero,
                        SkillCheckHelper.getBattleCheckMethod(skill),
                        encounteredDie,
                        CHOOSE_DICE_USE_SKILL,
                        SkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand),
                        resolveHeroModifiers() + skill.getModifier1Value(),
                        SkillCheckHelper.getDieDistributor(skill),
                        ENEMY_DEFEAT_SUCCESS,
                        ENEMY_DEFEAT_FAILURE,
                        battleResultSuccess,
                        battleResultFailure)
            PRAYER -> if (performActionWithPile(currentHero.discardPile, CHOOSE_DICE_RETURN_BAG with skill.getModifier1Value(),
                            SkillCheckHelper.getPileMaskRule(skill, currentHero.discardPile, skill.getModifier1Value())) {
                        Audio.playSound(Sound.DIE_HIDE)
                        currentHero.discardPile.removeDie(it)
                        currentHero.bag.put(it)
                        drawScreen()
                        slowDown()
                    }) {
                exploredThisTurn = true
                changePhaseLocationCorrectExploration()
            }
            FORESIGHT -> if (performActionWithHand(currentHero, CHOOSE_DICE_USE_SKILL,
                            SkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand)) { die ->
                        Audio.playSound(Sound.DIE_HIDE)
                        currentHero.hideDieFromHand(die)
                        encounteredDie = DiePair(currentLocation.bag.draw())
                        if (encounteredDie!!.die.type == Die.Type.VILLAIN) {
                            Audio.playSound(Sound.ENCOUNTER_VILLAIN)
                            statusMessage = ENCOUNTER_VILLAIN
                            actions = ActionList.EMPTY
                            drawScreen()
                            interactor.anyInput()
                        } else {
                            performGenericBattleCheck(
                                    currentHero,
                                    SkillCheckHelper.getBattleCheckMethod(skill),
                                    encounteredDie,
                                    CHOOSE_DICE_USE_SKILL,
                                    SkillCheckHelper.getHandMaskRuleSecondary(skill, currentHero.hand),
                                    resolveHeroModifiers() + skill.getModifier1Value(),
                                    SkillCheckHelper.getDieDistributor(skill),
                                    ENCOUNTER_EVADE_SUCCESS,
                                    ENCOUNTER_EVADE_FAILURE,
                                    { encounteredDie = null },
                                    { })
                        }
                        encounteredDie?.let {
                            Audio.playSound(Sound.DIE_HIDE)
                            hideEncounteredDieToLocationBag()
                        }
                    }) {
                changePhaseLocationCorrectExploration()
            }
            BLESSING -> performGenericBattleCheck(
                    currentHero,
                    SkillCheckHelper.getBattleCheckMethod(skill),
                    null,
                    CHOOSE_DICE_USE_SKILL,
                    SkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand),
                    skill.getModifier1Value(),
                    SkillCheckHelper.getDieDistributor(skill),
                    EMPTY,
                    EMPTY,
                    { result ->
                        if (result > 0) {
                            Audio.playSound(Sound.UPGRADE)
                            pendingHeroModifiers1.add(PendingSkillModifier(skill.type, result))
                        }
                    },
                    {})
            LORDS_GRACE -> {
                actions = ActionList.EMPTY
                statusMessage = EMPTY
                drawScreen()
                slowDown()
                Audio.playSound(Sound.DIE_DRAW)
                val die = currentHero.bag.draw()
                currentHero.hand.addDie(die)
                if (die.type == Die.Type.DIVINE) {
                    statusMessage = LORDS_GRACE_DIVINE
                    drawScreen()
                    interactor.anyInput()
                    Audio.playSound(Sound.ENCOUNTER_DIVINE)
                    currentHero.hand.addDie(Die(Die.Type.DIVINE, skill.getModifier1Value()))
                } else {
                    statusMessage = LORDS_GRACE_NOT_DIVINE
                    drawScreen()
                    interactor.anyInput()
                    Audio.playSound(Sound.DIE_DISCARD)
                    currentHero.discardDieFromHand(die)
                }
                changePhaseLocationCorrectExploration()
            }
            CELESTIAL_OMEN -> {
                performGenericBattleCheck(
                        currentHero,
                        SkillCheckHelper.getBattleCheckMethod(skill),
                        null,
                        CHOOSE_DICE_USE_SKILL,
                        SkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand),
                        skill.getModifier1Value(),
                        SkillCheckHelper.getDieDistributor(skill),
                        EMPTY,
                        EMPTY,
                        { result ->
                            actions = ActionList.EMPTY
                            statusMessage = EMPTY
                            (1..result).forEach {
                                increaseTimer()
                            }
                        },
                        {})
                changePhaseLocationCorrectExploration()
            }
            SUMMON -> if (performActionWithHand(currentHero, CHOOSE_DICE_USE_SKILL,
                            SkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand)) { die ->
                        Audio.playSound(Sound.DIE_HIDE)
                        currentHero.hideDieFromHand(die)
                        drawScreen()
                        slowDown()
                    }) {
                (1..min(skill.getModifier1Value(), currentHero.bag.size)).forEach {
                    drawDieFromHeroBag(currentHero)
                }
            }
            PAST_EXPERIENCE -> if (performActionWithHand(currentHero, CHOOSE_DICE_USE_SKILL,
                            SkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand)) {
                        Audio.playSound(Sound.DIE_DISCARD)
                        currentHero.discardDieFromHand(it)
                        drawScreen()
                        slowDown()
                    }) {
                if (performActionWithPile(currentHero.discardPile, CHOOSE_DICE_RETURN_BAG with skill.getModifier1Value(),
                                SkillCheckHelper.getPileMaskRule(skill, currentHero.discardPile, skill.getModifier1Value()), false) {
                            Audio.playSound(Sound.DIE_HIDE)
                            currentHero.discardPile.removeDie(it)
                            currentHero.bag.put(it)
                            drawScreen()
                            slowDown()
                        }) {
                    changePhaseLocationCorrectExploration()
                }
            }
            RESUSCITATION -> {
                val type = quickActionPick(CHOOSE_HOW_USE_SKILL, HIDE, DISCARD, CANCEL)
                when (type) {
                    HIDE, DISCARD -> {
                        val shouldDiscard = type === DISCARD
                        val dice = mutableListOf<Die>()
                        if (performActionWithPile(currentHero.discardPile, CHOOSE_DICE_RETURN_BAG with if (shouldDiscard) 2 else 1,
                                        SkillCheckHelper.getPileMaskRule(skill, currentHero.discardPile, if (shouldDiscard) 2 else 1)) { dice.add(it) }) {
                            encounteredDie = DiePair(dice.minBy(Die::size)!!, 0)
                            performGenericBattleCheck(
                                    currentHero,
                                    SkillCheckHelper.getBattleCheckMethod(skill, shouldDiscard),
                                    encounteredDie,
                                    CHOOSE_DICE_USE_SKILL,
                                    SkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand),
                                    resolveHeroModifiers() + skill.getModifier1Value(),
                                    SkillCheckHelper.getDieDistributor(skill, shouldDiscard),
                                    ALLY_ACQUIRE_SUCCESS,
                                    ALLY_ACQUIRE_FAILURE,
                                    {
                                        encounteredDie = null
                                        actions = ActionList.EMPTY
                                        statusMessage = EMPTY
                                        dice.forEach { die ->
                                            Audio.playSound(Sound.DIE_HIDE)
                                            currentHero.discardPile.removeDie(die)
                                            currentHero.bag.put(die)
                                            drawScreen()
                                            slowDown()
                                        }
                                    },
                                    {})
                            changePhaseLocationCorrectExploration()
                        }
                    }
                    CANCEL -> { //Do nothing
                    }
                    else -> throw AssertionError("Should not happen")
                }
            }
            EXPENDABLES -> {
                encounteredDie = DiePair(currentLocation.bag.draw())
                statusMessage = EMPTY
                actions = ActionList.EMPTY
                drawScreen()
                interactor.anyInput()
                when (encounteredDie!!.die.type) {
                    Die.Type.ENEMY, Die.Type.OBSTACLE -> performGenericBattleCheck(
                            currentHero,
                            SkillCheckHelper.getBattleCheckMethod(skill),
                            encounteredDie,
                            CHOOSE_DICE_USE_SKILL,
                            SkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand),
                            resolveHeroModifiers() + skill.getModifier1Value(),
                            SkillCheckHelper.getDieDistributor(skill),
                            ENCOUNTER_EVADE_SUCCESS,
                            ENCOUNTER_EVADE_FAILURE,
                            { encounteredDie = null },
                            {
                                Audio.playSound(Sound.DIE_HIDE)
                                hideEncounteredDieToLocationBag()
                            }, false)
                    else -> {
                        Audio.playSound(Sound.DIE_HIDE)
                        hideEncounteredDieToLocationBag()
                        performActionWithHand(currentHero, CHOOSE_DICE_HIDE with 1,
                                SkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand), false) {
                            Audio.playSound(Sound.DIE_HIDE)
                            currentHero.hideDieFromHand(it)
                            drawScreen()
                            slowDown()
                        }
                    }
                }
                changePhaseLocationCorrectExploration()
            }
            else -> throw AssertionError("Should not happen")
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Use Ally skill by current hero
     * @param skill Ally skill to use
     */
    private fun useAllySkill(skill: AllySkill) {
        Audio.playSound(Sound.ENCOUNTER_ALLY)
        val inspirerModifier = (currentHero.getSkills().firstOrNull { it.type === INSPIRER }?.getModifier1Value() ?: 0)
        when (skill.type) {
            COMBAT_ADVISOR -> performGenericBattleCheck(
                    currentHero,
                    AllySkillCheckHelper.getBattleCheckMethod(skill),
                    null,
                    CHOOSE_DICE_USE_ALLY_SKILL,
                    AllySkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand),
                    inspirerModifier + skill.modifier1,
                    AllySkillCheckHelper.getDieDistributor(skill),
                    EMPTY,
                    EMPTY,
                    { result ->
                        if (result > 0) {
                            Audio.playSound(Sound.UPGRADE)
                            pendingHeroModifiers2.add(PendingAllyModifier(skill.type, result))
                        }
                    },
                    { })
            DARK_ENCHANTER -> performGenericBattleCheck(
                    currentHero,
                    AllySkillCheckHelper.getBattleCheckMethod(skill),
                    null,
                    CHOOSE_DICE_USE_ALLY_SKILL,
                    AllySkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand),
                    inspirerModifier + skill.modifier1,
                    AllySkillCheckHelper.getDieDistributor(skill),
                    EMPTY,
                    EMPTY,
                    { result ->
                        if (result > 0) {
                            Audio.playSound(Sound.UPGRADE)
                            encounteredDie!!.modifier -= result
                        }
                    },
                    { result ->
                        Audio.playSound(Sound.UPGRADE)
                        encounteredDie!!.modifier -= result

                    })
            DEFENDER -> performGenericBattleCheck(
                    currentHero,
                    AllySkillCheckHelper.getBattleCheckMethod(skill),
                    null,
                    CHOOSE_DICE_USE_ALLY_SKILL,
                    AllySkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand),
                    inspirerModifier + skill.modifier1,
                    AllySkillCheckHelper.getDieDistributor(skill),
                    EMPTY,
                    EMPTY,
                    { result ->
                        fun showStatus() {
                            statusMessage = DAMAGE_LEFT with currentDamage!!.amount
                            drawScreen()
                            slowDown()
                        }
                        Audio.playSound(Sound.DIE_DISCARD)
                        actions = ActionList.EMPTY
                        showStatus()
                        (1..min(result, currentDamage!!.amount)).forEach {
                            currentDamage?.reduce(1)
                            showStatus()
                        }
                    },
                    {})
            DIPLOMAT -> {
                val type = quickActionPick(CHOOSE_HOW_USE_SKILL, HIDE, DISCARD, CANCEL)
                when (type) {
                    HIDE, DISCARD -> {
                        val shouldDiscard = type === DISCARD
                        performGenericBattleCheck(
                                currentHero,
                                AllySkillCheckHelper.getBattleCheckMethod(skill, shouldDiscard),
                                encounteredDie,
                                CHOOSE_DICE_USE_ALLY_SKILL,
                                AllySkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand),
                                resolveHeroModifiers() + inspirerModifier + if (shouldDiscard) skill.modifier2 else skill.modifier1,
                                AllySkillCheckHelper.getDieDistributor(skill, shouldDiscard),
                                ENCOUNTER_EVADE_SUCCESS,
                                ENCOUNTER_EVADE_FAILURE,
                                {
                                    Audio.playSound(Sound.DIE_HIDE)
                                    hideEncounteredDieToLocationBag()
                                    changePhaseLocationCorrectExploration()
                                },
                                {})
                    }
                    CANCEL -> { //Do nothing
                    }
                    else -> throw AssertionError("Should not happen")
                }
            }
            FALCON -> if (performActionWithHand(currentHero, CHOOSE_DICE_USE_ALLY_SKILL,
                            AllySkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand)) { die ->
                        currentHero.discardDieFromHand(die)
                        encounteredDie = DiePair(currentLocation.bag.draw())
                        if (encounteredDie!!.die.type == Die.Type.VILLAIN) {
                            Audio.playSound(Sound.ENCOUNTER_VILLAIN)
                            statusMessage = ENCOUNTER_VILLAIN
                            drawScreen()
                            interactor.anyInput()
                            Audio.playSound(Sound.DIE_HIDE)
                            hideEncounteredDieToLocationBag()
                        } else {
                            Audio.playSound(Sound.BATTLE_CHECK_SUCCESS)
                            statusMessage = ENCOUNTER_EVADE_SUCCESS
                            drawScreen()
                            interactor.anyInput()
                            Audio.playSound(Sound.DIE_REMOVE)
                        }
                    }) {
                changePhaseLocationCorrectExploration()
            }
            FIELD_MEDIC -> if (performActionWithHand(currentHero, CHOOSE_DICE_USE_ALLY_SKILL,
                            AllySkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand)) { die ->
                        encounteredDie = DiePair(die, 0)
                        performGenericBattleCheck(
                                currentHero,
                                AllySkillCheckHelper.getBattleCheckMethod(skill),
                                encounteredDie,
                                CHOOSE_DICE_USE_ALLY_SKILL,
                                AllySkillCheckHelper.getHandMaskRuleSecondary(skill, currentHero.hand),
                                resolveHeroModifiers() + inspirerModifier + skill.modifier1,
                                AllySkillCheckHelper.getDieDistributor(skill),
                                WOUND_REMOVE_SUCCESS,
                                WOUND_REMOVE_FAILURE,
                                {
                                    Audio.playSound(Sound.DIE_REMOVE)
                                    currentHero.hand.removeDie(die)
                                },
                                {})
                        encounteredDie = null
                    }) {
                changePhaseLocationCorrectExploration()
            }
            HERMIT -> performGenericBattleCheck(
                    currentHero,
                    AllySkillCheckHelper.getBattleCheckMethod(skill),
                    null,
                    CHOOSE_DICE_USE_ALLY_SKILL,
                    AllySkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand),
                    inspirerModifier + skill.modifier1,
                    AllySkillCheckHelper.getDieDistributor(skill),
                    EMPTY,
                    EMPTY,
                    { result ->
                        if (result > 0) {
                            Audio.playSound(Sound.UPGRADE)
                            pendingHeroModifiers2.add(PendingAllyModifier(skill.type, result))
                        }
                    },
                    {})
            INTERN -> if (performActionWithHand(currentHero, CHOOSE_DICE_USE_ALLY_SKILL,
                            AllySkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand)) {
                        Audio.playSound(Sound.DIE_DISCARD)
                        currentHero.hand.removeDie(it)
                        deterrentPile.put(it)
                        drawScreen()
                        slowDown()
                    }) {
                if (performActionWithPile(currentHero.discardPile, CHOOSE_DICE_RETURN_HAND with skill.modifier1,
                                AllySkillCheckHelper.getPileMaskRule(skill, currentHero.discardPile, skill.modifier1), false) {
                            Audio.playSound(Sound.DIE_DRAW)
                            currentHero.discardPile.removeDie(it)
                            currentHero.hand.addDie(it)
                            drawScreen()
                            slowDown()
                        }) {
                    changePhaseLocationCorrectExploration()
                }
            }
            ILLUSIONIST -> performGenericBattleCheck(
                    currentHero,
                    AllySkillCheckHelper.getBattleCheckMethod(skill),
                    null,
                    CHOOSE_DICE_USE_ALLY_SKILL,
                    AllySkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand),
                    inspirerModifier + skill.modifier1 - currentDamage!!.amount,
                    AllySkillCheckHelper.getDieDistributor(skill),
                    DAMAGE_PREVENT_SUCCESS,
                    DAMAGE_PREVENT_FAILURE,
                    { currentDamage!!.prevent() },
                    { Audio.playSound(Sound.TAKE_DAMAGE) })
            LOCAL_DWELLER -> if (performActionWithHand(currentHero, CHOOSE_DICE_USE_ALLY_SKILL,
                            AllySkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand)) { currentHero.hideDieFromHand(it) }) {
                Audio.playSound(Sound.UPGRADE)
                pendingHeroModifiers2.add(PendingAllyModifier(skill.type, skill.modifier1))
                changePhaseLocationCorrectExploration()
            }
            MASTERFUL_DISTRACTION -> {
                //Prepare check
                saveState()
                phase = GamePhase.LOCATION_CLOSE_ATTEMPT
                //Perform check
                val checkPerformed = performGenericBattleCheck(
                        currentHero,
                        AllySkillCheckHelper.getBattleCheckMethod(skill),
                        null,
                        CHOOSE_DICE_USE_ALLY_SKILL,
                        AllySkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand),
                        -getClosingDifficulty(currentLocation) + resolveHeroModifiers() + inspirerModifier + skill.modifier1,
                        AllySkillCheckHelper.getDieDistributor(skill),
                        LOCATION_CLOSE_SUCCESS,
                        LOCATION_CLOSE_FAILURE,
                        {
                            clearPendingHeroModifiers()
                            attemptedCloseThisTurn = true
                            exploredThisTurn = true
                            closeLocation(currentLocation)
                        },
                        {
                            clearPendingHeroModifiers()
                            attemptedCloseThisTurn = true
                            exploredThisTurn = true
                            changePhaseLocationCorrectExploration()
                        })
                if (!checkPerformed) {
                    restoreState()
                }
            }
            PATHFINDER -> if (performActionWithHand(currentHero, CHOOSE_DICE_USE_ALLY_SKILL,
                            AllySkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand)) { currentHero.hideDieFromHand(it) }) {
                travelToOtherLocation(currentHero, allowCancel = true, needToSetTraveledFlag = false)
            }
            PHYSICIAN -> when (quickActionPick(CHOOSE_HOW_USE_SKILL, HIDE, DETER, CANCEL)) {
                HIDE -> if (performActionWithHand(currentHero, CHOOSE_DICE_USE_ALLY_SKILL,
                                AllySkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand)) {
                            Audio.playSound(Sound.DIE_HIDE)
                            currentHero.hideDieFromHand(it)
                            drawScreen()
                            slowDown()
                        }) {
                    if (performActionWithPile(currentHero.discardPile, CHOOSE_DICE_RETURN_BAG with skill.modifier1,
                                    AllySkillCheckHelper.getPileMaskRule(skill, currentHero.discardPile, skill.modifier1), false) {
                                Audio.playSound(Sound.DIE_HIDE)
                                currentHero.discardPile.removeDie(it)
                                currentHero.bag.put(it)
                                drawScreen()
                                slowDown()
                            }) {
                        changePhaseLocationCorrectExploration()
                    }
                }
                DETER -> performGenericBattleCheck(
                        currentHero,
                        AllySkillCheckHelper.getBattleCheckMethod(skill),
                        null,
                        CHOOSE_DICE_USE_ALLY_SKILL,
                        AllySkillCheckHelper.getHandMaskRuleSecondary(skill, currentHero.hand),
                        inspirerModifier + skill.modifier2,
                        AllySkillCheckHelper.getDieDistributor(skill),
                        EMPTY,
                        EMPTY,
                        { result ->
                            if (result > 0) {
                                performActionWithPile(currentHero.discardPile, CHOOSE_DICE_RETURN_BAG with result / 2,
                                        AllySkillCheckHelper.getPileMaskRule(skill, currentHero.discardPile, result / 2)) {
                                    Audio.playSound(Sound.DIE_HIDE)
                                    currentHero.discardPile.removeDie(it)
                                    currentHero.bag.put(it)
                                    drawScreen()
                                    slowDown()
                                }
                            }
                        },
                        {})
                CANCEL -> { //Do nothing
                }
                else -> throw AssertionError("Should not happen")
            }
            PYROMANIAC, RETIRED_SOLDIER -> {
                val type = quickActionPick(CHOOSE_HOW_USE_SKILL, HIDE, DISCARD, CANCEL)
                when (type) {
                    HIDE, DISCARD -> {
                        val shouldDiscard = type === DISCARD
                        performGenericBattleCheck(
                                currentHero,
                                AllySkillCheckHelper.getBattleCheckMethod(skill, shouldDiscard),
                                encounteredDie,
                                CHOOSE_DICE_USE_ALLY_SKILL,
                                AllySkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand),
                                resolveHeroModifiers() + inspirerModifier + if (shouldDiscard) skill.modifier2 else skill.modifier1,
                                AllySkillCheckHelper.getDieDistributor(skill, shouldDiscard),
                                ENEMY_DEFEAT_SUCCESS,
                                ENEMY_DEFEAT_FAILURE,
                                battleResultSuccess,
                                battleResultFailure)
                    }
                    CANCEL -> { //Do nothing
                    }
                    else -> throw AssertionError("Should not happen")
                }
            }
            SABOTEUR -> if (performActionWithHand(currentHero, CHOOSE_DICE_USE_ALLY_SKILL,
                            AllySkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand)) {
                        Audio.playSound(Sound.DIE_DISCARD)
                        deterrentPile.put(it)
                        currentHero.hand.removeDie(it)
                    }) {
                closeLocation(currentLocation)
            }
            SCRYER -> {
                encounteredDie = DiePair(currentLocation.bag.draw())
                if (encounteredDie!!.die.type == Die.Type.VILLAIN) {
                    Audio.playSound(Sound.ENCOUNTER_VILLAIN)
                    statusMessage = ENCOUNTER_VILLAIN
                    actions = ActionList.EMPTY
                    drawScreen()
                    interactor.anyInput()
                } else {
                    performGenericBattleCheck(
                            currentHero,
                            AllySkillCheckHelper.getBattleCheckMethod(skill),
                            encounteredDie,
                            CHOOSE_DICE_USE_ALLY_SKILL,
                            AllySkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand),
                            resolveHeroModifiers() + inspirerModifier + skill.modifier1,
                            AllySkillCheckHelper.getDieDistributor(skill),
                            ENCOUNTER_EVADE_SUCCESS,
                            ENCOUNTER_EVADE_FAILURE,
                            { encounteredDie = null },
                            { }, false)
                }
                encounteredDie?.let {
                    Audio.playSound(Sound.DIE_HIDE)
                    hideEncounteredDieToLocationBag()
                }
                changePhaseLocationCorrectExploration()
            }
            TOUR_GUIDE -> if (performActionWithHand(currentHero, CHOOSE_DICE_USE_ALLY_SKILL,
                            AllySkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand)) { currentHero.hideDieFromHand(it) }) {
                changePhaseLocationCorrectEncounter(currentLocation.bag.draw())
            }
            TRICKSTER -> if (performActionWithHand(currentHero, CHOOSE_DICE_USE_ALLY_SKILL,
                            AllySkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand)) {
                        Audio.playSound(Sound.DIE_HIDE)
                        currentHero.hideDieFromHand(it)
                    }) {
                saveState()
                statusMessage = EMPTY
                actions = ActionList.EMPTY
                drawScreen()
                slowDown()
                drawDieFromHeroBag(currentHero)
                restoreState()
            }
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Use skill of assisting hero to help current hero
     * @param assistingHero Hero who uses the skill
     * @param skill         Skill to use
     */
    private fun assistWithSkill(assistingHero: Hero, skill: Skill) {
        Audio.playSound(Sound.SKILL_PREPARE)
        when (skill.type) {
            PROTECT, ELEMENTAL_SHIELD -> performGenericBattleCheck(
                    assistingHero,
                    SkillCheckHelper.getBattleCheckMethod(skill),
                    null,
                    CHOOSE_DICE_USE_SKILL,
                    SkillCheckHelper.getHandMaskRulePrimary(skill, assistingHero.hand),
                    skill.getModifier1Value(),
                    SkillCheckHelper.getDieDistributor(skill),
                    EMPTY,
                    EMPTY,
                    { result ->
                        fun showStatus() {
                            statusMessage = DAMAGE_LEFT with currentDamage!!.amount
                            drawScreen()
                            slowDown()
                        }
                        Audio.playSound(Sound.DIE_DISCARD)
                        actions = ActionList.EMPTY
                        showStatus()
                        (1..min(result, currentDamage!!.amount)).forEach {
                            currentDamage?.reduce(1)
                            showStatus()
                        }
                    },
                    {})
            WARCRY, INVIGORATE, BLESSING, GOOD_ADVICE -> performGenericBattleCheck(
                    assistingHero,
                    SkillCheckHelper.getBattleCheckMethod(skill),
                    null,
                    CHOOSE_DICE_USE_SKILL,
                    SkillCheckHelper.getHandMaskRulePrimary(skill, assistingHero.hand),
                    skill.getModifier1Value(),
                    SkillCheckHelper.getDieDistributor(skill),
                    EMPTY,
                    EMPTY,
                    { result ->
                        if (result > 0) {
                            Audio.playSound(Sound.UPGRADE)
                            pendingHeroModifiers1.add(PendingSkillModifier(skill.type, result))
                        }
                    },
                    {})
            INTERVENE, COVER_FIRE -> performGenericBattleCheck(
                    assistingHero,
                    SkillCheckHelper.getBattleCheckMethod(skill),
                    encounteredDie,
                    CHOOSE_DICE_USE_SKILL,
                    SkillCheckHelper.getHandMaskRulePrimary(skill, assistingHero.hand),
                    skill.getModifier1Value(),
                    SkillCheckHelper.getDieDistributor(skill),
                    ENEMY_DEFEAT_SUCCESS,
                    ENEMY_DEFEAT_FAILURE,
                    battleResultSuccess,
                    battleResultFailure)
            LAY_TRAP ->
                performGenericBattleCheck(
                        assistingHero,
                        SkillCheckHelper.getBattleCheckMethod(skill),
                        null,
                        CHOOSE_DICE_USE_SKILL,
                        SkillCheckHelper.getHandMaskRulePrimary(skill, assistingHero.hand),
                        skill.getModifier1Value(),
                        SkillCheckHelper.getDieDistributor(skill),
                        EMPTY,
                        EMPTY,
                        { result ->
                            exploredThisTurn = true
                            changePhaseLocationCorrectEncounter(currentLocation.bag.draw())
                            if (result > 0 && encounteredDie!!.die.type in setOf(Die.Type.ENEMY, Die.Type.VILLAIN)
                                    && SkillCheckHelper.checkTraitsAllowToUseSkill(skill, encounteredThreat?.getTraits())) {
                                Audio.playSound(Sound.UPGRADE)
                                encounteredDie!!.modifier -= result
                            }
                        },
                        {
                            exploredThisTurn = true
                            changePhaseLocationCorrectEncounter(currentLocation.bag.draw())
                        })
            CAUTERIZE -> if (performActionWithHand(currentHero, CHOOSE_WOUND_FROM_HAND,
                            SkillCheckHelper.getHandMaskRulePrimary(skill, currentHero.hand)) { die ->
                        encounteredDie = DiePair(die, 0)
                        performGenericBattleCheck(
                                assistingHero,
                                SkillCheckHelper.getBattleCheckMethod(skill),
                                encounteredDie,
                                CHOOSE_DICE_USE_SKILL,
                                SkillCheckHelper.getHandMaskRuleSecondary(skill, assistingHero.hand),
                                resolveHeroModifiers() + skill.getModifier1Value(),
                                SkillCheckHelper.getDieDistributor(skill),
                                WOUND_REMOVE_SUCCESS,
                                WOUND_REMOVE_FAILURE,
                                {
                                    Audio.playSound(Sound.DIE_REMOVE)
                                    currentHero.hand.removeDie(die)
                                },
                                {})
                        encounteredDie = null
                    }) {
                changePhaseLocationCorrectExploration()
            }
            HEAL -> performGenericBattleCheck(
                    assistingHero,
                    SkillCheckHelper.getBattleCheckMethod(skill),
                    null,
                    CHOOSE_DICE_USE_SKILL,
                    SkillCheckHelper.getHandMaskRulePrimary(skill, assistingHero.hand),
                    skill.getModifier1Value(),
                    SkillCheckHelper.getDieDistributor(skill),
                    EMPTY,
                    EMPTY,
                    { result ->
                        if (result > 0) {
                            performActionWithPile(currentHero.discardPile, CHOOSE_DICE_RETURN_BAG with result,
                                    SkillCheckHelper.getPileMaskRule(skill, currentHero.discardPile, result)) { die ->
                                Audio.playSound(Sound.DIE_HIDE)
                                currentHero.discardPile.removeDie(die)
                                currentHero.bag.put(die)
                                drawScreen()
                                slowDown()
                            }
                        }
                    },
                    {})
            ERRAND -> if (performActionWithHand(assistingHero, CHOOSE_DICE_USE_SKILL,
                            SkillCheckHelper.getHandMaskRulePrimary(skill, assistingHero.hand)) {
                        Audio.playSound(Sound.DIE_HIDE)
                        assistingHero.hideDieFromHand(it)
                        drawScreen()
                        slowDown()
                    }) {
                giveDieToOtherHero(assistingHero, currentHero, false)
                changePhaseLocationCorrectExploration()
            }
            else -> throw AssertionError("Should not happen")
        }
    }

    //==================================================================================================================

    /**
     * Wait a bit
     */
    private fun slowBit() = Thread.sleep(100)

    /**
     * Wait a bit more
     */
    private fun slowDown() = Thread.sleep(500)

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Draws current screen
     */
    private fun drawScreen() {
        when (screen) {
            GameScreen.SCENARIO_INTRO -> renderer.drawScenarioScenicText(scenario, false, infoCurrentPage, infoPageCount, actions)
            GameScreen.SCENARIO_OUTRO -> renderer.drawScenarioScenicText(scenario, true, infoCurrentPage, infoPageCount, actions)
            GameScreen.HERO_TURN_START -> renderer.drawHeroTurnStart(currentHero)
            GameScreen.HERO_DEATH -> renderer.drawHeroDeath(currentHero)
            GameScreen.LOCATION_INTERIOR -> renderer.drawLocationInteriorScreen(currentLocation, heroPlacement[currentLocation]!!, timer, currentHero, battleCheck, encounteredDie, encounteredThreat, pickedHandPositions, activeHandPositions, statusMessage, actions)
            GameScreen.LOCATION_TRAVEL -> renderer.drawLocationTravelScreen(currentLocation, travelDestination!!, currentHero, travelProgress, statusMessage, actions)
            GameScreen.DIALOG_HERO_SELECT -> renderer.drawHeroSelectionDialog(heroSelectionDialog, statusMessage, actions)
            GameScreen.DIALOG_SKILL_SELECT -> renderer.drawSkillSelectionDialog(skillSelectionDialog, allySkillSelectionDialog, statusMessage, actions)
            GameScreen.DIALOG_PILE_PICK -> renderer.drawPileDiceSelectionDialog(pilePickDialog!!, pickedPilePositions, activePilePositions, statusMessage, actions)
            GameScreen.DIALOG_LOCATION_SELECT -> renderer.drawLocationSelectionDialog(scenario, timer, locationSelectionDialog, heroPlacement, currentHero, statusMessage, actions)
            GameScreen.INFO_SCENARIO_GENERAL -> renderer.drawScenarioInfoGeneral(scenario, deterrentPile, infoCurrentPage, infoPageCount, statusMessage, actions)
            GameScreen.INFO_SCENARIO_RULES -> renderer.drawScenarioInfoRules(scenario, infoCurrentPage, infoPageCount, statusMessage, actions)
            GameScreen.INFO_SCENARIO_ALLIES -> renderer.drawScenarioInfoAllies(scenario, infoCurrentPage, infoPageCount, statusMessage, actions)
            GameScreen.INFO_LOCATION_GENERAL -> renderer.drawLocationInfoGeneral(infoLocation!!, infoCurrentPage, infoPageCount, statusMessage, actions)
            GameScreen.INFO_LOCATION_RULES -> renderer.drawLocationInfoRules(infoLocation!!, infoCurrentPage, infoPageCount, statusMessage, actions)
            GameScreen.INFO_HERO_GENERAL -> renderer.drawHeroInfoGeneral(infoHero!!, statusMessage, actions)
            GameScreen.INFO_HERO_DICE -> renderer.drawHeroInfoDice(infoHero!!, statusMessage, actions)
            GameScreen.INFO_HERO_SKILLS -> renderer.drawHeroInfoSkills(infoHero!!, infoCurrentPage, infoPageCount, statusMessage, actions)
            GameScreen.INFO_ENEMY -> renderer.drawEnemyInfo(encounteredThreat as Enemy, statusMessage, actions)
            GameScreen.INFO_VILLAIN -> renderer.drawVillainInfo(encounteredThreat as Villain, infoCurrentPage, infoPageCount, statusMessage, actions)
            GameScreen.INFO_OBSTACLE -> renderer.drawObstacleInfo(encounteredThreat as Obstacle, statusMessage, actions)
            GameScreen.INFO_MODIFIERS -> renderer.drawModifiersInfo(currentHero, pendingHeroModifiers1, pendingHeroModifiers2, statusMessage, actions)
            GameScreen.GAME_VICTORY -> renderer.drawGameVictory(statusMessage)
            GameScreen.GAME_LOSS -> renderer.drawGameLoss(statusMessage)
            GameScreen.PAUSE_MENU -> renderer.drawGamePauseMenu(statusMessage, actions)
        }

    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Get list of dice (and ally dice) which correspond to the current mask
     * @param hand Hand to get dice from
     * @return List of dice
     */
    private fun collectPickedDice(hand: Hand) =
            (0 until hand.dieCount)
                    .filter(pickedHandPositions::checkPosition)
                    .mapNotNull(hand::dieAt)

    /**
     * Get list of dice (and ally dice) which correspond to the current mask
     * @param hand Hand to get dice from
     * @return List of dice
     */
    private fun collectPickedAllyDice(hand: Hand) =
            (0 until hand.allyDieCount)
                    .filter(pickedHandPositions::checkAllyPosition)
                    .mapNotNull(hand::allyDieAt)

    /**
     * Get list of dice which correspond to the current mask
     * @param pile Pile to get dice from
     * @return List of dice
     */
    private fun collectPickedDice(pile: Pile): List<Die> {
        val dice = pile.examine()
        return (0 until pile.size)
                .filter(pickedPilePositions::checkPosition)
                .map { dice[it] }
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Draw initial hand for hero with respect to favored die type
     * @param hero Hero to draw dice for
     */
    private fun drawInitialHand(hero: Hero) {
        val hand = hero.hand
        val favoredDie = hero.bag.drawOfType(hero.favoredDieType)
        hand.addDie(favoredDie!!)
        refillHeroHand(hero, false)
    }

    /**
     * Hide all dice from hero hand (except for wounds) to the bag
     * @param hero Hero whose hand to hide
     */
    private fun hideHeroHand(hero: Hero) {
        saveState()
        currentHero = hero
        currentLocation = findLocationForHero(currentHero)
        val hand = hero.hand
        statusMessage = EMPTY
        actions = ActionList.EMPTY
        (((0 until hand.dieCount).map { hand.dieAt(it) }) +
                ((0 until hand.allyDieCount).map { hand.allyDieAt(it) }))
                .asSequence()
                .filterNotNull()
                .filter { it.type !== Die.Type.WOUND }
                .forEach {
                    Audio.playSound(Sound.DIE_HIDE)
                    hero.hideDieFromHand(it)
                    drawScreen()
                    slowDown()
                }
        restoreState()
    }

    /**
     * Discard all dice from hero's hand (except for wounds)
     * @param hero Hero whose hand to discard
     */
    private fun discardHeroHand(hero: Hero) {
        saveState()
        currentHero = hero
        currentLocation = findLocationForHero(currentHero)
        val hand = hero.hand
        statusMessage = EMPTY
        actions = ActionList.EMPTY
        drawScreen()
        slowDown()
        (((0 until hand.dieCount).map { hand.dieAt(it) }) +
                ((0 until hand.allyDieCount).map { hand.allyDieAt(it) }))
                .asSequence()
                .filterNotNull()
                .filter { it.type !== Die.Type.WOUND }
                .forEach {
                    Audio.playSound(Sound.DIE_DISCARD)
                    hero.discardDieFromHand(it)
                    drawScreen()
                    slowDown()
                }
        restoreState()
    }

    /**
     * Draws dice from bag to hand
     * @param hero Hero whose hand to refill
     * @param redrawScreen true to play sounds and slow down, false to draw quietly
     */
    private fun refillHeroHand(hero: Hero, redrawScreen: Boolean = true) {
        val hand = hero.hand
        var count = 0
        while (hand.dieCount < hand.capacity && hero.bag.size > 0) {
            val die = hero.bag.draw()
            if (die.type == Die.Type.ALLY && hand.allyDieCount >= MAX_HAND_ALLY_SIZE) {
                hero.bag.put(die)
                if (!SingleNonAllyDieBagFilter().test(hero.bag)) { //Only ALLY dice left in bag
                    break
                }
                continue
            }
            hand.addDie(die)
            if (redrawScreen) {
                Audio.playSound(if (die.type == Die.Type.WOUND) Sound.ENCOUNTER_WOUND else Sound.DIE_DRAW)
                drawScreen()
                slowDown()
            }
            count++
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Find location in which specified hero is currently placed
     * @param hero Hero to search
     * @return Location in which this hero resides
     */
    private fun findLocationForHero(hero: Hero) = locations.first { hero in heroPlacement[it]!! }

    /**
     * Get closing difficulty of specific location
     * @param location Location to get difficulty for
     * @return Resulting closing difficulty
     */
    private fun getClosingDifficulty(location: Location) =
            location.closingDifficulty + if (STAKES_GET_HIGHER in scenario.getSpecialRules()) 1 else 0

    /**
     * Check whether specified location can be explored
     * @param location Location to check
     * @return true if location can be explored, false otherwise
     */
    private fun checkLocationCanBeExplored(location: Location) = location.isOpen && location.bag.size > 0

    /**
     * Check whether specific location can be explored again
     * @param location Location to check
     * @return true if location can be explored again, false otherwise
     */
    private fun checkLocationCanBeExploredAgain(location: Location) =
            checkLocationCanBeExplored(location) && THREAD_CAREFULLY !in location.getSpecialRules() && HASTE_MAKES_WASTE !in scenario.getSpecialRules()

    /**
     * Check whether hero is able to re-explore the location
     * @param hero Hero to perform the check
     * @return true if location can be explored again, false otherwise
     */
    private fun checkHeroCanExploreAgain(hero: Hero) = hero.isAlive && SingleNonWoundDieHandFilter().test(hero.hand)

    /**
     * Check whether specified location can be closed
     * @param location Location to check
     * @return true if location can be closed, false otherwise
     */
    private fun checkLocationCanBeClosed(location: Location) =
            (location.isOpen && location.bag.size == 0 && TOO_DANGEROUS_HERE !in location.getSpecialRules())

    /**
     * Check hero can attempt to close location
     * @param hero Hero to check
     * @return true or false
     */
    private fun checkHeroCanCloseLocation(hero: Hero) = (hero.isAlive && SingleStatDieHandFilter().test(hero.hand))

    /**
     * Check whether heroes accomplished enough to actually win the scenario
     * @return true if all locations are closed, false otherwise
     */
    private fun checkWinningConditions() =
            locations.asSequence().filter { l -> NOTHING_OF_IMPORTANCE !in l.getSpecialRules() }.none(Location::isOpen)

    /**
     * Check whether specified hero can travel to another location
     * @param hero Hero who wants to travel
     * @return true if hero can travel, false otherwise
     */
    private fun checkHeroCanTravel(hero: Hero) = hero.isAlive && locations.size > 1
            && (NOT_SO_FAST !in currentLocation.getSpecialRules() || DoubleNonWoundDieHandFilter().test(hero.hand))

    /**
     * Prepare a list of heroes who the die can be given to
     * @param hero Hero who tries to give die
     * @return List of heroes who can receive the die
     */
    private fun getHeroesToGiveDieTo(hero: Hero) = with(heroPlacement[findLocationForHero(hero)]!!) {
        val filter = FreeSpaceHandFilter()
        heroes.asSequence()
                .filter { it !== hero }
                .filter { it.isAlive }
                .filter { filter.test(it.hand) }
                .filter { it in this }
                .toList()
    }

    /**
     * Check whether specified hero can give die to another hero
     * @param hero Hero to check for
     * @return true if die can be passed, false otherwise
     */
    private fun checkHeroCanGiveDie(hero: Hero) =
            (hero.isAlive && SingleNonWoundDieHandFilter().test(hero.hand) && !getHeroesToGiveDieTo(hero).isEmpty())

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Checks whether specified [Hero] has means to attempt the STAT check
     * @param hero Hero to check
     * @param type Type of (stat) die to check for
     * @return true or false
     */
    private fun checkHeroCanAttemptStatCheck(hero: Hero, type: Die.Type): Boolean {
        return hero.isAlive && SingleDieHandFilter(type).test(hero.hand)
    }

    /**
     * Checks whether specified [Hero] has the ability to acquire the die of specified type
     * @param hero Hero to check
     * @param type Type of the die
     * @return true or false
     */
    private fun checkHeroCanAcquireDie(hero: Hero, type: Die.Type): Boolean {
        if (!hero.isAlive) {
            return false
        }
        return when (type) {
            Die.Type.ALLY -> hero.hand.allyDieCount < MAX_HAND_ALLY_SIZE
            else -> hero.hand.dieCount < MAX_HAND_SIZE
        }
    }

    /**
     * Check whether current hero can use any skills at the moment
     * @param hero Hero to check
     * @return true or false
     */
    private fun checkHeroCanUseSkills(hero: Hero) = hero.isAlive &&
            (SkillCheckHelper.getSkillsHeroCanUse(phase, hero, currentLocation, encounteredThreat?.getTraits()).isNotEmpty() ||
                    AllySkillCheckHelper.getAllySkillsHeroCanUse(scenario, phase, hero, currentLocation, encounteredThreat?.getTraits()).isNotEmpty())


    /**
     * Check whether current hero can be assisted by any other hero
     * @param hero Hero to check
     * @return true or false
     */
    private fun checkHeroCanBeAssisted(hero: Hero): Boolean {
        val location = findLocationForHero(hero)
        return CLAUSTROPHOBIA !in location.getSpecialRules() && heroPlacement
                .flatMap { e ->
                    e.value.map { it !== hero && it.isAlive && SkillCheckHelper.getSkillsHeroCanAssistWith(phase, hero, it, e.key === location, encounteredThreat?.getTraits()).isNotEmpty() }
                }.fold(false) { a, b -> a || b }
    }

    /**
     * Exactly what the name says
     * @param bag Bag to hide die to
     */
    private fun hideEncounteredDieToLocationBag(bag: Bag = currentLocation.bag) {
        Audio.playSound(Sound.DIE_HIDE)
        bag.put(encounteredDie!!.die)
        encounteredDie = null
    }

    /**
     * Flee from encountered ENEMY or VILLAIN
     */
    private fun fleeFromEncounter() {
        if (COVERT_OPERATION in scenario.getSpecialRules()) {
            hideEncounteredDieToLocationBag()
            changePhaseLocationCorrectExploration()
        } else {
            battleResultFailure(-encounteredDie!!.die.size / 2 - encounteredDie!!.modifier)
        }
    }

    //-----------------------------------------------------------------------------------------------------------------

    /**
     * Start playing music
     */
    private fun startSpecificMusic(music: Music) {
        Audio.playMusic(music)
        currentMusic = music
    }

    /**
     * Start scenario music (based on the mood)
     */
    private fun startScenarioMusic() {
        muteMusic() //TODO change implementation
    }

    /**
     * Start playing music for VILLAIN encounter
     */
    private fun startVillainMusic() =
            startSpecificMusic(when ((1..3).random()) {
                1 -> Music.SCENARIO_VILLAIN_1
                2 -> Music.SCENARIO_VILLAIN_2
                else -> Music.SCENARIO_VILLAIN_3
            })

    /**
     * Stop playing music
     */
    private fun muteMusic() {
        Audio.stopMusic()
        currentMusic = null
    }

    //-----------------------------------------------------------------------------------------------------------------

    /**
     * Class for storing saved states
     */
    private data class SavedState(val phase: GamePhase,
                                  val screen: GameScreen,
                                  val statusMessage: StatusMessage,
                                  val actionList: ActionList,
                                  val currentHero: Hero,
                                  val currentLocation: Location)

    private val savedStates = mutableListOf<SavedState>()

    /**
     * Save state of the specified Game
     */
    private fun saveState() {
        savedStates.add(SavedState(phase, screen, statusMessage, actions, currentHero, currentLocation))
    }

    /**
     * Restore state of the specified game
     */
    private fun restoreState() {
        if (savedStates.isNotEmpty()) with(savedStates.last()) {
            this@Game.phase = this.component1()
            this@Game.screen = this.component2()
            this@Game.statusMessage = this.component3()
            this@Game.actions = this.component4()
            this@Game.currentHero = this.component5()
            this@Game.currentLocation = this.component6()
            savedStates.remove(this)
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Store current game data as [GameInfo] object
     */
    private fun storeGameData() {
        val info = GameInfo(this.partyId)
        info.phaseIndex = this.phaseIndex

        info.scenario = this.scenario
        info.heroes = this.heroes
        info.locations = this.locations

        info.timer = this.timer
        info.currentHeroIndex = this.currentHeroIndex
        info.currentLocationIndex = locations.indexOf(this.currentLocation)
        info.deterrentPile = this.deterrentPile
        this.heroPlacement.forEach { k, v -> info.heroPlacement[locations.indexOf(k)] = v.map { heroes.indexOf(it) }.toList() }

        info.traveledThisTurn = this.traveledThisTurn
        info.gaveDieThisTurn = this.gaveDieThisTurn
        info.exploredThisTurn = this.exploredThisTurn
        info.exploredAgainThisTurn = this.exploredAgainThisTurn
        info.attemptedCloseThisTurn = this.attemptedCloseThisTurn
        info.encounteredDie = this.encounteredDie
        info.encounteredThreat = this.encounteredThreat
        info.pendingHeroModifiers1 = this.pendingHeroModifiers1
        info.pendingHeroModifiers2 = this.pendingHeroModifiers2
        info.pendingEncounterModifiers = this.pendingEncounterModifiers

        info.phase = this.phase
        info.currentMusic = this.currentMusic

        info.screen = this.screen
        info.statusMessage = this.statusMessage
        info.actions = this.actions

        realTimeSave.saveGame(info)
    }

}