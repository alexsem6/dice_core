package org.alexsem.dice.game

/**
 * Used to filter some specific dice from the pile. Contains a number of dice positions in the sorted pile contents
 * returned by [org.alexsem.dice.model.Bag.examine] method.
 */
class PileMask {

    private val positions = mutableSetOf<Int>()
    val positionCount
        get() = positions.size

    /**
     * Add position to the mask
     * @param position Position to add
     */
    fun addPosition(position: Int) = positions.add(position)

    /**
     * Remove position from the mask
     * @param position Position to remove
     */
    fun removePosition(position: Int) = positions.remove(position)

    /**
     * Checks whether specific position is contained within mask.
     * @param position Die position in pile (0-based)
     * @return true if die is contained inside mask, false otherwise
     */
    fun checkPosition(position: Int) = position in positions

    /**
     * Adds position if it is not present, removes otherwise
     * @param position Position to switch
     */
    fun switchPosition(position: Int) {
        if (!removePosition(position)) {
            addPosition(position)
        }
    }

    /**
     * Remove all positions from the mask
     */
    fun clear() = positions.clear()

    override fun toString() = positions.joinToString(prefix="[", postfix = "]")
}