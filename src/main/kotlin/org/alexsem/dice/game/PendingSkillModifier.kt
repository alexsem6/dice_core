package org.alexsem.dice.game

import org.alexsem.dice.model.Skill

/**
 * Special modifier which is applied only when certain game state is reached
 */
class PendingSkillModifier(val type : Skill.Type, val value:Int)