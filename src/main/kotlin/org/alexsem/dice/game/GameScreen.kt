package org.alexsem.dice.game

/**
 * List of possible screen types which appear during the game
 */
enum class GameScreen {
    SCENARIO_INTRO,
    SCENARIO_OUTRO,
    HERO_TURN_START,
    HERO_DEATH,
    LOCATION_INTERIOR,
    LOCATION_TRAVEL,
    DIALOG_LOCATION_SELECT,
    DIALOG_HERO_SELECT,
    DIALOG_SKILL_SELECT,
    DIALOG_PILE_PICK,
    INFO_SCENARIO_GENERAL,
    INFO_SCENARIO_RULES,
    INFO_SCENARIO_ALLIES,
    INFO_LOCATION_GENERAL,
    INFO_LOCATION_RULES,
    INFO_HERO_GENERAL,
    INFO_HERO_DICE,
    INFO_HERO_SKILLS,
    INFO_ENEMY,
    INFO_VILLAIN,
    INFO_OBSTACLE,
    INFO_MODIFIERS,
    GAME_VICTORY,
    GAME_LOSS,
    PAUSE_MENU
}