package org.alexsem.dice.game

/**
 * Used to filter some specific dice from the hand. Contains a number of dice positions.
 */
class HandMask {

    private val positions = mutableSetOf<Int>()
    private val allyPositions = mutableSetOf<Int>()
    val positionCount
        get() = positions.size
    val allyPositionCount
        get() = allyPositions.size

    /**
     * Add position to the mask
     * @param position Position to add
     */
    fun addPosition(position: Int) = positions.add(position)

    /**
     * Remove position from the mask
     * @param position Position to remove
     */
    fun removePosition(position: Int) = positions.remove(position)

    /**
     * Add ally position to the mask
     * @param position Position to add
     */
    fun addAllyPosition(position: Int) = allyPositions.add(position)

    /**
     * Remove ally position from the mask
     * @param position Position to remove
     */
    fun removeAllyPosition(position: Int) = allyPositions.remove(position)

    /**
     * Checks whether specific position is contained within mask.
     * @param position Die position in hand (0-based)
     * @return true if die is contained inside mask, false otherwise
     */
    fun checkPosition(position: Int) = position in positions

    /**
     * Checks whether specific ally position is contained within mask.
     * @param position Die position in hand (0-based)
     * @return true if die is contained inside mask, false otherwise
     */
    fun checkAllyPosition(position: Int) = position in allyPositions

    /**
     * Adds position if it is not present, removes otherwise
     * @param position Position to switch
     */
    fun switchPosition(position: Int) {
        if (!removePosition(position)) {
            addPosition(position)
        }
    }

    /**
     * Adds ally position if it is not present, removes otherwise
     * @param position Position to switch
     */
    fun switchAllyPosition(position: Int) {
        if (!removeAllyPosition(position)) {
            addAllyPosition(position)
        }
    }

    /**
     * Remove all positions and ally positions from the mask
     */
    fun clear() {
        positions.clear()
        allyPositions.clear()
    }

}