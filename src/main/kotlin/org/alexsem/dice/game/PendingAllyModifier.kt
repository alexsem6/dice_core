package org.alexsem.dice.game

import org.alexsem.dice.model.AllySkill

/**
 * Special modifier which is applied only when certain game state is reached
 */
class PendingAllyModifier(val type: AllySkill.Type, val value: Int)