package org.alexsem.dice.game

import org.alexsem.dice.ui.storage.GameInfo
import org.alexsem.dice.ui.storage.Storage
import java.util.concurrent.ArrayBlockingQueue

/**
 * Class responsible for saving [GameInfo] real time during [Game] play
 */
class RealTimeSave(private val storage: Storage) : Thread() {

    private val queue = ArrayBlockingQueue<GameInfo>(5)

    /**
     * Save game data to [Storage]
     * @param info Data to save
     */
    fun saveGame(info: GameInfo) {
        queue.clear()
        queue.put(info)
    }

    /**
     * Finish waiting for saved states
     */
    fun shutdown() {
        queue.add(GameInfo(""))
    }

    override fun run() {
        while (true) {
            try {
                val info = queue.take()
                if (info.partyId == "") {
                    break
                }
                storage.saveGame(info)
            } catch (e: InterruptedException) {
            }
        }
    }

    override fun interrupt() {
        shutdown()
    }
}