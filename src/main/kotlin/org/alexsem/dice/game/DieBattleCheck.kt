package org.alexsem.dice.game

import org.alexsem.dice.model.Die
import kotlin.math.ceil
import kotlin.math.floor

/**
 * Contains opponent pair along with own pairs to perform a die battle check. Can be rolled to get some values for each
 * die.
 */
class DieBattleCheck(val method: Method, opponent: DiePair? = null) {

    enum class Method { SUM, AVG_UP, AVG_DOWN, MAX, MIN }

    private inner class Wrap(val pair: DiePair, var roll: Int)

    private infix fun DiePair.with(roll: Int) = Wrap(this, roll)

    //------------------------------------------------------------------------------------------------------------------
    private val opponent: Wrap? = opponent?.with(0)
    private val heroics = ArrayList<Wrap>()
    var isRolled = false
    var result: Int? = null
    val heroPairCount
        get() = heroics.size

    /**
     * Get opponent [DiePair]
     * @return Pair (may be null)
     */
    fun getOpponentPair() = opponent?.pair

    /**
     * Get roll result of opponent pair (if available)
     * @return result or null
     * @throws IllegalStateException in case rolling was not yet performed
     */
    fun getOpponentResult() = when {
        isRolled -> opponent?.roll ?: 0
        else -> throw IllegalStateException("Not rolled yet")
    }

    /**
     * Add new [DiePair] to hero's list
     * @param pair Pair to add
     */
    fun addHeroPair(pair: DiePair) {
        if (method == Method.SUM && heroics.size > 0) {
            pair.modifier = 0
        }
        heroics.add(pair with 0)
    }

    /**
     * Add hero pair to battle check
     * @param die Die to add
     * @param modifier Modifier to add
     */
    fun addHeroPair(die: Die, modifier: Int) = addHeroPair(DiePair(die, modifier))

    /**
     * Remove all hero pairs
     */
    fun clearHeroPairs() = heroics.clear()

    /**
     * Get hero pair at specified position
     * @param index Position
     * @return Hero pair
     */
    fun getHeroPairAt(index: Int) = heroics[index].pair

    /**
     * Get roll result of specific hero pair (if available)
     * @param index Position
     * @return result or null
     * @throws IllegalStateException in case rolling was not yet performed
     */
    fun getHeroResultAt(index: Int) = when {
        isRolled -> when {
            (index in 0 until heroics.size) -> heroics[index].roll
            else -> 0
        }
        else -> throw IllegalStateException("Not rolled yet")
    }

    /**
     * Roll each die of the check
     */
    fun roll() {
        fun roll(wrap: Wrap) {
            wrap.roll = wrap.pair.die.roll()
        }
        isRolled = true
        opponent?.let { roll(it) }
        heroics.forEach { roll(it) }
    }

    /**
     * Perform result calculation using battle check method
     */
    fun calculateResult() {
        if (!isRolled) {
            throw IllegalStateException("Not rolled yet")
        }
        val opponentResult = opponent?.let { it.roll + it.pair.modifier } ?: 0
        val stats = heroics.map { it.roll + it.pair.modifier }
        val heroResult = when (method) {
            DieBattleCheck.Method.SUM -> stats.sum()
            DieBattleCheck.Method.AVG_UP -> ceil(stats.average()).toInt()
            DieBattleCheck.Method.AVG_DOWN -> floor(stats.average()).toInt()
            DieBattleCheck.Method.MAX -> stats.max() ?: 0
            DieBattleCheck.Method.MIN -> stats.min() ?: 0
        }
        result = heroResult - opponentResult
    }

}
