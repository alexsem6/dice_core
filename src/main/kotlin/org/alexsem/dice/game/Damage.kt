package org.alexsem.dice.game

import kotlin.math.max

/**
 * Class which stored information about current damage to be dealt to hero
 */
class Damage(val type: Type, var amount: Int) {

    enum class Type {
        PHYSICAL,
        MAGICAL,
    }

    /**
     * Reduce damage to zero
     */
    fun prevent() {
        amount = 0
    }

    /**
     * Reduce damage by specific amount
     * @param amount Amount of damage to reduce current damage by
     */
    fun reduce(amount:Int) {
        this.amount = max(0, this.amount - amount)
    }

}
