package org.alexsem.dice.game.filter.hand

import org.alexsem.dice.model.Die

/**
 * [HandFilter] implementation to match two dice of any types other than those specified in two respective arrays
 */
open class DoubleNotSpecificDieHandFilter(types1: Array<Die.Type>, types2: Array<Die.Type>) :
        DoubleDieHandFilter(
                Die.Type.values().filterNot { it in types1 }.toTypedArray(),
                Die.Type.values().filterNot { it in types2 }.toTypedArray()
        )