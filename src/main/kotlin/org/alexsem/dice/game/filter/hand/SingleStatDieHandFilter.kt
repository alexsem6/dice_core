package org.alexsem.dice.game.filter.hand

import org.alexsem.dice.model.Die.Type.*

/**
 * [HandFilter] implementation for performing checks with a single die of one of stat types
 */
class SingleStatDieHandFilter : SingleDieHandFilter(PHYSICAL, SOMATIC, MENTAL, VERBAL)