package org.alexsem.dice.game.filter.bag

import org.alexsem.dice.model.Bag

/**
 * [BagFilter] implementation for performing actions on a single die of any type
 */
class SingleAnyDieBagFilter : BagFilter {
    override fun test(bag: Bag) = bag.size > 0
}