package org.alexsem.dice.game.filter.bag

import org.alexsem.dice.model.Bag
import org.alexsem.dice.model.Die

/**
 * [BagFilter] implementation for performing actions on a single die of one of specified types
 */
open class SingleDieBagFilter(private vararg val types: Die.Type) : BagFilter {
    override fun test(bag: Bag) = bag.examine().any { it.type in types }
}