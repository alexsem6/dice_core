package org.alexsem.dice.game.filter.hand

import org.alexsem.dice.model.Die

/**
 * [HandFilter] implementation for dealing with Tier-3 Obstacles (which require 2 dice of specific types)
 */
class TwoSpecificDiceObstacleHandFilter(type1: Die.Type, type2: Die.Type)
    : DoubleDieHandFilter(arrayOf(type1, Die.Type.DIVINE), arrayOf(type2, Die.Type.DIVINE))
