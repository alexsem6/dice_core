package org.alexsem.dice.game.filter.hand

import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Hand

/**
 * [HandFilter] implementation for performing checks with a single die of one of specified types
 */
open class SingleDieHandFilter(private vararg val types: Die.Type) : HandFilter {

    override fun test(hand: Hand) =
            (0 until hand.dieCount).mapNotNull { hand.dieAt(it) }.any { it.type in types }
                    || (Die.Type.ALLY in types && hand.allyDieCount > 0)

}