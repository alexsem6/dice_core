package org.alexsem.dice.game.filter.hand

import org.alexsem.dice.model.Die.Type.*

/**
 * [HandFilter] implementation to match any type other than WOUND
 */
class DoubleNonWoundDieHandFilter : DoubleNotSpecificDieHandFilter(arrayOf(WOUND), arrayOf(WOUND))