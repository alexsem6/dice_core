package org.alexsem.dice.game.filter.bag

import org.alexsem.dice.model.Bag

/**
 * Checks whether specified Bag corresponds to some rules
 */
interface BagFilter {

    /**
     * Check specified bag
     * @param bag Bag to check
     * @return true or false
     */
    fun test(bag: Bag): Boolean

}
