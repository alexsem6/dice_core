package org.alexsem.dice.game.filter.hand

import org.alexsem.dice.model.Hand
import org.alexsem.dice.model.MAX_HAND_ALLY_SIZE
import org.alexsem.dice.model.MAX_HAND_SIZE

/**
 * Implementation of [[HandFilter]] that checks hero has space for another die
 */
class FreeSpaceHandFilter: HandFilter {
    override fun test(hand: Hand) = hand.dieCount < MAX_HAND_SIZE || hand.allyDieCount < MAX_HAND_ALLY_SIZE
}