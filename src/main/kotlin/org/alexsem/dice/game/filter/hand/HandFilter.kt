package org.alexsem.dice.game.filter.hand

import org.alexsem.dice.model.Hand

/**
 * Checks whether specified hand corresponds to some rules
 */
interface HandFilter {

    /**
     * Check specified hand
     * @param hand Hand to check
     * @return true or false
     */
    fun test(hand: Hand): Boolean

}
