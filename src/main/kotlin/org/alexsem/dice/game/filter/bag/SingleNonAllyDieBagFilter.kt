package org.alexsem.dice.game.filter.bag

import org.alexsem.dice.model.Die

class SingleNonAllyDieBagFilter : SingleNotSpecificDieBagFilter(Die.Type.ALLY)