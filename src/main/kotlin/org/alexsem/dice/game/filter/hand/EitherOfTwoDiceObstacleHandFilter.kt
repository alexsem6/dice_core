package org.alexsem.dice.game.filter.hand

import org.alexsem.dice.model.Die

/**
 * [HandFilter] implementation for dealing with Tier-1 Obstacles (which require 1 die of two possible types)
 */
class EitherOfTwoDiceObstacleHandFilter(type1: Die.Type, type2: Die.Type)
    : SingleDieHandFilter(type1, type2, Die.Type.DIVINE)
