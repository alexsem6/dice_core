package org.alexsem.dice.game.filter.bag

import org.alexsem.dice.model.Bag

/**
 * [BagFilter] implementation for checking whether bag is empty
 */
class EmptyBagFilter : BagFilter {
    override fun test(bag: Bag) = bag.size == 0
}