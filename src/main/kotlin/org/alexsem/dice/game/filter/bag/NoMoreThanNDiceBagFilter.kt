package org.alexsem.dice.game.filter.bag

import org.alexsem.dice.model.Bag

/**
 * [BagFilter] implementation for checking whether bag has no more than specific amount of dice
 */
class NoMoreThanNDiceBagFilter(private val amount: Int) : BagFilter {
    override fun test(bag: Bag) = bag.size <= amount
}