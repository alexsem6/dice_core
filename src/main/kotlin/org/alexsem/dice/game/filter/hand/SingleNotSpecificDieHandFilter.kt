package org.alexsem.dice.game.filter.hand

import org.alexsem.dice.model.Die

/**
 * [HandFilter] implementation to match any type other than those specified
 */
open class SingleNotSpecificDieHandFilter(vararg types: Die.Type) :
        SingleDieHandFilter(*Die.Type.values()
                .filterNot { it in types }
                .toTypedArray()
        )