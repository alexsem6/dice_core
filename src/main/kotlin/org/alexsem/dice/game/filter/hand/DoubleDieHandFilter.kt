package org.alexsem.dice.game.filter.hand

import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Hand

/**
 * [HandFilter] implementation for performing checks with two dice, each of which has own list of possible types
 */
open class DoubleDieHandFilter(types1: Array<Die.Type>, types2: Array<Die.Type>) : HandFilter {

    private val types1 = types1.toSet()
    private val types2 = types2.toSet()

    override fun test(hand: Hand): Boolean = with(hand.examine()) {
        for (type1 in types1) {
            (0 until this.size).firstOrNull { this[it].type == type1 }?.let { index1 ->
                for (type2 in types2) {
                    (0 until this.size).filter { it != index1 }.firstOrNull { this[it].type == type2 }?.let {
                        return true
                    }
                }
            }
        }
        return false
    }

}
