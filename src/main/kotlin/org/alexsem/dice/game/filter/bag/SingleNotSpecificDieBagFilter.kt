package org.alexsem.dice.game.filter.bag

import org.alexsem.dice.model.Die

/**
 * [BagFilter] implementation to match any type other than those specified
 */
open class SingleNotSpecificDieBagFilter(vararg types: Die.Type) :
        SingleDieBagFilter(*Die.Type.values()
                .filterNot { it in types }
                .toTypedArray()
        )