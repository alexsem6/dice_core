package org.alexsem.dice.game.filter.hand

import org.alexsem.dice.model.Die

/**
 * [HandFilter] implementation for dealing with Tier-2 Obstacles (which require 1 specific die)
 */
class SpecificDieObstacleHandFilter(type: Die.Type)
    : SingleDieHandFilter(type, Die.Type.DIVINE)
