package org.alexsem.dice.manager

import org.alexsem.dice.generator.generateHero
import org.alexsem.dice.ui.Action.Type.*
import org.alexsem.dice.ui.ActionList
import org.alexsem.dice.ui.StatusMessage
import org.alexsem.dice.ui.UI
import org.alexsem.dice.ui.audio.Audio
import org.alexsem.dice.ui.audio.Sound
import org.alexsem.dice.model.Hero


private const val MIN_PARTY_SIZE = 2
private const val MAX_PARTY_SIZE = 5

class PartyManager(ui: UI, private val callback: (Boolean, String, List<Hero>) -> Unit) {

    private val renderer = ui.managerRenderer
    private val interactor = ui.managerInteractor

    private var partyName = ""
    private var heroes = mutableListOf<Hero>()
    private lateinit var phase: Phase
    private var message = StatusMessage.EMPTY
    private var actions = ActionList.EMPTY

    private var currentInput = ""
    private var selectedHeroIndex = 1
    private var newHeroClass: Hero.Type = Hero.Type.BRAWLER
    private var dialogHeroClasses: List<Hero.Type> = emptyList()

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Start the cycle
     */
    fun start() {
        changePhaseChoosePartyName()
        processCycle()
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Various party manager phases
     */
    private enum class Phase {
        CHOOSE_PARTY_NAME,
        GENERAL_OVERVIEW,
        RENAME_PARTY,
        CHOOSE_HERO_CLASS,
        CHOOSE_HERO_NAME,
        SELECT_HERO_RENAME,
        SELECT_HERO_DELETE,
        RENAME_HERO
    }

    /**
     * PHASE: (Initial) Choose party name
     */
    private fun changePhaseChoosePartyName() {
        phase = Phase.CHOOSE_PARTY_NAME
        currentInput = partyName
        message = StatusMessage.ENTER_PARTY_NAME with renderer.maxPartyNameLength
        actions = ActionList()
        actions.add(CONFIRM, false)
        actions.add(CANCEL)
    }

    /**
     * PHASE: Show general overview screen
     */
    private fun changePhaseGeneralOverview() {
        phase = Phase.GENERAL_OVERVIEW
        message = StatusMessage.CHOOSE_PARTY_MANAGER_ACTION
        actions = ActionList()
        actions.add(ADD_HERO, heroes.size < MAX_PARTY_SIZE)
        if (heroes.size > 0) {
            actions.add(DELETE_HERO)
            actions.add(RENAME_HERO)
        }
        actions.add(RENAME_PARTY)
        actions.add(FINISH, heroes.size in MIN_PARTY_SIZE..MAX_PARTY_SIZE)
        actions.add(CANCEL)
    }

    /**
     * PHASE: Show party name editing screen
     */
    private fun changePhaseRenameParty() {
        Audio.playSound(Sound.DIE_PICK)
        phase = Phase.RENAME_PARTY
        currentInput = partyName
        message = StatusMessage.ENTER_PARTY_NAME with renderer.maxPartyNameLength
        actions = ActionList()
        actions.add(CONFIRM, true)
        actions.add(CANCEL)
    }

    /**
     * PHASE: Choose new hero class choose
     */
    private fun changePhaseChooseHeroClass() {
        Audio.playSound(Sound.DIE_PICK)
        phase = Phase.CHOOSE_HERO_CLASS
        val availableClasses = heroes.asSequence().map { it.type }.toSet()
        dialogHeroClasses = Hero.Type.values().filter { it !in availableClasses }
        message = StatusMessage.CHOOSE_NEW_HERO_CLASS
        actions = ActionList().add(CANCEL)
    }

    /**
     * PHASE: Choose name for the new hero
     */
    private fun changePhaseChooseHeroName() {
        Audio.playSound(Sound.DIE_PICK)
        phase = Phase.CHOOSE_HERO_NAME
        currentInput = ""
        message = StatusMessage.CHOOSE_NEW_HERO_NAME with renderer.maxHeroNameLength
        actions = ActionList()
        actions.add(CONFIRM, false)
        actions.add(CANCEL)
    }

    /**
     * PHASE: Select hero to rename
     */
    private fun changePhaseSelectHeroRename() {
        Audio.playSound(Sound.DIE_PICK)
        phase = Phase.SELECT_HERO_RENAME
        message = StatusMessage.SELECT_HERO_RENAME
        actions = ActionList().add(CANCEL)
    }

    /**
     * PHASE: Select hero to delete
     */
    private fun changePhaseSelectHeroDelete() {
        Audio.playSound(Sound.DIE_PICK)
        phase = Phase.SELECT_HERO_DELETE
        message = StatusMessage.SELECT_HERO_DELETE
        actions = ActionList().add(CANCEL)
    }

    /**
     * PHASE: Choose name for the existing hero
     */
    private fun changePhaseRenameHero() {
        Audio.playSound(Sound.DIE_PICK)
        phase = Phase.RENAME_HERO
        currentInput = heroes[selectedHeroIndex].name
        message = StatusMessage.ENTER_HERO_NAME with renderer.maxHeroNameLength
        actions = ActionList()
        actions.add(CONFIRM)
        actions.add(CANCEL)
    }

    //==================================================================================================================

    /**
     * Run main game cycle
     */
    private fun processCycle() {
        while (true) {
            drawScreen()
            when (phase) {
                //----------------------------------------------------------------------------
                Phase.CHOOSE_PARTY_NAME -> {
                    val name = interactor.inputText(currentInput, renderer.maxPartyNameLength, true, true, textInputListener)
                    if (name != null) {
                        Audio.playSound(Sound.PAGE_NEXT)
                        partyName = name.trim()
                        changePhaseGeneralOverview()
                    } else {
                        callback.invoke(false, "", emptyList())
                        return
                    }
                }
                //----------------------------------------------------------------------------
                Phase.GENERAL_OVERVIEW -> when (interactor.pickAction(actions).type) {
                    ADD_HERO -> changePhaseChooseHeroClass()
                    RENAME_HERO -> changePhaseSelectHeroRename()
                    DELETE_HERO -> changePhaseSelectHeroDelete()
                    RENAME_PARTY -> changePhaseRenameParty()
                    FINISH -> {
                        Audio.playSound(Sound.CONFIRM_PROMPT)
                        message = StatusMessage.CONFIRM_CREATE_PARTY
                        actions = ActionList().add(CONFIRM).add(CANCEL)
                        drawScreen()
                        when (interactor.pickAction(actions).type) {
                            CONFIRM -> {
                                Audio.playSound(Sound.UPGRADE)
                                callback(true, partyName, heroes)
                                return
                            }
                            CANCEL -> changePhaseGeneralOverview()
                            else -> throw AssertionError("Should not happen")
                        }
                    }
                    CANCEL -> {
                        Audio.playSound(Sound.CONFIRM_PROMPT)
                        message = StatusMessage.CONFIRM_QUIT_PARTY_MANAGER
                        actions = ActionList().add(CONFIRM).add(CANCEL)
                        drawScreen()
                        when (interactor.pickAction(actions).type) {
                            CONFIRM -> {
                                callback(false, "", emptyList())
                                return
                            }
                            CANCEL -> changePhaseGeneralOverview()
                            else -> throw AssertionError("Should not happen")
                        }
                    }
                    else -> throw AssertionError("Should not happen")
                }
                //----------------------------------------------------------------------------
                Phase.RENAME_PARTY -> {
                    interactor.inputText(currentInput, renderer.maxPartyNameLength, true, true, textInputListener)?.let { newName ->
                        Audio.playSound(Sound.PAGE_NEXT)
                        partyName = newName.trim()
                    }
                    changePhaseGeneralOverview()
                }
                //----------------------------------------------------------------------------
                Phase.CHOOSE_HERO_CLASS -> {
                    val action = interactor.pickItemFromList(dialogHeroClasses.size)
                    when (action.type) {
                        LIST_ITEM -> {
                            newHeroClass = dialogHeroClasses[action.data]
                            changePhaseChooseHeroName()
                        }
                        CANCEL -> changePhaseGeneralOverview()
                        else -> throw AssertionError("Should not happen")
                    }
                }
                //----------------------------------------------------------------------------
                Phase.CHOOSE_HERO_NAME -> {
                    interactor.inputText(currentInput, renderer.maxHeroNameLength, true, true, textInputListener)?.let { newName ->
                        Audio.playSound(Sound.DIE_DRAW)
                        heroes.add(generateHero(newHeroClass, newName.trim()))
                    }
                    changePhaseGeneralOverview()
                }
                //----------------------------------------------------------------------------
                Phase.SELECT_HERO_RENAME -> {
                    val action = interactor.pickItemFromList(heroes.size)
                    when (action.type) {
                        LIST_ITEM -> {
                            selectedHeroIndex = action.data
                            changePhaseRenameHero()
                        }
                        CANCEL -> changePhaseGeneralOverview()
                        else -> throw AssertionError("Should not happen")
                    }
                }
                //----------------------------------------------------------------------------
                Phase.RENAME_HERO -> {
                    interactor.inputText(currentInput, renderer.maxHeroNameLength, true, true, textInputListener)?.let { newName ->
                        Audio.playSound(Sound.DIE_DRAW)
                        heroes[selectedHeroIndex].name = newName.trim()
                    }
                    changePhaseGeneralOverview()
                }
                //----------------------------------------------------------------------------
                Phase.SELECT_HERO_DELETE -> {
                    val action = interactor.pickItemFromList(heroes.size)
                    when (action.type) {
                        LIST_ITEM -> {
                            Audio.playSound(Sound.DIE_REMOVE)
                            heroes.removeAt(action.data)
                            changePhaseGeneralOverview()
                        }
                        CANCEL -> changePhaseGeneralOverview()
                        else -> throw AssertionError("Should not happen")
                    }
                }

            }

        }


    }

    /**
     * Listener to use for input checking
     */
    private val textInputListener: (String) -> Unit = { text ->
        currentInput = text
        actions[CONFIRM]?.isEnabled = text.isNotBlank()
        drawScreen()
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Draw current screen
     */
    private fun drawScreen() = when (phase) {
        Phase.GENERAL_OVERVIEW -> renderer.drawPartyManagerGeneral(partyName, heroes, message, actions)
        Phase.CHOOSE_PARTY_NAME, Phase.RENAME_PARTY -> renderer.drawPartyNameEditScreen(currentInput, message, actions)
        Phase.CHOOSE_HERO_CLASS -> renderer.drawHeroClassSelectionDialog(partyName, heroes, dialogHeroClasses, message, actions)
        Phase.CHOOSE_HERO_NAME -> renderer.drawHeroNameEditScreen(partyName, heroes, heroes.size, newHeroClass, currentInput, message, actions)
        Phase.SELECT_HERO_RENAME, Phase.SELECT_HERO_DELETE -> renderer.drawPartyManagerHeroSelection(partyName, heroes, message, actions)
        Phase.RENAME_HERO -> renderer.drawHeroNameEditScreen(partyName, heroes, selectedHeroIndex, heroes[selectedHeroIndex].type, currentInput, message, actions)
    }

}