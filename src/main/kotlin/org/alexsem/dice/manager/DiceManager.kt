package org.alexsem.dice.manager

import org.alexsem.dice.ui.Action.Type.*
import org.alexsem.dice.ui.ActionList
import org.alexsem.dice.ui.StatusMessage
import org.alexsem.dice.ui.UI
import org.alexsem.dice.ui.audio.Audio
import org.alexsem.dice.ui.audio.Sound
import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Hero
import org.alexsem.dice.model.Pile
import java.util.*
import kotlin.math.max
import kotlin.math.min

class DiceManager(ui: UI, private val heroes: List<Hero>, deterrentPile: Pile) {

    private val renderer = ui.managerRenderer
    private val interactor = ui.managerInteractor

    private var currentHeroIndex = -1
    private lateinit var currentHero: Hero
    private lateinit var currentHeroDice: Map<Die.Type, MutableList<Die>>
    private var message = StatusMessage.EMPTY
    private var actions = ActionList.EMPTY

    private val heroesDiceByType: MutableList<Map<Die.Type, MutableList<Die>>> = mutableListOf()
    private var pickedType: Die.Type? = null
    private val commonPool = deterrentPile.examine().toMutableList()
    private var currentPage = 1
    private var totalPages = 1
    private val activePositions = mutableSetOf<Int>()

    private val dieToAction = mapOf(
            Die.Type.PHYSICAL to PHYSICAL,
            Die.Type.SOMATIC to SOMATIC,
            Die.Type.MENTAL to MENTAL,
            Die.Type.VERBAL to VERBAL,
            Die.Type.DIVINE to DIVINE,
            Die.Type.ALLY to ALLY
    )

    private val actionToDie = mapOf(
            PHYSICAL to Die.Type.PHYSICAL,
            SOMATIC to Die.Type.SOMATIC,
            MENTAL to Die.Type.MENTAL,
            VERBAL to Die.Type.VERBAL,
            DIVINE to Die.Type.DIVINE,
            ALLY to Die.Type.ALLY
    )
    //------------------------------------------------------------------------------------------------------------------
    /**
     * Start the cycle
     */
    fun start() {
        if (heroes.isEmpty()) {
            throw IllegalStateException("No heroes defined")
        }
        //Prepare heroes
        heroes.forEach { h ->

            //Dump all dice to bag
            while (h.discardPile.size > 0) {
                h.bag.put(h.discardPile.draw())
            }
            while (h.hand.dieCount > 0) {
                h.hideDieFromHand(h.hand.dieAt(0)!!)
            }
            while (h.hand.allyDieCount > 0) {
                h.hideDieFromHand(h.hand.allyDieAt(0)!!)
            }

            //Remove wounds from bag
            var die: Die?
            do {
                die = h.bag.drawOfType(Die.Type.WOUND)
            } while (die != null)

            //Prepare the maps to work with
            val diceByType = TreeMap<Die.Type, MutableList<Die>>()
            val limits = h.getDiceLimits().associate { it.type to it.current }
            h.bag.examine().groupBy { it.type }.forEach { type, dice ->
                if (limits.containsKey(type)) {
                    val list = mutableListOf<Die>()
                    val end = min(dice.size, limits.getValue(type))
                    (0 until end).map { dice[it] }.forEach { list.add(it) }
                    (end until dice.size).map { dice[it] }.forEach { commonPool.add(it) }
                    diceByType[type] = list
                } else {
                    commonPool.addAll(dice)
                }
            }
            heroesDiceByType.add(diceByType)
        }
        commonPool.sort()

        adjustCommonPoolPages()
        pickHero(0)
        processCycle()
    }

    private fun pickNextHero() = pickHero(++currentHeroIndex % heroes.size)
    private fun pickPreviousHero() = pickHero((--currentHeroIndex + heroes.size) % heroes.size)

    /**
     * Calculate number of total pages for common pool
     */
    private fun adjustCommonPoolPages() {
        totalPages = max(1, commonPool.size / renderer.commonPoolDicePerPage + (if (commonPool.size % renderer.commonPoolDicePerPage > 0) 1 else 0))
        if (currentPage < 1) {
            currentPage = 1
        }
        if (currentPage > totalPages) {
            currentPage = totalPages
        }
    }

    /**
     * Show information about selected hero
     */
    private fun pickHero(index: Int) {
        Audio.playSound(Sound.PAGE_CLOSE)
        currentHeroIndex = index
        currentHero = heroes[index]
        currentHeroDice = heroesDiceByType[index]
        pickedType = null
        generateActions()
    }

    /**
     * Prepare a list of common actions
     */
    private fun generateActions() {
        message = StatusMessage.CHOOSE_DICE_MANAGER_ACTION
        actions = ActionList()
        currentHeroDice.forEach { t, d ->
            actions.add(dieToAction.getValue(t), d.isNotEmpty())
        }
        actions.add(INFO_PAGE_DOWN, currentPage < totalPages)
        actions.add(INFO_PAGE_UP, currentPage > 1)
        actions.add(NEXT_HERO, heroes.size > 1)
        actions.add(PREVIOUS_HERO, heroes.size > 1)
        actions.add(FINISH, checkCanBeFinished())
        activePositions.clear()
        val allowedTypes = currentHero.getDiceLimits().asSequence()
                .filter { currentHeroDice[it.type]?.size ?: 0 < it.current }
                .map { it.type }.toSet()
        commonPool.forEachIndexed { i, d ->
            if (d.type in allowedTypes) {
                activePositions.add(i)
            }
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Defines whether specified hero has all the dice he needs
     */
    private fun checkHeroCanFinish(index: Int) = with(heroesDiceByType[index]) {
        heroes[index].getDiceLimits().all { l ->
            (l.current == this[l.type]?.size) ||
                    commonPool.none { it.type == l.type }
        }
    }

    /**
     * Defines whether all hero dice are correctly distributed
     */
    private fun checkCanBeFinished() =
            (0 until heroes.size).all(::checkHeroCanFinish)

    //==================================================================================================================
    /**
     * Run main cycle
     */
    private fun processCycle() {
        while (true) {
            drawScreen()
            val minPosition = (currentPage - 1) * renderer.commonPoolDicePerPage
            val positions = activePositions
                    .filter { p -> p in (minPosition until minPosition + renderer.commonPoolDicePerPage) }
                    .map { p -> p - minPosition }
                    .toSet()
            val action = interactor.pickDiceManagerAction(actions, positions)
            when (action.type) {
                PHYSICAL, SOMATIC, MENTAL, VERBAL, DIVINE, ALLY -> {
                    val dieType = actionToDie[action.type]
                    if (currentHeroDice.containsKey(dieType)) {
                        Audio.playSound(Sound.DIE_PICK)
                        pickedType = dieType
                        message = StatusMessage.CHOOSE_DICE_MANAGER_DIE
                        actions = ActionList().add(CANCEL)
                        drawScreen()
                        val dice = currentHeroDice[dieType]!!
                        val picked = interactor.pickItemFromList(dice.size)
                        if (picked.type != CANCEL) {
                            Audio.playSound(Sound.DIE_DISCARD)
                            commonPool.add(dice.removeAt(picked.data))
                            commonPool.sort()
                            adjustCommonPoolPages()
                        }
                        pickedType = null
                        generateActions()
                    }
                }
                LIST_ITEM -> {
                    commonPool[action.data + minPosition].let { d ->
                        Audio.playSound(Sound.DIE_DRAW)
                        currentHeroDice[d.type]?.let {
                            it.add(commonPool.removeAt(action.data + minPosition))
                            it.sort()
                        }
                    }
                    generateActions()
                }
                INFO_PAGE_DOWN -> {
                    Audio.playSound(Sound.PAGE_NEXT)
                    currentPage++
                    actions[INFO_PAGE_DOWN]?.isEnabled = currentPage < totalPages
                    actions[INFO_PAGE_UP]?.isEnabled = currentPage > 1
                }
                INFO_PAGE_UP -> {
                    Audio.playSound(Sound.PAGE_PREVIOUS)
                    currentPage--
                    actions[INFO_PAGE_DOWN]?.isEnabled = currentPage < totalPages
                    actions[INFO_PAGE_UP]?.isEnabled = currentPage > 1
                }
                NEXT_HERO -> pickNextHero()
                PREVIOUS_HERO -> pickPreviousHero()
                FINISH -> {
                    heroes.forEachIndexed { i, h ->
                        h.bag.clear()
                        h.getDiceLimits().forEach { l ->
                            val dice = heroesDiceByType[i].getValue(l.type)
                            while (dice.size < l.current) {
                                dice.add(Die(l.type, 4))
                            }
                            h.bag.put(*dice.toTypedArray())
                        }
                    }
                    Audio.playSound(Sound.UPGRADE)
                    return
                }
                else -> throw AssertionError("Should not happen")
            }
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Draw current screen
     */
    private fun drawScreen() =
            renderer.drawDiceManagerScreen(currentHero, currentHeroDice, pickedType, commonPool, currentPage, totalPages, activePositions, message, actions)

}