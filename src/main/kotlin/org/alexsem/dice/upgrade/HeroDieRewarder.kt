package org.alexsem.dice.upgrade

import org.alexsem.dice.generator.generateDie
import org.alexsem.dice.generator.template.die.SingleDieTypeFilter
import org.alexsem.dice.ui.Action
import org.alexsem.dice.ui.ActionList
import org.alexsem.dice.ui.StatusMessage
import org.alexsem.dice.ui.UI
import org.alexsem.dice.ui.audio.Audio
import org.alexsem.dice.ui.audio.Sound
import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Hero

/**
 * Used to reward [Hero] with new [Die] of specific level as per [org.alexsem.dice.model.ScenarioReward.GAIN_DIE]
 */
class HeroDieRewarder(ui: UI, private val heroes: List<Hero>, private val level: Int) {

    private val renderer = ui.upgradeRenderer
    private val interactor = ui.menuInteractor

    private var currentHeroIndex = -1
    private lateinit var currentHero: Hero
    private var message = StatusMessage.EMPTY
    private var actions = ActionList.EMPTY

    private val actionToDie = mapOf(
            Action.Type.PHYSICAL to Die.Type.PHYSICAL,
            Action.Type.SOMATIC to Die.Type.SOMATIC,
            Action.Type.MENTAL to Die.Type.MENTAL,
            Action.Type.VERBAL to Die.Type.VERBAL,
            Action.Type.DIVINE to Die.Type.DIVINE,
            Action.Type.ALLY to Die.Type.ALLY
    )

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Start the cycle
     */
    fun start() {
        if (heroes.isEmpty()) {
            throw IllegalStateException("No heroes defined")
        }
        currentHeroIndex = -1
        message = StatusMessage.CHOOSE_DIE_TYPE_RECEIVE
        actions = ActionList()
        actions.add(Action.Type.PHYSICAL)
        actions.add(Action.Type.SOMATIC)
        actions.add(Action.Type.MENTAL)
        actions.add(Action.Type.VERBAL)
        actions.add(Action.Type.DIVINE)
        actions.add(Action.Type.ALLY)
        pickNextHero()
        processCycle()
    }

    /**
     * Pass turn to the next hero
     */
    private fun pickNextHero() {
        currentHeroIndex++
        if (currentHeroIndex >= heroes.size) {
            currentHeroIndex = -1
            return
        }
        currentHero = heroes[currentHeroIndex]
    }

    //==================================================================================================================
    /**
     * Run main game cycle
     */
    private fun processCycle() {
        while (currentHeroIndex > -1) {
            renderer.drawDieTypeSelectionDialog(currentHero, message, actions)
            with(interactor.pickAction(actions).type) {
                actionToDie[this]?.let {
                    Audio.playSound(Sound.UPGRADE)
                    val die = generateDie(SingleDieTypeFilter(it), level)
                    currentHero.bag.put(die)
                    renderer.drawNewDieReceived(currentHero, die)
                    interactor.anyInput()
                    pickNextHero()
                }
            }
        }
    }

    //==================================================================================================================

}