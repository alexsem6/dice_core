package org.alexsem.dice.upgrade

import org.alexsem.dice.model.Die
import org.alexsem.dice.model.Hero
import org.alexsem.dice.ui.Action
import org.alexsem.dice.ui.ActionList
import org.alexsem.dice.ui.StatusMessage
import org.alexsem.dice.ui.UI
import org.alexsem.dice.ui.audio.Audio
import org.alexsem.dice.ui.audio.Sound

/**
 * Performs hero dice limit upgrading process as per [org.alexsem.dice.model.ScenarioReward.INCREASE_DIE_LIMIT]
 */
class HeroDiceLimitsUpgrader(ui: UI, private val heroes: List<Hero>) {

    private val renderer = ui.upgradeRenderer
    private val interactor = ui.menuInteractor

    private var currentHeroIndex = -1
    private lateinit var currentHero: Hero
    private var pickedPlusOneDieType: Die.Type? = null
    private var message = StatusMessage.EMPTY
    private var actions = ActionList.EMPTY

    private val dieToAction = mapOf(
            Die.Type.PHYSICAL to Action.Type.PHYSICAL,
            Die.Type.SOMATIC to Action.Type.SOMATIC,
            Die.Type.MENTAL to Action.Type.MENTAL,
            Die.Type.VERBAL to Action.Type.VERBAL,
            Die.Type.DIVINE to Action.Type.DIVINE,
            Die.Type.ALLY to Action.Type.ALLY
    )

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Start the cycle
     */
    fun start() {
        if (heroes.isEmpty()) {
            throw IllegalStateException("No heroes defined")
        }
        currentHeroIndex = -1
        pickNextHero()
        processCycle()
    }

    /**
     * Pass turn to the next hero
     */
    private fun pickNextHero() {
        currentHeroIndex++
        if (currentHeroIndex >= heroes.size) {
            currentHeroIndex = -1
            return
        }
        currentHero = heroes[currentHeroIndex]
        pickedPlusOneDieType = null
        message = StatusMessage.CHOOSE_DIE_TYPE_TO_UPGRADE
        actions = ActionList()
        actions.add(Action.Type.CONFIRM, false)
        currentHero.getDiceLimits().forEach {
            actions.add(dieToAction.getValue(it.type), it.current < it.maximal)
        }
    }

    //==================================================================================================================
    /**
     * Process selection/deselection of the specific die type
     * @param type Type selected
     */
    private fun processTypeSelected(type: Die.Type) {
        Audio.playSound(Sound.DIE_PICK)
        if (pickedPlusOneDieType === null || pickedPlusOneDieType !== type) {
            pickedPlusOneDieType = type
            actions[Action.Type.CONFIRM]?.isEnabled = true
        } else {
            pickedPlusOneDieType = null
            actions[Action.Type.CONFIRM]?.isEnabled = false
        }
    }

    /**
     * Run main game cycle
     */
    private fun processCycle() {
        while (currentHeroIndex > -1) {
            renderer.drawHeroDiceLimits(currentHero, pickedPlusOneDieType, message, actions)
            val action = interactor.pickAction(actions)
            when (action.type) {
                Action.Type.PHYSICAL -> processTypeSelected(Die.Type.PHYSICAL)
                Action.Type.SOMATIC -> processTypeSelected(Die.Type.SOMATIC)
                Action.Type.MENTAL -> processTypeSelected(Die.Type.MENTAL)
                Action.Type.VERBAL -> processTypeSelected(Die.Type.VERBAL)
                Action.Type.DIVINE -> processTypeSelected(Die.Type.DIVINE)
                Action.Type.ALLY -> processTypeSelected(Die.Type.ALLY)
                Action.Type.CONFIRM -> {
                    Audio.playSound(Sound.UPGRADE)
                    currentHero.increaseDiceLimit(pickedPlusOneDieType!!)
                    pickNextHero()
                }
                else -> throw AssertionError("Should not happen")
            }
        }
    }
    //==================================================================================================================

}