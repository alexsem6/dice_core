package org.alexsem.dice.upgrade

import org.alexsem.dice.model.Hero
import org.alexsem.dice.model.Skill
import org.alexsem.dice.ui.Action
import org.alexsem.dice.ui.ActionList
import org.alexsem.dice.ui.StatusMessage
import org.alexsem.dice.ui.UI
import org.alexsem.dice.ui.audio.Audio
import org.alexsem.dice.ui.audio.Sound

/**
 * Performs hero skill learning/upgrading process as per [org.alexsem.dice.model.ScenarioReward.LEARN_SKILL] or [org.alexsem.dice.model.ScenarioReward.UPGRADE_SKILL]
 */
class HeroSkillsUpgrader(ui: UI, private val heroes: List<Hero>, private val skillSet: SkillSet) {

    private val renderer = ui.upgradeRenderer
    private val interactor = ui.menuInteractor

    /**
     * Specify which skill set to use
     */
    enum class SkillSet {
        LEARN_NEW, //Learn level 1 of the dormant skill
        IMPROVE_EXISTING //Increase level of skills already learnt
    }

    private lateinit var currentSkills: List<Skill>
    private var currentHeroIndex = -1
    private lateinit var currentHero: Hero
    private var message = StatusMessage.EMPTY
    private var actions = ActionList.EMPTY

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Start the cycle
     */
    fun start() {
        if (heroes.isEmpty()) {
            throw IllegalStateException("No heroes defined")
        }
        currentHeroIndex = -1
        pickNextHero()
        processCycle()
    }

    /**
     * Pass turn to the next hero
     */
    private fun pickNextHero() {
        currentHeroIndex++
        if (currentHeroIndex >= heroes.size) {
            currentHeroIndex = -1
            return
        }
        currentHero = heroes[currentHeroIndex]
        when (skillSet) {
            SkillSet.LEARN_NEW -> {
                currentSkills = currentHero.getDormantSkills()
                message = StatusMessage.CHOOSE_SKILL_TO_LEARN
            }
            SkillSet.IMPROVE_EXISTING -> {
                currentSkills = currentHero.getSkills()
                message = StatusMessage.CHOOSE_SKILL_TO_UPGRADE
            }
        }
        actions = ActionList()
    }

    //==================================================================================================================

    /**
     * Show info about selected skill
     * @param skill Selected skill
     * @return true if upgrading was confirmed, false of canceled
     */
    private fun showSkillInfo(skill: Skill): Boolean {
        //Show page info
        Audio.playSound(Sound.PAGE_NEXT)
        val oldActions = actions
        val oldMessage = message
        actions = ActionList()
        actions.add(Action.Type.CONFIRM)
        actions.add(Action.Type.CANCEL)
        message = StatusMessage.CONFIRM_SKILL_UPGRADE
        renderer.drawSkillInfo(skill, message, actions)
        return when (interactor.pickAction(actions).type) {
            Action.Type.CANCEL -> {
                Audio.playSound(Sound.PAGE_CLOSE)
                actions = oldActions
                message = oldMessage
                false
            }
            Action.Type.CONFIRM -> true
            else -> throw AssertionError("Should not happen")
        }
    }

    /**
     * Run main game cycle
     */
    private fun processCycle() {
        while (currentHeroIndex > -1) {
            renderer.drawHeroSkills(currentHero, currentSkills, message, actions)
            with(interactor.pickItemFromList(currentSkills.size)) {
                if (this.type === Action.Type.LIST_ITEM) {
                    with(currentSkills[this.data]) {
                        if (this.level < this.maxLevel && showSkillInfo(this)) {
                            Audio.playSound(Sound.UPGRADE)
                            currentHero.improveSkill(this.type)
                            pickNextHero()
                        }
                    }
                }
            }
        }
    }

    //==================================================================================================================


}