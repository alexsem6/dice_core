package org.alexsem.dice.upgrade

import org.alexsem.dice.model.Hero
import org.alexsem.dice.ui.StatusMessage
import org.alexsem.dice.ui.UI
import org.alexsem.dice.ui.audio.Audio
import org.alexsem.dice.ui.audio.Sound

/**
 * Performs hero hand capacity upgrade process as per [org.alexsem.dice.model.ScenarioReward.INCREASE_HAND_CAPACITY]
 */
class HeroHandCapacityUpgrader(ui: UI, private val heroes: List<Hero>) {

    private val renderer = ui.upgradeRenderer
    private val interactor = ui.menuInteractor

    private lateinit var currentHero: Hero
    private var message = StatusMessage.EMPTY

    //------------------------------------------------------------------------------------------------------------------
    /**
     * Start the cycle
     */
    fun start() {
        if (heroes.isEmpty()) {
            throw IllegalStateException("No heroes defined")
        }
        processCycle()
    }

    //==================================================================================================================

    /**
     * Run main game cycle
     */
    private fun processCycle() {
        heroes.forEach { hero ->
            Audio.playSound(Sound.UPGRADE)
            currentHero = hero
            hero.hand.capacity += 1
            message = StatusMessage.HAND_CAPACITY_INCREASED with hero.hand.capacity
            renderer.drawHeroHandCapacityIncrease(currentHero, message)
            interactor.anyInput()
        }
    }

    //==================================================================================================================

}