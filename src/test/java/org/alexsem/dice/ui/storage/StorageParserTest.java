package org.alexsem.dice.ui.storage;

import org.alexsem.dice.generator.HeroGeneratorKt;
import org.alexsem.dice.generator.template.adventure.AdventureTemplate;
import org.alexsem.dice.model.Die;
import org.alexsem.dice.model.Hero;
import org.alexsem.dice.model.Skill;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

class TestAdventureTemplate implements AdventureTemplate {
    @NotNull
    @Override
    public String getName() {
        return "Test adventure";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Description";
    }

    @NotNull
    @Override
    public List<Phase> getPhases() {
        return new ArrayList<>();
    }

    @NotNull
    @Override
    public Map<String, String> getNameLocalizations() {
        HashMap<String, String> result = new HashMap<>();
        result.put("ru", "тест");
        return result;
    }

    @NotNull
    @Override
    public Map<String, String> getDescriptionLocalizations() {
        return new HashMap<>();
    }
}


/**
 * Test for methods of the {@link StorageParserKt} class
 */
public class StorageParserTest {

    @Test
    public void testPartyInfoShort() throws IOException {

        PartyInfoShort source = new PartyInfoShort("identifier");
        source.setTimestamp(new Date());
        source.setPartyName("Party имя");
        source.setHeroNames(Arrays.asList("Hero 1", "Hero2", "Герой3"));
        source.setHeroTypes(Arrays.asList(Hero.Type.BRAWLER, Hero.Type.PROPHET, Hero.Type.MYSTIC, Hero.Type.HUNTER));
        source.setAdventure(new TestAdventureTemplate());

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        StorageParserKt.writePartyInfoShort(buffer, source);
        buffer.close();

        ByteArrayInputStream stream = new ByteArrayInputStream(buffer.toByteArray());
        PartyInfoShort result = StorageParserKt.readPartyInfoShort(stream);
        stream.close();

        assertEquals("Ids do not match", source.getId(), result.getId());
        assertEquals("Dates do not match", source.getTimestamp(), result.getTimestamp());
        assertEquals("Party names do not match", source.getPartyName(), result.getPartyName());
        assertEquals("Hero names do not match", source.getHeroNames().size(), result.getHeroNames().size());
        assertEquals("Hero name is incorrect", "Герой3", result.getHeroNames().get(2));
        assertEquals("Hero types do not match", 3, result.getHeroTypes().size());
        assertEquals("Hero types do not match", source.getHeroTypes().get(1), result.getHeroTypes().get(1));
        assertEquals("Hero type is incorrect", Hero.Type.BRAWLER, result.getHeroTypes().get(0));
        assertEquals("Adventure names do not match", source.getAdventureName().get(""), result.getAdventureName().get(""));
        assertEquals("Adventure names do not match", source.getAdventureName().get("ru"), result.getAdventureName().get("ru"));
        assertEquals("Adventure name is incorrect", "тест", result.getAdventureName().get("ru"));
    }

    @Test
    public void testPartyInfoFull() throws IOException {
        Hero hero1 = HeroGeneratorKt.generateHero(Hero.Type.HUNTER, "Хантер");
        hero1.getHand().addDie(new Die(Die.Type.SOMATIC, 6));
        hero1.getHand().addDie(new Die(Die.Type.PHYSICAL, 4));
        hero1.getHand().addDie(new Die(Die.Type.WOUND, 12));
        hero1.getHand().addDie(new Die(Die.Type.ALLY, 10));
        hero1.getDiscardPile().put(new Die(Die.Type.DIVINE, 8));
        hero1.getDiscardPile().put(new Die(Die.Type.ALLY, 10));
        hero1.getDiscardPile().put(new Die(Die.Type.MENTAL, 4));
        hero1.getDiceLimits().get(1).setCurrent(hero1.getDiceLimits().get(1).getCurrent() + 1);
        hero1.improveSkill(Skill.Type.SHOOT);
        hero1.improveSkill(Skill.Type.SHOOT);
        Hero hero2 = HeroGeneratorKt.generateHero(Hero.Type.MYSTIC, "Wazza");

        PartyInfoFull source = new PartyInfoFull();
        source.setId("Identifier");
        source.setPartyName("Party Name");
        source.setHeroes(Arrays.asList(hero1, hero2));
        source.setAdventure(new TestAdventureTemplate());
        source.setProgress(4);

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        StorageParserKt.writePartyInfoFull(buffer, source);
        buffer.close();

        ByteArrayInputStream stream = new ByteArrayInputStream(buffer.toByteArray());
        PartyInfoFull result = StorageParserKt.readPartyInfoFull(stream);
        stream.close();

        assertEquals("Ids do not match", source.getId(), result.getId());
        assertEquals("Party names do not match", source.getPartyName(), result.getPartyName());
        assertEquals("Heroes do not match", source.getHeroes().size(), result.getHeroes().size());
        Hero sourceHero = source.getHeroes().get(0);
        Hero resultHero = result.getHeroes().get(0);
        assertEquals("Hero names do not match", sourceHero.getName(), resultHero.getName());
        assertEquals("Hero types do not match", sourceHero.getType(), resultHero.getType());
        assertEquals("Hero favored die types do not match", sourceHero.getFavoredDieType(), resultHero.getFavoredDieType());
        assertEquals("Hero hand capacities do not match", sourceHero.getHand().getCapacity(), resultHero.getHand().getCapacity());
        assertEquals("Hero hand sizes do not match", sourceHero.getHand().getDieCount(), resultHero.getHand().getDieCount());
        assertEquals("Hero hand sizes do not match", sourceHero.getHand().getAllyDieCount(), resultHero.getHand().getAllyDieCount());
        assertEquals("Hero hands do not match", sourceHero.getHand().examine().get(0).getType(), resultHero.getHand().examine().get(0).getType());
        assertEquals("Hero hands do not match", sourceHero.getHand().examine().get(0).getSize(), resultHero.getHand().examine().get(0).getSize());
        assertEquals("Hero bag sizes do not match", sourceHero.getBag().getSize(), resultHero.getBag().getSize());
        assertEquals("Hero bags do not match", sourceHero.getBag().examine().get(0).getType(), resultHero.getBag().examine().get(0).getType());
        assertEquals("Hero discard pile sizes do not match", sourceHero.getDiscardPile().getSize(), resultHero.getDiscardPile().getSize());
        assertEquals("Hero discard piles do not match", sourceHero.getDiscardPile().examine().get(0).getType(), resultHero.getDiscardPile().examine().get(0).getType());
        assertEquals("Hero dice limit sizes do not match", sourceHero.getDiceLimits().size(), resultHero.getDiceLimits().size());
        assertEquals("Hero dice limits do not match", sourceHero.getDiceLimits().get(1).getType(), resultHero.getDiceLimits().get(1).getType());
        assertEquals("Hero dice limits do not match", sourceHero.getDiceLimits().get(1).getInitial(), resultHero.getDiceLimits().get(1).getInitial());
        assertEquals("Hero dice limits do not match", sourceHero.getDiceLimits().get(1).getMaximal(), resultHero.getDiceLimits().get(1).getMaximal());
        assertEquals("Hero dice limits do not match", sourceHero.getDiceLimits().get(1).getCurrent(), resultHero.getDiceLimits().get(1).getCurrent());
        assertEquals("Hero skill sizes do not match", sourceHero.getSkills().size(), resultHero.getSkills().size());
        assertEquals("Hero skills do not match", sourceHero.getSkills().get(0).getType(), resultHero.getSkills().get(0).getType());
        assertEquals("Hero skills do not match", sourceHero.getSkills().get(0).getLevel(), resultHero.getSkills().get(0).getLevel());
        assertEquals("Hero skills do not match", sourceHero.getSkills().get(0).getMaxLevel(), resultHero.getSkills().get(0).getMaxLevel());
        assertEquals("Hero skills do not match", sourceHero.getSkills().get(0).isActive(), resultHero.getSkills().get(0).isActive());
        assertNotNull("Hero skill modifier is incorrect", sourceHero.getSkills().get(0).getModifier1());
        assertNotNull("Hero skill modifier is incorrect", resultHero.getSkills().get(0).getModifier1());
        assertEquals("Hero skills do not match", sourceHero.getSkills().get(0).getModifier1().getType(), resultHero.getSkills().get(0).getModifier1().getType());
        assertEquals("Hero skills do not match", sourceHero.getSkills().get(0).getModifier1().getValue(), resultHero.getSkills().get(0).getModifier1().getValue());
        assertNotNull("Hero skill modifier is incorrect", sourceHero.getSkills().get(0).getModifier2());
        assertNotNull("Hero skill modifier is incorrect", resultHero.getSkills().get(0).getModifier2());
        assertEquals("Hero skills do not match", sourceHero.getSkills().get(0).getModifier2().getType(), resultHero.getSkills().get(0).getModifier2().getType());
        assertEquals("Hero skills do not match", sourceHero.getSkills().get(0).getModifier2().getValue(), resultHero.getSkills().get(0).getModifier2().getValue());
        assertEquals("Hero skills do not match", sourceHero.getSkills().get(1).getModifier2(), resultHero.getSkills().get(1).getModifier2());
        assertEquals("Hero skills do not match", sourceHero.getSkills().get(2).getModifier2(), resultHero.getSkills().get(2).getModifier2());
        assertEquals("Hero skills do not match", sourceHero.getSkills().get(3).getModifier2(), resultHero.getSkills().get(3).getModifier2());
        assertEquals("Hero skill sizes do not match", sourceHero.getDormantSkills().size(), resultHero.getDormantSkills().size());
        if (sourceHero.getDormantSkills().size() > 0) {
            assertEquals("Hero skills do not match", sourceHero.getDormantSkills().get(0).getType(), resultHero.getDormantSkills().get(0).getType());
            assertEquals("Hero skills do not match", sourceHero.getDormantSkills().get(0).getLevel(), resultHero.getDormantSkills().get(0).getLevel());
            assertEquals("Hero skills do not match", sourceHero.getDormantSkills().get(0).getMaxLevel(), resultHero.getDormantSkills().get(0).getMaxLevel());
            assertEquals("Hero skills do not match", sourceHero.getDormantSkills().get(0).isActive(), resultHero.getDormantSkills().get(0).isActive());
            assertEquals("Hero skills do not match", sourceHero.getDormantSkills().get(0).getModifier1().getType(), resultHero.getDormantSkills().get(0).getModifier1().getType());
            assertEquals("Hero skills do not match", sourceHero.getDormantSkills().get(0).getModifier1().getValue(), resultHero.getDormantSkills().get(0).getModifier1().getValue());
            assertEquals("Hero skills do not match", sourceHero.getDormantSkills().get(0).getModifier2(), resultHero.getDormantSkills().get(0).getModifier2());
        }
        assertEquals("Adventures do not match", source.getAdventure().getName(), result.getAdventure().getName());
        assertEquals("Adventures do not match", source.getAdventure().getClass(), result.getAdventure().getClass());
        assertEquals("Progresses do not match", source.getProgress(), result.getProgress());
    }

    @Test
    public void testAll() throws IOException {
        PartyInfoShort sourceShort = new PartyInfoShort("identifier");
        sourceShort.setTimestamp(new Date());
        sourceShort.setPartyName("Party имя");
        sourceShort.setHeroNames(Arrays.asList("Hero 1", "Hero2", "Герой3"));
        sourceShort.setHeroTypes(Arrays.asList(Hero.Type.BRAWLER, Hero.Type.PROPHET, Hero.Type.MYSTIC, Hero.Type.HUNTER));
        sourceShort.setAdventure(new TestAdventureTemplate());

        Hero hero1 = HeroGeneratorKt.generateHero(Hero.Type.HUNTER, "Хантер");
        Hero hero2 = HeroGeneratorKt.generateHero(Hero.Type.MYSTIC, "Wazza");
        PartyInfoFull sourceFull = new PartyInfoFull();
        sourceFull.setId("Identifier");
        sourceFull.setPartyName("Party Name");
        sourceFull.setHeroes(Arrays.asList(hero1, hero2));
        sourceFull.setAdventure(new TestAdventureTemplate());
        sourceFull.setProgress(4);

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        StorageParserKt.writePartyInfoShort(buffer, sourceShort);
        StorageParserKt.writePartyInfoFull(buffer, sourceFull);
        buffer.close();

        ByteArrayInputStream stream = new ByteArrayInputStream(buffer.toByteArray());
        PartyInfoShort resultShort = StorageParserKt.readPartyInfoShort(stream);
        assertEquals("Results do not match", sourceShort.getPartyName(), resultShort.getPartyName());
        PartyInfoFull resultFull = StorageParserKt.readPartyInfoFull(stream);
        stream.close();

        assertEquals("Results do not match", sourceShort.getPartyName(), resultShort.getPartyName());
        assertEquals("Results do not match", sourceFull.getProgress(), resultFull.getProgress());
    }



}
