package org.alexsem.dice.game.helper;

import org.alexsem.dice.game.GamePhase;
import org.alexsem.dice.generator.HeroGeneratorKt;
import org.alexsem.dice.generator.LocationGeneratorKt;
import org.alexsem.dice.adventure.TestLocationTemplate;
import org.alexsem.dice.model.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test for {@link SkillCheckHelper class}
 */
public class SkillCheckHelperTest {

    @Test
    public void testCheckHeroCanUseSkill() {
        Location location = LocationGeneratorKt.generateLocation(new TestLocationTemplate(), 1);
        Hero hero = HeroGeneratorKt.generateHero(Hero.Type.MYSTIC, "");
        hero.getHand().addDie(new Die(Die.Type.MENTAL, 4));
        hero.getHand().addDie(new Die(Die.Type.MENTAL, 4));
        GamePhase state;
        List<Trait> opponentTraits;

        state = GamePhase.LOCATION_ENCOUNTER_ENEMY;
        Skill.Type type1 = Skill.Type.MAGIC_BOLT;
        opponentTraits = null;
        assertTrue("Skill should be usable", SkillCheckHelper.INSTANCE.getSkillsHeroCanUse(state, hero, location, opponentTraits).stream().anyMatch(s -> s.getType() == type1));
        opponentTraits = new ArrayList<>();
        assertTrue("Skill should be usable", SkillCheckHelper.INSTANCE.getSkillsHeroCanUse(state, hero, location, opponentTraits).stream().anyMatch(s -> s.getType() == type1));
        opponentTraits.add(Trait.IMMUNE_MAGICAL);
        assertFalse("Skill should not be usable", SkillCheckHelper.INSTANCE.getSkillsHeroCanUse(state, hero, location, opponentTraits).stream().anyMatch(s -> s.getType() == type1));

        state = GamePhase.HERO_TAKES_DAMAGE;
        Skill.Type type2 = Skill.Type.ELEMENTAL_SHIELD;
        opponentTraits = null;
        assertFalse("Skill should not be usable", SkillCheckHelper.INSTANCE.getSkillsHeroCanUse(state, hero, location, opponentTraits).stream().anyMatch(s -> s.getType() == type2));
        opponentTraits = new ArrayList<>();
        assertFalse("Skill should not be usable", SkillCheckHelper.INSTANCE.getSkillsHeroCanUse(state, hero, location, opponentTraits).stream().anyMatch(s -> s.getType() == type2));
        opponentTraits.add(Trait.DAMAGE_MAGICAL);
        assertTrue("Skill should be usable", SkillCheckHelper.INSTANCE.getSkillsHeroCanUse(state, hero, location, opponentTraits).stream().anyMatch(s -> s.getType() == type2));
    }

}
