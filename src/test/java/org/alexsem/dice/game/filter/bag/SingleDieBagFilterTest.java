package org.alexsem.dice.game.filter.bag;

import org.alexsem.dice.model.Bag;
import org.alexsem.dice.model.Die;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test for {@link SingleDieBagFilter} class
 */
public class SingleDieBagFilterTest {

    @Test
    public void test() {
        BagFilter filter = new SingleDieBagFilter(Die.Type.PHYSICAL, Die.Type.SOMATIC, Die.Type.MENTAL);
        Bag bag = new Bag();
        bag.put(new Die(Die.Type.VERBAL, 5));
        assertFalse("Hand should not satisfy the filter!", filter.test(bag));
        bag.put(new Die(Die.Type.PHYSICAL, 5));
        assertTrue("Hand should satisfy the filter!", filter.test(bag));
        bag.put(new Die(Die.Type.MENTAL, 5));
        assertTrue("Hand should satisfy the filter!", filter.test(bag));
        bag.drawOfType(Die.Type.PHYSICAL);
        assertTrue("Hand should satisfy the filter!", filter.test(bag));
        bag.put(new Die(Die.Type.SOMATIC, 5));
        assertTrue("Hand should satisfy the filter!", filter.test(bag));
        bag.drawOfType(Die.Type.MENTAL);
        assertTrue("Hand should satisfy the filter!", filter.test(bag));
    }

}

