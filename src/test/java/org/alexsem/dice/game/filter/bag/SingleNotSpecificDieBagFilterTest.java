package org.alexsem.dice.game.filter.bag;

import org.alexsem.dice.model.Bag;
import org.alexsem.dice.model.Die;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Test for {@link SingleNotSpecificDieBagFilter} class
 */
public class SingleNotSpecificDieBagFilterTest {

    @Test
    public void test() {
        BagFilter filter1 = new SingleDieBagFilter(
                Die.Type.PHYSICAL,
                Die.Type.SOMATIC,
                Die.Type.MENTAL,
                Die.Type.VERBAL,
                Die.Type.DIVINE,
                Die.Type.ALLY,
                Die.Type.ENEMY,
                Die.Type.OBSTACLE,
                Die.Type.VILLAIN);
        BagFilter filter2 = new SingleNotSpecificDieBagFilter(Die.Type.WOUND);
        Bag bag = new Bag();
        assertEquals("Bag filters do not match!", filter1.test(bag), filter2.test(bag));
        for (int i = 0; i < Die.Type.values().length; i++) {
            bag.clear();
            bag.put(new Die(Die.Type.values()[i], 8));
            assertEquals("Bag filters do not match!", filter1.test(bag), filter2.test(bag));
        }
    }

}

