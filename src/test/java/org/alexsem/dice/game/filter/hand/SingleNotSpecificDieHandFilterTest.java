package org.alexsem.dice.game.filter.hand;

import org.alexsem.dice.model.Die;
import org.alexsem.dice.model.Hand;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Test for {@link SingleNotSpecificDieHandFilter} class
 */
public class SingleNotSpecificDieHandFilterTest {

    @Test
    public void test() {
        HandFilter filter1 = new SingleDieHandFilter(
                Die.Type.PHYSICAL,
                Die.Type.SOMATIC,
                Die.Type.MENTAL,
                Die.Type.VERBAL,
                Die.Type.DIVINE,
                Die.Type.ALLY,
                Die.Type.ENEMY,
                Die.Type.OBSTACLE,
                Die.Type.VILLAIN);
        HandFilter filter2 = new SingleNotSpecificDieHandFilter(Die.Type.WOUND);
        Hand hand = new Hand(5);
        assertEquals("Hand filters do not match!", filter1.test(hand), filter2.test(hand));
        for (int i = 0; i < Die.Type.values().length; i++) {
            hand.clear();
            hand.addDie(new Die(Die.Type.values()[i], 8));
            assertEquals("Hand filters do not match!", filter1.test(hand), filter2.test(hand));
        }
    }

}

