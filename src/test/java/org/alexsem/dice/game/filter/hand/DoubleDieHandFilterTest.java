package org.alexsem.dice.game.filter.hand;

import org.alexsem.dice.model.Die;
import org.alexsem.dice.model.Hand;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test for {@link DoubleDieHandFilter} class
 */
public class DoubleDieHandFilterTest {

    @Test
    public void test() {
        HandFilter filter = new DoubleDieHandFilter(
                new Die.Type[]{Die.Type.PHYSICAL, Die.Type.SOMATIC},
                new Die.Type[]{Die.Type.SOMATIC}
        );
        Hand hand = new Hand(5);
        Die phys1 = new Die(Die.Type.PHYSICAL, 5);
        Die phys2 = new Die(Die.Type.PHYSICAL, 5);
        Die som1 = new Die(Die.Type.SOMATIC, 5);
        Die som2 = new Die(Die.Type.SOMATIC, 5);
        hand.addDie(phys1);
        assertFalse("Hand should not satisfy the filter!", filter.test(hand));
        hand.addDie(phys2);
        assertFalse("Hand should not satisfy the filter!", filter.test(hand));
        hand.addDie(som1);
        assertTrue("Hand should satisfy the filter!", filter.test(hand));
        hand.removeDie(phys1);
        assertTrue("Hand should satisfy the filter!", filter.test(hand));
        hand.addDie(som2);
        assertTrue("Hand should satisfy the filter!", filter.test(hand));
        hand.removeDie(phys2);
        assertTrue("Hand should satisfy the filter!", filter.test(hand));
        hand.removeDie(som1);
        assertFalse("Hand should not satisfy the filter!", filter.test(hand));
    }

}

