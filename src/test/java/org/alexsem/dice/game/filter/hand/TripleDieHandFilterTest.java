package org.alexsem.dice.game.filter.hand;

import org.alexsem.dice.model.Die;
import org.alexsem.dice.model.Hand;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test for {@link TripleDieHandFilter} class
 */
public class TripleDieHandFilterTest {

    @Test
    public void test() {
        HandFilter filter = new TripleDieHandFilter(
                new Die.Type[]{Die.Type.PHYSICAL, Die.Type.SOMATIC},
                new Die.Type[]{Die.Type.MENTAL},
                new Die.Type[]{Die.Type.VERBAL, Die.Type.DIVINE, Die.Type.ALLY}
        );
        Hand hand = new Hand(8);
        Die phys = new Die(Die.Type.PHYSICAL, 5);
        Die som = new Die(Die.Type.SOMATIC, 5);
        Die men = new Die(Die.Type.MENTAL, 5);
        Die ver = new Die(Die.Type.VERBAL, 5);
        Die div = new Die(Die.Type.DIVINE, 5);
        Die all = new Die(Die.Type.ALLY, 5);
        hand.addDie(phys);
        assertFalse("Hand should not satisfy the filter!", filter.test(hand));
        hand.addDie(men);
        assertFalse("Hand should not satisfy the filter!", filter.test(hand));
        hand.addDie(ver);
        assertTrue("Hand should satisfy the filter!", filter.test(hand));
        hand.removeDie(phys);
        assertFalse("Hand should not satisfy the filter!", filter.test(hand));
        hand.addDie(som);
        assertTrue("Hand should satisfy the filter!", filter.test(hand));
        hand.removeDie(ver);
        assertFalse("Hand should not satisfy the filter!", filter.test(hand));
        hand.removeDie(men);
        assertFalse("Hand should not satisfy the filter!", filter.test(hand));
        hand.addDie(div);
        assertFalse("Hand should not satisfy the filter!", filter.test(hand));
        hand.addDie(all);
        assertFalse("Hand should not satisfy the filter!", filter.test(hand));
        hand.addDie(men);
        assertTrue("Hand should satisfy the filter!", filter.test(hand));
    }

}

