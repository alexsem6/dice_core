package org.alexsem.dice.game.filter.hand;

import org.alexsem.dice.model.Die;
import org.alexsem.dice.model.Hand;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test for {@link SingleDieHandFilter} class
 */
public class SingleDieHandFilterTest {

    @Test
    public void test() {
        HandFilter filter = new SingleDieHandFilter(Die.Type.PHYSICAL, Die.Type.SOMATIC, Die.Type.MENTAL);
        Hand hand = new Hand(5);
        Die phys = new Die(Die.Type.PHYSICAL, 5);
        Die som = new Die(Die.Type.SOMATIC, 5);
        Die men = new Die(Die.Type.MENTAL, 5);
        Die ver = new Die(Die.Type.VERBAL, 5);
        hand.addDie(ver);
        assertFalse("Hand should not satisfy the filter!", filter.test(hand));
        hand.addDie(phys);
        assertTrue("Hand should satisfy the filter!", filter.test(hand));
        hand.addDie(men);
        assertTrue("Hand should satisfy the filter!", filter.test(hand));
        hand.removeDie(phys);
        assertTrue("Hand should satisfy the filter!", filter.test(hand));
        hand.addDie(som);
        assertTrue("Hand should satisfy the filter!", filter.test(hand));
        hand.removeDie(men);
        assertTrue("Hand should satisfy the filter!", filter.test(hand));
    }

}

