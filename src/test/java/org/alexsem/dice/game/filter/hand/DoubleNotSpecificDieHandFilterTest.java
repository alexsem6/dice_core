package org.alexsem.dice.game.filter.hand;

import org.alexsem.dice.model.Die;
import org.alexsem.dice.model.Hand;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Test for {@link DoubleNotSpecificDieHandFilter} class
 */
public class DoubleNotSpecificDieHandFilterTest {

    @Test
    public void test() {
        HandFilter filter1 = new DoubleDieHandFilter(
                new Die.Type[]{Die.Type.PHYSICAL, Die.Type.SOMATIC, Die.Type.MENTAL, Die.Type.VERBAL, Die.Type.DIVINE, Die.Type.ALLY, Die.Type.ENEMY, Die.Type.OBSTACLE, Die.Type.VILLAIN},
                new Die.Type[]{Die.Type.PHYSICAL, Die.Type.VERBAL, Die.Type.DIVINE, Die.Type.ALLY, Die.Type.WOUND, Die.Type.ENEMY, Die.Type.OBSTACLE, Die.Type.VILLAIN}
        );
        HandFilter filter2 = new DoubleNotSpecificDieHandFilter(
                new Die.Type[]{Die.Type.WOUND},
                new Die.Type[]{Die.Type.SOMATIC, Die.Type.MENTAL}
        );
        Hand hand = new Hand(5);
        assertEquals("Hand filters do not match!", filter1.test(hand), filter2.test(hand));
        for (int i = 0; i < Die.Type.values().length; i++) {
            for (int j = 0; j < Die.Type.values().length; j++) {
                hand.clear();
                hand.addDie(new Die(Die.Type.values()[i], 8));
                hand.addDie(new Die(Die.Type.values()[j], 8));
                assertEquals("Hand filters do not match!", filter1.test(hand), filter2.test(hand));
            }
        }
    }

}

