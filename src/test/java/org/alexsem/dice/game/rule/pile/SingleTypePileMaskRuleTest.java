package org.alexsem.dice.game.rule.pile;

import org.alexsem.dice.game.PileMask;
import org.alexsem.dice.model.Die;
import org.alexsem.dice.model.Pile;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test for {@link SingleTypePileMaskRule}
 */
public class SingleTypePileMaskRuleTest {

    private Pile pile;

    @Before
    public void setUp() {
        pile = new Pile();
        pile.put(new Die(Die.Type.PHYSICAL, 10)); //0
        pile.put(new Die(Die.Type.PHYSICAL, 8)); //1
        pile.put(new Die(Die.Type.PHYSICAL, 6)); //2
        pile.put(new Die(Die.Type.PHYSICAL, 4)); //3
        pile.put(new Die(Die.Type.DIVINE, 4)); //4
        pile.put(new Die(Die.Type.WOUND, 6)); //5
        pile.put(new Die(Die.Type.WOUND, 4)); //6
        pile.put(new Die(Die.Type.ALLY, 4)); //7
    }

    @Test
    public void testRule1() {
        PileMaskRule rule = new SingleTypePileMaskRule(pile, Die.Type.PHYSICAL, 2, 3);
        PileMask mask = new PileMask();
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 1));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 2));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 3));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 4));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 5));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 6));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 7));
        assertFalse("Rule should not be met yet", rule.checkMask(mask));
        mask.addPosition(0);
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 1));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 2));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 3));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 4));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 5));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 6));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 7));
        assertFalse("Rule should not be met yet", rule.checkMask(mask));
        mask.addPosition(7);
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 1));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 2));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 3));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 4));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 5));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 6));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 7));
        assertFalse("Rule should not be met yet", rule.checkMask(mask));
        mask.addPosition(1);
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 1));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 2));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 3));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 4));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 5));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 6));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 7));
        assertFalse("Rule should not be met yet", rule.checkMask(mask));
        mask.removePosition(7);
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 1));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 2));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 3));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 4));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 5));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 6));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 7));
        assertTrue("Rule should be met by now", rule.checkMask(mask));
        mask.addPosition(3);
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 1));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 2));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 3));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 4));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 5));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 6));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 7));
        assertTrue("Rule should be met by now", rule.checkMask(mask));
        mask.addPosition(2);
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 1));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 2));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 3));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 4));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 5));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 6));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 7));
        assertFalse("Rule should not be met again", rule.checkMask(mask));
    }


}
