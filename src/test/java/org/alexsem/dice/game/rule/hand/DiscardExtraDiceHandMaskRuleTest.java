package org.alexsem.dice.game.rule.hand;

import org.alexsem.dice.game.HandMask;
import org.alexsem.dice.model.Die;
import org.alexsem.dice.model.Hand;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test for {@link DiscardExtraDiceHandMaskRule} class
 */
public class DiscardExtraDiceHandMaskRuleTest {

    private Hand hand;

    @Before
    public void init() {
        hand = new Hand(0);
        hand.addDie(new Die(Die.Type.PHYSICAL, 4)); //0
        hand.addDie(new Die(Die.Type.SOMATIC, 4)); //1
        hand.addDie(new Die(Die.Type.MENTAL, 4)); //2
        hand.addDie(new Die(Die.Type.VERBAL, 4)); //3
        hand.addDie(new Die(Die.Type.DIVINE, 4)); //4
        hand.addDie(new Die(Die.Type.WOUND, 4)); //5
        hand.addDie(new Die(Die.Type.WOUND, 4)); //6
        hand.addDie(new Die(Die.Type.ALLY, 4)); //A (0)
    }

    @Test
    public void testRule1() {
        hand.setCapacity(4);
        HandMaskRule rule = new DiscardExtraDiceHandMaskRule(hand);
        HandMask mask = new HandMask();
        assertTrue("Ally position should be enabled", rule.isAllyPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 1));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 2));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 3));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 4));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 5));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 6));
        assertFalse("Rule should not be met yet", rule.checkMask(mask));
        mask.addPosition(0);
        assertTrue("Ally position should be enabled", rule.isAllyPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 1));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 2));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 3));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 4));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 5));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 6));
        assertFalse("Rule should not be met yet", rule.checkMask(mask));
        mask.addAllyPosition(0);
        assertTrue("Ally position should be enabled", rule.isAllyPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 1));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 2));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 3));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 4));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 5));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 6));
        assertFalse("Rule should not be met yet", rule.checkMask(mask));
        mask.addPosition(1);
        assertTrue("Ally position should be enabled", rule.isAllyPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 1));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 2));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 3));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 4));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 5));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 6));
        assertFalse("Rule should not be met yet", rule.checkMask(mask));
        mask.addPosition(2);
        assertTrue("Ally position should be enabled", rule.isAllyPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 1));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 2));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 3));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 4));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 5));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 6));
        assertTrue("Rule should be met by now", rule.checkMask(mask));
        mask.addPosition(3);
        assertTrue("Ally position should be enabled", rule.isAllyPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 1));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 2));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 3));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 4));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 5));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 6));
        assertTrue("Rule should be met by now", rule.checkMask(mask));
        mask.addPosition(4);
        assertTrue("Ally position should be enabled", rule.isAllyPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 1));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 2));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 3));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 4));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 5));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 6));
        assertTrue("Rule should be met by now", rule.checkMask(mask));
    }

    @Test
    public void testRule2() {
        hand.setCapacity(8);
        HandMaskRule rule = new DiscardExtraDiceHandMaskRule(hand);
        HandMask mask = new HandMask();
        assertTrue("Ally position should be enabled", rule.isAllyPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 1));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 2));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 3));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 4));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 5));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 6));
        assertTrue("Rule should be met by now", rule.checkMask(mask));
        mask.addPosition(0);
        assertTrue("Ally position should be enabled", rule.isAllyPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 1));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 2));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 3));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 4));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 5));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 6));
        assertTrue("Rule should be met by now", rule.checkMask(mask));
        mask.addAllyPosition(0);
        assertTrue("Ally position should be enabled", rule.isAllyPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 1));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 2));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 3));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 4));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 5));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 6));
        assertTrue("Rule should be met by now", rule.checkMask(mask));
        mask.addPosition(1);
        assertTrue("Ally position should be enabled", rule.isAllyPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 1));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 2));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 3));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 4));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 5));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 6));
        assertTrue("Rule should be met by now", rule.checkMask(mask));
        mask.addPosition(2);
        assertTrue("Ally position should be enabled", rule.isAllyPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 1));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 2));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 3));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 4));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 5));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 6));
        assertTrue("Rule should be met by now", rule.checkMask(mask));
        mask.addPosition(3);
        assertTrue("Ally position should be enabled", rule.isAllyPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 1));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 2));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 3));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 4));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 5));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 6));
        assertTrue("Rule should be met by now", rule.checkMask(mask));
        mask.addPosition(4);
        assertTrue("Ally position should be enabled", rule.isAllyPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 1));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 2));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 3));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 4));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 5));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 6));
        assertTrue("Rule should be met by now", rule.checkMask(mask));
    }

}
