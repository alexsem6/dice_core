package org.alexsem.dice.game.rule.hand;

import org.alexsem.dice.game.HandMask;
import org.alexsem.dice.model.Die;
import org.alexsem.dice.model.Hand;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test for {@link DoubleDieHandMaskRule} class
 */
public class TripleDieHandMaskRuleTest {

    private Hand hand;

    @Before
    public void init() {
        hand = new Hand(10);
        hand.addDie(new Die(Die.Type.PHYSICAL, 4)); //0
        hand.addDie(new Die(Die.Type.PHYSICAL, 4)); //1
        hand.addDie(new Die(Die.Type.SOMATIC, 4)); //2 
        hand.addDie(new Die(Die.Type.SOMATIC, 4)); //3
        hand.addDie(new Die(Die.Type.MENTAL, 4)); //4
        hand.addDie(new Die(Die.Type.MENTAL, 4)); //5
        hand.addDie(new Die(Die.Type.VERBAL, 4)); //6
        hand.addDie(new Die(Die.Type.VERBAL, 4)); //7
        hand.addDie(new Die(Die.Type.DIVINE, 4)); //8
        hand.addDie(new Die(Die.Type.DIVINE, 4)); //9
        hand.addDie(new Die(Die.Type.ALLY, 4)); //A (0)
        hand.addDie(new Die(Die.Type.ALLY, 4)); //B (1)
    }

    @Test
    public void testRule1() {
        HandMaskRule rule = new TripleDieHandMaskRule(
                hand,
                new Die.Type[]{Die.Type.PHYSICAL, Die.Type.SOMATIC},
                new Die.Type[]{Die.Type.MENTAL, Die.Type.VERBAL},
                new Die.Type[]{Die.Type.PHYSICAL, Die.Type.ALLY}
        );
        HandMask mask = new HandMask();
        assertTrue("Ally position should be enabled", rule.isAllyPositionActive(mask, 0));
        assertTrue("Ally position should be enabled", rule.isAllyPositionActive(mask, 1));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 1));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 2));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 3));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 4));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 5));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 6));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 7));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 8));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 9));
        assertFalse("Rule should not be met yet", rule.checkMask(mask));
        mask.addPosition(0);
        assertTrue("Ally position should be enabled", rule.isAllyPositionActive(mask, 0));
        assertTrue("Ally position should be enabled", rule.isAllyPositionActive(mask, 1));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 1));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 2));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 3));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 4));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 5));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 6));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 7));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 8));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 9));
        assertFalse("Rule should not be met yet", rule.checkMask(mask));
        mask.addPosition(4);
        assertTrue("Ally position should be enabled", rule.isAllyPositionActive(mask, 0));
        assertTrue("Ally position should be enabled", rule.isAllyPositionActive(mask, 1));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 1));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 2));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 3));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 4));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 5));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 6));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 7));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 8));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 9));
        assertFalse("Rule should not be met yet", rule.checkMask(mask));
        mask.addAllyPosition(0);
        assertTrue("Ally position should be enabled", rule.isAllyPositionActive(mask, 0));
        assertFalse("Ally position should be disabled", rule.isAllyPositionActive(mask, 1));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 1));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 2));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 3));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 4));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 5));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 6));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 7));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 8));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 9));
        assertTrue("Rule should be met", rule.checkMask(mask));
        mask.removePosition(0);
        assertTrue("Ally position should be enabled", rule.isAllyPositionActive(mask, 0));
        assertFalse("Ally position should be disabled", rule.isAllyPositionActive(mask, 1));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 1));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 2));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 3));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 4));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 5));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 6));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 7));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 8));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 9));
        assertFalse("Rule should not be met again", rule.checkMask(mask));
    }

}
