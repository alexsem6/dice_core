package org.alexsem.dice.game.rule.hand;

import org.alexsem.dice.game.HandMask;
import org.alexsem.dice.model.Die;
import org.alexsem.dice.model.Hand;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test for {@link StatDieAcquireHandMaskRule} class
 */
public class StatDieAcquireHandMaskRuleTest {

    private Hand hand;

    @Before
    public void init() {
        hand = new Hand(8);
        hand.addDie(new Die(Die.Type.PHYSICAL, 4)); //0
        hand.addDie(new Die(Die.Type.PHYSICAL, 4)); //1
        hand.addDie(new Die(Die.Type.MENTAL, 4)); //2
        hand.addDie(new Die(Die.Type.VERBAL, 4)); //3
        hand.addDie(new Die(Die.Type.DIVINE, 4)); //4
        hand.addDie(new Die(Die.Type.DIVINE, 4)); //5
        hand.addDie(new Die(Die.Type.DIVINE, 4)); //6
        hand.addDie(new Die(Die.Type.ALLY, 4)); //A (0)
    }

    @Test
    public void testRule1() {
        HandMaskRule rule = new StatDieAcquireHandMaskRule(hand, Die.Type.PHYSICAL);
        HandMask mask = new HandMask();
        assertFalse("Ally position should be disabled", rule.isAllyPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 1));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 2));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 3));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 4));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 5));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 6));
        assertFalse("Rule should not be met yet", rule.checkMask(mask));
        mask.addPosition(6);
        assertFalse("Ally position should be disabled", rule.isAllyPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 1));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 2));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 3));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 4));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 5));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 6));
        assertFalse("Rule should not be met yet", rule.checkMask(mask));
        mask.addAllyPosition(0);
        assertFalse("Ally position should be disabled", rule.isAllyPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 1));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 2));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 3));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 4));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 5));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 6));
        assertFalse("Rule should not be met yet", rule.checkMask(mask));
        mask.addPosition(0);
        assertFalse("Ally position should be disabled", rule.isAllyPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 1));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 2));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 3));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 4));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 5));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 6));
        assertFalse("Rule should not be met yet", rule.checkMask(mask));
        mask.removeAllyPosition(0);
        assertFalse("Ally position should be disabled", rule.isAllyPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 1));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 2));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 3));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 4));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 5));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 6));
        assertTrue("Rule should be met by now", rule.checkMask(mask));
        mask.addPosition(5);
        assertFalse("Ally position should be disabled", rule.isAllyPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 1));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 2));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 3));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 4));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 5));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 6));
        assertTrue("Rule should be met by now", rule.checkMask(mask));
        mask.addPosition(1);
        assertFalse("Ally position should be disabled", rule.isAllyPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 0));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 1));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 2));
        assertFalse("Position should be disabled", rule.isPositionActive(mask, 3));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 4));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 5));
        assertTrue("Position should be enabled", rule.isPositionActive(mask, 6));
        assertFalse("Rule should not be met again", rule.checkMask(mask));
    }


}
