package org.alexsem.dice.game.distribute;

import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import org.alexsem.dice.game.DieBattleCheck;
import org.alexsem.dice.generator.HeroGeneratorKt;
import org.alexsem.dice.model.Die;
import org.alexsem.dice.model.Hero;
import org.alexsem.dice.model.Pile;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

/**
 * Test for complex logic of {@link DieDistributorsKt#discardSmallestPhysicalHideRestDieDistributor} and similar
 */
public class DiscardSmallestSpecificHideRestDistributorTest {

    private Hero hero;
    private DieBattleCheck battleCheck;

    @Before
    public void init() {
        hero = HeroGeneratorKt.generateHero(Hero.Type.BRAWLER, "");
        hero.setName("Test");
        battleCheck = new DieBattleCheck(DieBattleCheck.Method.MAX, null);
    }

    @Test
    public void testDistribute1() {
        hero.getDiscardPile().clear();
        hero.getBag().clear();
        battleCheck.clearHeroPairs();
        Die die1 = new Die(Die.Type.PHYSICAL, 4);
        Die die2 = new Die(Die.Type.SOMATIC, 6);
        battleCheck.addHeroPair(die1, 0);
        battleCheck.addHeroPair(die2, 0);
        Function3<DieBattleCheck, Hero, Pile, Unit> distributor;
        distributor = DieDistributorsKt.getDiscardSmallestSomaticHideRestDieDistributor();
        distributor.invoke(battleCheck, hero, null);
        assertEquals("Incorrect number of dice in bag", 1, hero.getBag().getSize());
        assertEquals("Incorrect number of dice in pile", 1, hero.getDiscardPile().getSize());
        assertSame("Incorrectly distributed die: " + die1, die1, hero.getBag().draw());
        assertSame("Incorrectly distributed die: " + die2, die2, hero.getDiscardPile().draw());
    }

    @Test
    public void testDistribute2() {
        hero.getDiscardPile().clear();
        hero.getBag().clear();
        battleCheck.clearHeroPairs();
        Die die1 = new Die(Die.Type.PHYSICAL, 4);
        Die die2 = new Die(Die.Type.PHYSICAL, 6);
        Die die3 = new Die(Die.Type.PHYSICAL, 6);
        battleCheck.addHeroPair(die1, 0);
        battleCheck.addHeroPair(die2, 0);
        battleCheck.addHeroPair(die3, 0);
        Function3<DieBattleCheck, Hero, Pile, Unit> distributor;
        distributor = DieDistributorsKt.getDiscardSmallestPhysicalHideRestDieDistributor();
        distributor.invoke(battleCheck, hero, null);
        assertEquals("Incorrect number of dice in bag", 2, hero.getBag().getSize());
        assertEquals("Incorrect number of dice in pile", 1, hero.getDiscardPile().getSize());
        assertSame("Incorrectly distributed die: " + die1, die1, hero.getDiscardPile().draw());
        hero.getDiscardPile().clear();
        hero.getBag().clear();
        distributor = DieDistributorsKt.getDiscardSmallestSomaticHideRestDieDistributor();
        distributor.invoke(battleCheck, hero, null);
        assertEquals("Incorrect number of dice in bag", 3, hero.getBag().getSize());
        assertEquals("Incorrect number of dice in pile", 0, hero.getDiscardPile().getSize());
    }

    @Test
    public void testDistribute3() {
        hero.getDiscardPile().clear();
        hero.getBag().clear();
        battleCheck.clearHeroPairs();
        Die die1 = new Die(Die.Type.SOMATIC, 4);
        Die die2 = new Die(Die.Type.MENTAL, 4);
        Die die3 = new Die(Die.Type.VERBAL, 12);
        Die die4 = new Die(Die.Type.VERBAL, 10);
        battleCheck.addHeroPair(die1, 0);
        battleCheck.addHeroPair(die2, 0);
        battleCheck.addHeroPair(die3, 0);
        battleCheck.addHeroPair(die4, 0);
        Function3<DieBattleCheck, Hero, Pile, Unit> distributor;
        distributor = DieDistributorsKt.getDiscardSmallestVerbalHideRestDieDistributor();
        distributor.invoke(battleCheck, hero, null);
        assertEquals("Incorrect number of dice in bag", 3, hero.getBag().getSize());
        assertEquals("Incorrect number of dice in pile", 1, hero.getDiscardPile().getSize());
        assertSame("Incorrectly distributed die: " + die4, die4, hero.getDiscardPile().draw());
    }

}
