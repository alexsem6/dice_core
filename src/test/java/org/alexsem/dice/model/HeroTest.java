package org.alexsem.dice.model;

import org.alexsem.dice.generator.HeroGeneratorKt;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Test for {@link Hero} class
 */
public class HeroTest {

    private Hero hero;

    @Before
    public void init() {
        hero = HeroGeneratorKt.generateHero(Hero.Type.BRAWLER, "");
    }

    @Test
    public void testImproveSkill() {
        hero.getSkills().forEach(skill -> {
            int currentLevel = skill.getLevel();
            if (currentLevel < skill.getMaxLevel()) {
                hero.improveSkill(skill.getType());
                assertEquals("Level increased incorrectly!", currentLevel + 1, skill.getLevel());
            }
        });
        new ArrayList<>(hero.getDormantSkills()).forEach(skill -> {
            hero.improveSkill(skill.getType());
            Optional<Skill> newSkill = hero.getSkills().stream().filter(s -> s.getType() == skill.getType()).findFirst();
            assertTrue("Dormant skill not learnt", newSkill.isPresent());
            assertEquals("Level set incorrectly!", 1, newSkill.get().getLevel());
        });
    }

    @Test(expected = IllegalStateException.class)
    public void testImproveSkillMaxedOut() {
        hero.getSkills().stream()
                .filter(s -> s.getLevel() == s.getMaxLevel())
                .map(Skill::getType)
                .forEach(hero::improveSkill);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testImproveSkillNotFound() {
        hero.improveSkill(Skill.Type.PRAYER);
    }
}
