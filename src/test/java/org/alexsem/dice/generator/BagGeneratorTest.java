package org.alexsem.dice.generator;

import java.util.List;
import java.util.stream.Collectors;
import org.alexsem.dice.generator.template.bag.BagTemplate;
import org.alexsem.dice.generator.template.die.SingleDieTypeFilter;
import org.alexsem.dice.model.Bag;
import org.alexsem.dice.model.Die;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Test for BagGenerator class
 */
public class BagGeneratorTest {

    @Test
    public void testGenerateBag() {
        BagTemplate template1 = new BagTemplate();
        template1.addPlan(0, 10, new SingleDieTypeFilter(Die.Type.PHYSICAL));
        template1.addPlan(5, 5, new SingleDieTypeFilter(Die.Type.SOMATIC));
        template1.setFixedDieCount(null);
        BagTemplate template2 = new BagTemplate();
        template2.addPlan(10, 10, new SingleDieTypeFilter(Die.Type.DIVINE));
        template2.setFixedDieCount(5);
        BagTemplate template3 = new BagTemplate();
        template3.addPlan(10, 10, new SingleDieTypeFilter(Die.Type.ALLY));
        template3.setFixedDieCount(50);

        for (int i = 0; i < 10; i++) {
            Bag bag1 = BagGeneratorKt.generateBag(template1, 1);
            assertTrue("Incorrect bag size: " + bag1.getSize(), bag1.getSize() >= 5 && bag1.getSize() <= 15);
            assertEquals("Incorrect number of SOMATIC dice", 5, bag1.examine().stream().filter(d -> d.getType() == Die.Type.SOMATIC).count());
            Bag bag2 = BagGeneratorKt.generateBag(template2, 1);
            assertEquals("Incorrect bag size", 5, bag2.getSize());
            Bag bag3 = BagGeneratorKt.generateBag(template3, 1);
            assertEquals("Incorrect bag size", 50, bag3.getSize());
            List<Die.Type> dieTypes3 = bag3.examine().stream().map(Die::getType).distinct().collect(Collectors.toList());
            assertEquals("Incorrect die types", 1, dieTypes3.size());
            assertEquals("Incorrect die types", Die.Type.ALLY, dieTypes3.get(0));
        }

    }

}
