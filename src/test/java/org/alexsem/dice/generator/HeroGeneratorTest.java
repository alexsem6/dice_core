package org.alexsem.dice.generator;

import org.alexsem.dice.model.DiceLimit;
import org.alexsem.dice.model.Die;
import org.alexsem.dice.model.Hero;
import org.alexsem.dice.model.Skill;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class HeroGeneratorTest {

    @Test
    public void testRegenerateHero() {
        final Hero heroBefore = HeroGeneratorKt.generateHero(Hero.Type.BRAWLER, "test");
        heroBefore.setFavoredDieType(Die.Type.SOMATIC);
        heroBefore.getHand().setCapacity(5);
        heroBefore.getHand().addDie(new Die(Die.Type.DIVINE, 8));
        heroBefore.getHand().addDie(new Die(Die.Type.ALLY, 8));
        heroBefore.getBag().put(new Die(Die.Type.WOUND, 4));
        heroBefore.getDiscardPile().put(new Die(Die.Type.MENTAL, 12));
        heroBefore.increaseDiceLimit(Die.Type.PHYSICAL);
        heroBefore.increaseDiceLimit(Die.Type.PHYSICAL);
        heroBefore.increaseDiceLimit(Die.Type.SOMATIC);
        heroBefore.improveSkill(Skill.Type.HIT);
        heroBefore.improveSkill(Skill.Type.STONE_SKIN);
        heroBefore.improveSkill(Skill.Type.INTERVENE);
        heroBefore.getSkills().get(0).setModifier1(new Skill.Modifier(Skill.Modifier.Type.PLUS_LEVEL, 15));
        heroBefore.getSkills().get(0).setModifier2(null);
        final Hero heroAfter = HeroGeneratorKt.regenerateHero(heroBefore);
        assertEquals("Names do not match", heroBefore.getName(), heroAfter.getName());
        assertEquals("Types do not match", heroBefore.getType(), heroAfter.getType());
        assertEquals("isAlive do not match", heroBefore.isAlive(), heroAfter.isAlive());
        assertEquals("Hand capacities do not match", heroBefore.getHand().getCapacity(), heroAfter.getHand().getCapacity());
        assertDieListsEquals("Hand ", heroBefore.getHand().examine(), heroAfter.getHand().examine());
        assertDieListsEquals("Bag ", heroBefore.getBag().examine(), heroAfter.getBag().examine());
        assertDieListsEquals("Discard pile ", heroBefore.getDiscardPile().examine(), heroAfter.getDiscardPile().examine());
        assertEquals("Dice limit sizes do not match", heroBefore.getDiceLimits().size(), heroAfter.getDiceLimits().size());
        for (int i = 0; i < heroBefore.getDiceLimits().size(); i++) {
            final DiceLimit limitBefore = heroBefore.getDiceLimits().get(i);
            final DiceLimit limitAfter = heroAfter.getDiceLimits().get(i);
            assertEquals("Dice limit types do not match", limitBefore.getType(), limitAfter.getType());
            assertEquals(limitBefore.getType() + ": initial do not match", limitBefore.getInitial(), limitAfter.getInitial());
            assertEquals(limitBefore.getType() + ": current do not match", limitBefore.getCurrent(), limitAfter.getCurrent());
            assertEquals(limitBefore.getType() + ": maximal do not match", limitBefore.getMaximal(), limitAfter.getMaximal());
        }
        assertEquals("Skill sizes do not match", heroBefore.getSkills().size(), heroAfter.getSkills().size());
        for (int i = 0; i < heroBefore.getSkills().size(); i++) {
            final Skill skillBefore = heroBefore.getSkills().get(i);
            final Skill skillAfter = heroAfter.getSkills().get(i);
            assertEquals("Skill types do not match", skillBefore.getType(), skillAfter.getType());
            assertEquals(skillBefore.getType() + ": levels do not match", skillBefore.getLevel(), skillAfter.getLevel());
            assertEquals(skillBefore.getType() + ": max levels do not match", skillBefore.getMaxLevel(), skillAfter.getMaxLevel());
        }
        assertEquals("Dormant skill sizes do not match", heroBefore.getDormantSkills().size(), heroAfter.getDormantSkills().size());
        for (int i = 0; i < heroBefore.getDormantSkills().size(); i++) {
            final Skill skillBefore = heroBefore.getDormantSkills().get(i);
            final Skill skillAfter = heroAfter.getDormantSkills().get(i);
            assertEquals("Skill types do not match", skillBefore.getType(), skillAfter.getType());
            assertEquals(skillBefore.getType() + ": levels do not match", skillBefore.getLevel(), skillAfter.getLevel());
            assertEquals(skillBefore.getType() + ": max levels do not match", skillBefore.getMaxLevel(), skillAfter.getMaxLevel());
        }
        assertEquals("Favored die type incorrect", Die.Type.PHYSICAL, heroAfter.getFavoredDieType());
        assertEquals("Modifier incorrect", 1, heroAfter.getSkills().get(0).getModifier1Value());
        assertEquals("Modifier incorrect", 3, heroAfter.getSkills().get(0).getModifier2Value());
    }

    /**
     * Compares two list of dice for pseudo-equality (two dice considered equal if their types and sizes match)
     * @param listType List type (name) to use in messages (hand, bag, pile etc.)
     * @param list1 First list to compare
     * @param list2 Second list to compare
     */
    private void assertDieListsEquals(String listType, List<Die> list1, List<Die> list2) {
        assertEquals(listType + " sizes do not match", list1.size(), list2.size());
        for (int i = 0; i < list1.size(); i++) {
            final Die die1 = list1.get(i);
            final Die die2 = list2.get(i);
            assertEquals(listType + " die types do not match", die1.getType(), die2.getType());
            assertEquals(listType + " die sizes do not match", die1.getSize(), die2.getSize());
        }
    }

}
