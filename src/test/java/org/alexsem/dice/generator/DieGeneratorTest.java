package org.alexsem.dice.generator;

import org.alexsem.dice.generator.template.die.DieTypeFilter;
import org.alexsem.dice.generator.template.die.MultipleDieTypeFilter;
import org.alexsem.dice.generator.template.die.SingleDieTypeFilter;
import org.alexsem.dice.generator.template.die.StatsDieTypeFilter;
import org.alexsem.dice.model.Die;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DieGeneratorTest {

    @Test
    public void testGetMaxLevel() {
        assertEquals("Max level should be 3", 3, DieGeneratorKt.getMaxLevel());
    }

    @Test
    public void testDieGenerationSize() {
        DieTypeFilter filter = new SingleDieTypeFilter(Die.Type.ALLY);
        List<? extends List<Integer>> allowedSizes = Arrays.asList(
                null,
                Arrays.asList(4, 6, 8),
                Arrays.asList(4, 6, 8, 10),
                Arrays.asList(6, 8, 10, 12)
        );
        IntStream.rangeClosed(1, 3).forEach(level -> {
            for (int i = 0; i < 10; i++) {
                int size = DieGeneratorKt.generateDie(filter, level).getSize();
                assertTrue("Incorrect level of die generated: " + size, allowedSizes.get(level).contains(size));
                assertTrue("Incorrect die size: " + size, size >= 4);
                assertTrue("Incorrect die size: " + size, size <= 12);
                assertEquals("Incorrect die size: " + size, 0, size % 2);
            }
        });
    }

    @Test
    public void testDieGenerationType() {
        List<Die.Type> allowedTypes1 = Collections.singletonList(Die.Type.PHYSICAL);
        List<Die.Type> allowedTypes2 = Arrays.asList(Die.Type.PHYSICAL, Die.Type.SOMATIC, Die.Type.MENTAL, Die.Type.VERBAL);
        List<Die.Type> allowedTypes3 = Arrays.asList(Die.Type.ALLY, Die.Type.VILLAIN, Die.Type.ENEMY);
        for (int i = 0; i < 10; i++) {
            Die.Type type1 = DieGeneratorKt.generateDie(new SingleDieTypeFilter(Die.Type.PHYSICAL), 1).getType();
            assertTrue("Incorrect die type: " + type1, allowedTypes1.contains(type1));
            Die.Type type2 = DieGeneratorKt.generateDie(new StatsDieTypeFilter(), 1).getType();
            assertTrue("Incorrect die type: " + type2, allowedTypes2.contains(type2));
            Die.Type type3 = DieGeneratorKt.generateDie(new MultipleDieTypeFilter(Die.Type.ALLY, Die.Type.VILLAIN, Die.Type.ENEMY), 1).getType();
            assertTrue("Incorrect die type: " + type3, allowedTypes3.contains(type3));
        }
    }

}
