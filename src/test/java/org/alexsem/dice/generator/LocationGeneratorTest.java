package org.alexsem.dice.generator;

import org.alexsem.dice.adventure.TestEnemyTemplate;
import org.alexsem.dice.adventure.TestObstacleTemplate;
import org.alexsem.dice.adventure.TestObstacleTemplate2;
import org.alexsem.dice.generator.template.adventure.EnemyTemplate;
import org.alexsem.dice.generator.template.adventure.LocationTemplate;
import org.alexsem.dice.generator.template.adventure.ObstacleTemplate;
import org.alexsem.dice.generator.template.bag.BagTemplate;
import org.alexsem.dice.generator.template.die.MultipleDieTypeFilter;
import org.alexsem.dice.generator.template.die.SingleDieTypeFilter;
import org.alexsem.dice.model.*;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;

import java.util.*;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class LocationGeneratorTest {

    private class TestLocationTemplate implements LocationTemplate {

        @NotNull
        @Override
        public String getName() {
            return "Test Location";
        }

        @NotNull
        @Override
        public String getDescription() {
            return "Some Description";
        }

        @NotNull
        @Override
        public BagTemplate getBagTemplate() {
            BagTemplate template = new BagTemplate();
            template.addPlan(1, 2, new SingleDieTypeFilter(Die.Type.ENEMY));
            template.addPlan(1, 2, new SingleDieTypeFilter(Die.Type.OBSTACLE));
            template.addPlan(1, 2, new MultipleDieTypeFilter(Die.Type.PHYSICAL, Die.Type.MENTAL));
            template.addPlan(1, 2, new MultipleDieTypeFilter(Die.Type.DIVINE, Die.Type.ALLY));
            template.addPlan(0, 1, new SingleDieTypeFilter(Die.Type.WOUND));
            template.setFixedDieCount(7);
            return template;
        }

        @Override
        public int getBasicClosingDifficulty() {
            return 1;
        }

        @Override
        public int getEnemyCardsCount() {
            return 0;
        }

        @Override
        public int getObstacleCardsCount() {
            return 1;
        }

        @NotNull
        @Override
        public Collection<EnemyTemplate> getEnemyCardPool() {
            return Collections.singletonList(
                    new TestEnemyTemplate()
            );
        }

        @NotNull
        @Override
        public Collection<ObstacleTemplate> getObstacleCardPool() {
            return Arrays.asList(
                    new TestObstacleTemplate(),
                    new TestObstacleTemplate2()
            );
        }

        @NotNull
        @Override
        public List<SpecialRule> getSpecialRules() {
            return Arrays.asList(
                    SpecialRule.MUCH_BETTER,
                    SpecialRule.DONT_FORGET_TO_PAY_UP
            );
        }

        @NotNull
        @Override
        public Map<String, String> getNameLocalizations() {
            Map<String, String> localization = new HashMap<>();
            localization.put("ru", "Местность");
            return localization;
        }

        @NotNull
        @Override
        public Map<String, String> getDescriptionLocalizations() {
            Map<String, String> localization = new HashMap<>();
            localization.put("ru", "Описание местности");
            return localization;
        }

    }


    @Test
    public void testGenerateLocation() {
        LocationTemplate template = new TestLocationTemplate();
        IntStream.rangeClosed(1, 3).forEach(level -> {
            Location location = LocationGeneratorKt.generateLocation(template, level);
            assertEquals("Incorrect location type", template.getName(), location.getName().get(""));
            assertTrue("Location not open by default", location.isOpen());
            int closingDifficulty = location.getClosingDifficulty();
            assertTrue("Closing difficulty too small", closingDifficulty > 0);
            assertEquals("Incorrect closing difficulty", closingDifficulty, template.getBasicClosingDifficulty() + level * 2);
            Bag bag = location.getBag();
            assertNotNull("Bag is null", bag);
            assertTrue("Bag is empty", location.getBag().getSize() > 0);
            if (template.getBagTemplate().getFixedDieCount() != null) {
                assertEquals("Bag has incorrect number of dice", template.getBagTemplate().getFixedDieCount().intValue(), location.getBag().getSize());
            }
            Deck<Enemy> enemies = location.getEnemies();
            assertNotNull("Enemies are null", enemies);
            int enemyCardsCount = template.getEnemyCardsCount() > 0 ? template.getEnemyCardsCount() : template.getEnemyCardPool().size();
            assertEquals("Incorrect enemy threat count", enemies.getSize(), enemyCardsCount);
            if (bag.drawOfType(Die.Type.ENEMY) != null) {
                assertTrue("Enemy cards not specified", enemies.getSize() > 0);
            }
            Deck<Obstacle> obstacles = location.getObstacles();
            assertNotNull("Obstacles are null", obstacles);
            int obstacleCardsCount = template.getObstacleCardsCount() > 0 ? template.getObstacleCardsCount() : template.getObstacleCardPool().size();
            assertEquals("Incorrect obstacle threat count", obstacles.getSize(), obstacleCardsCount);
            if (bag.drawOfType(Die.Type.OBSTACLE) != null) {
                assertTrue("Obstacle cards not specified", enemies.getSize() > 0);
            }
            List<SpecialRule> specialRules = location.getSpecialRules();
            assertNotNull("SpecialRules are null", specialRules);
            template.getNameLocalizations().forEach((key, value) ->
                    assertEquals("Localization is incorrect", value, location.getName().get(key))
            );
            template.getDescriptionLocalizations().forEach((key, value) ->
                    assertEquals("Localization is incorrect", value, location.getDescription().get(key))
            );
        });
    }

}
