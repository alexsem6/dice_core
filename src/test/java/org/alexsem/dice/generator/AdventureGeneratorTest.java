package org.alexsem.dice.generator;

import kotlin.Pair;
import org.alexsem.dice.generator.template.adventure.InterludeTemplate;
import org.alexsem.dice.model.Hero;
import org.alexsem.dice.model.Interlude;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class AdventureGeneratorTest {

    @Test
    public void testGenerateInterlude() {
        Hero h1 = new Hero(Hero.Type.BRAWLER);
        h1.setName("Test1");
        Hero h2 = new Hero(Hero.Type.HUNTER);
        h2.setName("Test2");
        List<Hero> heroes = Arrays.asList(h1, h2);

        InterludeTemplate template = new InterludeTemplate() {
            @Override
            public String getName() {
                return "Test";
            }

            @Override
            public List<NPC> getNpcs() {
                return Arrays.asList(
                        new NPC() {
                            @NotNull
                            @Override
                            public String getName() {
                                return "Npc1";
                            }

                            @NotNull
                            @Override
                            public Map<String, String> getNameLocalizations() {
                                return new HashMap<>();
                            }
                        },
                        new NPC() {
                            @NotNull
                            @Override
                            public String getName() {
                                return "Npc2";
                            }

                            @NotNull
                            @Override
                            public Map<String, String> getNameLocalizations() {
                                return new HashMap<>();
                            }
                        }
                );
            }

            @Override
            public List<Pair<Subject, String>> getEvents() {
                return Arrays.asList(
                        new Pair<>(Subject.MOOD, "ominous"),
                        new Pair<>(Subject.NARRATION, "test caption"),
                        new Pair<>(Subject.HERO1, "hero1 speech"),
                        new Pair<>(Subject.NPC1, "NPC1 speech"),
                        new Pair<>(Subject.HERO2, "hero2 speech"),
                        new Pair<>(Subject.MOOD, "serene"),
                        new Pair<>(Subject.HERO3, "hero 3 speech"),
                        new Pair<>(Subject.NPC2, "NPC2 speech")
                );
            }

            @Override
            public Map<String, String> getNameLocalizations() {
                return new HashMap<>();
            }

            @NotNull
            @Override
            public Map<String, List<Pair<Subject, String>>> getEventsLocalizations() {
                Map<String, List<Pair<Subject, String>>> result = new HashMap<>();
                result.put("ru", Arrays.asList(
                        new Pair<>(Subject.MOOD, "ominous"),
                        new Pair<>(Subject.NARRATION, "тест заголовок"),
                        new Pair<>(Subject.HERO1, "речь героя 1"),
                        new Pair<>(Subject.NPC1, "NPC1 сказал"),
                        new Pair<>(Subject.HERO2, "речь героя 2"),
                        new Pair<>(Subject.MOOD, "serene"),
                        new Pair<>(Subject.HERO3, "речь героя 3"),
                        new Pair<>(Subject.NPC2, "NPC2 сказал")
                ));
                return result;
            }
        };

        Interlude interlude = AdventureGeneratorKt.generateInterlude(template, heroes);
        List<Interlude.Event> script = interlude.getScript();
        assertEquals("Incorrect number of events", 8, script.size());
        assertEquals("Incorrect mood", ((Interlude.Event.Tune) script.get(0)).getMood(), Interlude.Mood.OMINOUS);
        assertEquals("Incorrect text", "test caption", ((Interlude.Event.Narration) script.get(1)).getText().get(""));
        assertEquals("Incorrect text", "тест заголовок", ((Interlude.Event.Narration) script.get(1)).getText().get("ru"));
        assertEquals("Incorrect text", "тест заголовок", ((Interlude.Event.Narration) script.get(1)).getText().get("ru"));
        assertEquals("Incorrect hero", h1, ((Interlude.Actor.Hero) ((Interlude.Event.Phrase) script.get(2)).getActor()).getHero());
        assertEquals("Incorrect index", 0, ((Interlude.Actor.NPC) ((Interlude.Event.Phrase) script.get(3)).getActor()).getIndex());
        assertEquals("Incorrect mood", ((Interlude.Event.Tune) script.get(5)).getMood(), Interlude.Mood.SERENE);
        assertEquals("Incorrect hero", h1, ((Interlude.Actor.Hero) ((Interlude.Event.Phrase) script.get(6)).getActor()).getHero());
        assertEquals("Incorrect text", "NPC2 сказал", ((Interlude.Event.Phrase) script.get(7)).getText().get("ru"));
        assertEquals("Incorrect text", "NPC2 сказал", ((Interlude.Event.Phrase) script.get(7)).getText().get("ru"));
    }

}
